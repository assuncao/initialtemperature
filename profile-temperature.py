import numpy as np
import matplotlib.pyplot as plt

skiprows = 4
Nx, Ny, Nz = 351, 1, 101
path2Temp = './out/Temper_0_3D.txt'
temp = np.loadtxt(path2Temp,skiprows=skiprows)
temp = np.reshape(temp,(Nx,Ny,Nz),order='F')

xxO = temp[10,0,:]
zzO = np.linspace(1000,0,Nz)
xxC = temp[-10,0,:]
zzC = zzO
# extent = np.array([0,1700+fac,-1000,0])
# plt.ylim(-700,0)
# plt.xlim(0,1700)
# plt.plot(xx,zz)

z = np.linspace(-1000.0E3,0,Nz)

path2Temp2 = './out/Temper_0_3D_nodiffusion.txt'
temp2 = np.loadtxt(path2Temp2,skiprows=skiprows)
temp2 = np.reshape(temp2,(Nx,Ny,Nz),order='F')

xxC2 = temp2[10,0,:]
zzC2 = np.linspace(1000,0,Nz)
xxO2 = temp2[-10,0,:]
zzO2 = zzC2

lettersize = 14
plt.rcParams.update({'font.size': lettersize})

fig,ax = plt.subplots(figsize=(6,10))
plt.tight_layout(rect=[0.1,0.05,1,0.95])
#ax.set_aspect(2.6)

#box = plt.axes([0.2,0.1,0.5,0.9])
plt.ylim(0,z[-1]/1.0E3)
plt.xlim(0,1750.0)

# plt.plot(t_aux,z_cont/1.0E3,'--',color=(0.5,0.5,0.5))
# plt.plot(t_aux,z_ocea/1.0E3,'--',color=(0.5,0.5,0.5))

# plt.plot(t_cont_linear,z/1.0E3,'--',color=(0.5,0.5,0.5))
# plt.plot(t_ocea_linear,z/1.0E3,'--',color=(0.5,0.5,0.5))

# plt.plot(t_cont_adbtic,z/1.0E3,'--',color=(0.5,0.5,0.5))

# plt.plot(t_1300,z/1.0E3,'--',color=(0.5,0.5,0.5))

# plt.plot(t_cont_final,z/1.0E3,'--',label="Continents Region")
# plt.plot(t_ocea_final,z/1.0E3,'--',label="Oceans Region")

plt.hlines(-46.21855563,0,1750,linestyles='dashed',color=(0.5,0.5,0.5))
plt.hlines(-194.76608506,0,1750,linestyles='dashed',color=(0.5,0.5,0.5))

plt.plot(xxC2,-zzO2,'--',label="Oceanic Regions",color=(204./255,0,0))
plt.plot(xxO2,-zzC2,'--',label="Continental Regions",color=(0./255,102./255,51./255))


plt.plot(xxO,-zzO,linewidth=2,label="Oceanic Regions (Diffused 20 Myrs)",color=(204./255,0,0))
plt.plot(xxC,-zzC,linewidth=2,label="Cont. Regions (Diffused 20 Myrs)",color=(0./255,102./255,51./255))



plt.ylabel("Depth (km)")
plt.xlabel("Temperature ($\!^\circ\!$C)")

# plt.xticks(np.linspace(0,1750,8))
plt.xticks([0,250,500,750,1000,1250,1500,1750])
plt.yticks([-0,-46.21855563,-100,-194.76608506,-300,-400,-500,-600,-700,-800,-900,-1000])
plt.legend(loc=3)


#plt.tight_layout()
#ax.set_aspect(3.)
plt.savefig("figures/profile-temperature.png")
plt.show()

