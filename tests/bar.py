import sys, os
import time

################################################################################
# Setting up classes and methods
################################################################################

cwd = os.getcwd()
libsLocation = os.path.join(cwd,'lib')
sys.path.append(libsLocation)

import progressBar as pbar


bar = pbar.Bar(10)
bar.start()
for i in xrange(80):
	time.sleep(0.1)
	bar.update(i)
bar.end()

bar2 = pbar.Bar(10)
bar2.start()
for i in xrange(80):
	time.sleep(0.1)
	bar2.update(i)
bar2.end()
