import numpy as np

###############################################################################
# Read each slice of the model from the /out/model/ folder
# Polynomial fitting using the Least Square Method (Cramer's rule)
# y = a0 + a_1 * x + a_2 * x^2 + ... + a_k * x^k
# M * a = b
###############################################################################

def polApprox(abscissa, ordinate, polDegree):
	M = np.zeros(shape=(polDegree+1,polDegree+1))
	b = np.zeros(polDegree+1)
	
	# Recursive power
	def powSum(base,exponent,factor):
		p = pow(base,exponent)
		pSum = 0.
		for i in xrange(np.size(p)):
			pSum = (p[i] * factor[i]) + pSum
		return pSum
	
	# Calculate M
	for j in xrange(polDegree+1):
		for i in xrange(polDegree+1):
			M[i,j] = powSum(abscissa,j+i,np.ones(np.size(abscissa)))

	# Calculate b
	for j in xrange(polDegree+1):
		b[j] = powSum(abscissa,j,ordinate)
		
	a = np.linalg.solve(M,b)
	return a

# Polynomial fitting function calculator
def f(abscissa, A):
	ordinate = 0
	for i in xrange(np.size(A)):
		ordinate = A[i] * pow(abscissa,i) + ordinate
	return ordinate

# Polynomial fitting R2 (R-squared) analyser
def coefDetermination(ordinateData,abscissaData,acceptance):
	reDo = 'False'
	sqti = pow(ordinateData - np.mean(ordinateData),2)
	sqri = pow(ordinateData - abscissaData,2)
	sqts = 0.
	sqrs = 0.
	for i in xrange(abscissaData.size):
		sqts = sqti[i] + sqts
		sqrs = sqri[i] + sqrs
	r2 = 1 - (sqrs / sqts)
	if ((r2<acceptance) or (r2>1.)):
		reDo = 'True'
	return reDo
