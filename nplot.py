import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import os, sys, re
import concurrent.futures

################################################################################
# Setting up classes and methods
################################################################################

cwd = '/Users/kugelblitz/Desktop/initialTemperature'#os.getcwd()
libsLocation = os.path.join(cwd,'lib')
sys.path.append(libsLocation)

fig_dir = "figures/"

import classes
import progressBar as pbar

################################################################################
# Read data from ./src/params.txt
################################################################################

print("*** Reading initial parameters *** ")
parPath = './params.txt'
param 	= classes.Dimension(parPath)

param.lat.size = 1

################################################################################
# Configure NPROC
################################################################################

skiprows, num_processes, aux_print_step = 3, 14, 0
print(sys.argv,np.size(sys.argv))
for i in xrange(1,np.size(sys.argv),2):
	if (sys.argv[i]=='-k'):
		skiprows = int(sys.argv[i+1])
	elif (sys.argv[i]=='-n'):
		num_processes = int(sys.argv[i+1])
	elif (sys.argv[i]=='-step'):
		aux_print_step = int(sys.argv[i+1])

################################################################################
# Print parameters box (check up mistakes)
################################################################################

box = pbar.Box()

box.top()
box.mid("Chosen dimension:\t "+param.dim)
box.mid("Grid dimensions:\t ("+str(param.lon.size)+"x"+str(param.lat.size)+"x"+str(param.depth.size)+")")
box.mid("Longitude interval:\t ("+str(param.lon.start.degree)+","+str(param.lon.end.degree)+","+str(param.lon.delta.degree)+")")
box.mid("Longitude distance:\t ("+str(param.lon.start.km)+","+str(param.lon.end.km)+","+str(param.lon.delta.km)+")")
box.mid("Latitude interval:\t ("+str(param.lat.start.degree)+","+str(param.lat.end.degree)+","+str(param.lat.delta.degree)+")")
box.mid("Latitude distance:\t ("+str(param.lat.start.km)+","+str(param.lat.end.km)+","+str(param.lat.delta.km)+")")
box.mid("Depth interval:\t ("+str(param.depth.start)+","+str(param.depth.end)+","+str(param.depth.delta)+")")
box.top()
box.mid("Number of cores:\t "+str(num_processes))
box.mid("Skip headers:\t\t "+str(skiprows))
box.bot()

################################################################################
# Read Slab
################################################################################

slabGridPath = '/Users/kugelblitz/Desktop/initialTemperature/out/Slab2-clipped/Slab2-top.-1800'
data = np.loadtxt(slabGridPath,unpack='True')

################################################################################
# Read info from the param_1.5.3.txt
################################################################################

with open("param_1.5.3_2D.txt","r") as f:
	# Line 1
	line = f.readline()
	# Line 2
	line = f.readline()
	# Line 3
	line = f.readline()
	# Line 4
	line = f.readline()
	# Line 5
	line = f.readline()
	line = line.split()
	tag, step_max = line[0], int(line[1])
	# Line 6
	line = f.readline()
	# Line 7
	line = f.readline()
	# Line 8
	line = f.readline()
	# Line 9
	line = f.readline()
	# Line 10
	line = f.readline()
	line = line.split()
	tag, print_step = line[0], int(line[1])

if (aux_print_step!=0):
	print_step = aux_print_step

################################################################################
# Mesh and information for matplotlib
################################################################################

xi 		= np.linspace(0,param.lon.end.km,param.lon.size);
zi 		= np.linspace(param.depth.end,0,param.depth.size);
xx,zz 	= np.meshgrid(xi,zi);

extent			= [0,param.lon.end.km,0,param.depth.end]
width, height 	= 45, 12
markersize 		= 0.4

# Basin location
basinWest, basinEast = -58, -55
drawdepth	= -6 # Depth to draw line and identify basin location

dticks = 200.
arr_aux = np.arange(0,param.lon.end.km+dticks/2.,dticks)
if (param.lon.end.km-arr_aux[-1]<=dticks/2.):
	arr_aux = np.delete(arr_aux,-1)
xticks = np.append(arr_aux,param.lon.end.km)

dticks_degree = 2.5
arr_aux_degree = np.arange(param.lon.start.degree,param.lon.end.degree+dticks_degree/2.,dticks_degree)
if (param.lon.end.degree-arr_aux_degree[-1]<=dticks_degree/2.):
	arr_aux_degree = np.delete(arr_aux_degree,-1)
xticks_degree = np.append(arr_aux_degree,param.lon.end.degree)

plt.rcParams.update({'font.size': 32})
f_size = 32

dT = 200

step_min = 0
#step_max = 1
#step = 2

plt.close()

################################################################################
# Plots
################################################################################

# 3D
#temperPlot 		= True
#rhoPlot 		= True
#strainPlot		= True
#hPlot 			= False
#geoqPlot 		= True
#factorPlot 		= False
#velocityPlot 	= True
#viscPlot		= True
#tempVelo		= True

temperPlot 		= True
rhoPlot 		= True
strainPlot		= True
hPlot 			= False
geoqPlot 		= True
factorPlot 		= False
velocityPlot 	= True
viscPlot		= False
tempVelo		= True

n_dim_veloc = 3
if (param.lat.size==1):
	n_dim_veloc = 2

#bar = pbar.Bar(step_max)
#bar.start()

#print_step = 100

def ploter(cont):
	print_step = 1
#	bar.update(cont)
	print("loop step "+str(cont)+" @ file step " + str(cont*print_step))
	yrs = np.loadtxt("Tempo_"+str(cont*print_step)+".txt",comments="P",delimiter=":")
	if (num_processes!=1):
		value = format(np.round(yrs[0,1]/1.0E6,2), '.2f')
	else:
		value = format(np.round(yrs[1]/1.0E6,2), '.2f')
	
	############################################################################
	# Step "Plot"
	############################################################################
	
	x, y, z, cc, = [], [], [], []

	for rank in range(num_processes):
		x1,y1,z1,c0,c1,c2,c3,c4 = np.loadtxt("step_"+str(cont*print_step)+"-rank_new"+str(rank)+".txt",unpack=True)
		cor1 = (0,0,0)
		cor2 = (0,0,0)
		cor3 = (0,0,0)
		cc 	= np.append(cc,c1)
		x 	= np.append(x,x1)
		y 	= np.append(y,y1)
		z 	= np.append(z,z1)
	
	difere1 = 2.00
	difere2 = 0.95
	
	cond1 = (cc>=difere2) & (cc<=difere1)
	cond2 = (cc<difere2)
	cond3 = (cc>difere1)
	
	############################################################################
	# Temperature Plot
	############################################################################
	
	print("Temperature Plot for "+str(cont))
	if (temperPlot==True):
		print("IN!")
		A = np.loadtxt("Temper_"+str(cont*print_step)+".txt",unpack=True,comments="P",skiprows=skiprows)
		TT 						= A * 1.0
		TT[np.abs(TT)<1.0E-200] = 0
		TT 						= np.reshape(TT,(param.lon.size,param.lat.size,param.depth.size),order='F')
		TTT 					= TT[:,0,:]
		print("RESHAPED")
		
		print(width),
		print(height)
		
		fig, ax1 = plt.subplots(figsize=(width,height))
		print("FIG")
		plt.title(value+" Myrs\n\n",fontsize=f_size)
		print("TITLE")

		cs = ax1.contourf(xx,zz,np.transpose(TTT),100,cmap="jet",extent=extent)
		ax1.set_ylabel("Depth (km)")
		ax1.set_xlabel("Distance (km)")
		ax1.set_xticks(xticks)
		
		ax1.plot(x/1.0E3,y/1.0E3,"c.",color=cor2,markersize=markersize)
		
		ax1.plot(data[1,:],data[2,:],color=(1,1,1),linewidth=10)

		plt.xlim(0,param.lon.end.km)
		plt.ylim(param.depth.end,0)
		
		print("ax1 DONE")

		ax2 = ax1.twiny()
		ax2.set_xlim(param.lon.start.degree,param.lon.end.degree)
		ax2.set_xticks(xticks_degree)
		ax2.set_xlabel("Longitude ($\!^\circ\!$)")
		
		print("ax2 DONE")

		# Color bar
		auxTMax = np.around(np.max(TTT),-2)
		ticks = np.arange(0,auxTMax+1,dT)
		if (np.max(TTT)-ticks[-1])<dT/2:
			ticks = np.delete(ticks,-1)
			ticks = np.append(ticks,np.max(TTT))
		else:
			ticks = np.append(ticks,np.max(TTT))
		cbar = fig.colorbar(cs,ticks=ticks,pad=0.015)
		cbar.ax.set_ylabel('Temperature ($\!^\circ\!$C)')
		
		print("COLOR BAR DONE")
		
		# Basin location
		plt.hlines(drawdepth,basinWest,basinEast,linewidth=6,color=(1,1,1))
		
		plt.tight_layout(rect=[0,0,1.1,1])
		plt.savefig(fig_dir+"TemperPlot_"+str(cont).zfill(5)+".png")
		plt.close()
	print("Temperature Done for "+str(cont))
	
	############################################################################
	# Rho Plot
	############################################################################

	if (rhoPlot==True):
		R = np.loadtxt("Rho_"+str(cont*print_step)+".txt",unpack=True,comments="P",skiprows=skiprows)
		RR 						= R * 1.0
		RR[np.abs(RR)<1.0E-200] = 0
		RR 						= np.reshape(RR,(param.lon.size,param.lat.size,param.depth.size),order='F')
		RRR 					= RR[:,0,:]
		
		fig,ax1 = plt.subplots(figsize=(width,height))
		plt.title(value+" Myrs\n\n",fontsize=f_size)
		
		levels = np.linspace(np.floor(np.min(RRR)),np.ceil(np.max(RRR)),101)
		cs = ax1.contourf(xx,zz,np.transpose(RRR),extent=extent,levels=levels)
		ax1.set_ylabel("Depth (km)")
		ax1.set_xlabel("Distance (km)")
		ax1.set_xticks(xticks)
		
		ax1.plot(x/1.0E3,y/1.0E3,"c.",color=cor2,markersize=markersize)

		ax1.plot(data[1,:],data[2,:],color=(1,1,1),linewidth=10)
		
		plt.xlim(0,param.lon.end.km)
		plt.ylim(param.depth.end,0)
		
		ax2 = ax1.twiny()
		ax2.set_xlim(param.lon.start.degree,param.lon.end.degree)
		ax2.set_xticks(xticks_degree)
		ax2.set_xlabel("Longitude ($\!^\circ\!$)")
		
		# Color bar
		ticks = np.linspace(np.floor(np.min(RRR)),np.ceil(np.max(RRR)),11)
		cbar = fig.colorbar(cs,ticks=ticks,pad=0.015)
		cbar.ax.set_ylabel('Density ($kg/m\!^3\!$)')
		
		# Basin location
		plt.hlines(drawdepth,basinWest,basinEast,linewidth=6,color=(0,0,0))
		
		plt.tight_layout(rect=[0,0,1.1,1])
		plt.savefig(fig_dir+"RhoPlot_"+str(cont).zfill(5)+".png")
		plt.close()

	############################################################################
	# Strain plot
	############################################################################

	if (strainPlot==True):
		A = np.loadtxt("strain_"+str(cont*print_step)+".txt",unpack=True,comments="P",skiprows=skiprows)
		SS 						= A * 1.0
		SS[np.abs(SS)<1.0E-200] = 0
		SS[SS==0.0] 			= 1.0E-10
		SS 						= np.reshape(SS,(param.lon.size,param.lat.size,param.depth.size),order='F')
		SSS 					= SS[:,0,:]
		SSS 					= np.log10(SSS)
		
		fig, ax1 = plt.subplots(figsize=(width,height))
		plt.title(value+" Myrs\n\n",fontsize=f_size)
		
		cs = ax1.contourf(xx,zz,np.transpose(SSS),100,extent=extent)
		ax1.set_ylabel("Depth (km)")
		ax1.set_xlabel("Distance (km)")
		ax1.set_xticks(xticks)
		
#		ax1.plot(x[cond1]/1.0E3,z[cond1]/1.0E3,"c.",color=cor1,markersize=markersize)
#		ax1.plot(x[cond2]/1.0E3,z[cond2]/1.0E3,"c.",color=cor3,markersize=markersize)
#		ax1.plot(x[cond3]/1.0E3,z[cond3]/1.0E3,"c.",color=cor2,markersize=markersize)
		ax1.plot(x/1.0E3,y/1.0E3,"c.",color=cor2,markersize=markersize)
		
		ax1.plot(data[1,:],data[2,:],color=(1,1,1),linewidth=10)
		
		plt.xlim(0,param.lon.end.km)
		plt.ylim(param.depth.end,0)
		
		ax2 = ax1.twiny()
		ax2.set_xlim(param.lon.start.degree,param.lon.end.degree)
		ax2.set_xticks(xticks_degree)
		ax2.set_xlabel("Longitude ($\!^\circ\!$)")
		
		# Color bar
		tickss = np.linspace(np.min(SSS),np.max(SSS),10)
		cbar = fig.colorbar(cs,ticks=tickss,pad=0.015)
		cbar.ax.set_ylabel('$log_{10}\:\epsilon$')
		
		# Basin location
		plt.hlines(drawdepth,basinWest,basinEast,linewidth=6,color=(0,0,0))
		
		plt.tight_layout(rect=[0,0,1.1,1])
		plt.savefig(fig_dir+"strainPlot_"+str(cont).zfill(5)+".png")
		plt.close()

	############################################################################
	# H plot
	############################################################################

	if (hPlot==True):
		A = np.loadtxt("H_"+str(cont*print_step)+".txt",unpack=True,comments="P",skiprows=skiprows)
		HH 						= A*1.0
		HH[np.abs(HH)<1.0E-200] = 0
		HH 						= np.reshape(HH,(param.lon.size,param.lat.size,param.depth.size),order='F')
		HHH 					= HH[:,0,:]

		fig, ax1 = plt.subplots(figsize=(width,height))
		plt.title(value+" Myrs\n\n",fontsize=f_size)
		
		cs = ax1.contourf(xx,zz,np.transpose(HHH),extent=extent)
		ax1.set_ylabel("Depth (km)")
		ax1.set_xlabel("Distance (km)")
		ax1.set_xticks(xticks)

#		ax1.plot(x[cond1]/1.0E3,z[cond1]/1.0E3,"c.",color=cor1,markersize=markersize)
#		ax1.plot(x[cond2]/1.0E3,z[cond2]/1.0E3,"c.",color=cor3,markersize=markersize)
#		ax1.plot(x[cond3]/1.0E3,z[cond3]/1.0E3,"c.",color=cor2,markersize=markersize)
		ax1.plot(x/1.0E3,y/1.0E3,"c.",color=cor2,markersize=markersize)
		
		ax1.plot(data[1,:],data[2,:],color=(1,1,1),linewidth=10)
		
		plt.xlim(0,param.lon.end.km)
		plt.ylim(param.depth.end,0)
		
		ax2 = ax1.twiny()
		ax2.set_xlim(param.lon.start.degree,param.lon.end.degree)
		ax2.set_xticks(xticks_degree)
		ax2.set_xlabel("Longitude ($\!^\circ\!$)")
		
		# Color bar
#		ticks = np.linspace(0,np.max(TTT),10)
		cbar = fig.colorbar(cs,pad=0.015)
		cbar.ax.set_ylabel('H. Rate')
		
		# Basin location
		plt.hlines(drawdepth,basinWest,basinEast,linewidth=6,color=(0,0,0))
		
		plt.tight_layout(rect=[0,0,1.1,1])
		plt.savefig(fig_dir+"HPlot_"+str(cont).zfill(5)+".png")
		plt.close()

	############################################################################
	# Geoq plot
	############################################################################

	if (geoqPlot==True):
		G = np.loadtxt("Geoq_"+str(cont*print_step)+".txt",unpack=True,comments="P",skiprows=skiprows)
		GG 						= G * 1.0
		GG[np.abs(GG)<1.0E-200] = 0
		GG 						= np.reshape(GG,(param.lon.size,param.lat.size,param.depth.size),order='F')
		GGG 					= np.log10(GG[:,0,:])
#		GGG 					= GG[:,0,:]

		fig, ax1 = plt.subplots(figsize=(width,height))
		plt.title(value+" Myrs\n\n",fontsize=f_size)
		
		levels = np.linspace(np.floor(np.min(GGG)),np.ceil(np.max(GGG)),101)
		cs = ax1.contourf(xx,zz,np.transpose(GGG),extent=extent,levels=levels)
		ax1.set_ylabel("Depth (km)")
		ax1.set_xlabel("Distance (km)")
		ax1.set_xticks(xticks)
		
		ax1.plot(x/1.0E3,y/1.0E3,"c.",color=cor2,markersize=markersize)
		
#		ax1.plot(data[1,:],data[2,:],color=(1,1,1),linewidth=10)

		plt.xlim(0,param.lon.end.km)
		plt.ylim(param.depth.end,0)
		
		ax2 = ax1.twiny()
		ax2.set_xlim(param.lon.start.degree,param.lon.end.degree)
		ax2.set_xticks(xticks_degree)
		ax2.set_xlabel("Longitude ($\!^\circ\!$)")
		
		# Color bar
		ticks = np.linspace(np.floor(np.min(GGG)),np.ceil(np.max(GGG)),11)
		cbar = fig.colorbar(cs,ticks=ticks,pad=0.015)
		cbar.ax.set_ylabel('$\log_{10}\:\eta$ (Pa s)')
#		cbar.ax.set_ylabel('$log_{10}\:C$')

		# Basin location
		plt.hlines(drawdepth,basinWest,basinEast,linewidth=6,color=(0,0,0))
		
		plt.tight_layout(rect=[0,0,1.1,1])
		plt.savefig(fig_dir+"GeoqPlot_"+str(cont).zfill(5)+".png")
		plt.close()

	############################################################################
	# Factor Plot
	############################################################################

	if (factorPlot==True) and (geoqPlot==True):
		FFF = np.log10(GGG+10.) * RRR

		fig, ax1 = plt.subplots(figsize=(width,height))
		plt.title(value+" Myrs\n\n",fontsize=f_size)

		cs = ax1.contourf(xx,zz,np.transpose(FFF),extent=extent,cmap="viridis")
		ax1.set_ylabel("Depth (km)")
		ax1.set_xlabel("Distance (km)")
		ax1.set_xticks(xticks)

#		ax1.plot(x[cond1]/1.0E3,z[cond1]/1.0E3,"c.",color=cor1,markersize=markersize)
#		ax1.plot(x[cond2]/1.0E3,z[cond2]/1.0E3,"c.",color=cor3,markersize=markersize)
#		ax1.plot(x[cond3]/1.0E3,z[cond3]/1.0E3,"c.",color=cor2,markersize=markersize)
		ax1.plot(x/1.0E3,y/1.0E3,"c.",color=cor2,markersize=markersize)
		
		ax1.plot(data[1,:],data[2,:],color=(1,1,1),linewidth=10)
		
		plt.xlim(0,param.lon.end.km)
		plt.ylim(param.depth.end,0)
		
		ax2 = ax1.twiny()
		ax2.set_xlim(param.lon.start.degree,param.lon.end.degree)
		ax2.set_xticks(xticks_degree)
		ax2.set_xlabel("Longitude ($\!^\circ\!$)")
		
		# Color bar
#		ticks = np.linspace(0,np.max(FFF),10)
		cbar = fig.colorbar(cs,pad=0.015)
		cbar.ax.set_ylabel('Factor')

		# Basin location
		plt.hlines(drawdepth,basinWest,basinEast,linewidth=6,color=(0,0,0))
		
		plt.tight_layout(rect=[0,0,1.1,1])
		plt.savefig(fig_dir+"FactorPlot_"+str(cont).zfill(5)+".png")
		plt.close()

	############################################################################
	# Velocity Plot
	############################################################################

	if (velocityPlot==True):
		V = np.loadtxt("Veloc_fut_"+str(cont*print_step)+".txt",unpack=True,comments="P",skiprows=skiprows)
		VV 						= V * 1.0
		VV[np.abs(VV)<1.0E-200] = 0.0
		VV 						= np.reshape(VV,(n_dim_veloc,param.lon.size,param.lat.size,param.depth.size),order='F')
#		VV_max 					= np.max(np.abs(VV))
		if (n_dim_veloc==3):
			u 						= np.transpose(VV[0,:,0,:]) #/ VV_max
			v 						= np.transpose(VV[1,:,0,:]) #/ VV_max
			w 						= np.transpose(VV[2,:,0,:]) #/ VV_max
		else:
			u 						= np.transpose(VV[0,:,0,:]) #/ VV_max
#			v 						= np.transpose(VV[1,:,0,:]) #/ VV_max
			w 						= np.transpose(VV[1,:,0,:]) #/ VV_max
		colors 					= np.sqrt(u**2+w**2)
		colors_max				= np.max(colors)
		
		# Normalization
		colors 	= colors / colors_max
		u 		= u / colors_max
		w 		= w / colors_max
		
		fig, ax1 = plt.subplots(figsize=(width,height))
		plt.title(value+" Myrs\n\n",fontsize=f_size)
		
		cs = ax1.quiver(xi,zi,u,w,colors,width=0.001,scale=param.lon.size,pivot='mid')
		ax1.set_ylabel("Depth (km)")
		ax1.set_xlabel("Distance (km)")
		ax1.set_xticks(xticks)
		
		plt.xlim(0,param.lon.end.km)
		plt.ylim(param.depth.end,0)
		
		ax2 = ax1.twiny()
		ax2.set_xlim(param.lon.start.degree,param.lon.end.degree)
		ax2.set_xticks(xticks_degree)
		ax2.set_xlabel("Longitude ($\!^\circ\!$)")
		
#		print(np.max(colors),np.min(colors))
		# Color bar
#		ticks = np.linspace(0,np.max(colors),10)
		cbar = fig.colorbar(cs,pad=0.015)
		bar_title = "|v| x " + str(np.round(colors_max / 1.0E-3 * (365.25*24*60*60))) + " (mm/yr)"
		cbar.ax.set_ylabel(bar_title)

		plt.tight_layout(rect=[0,0,1.1,1])
		plt.savefig(fig_dir+"VelocityPlot_"+str(cont).zfill(5)+".png")
		plt.close()

	############################################################################
	# Viscosity Plot
	############################################################################

	if (viscPlot==True):
		N = np.zeros(shape=(param.lon.size-1,param.lat.size-1,param.depth.size-1))
		for rank in xrange(num_processes):
			viscFile = open("visc_"+str(cont*print_step)+"_"+str(rank)+".txt")
			for line in viscFile:
				line = line.split()
				i 			= int(line[0])
				j 			= int(line[1])
				k 			= int(line[2])
				N[i,j,k] 	= float(line[3])
			viscFile.close()

		N[np.abs(N)<1.0E-200] = 0.0
#		NNN = N[:,0,:]
#		print(np.max(NNN),np.min(NNN))
		NNN = np.log10(N[:,0,:])

		fig, ax1 = plt.subplots(figsize=(width,height))
		plt.title(value+" Myrs\n\n",fontsize=f_size)

		dx = (xx[0,1]-xx[0,0])/2.
		dz = (zz[1,0]-zz[0,0])/2.
		cs = ax1.contourf(xx[:-1,:-1]+dx,zz[:-1,:-1]+dz,np.transpose(NNN),extent=extent)
		ax1.set_ylabel("Depth (km)")
		ax1.set_xlabel("Distance (km)")
		ax1.set_xticks(xticks)

#		ax1.plot(x[cond1]/1.0E3,z[cond1]/1.0E3,"c.",color=cor1,markersize=markersize)
#		ax1.plot(x[cond2]/1.0E3,z[cond2]/1.0E3,"c.",color=cor3,markersize=markersize)
#		ax1.plot(x[cond3]/1.0E3,z[cond3]/1.0E3,"c.",color=cor2,markersize=markersize)
		ax1.plot(x/1.0E3,y/1.0E3,"c.",color=cor2,markersize=markersize)
		
		ax1.plot(data[1,:],data[2,:],color=(1,1,1),linewidth=10)

		plt.xlim(0,param.lon.end.km)
		plt.ylim(param.depth.end,0)
		
		ax2 = ax1.twiny()
		ax2.set_xlim(param.lon.start.degree,param.lon.end.degree)
		ax2.set_xticks(xticks_degree)
		ax2.set_xlabel("Longitude ($\!^\circ\!$)")
		
		# Color bar
		ticksN = np.linspace(np.min(NNN),np.max(NNN),10)
		cbar = fig.colorbar(cs,pad=0.015)
		cbar.ax.set_ylabel('$\log_{10}\:\eta$ (Pa s)')
	
		# Basin location
		plt.hlines(drawdepth,basinWest,basinEast,linewidth=6,color=(0,0,0))

		plt.tight_layout(rect=[0,0,1.1,1])
		plt.savefig(fig_dir+"ViscPlot_"+str(cont).zfill(5)+".png")
		plt.close()

	if (tempVelo==True):
	
		fig, ax1 = plt.subplots(figsize=(width,height))
		plt.title(value+" Myrs\n\n",fontsize=f_size)
		
		arrowSkip = 3
		cs = ax1.contourf(xx,zz,np.transpose(TTT),100,cmap="jet",extent=extent)
		ax1.quiver(xi[::arrowSkip],zi[::arrowSkip],u[::arrowSkip,::arrowSkip],w[::arrowSkip,::arrowSkip],units='width',width=0.001,scale=0.1*param.lon.size/(arrowSkip),pivot='mid')
		
		ax1.set_ylabel("Depth (km)")
		ax1.set_xlabel("Distance (km)")
		ax1.set_xticks(xticks)
		
		plt.xlim(0,param.lon.end.km)
		plt.ylim(param.depth.end,0)
		
		ax2 = ax1.twiny()
		ax2.set_xlim(param.lon.start.degree,param.lon.end.degree)
		ax2.set_xticks(xticks_degree)
		ax2.set_xlabel("Longitude ($\!^\circ\!$)")
		
		# Color bar
		auxTMax = np.around(np.max(TTT),-2)
		ticks = np.arange(0,auxTMax+1,dT)
		if (np.max(TTT)-ticks[-1])<dT/2:
			ticks = np.delete(ticks,-1)
			ticks = np.append(ticks,np.max(TTT))
		else:
			ticks = np.append(ticks,np.max(TTT))
		cbar = fig.colorbar(cs,ticks=ticks,pad=0.015)
		cbar.ax.set_ylabel('Temperature ($\!^\circ\!$C)')

		plt.tight_layout(rect=[0,0,1.1,1])
		plt.savefig(fig_dir+"TempVelocityPlot_"+str(cont).zfill(5)+".png")
		plt.close()
	print("Done?")
	return 0

	
with concurrent.futures.ProcessPoolExecutor() as executor:
	list = [i for i in xrange(0,5)]
	ex = executor.map(ploter,list)
	
	
#ploter(1)



