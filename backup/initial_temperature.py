import numpy as np
import re
import matplotlib.pyplot as plt
fileName = './out/temperatureField.dat'

file = open(fileName)
sx,sy,sz 	= re.split("[ \t]+", file.readline().strip())
sLon,sLat,sDep = int(sx),int(sy),int(sz)
w,e,s,n,t,b = re.split("[ \t]+", file.readline().strip())
westL,eastL,southL,northL,topL,bottomL = float(w),float(e),float(s),float(n),float(t),float(b)
w,e,s,n,t,b = re.split("[ \t]+", file.readline().strip())
westD,eastD,southD,northD,topD,bottomD = float(w),float(e),float(s),float(n),float(t),float(b)

temp = np.zeros(shape=(sLon,sLat,sDep))

for j in xrange(sLat):
	for i in xrange(sLon):
		for k in xrange(sDep):
			temp[i,j,k] = file.readline()


txtName = "./out/Temper_0_3D.txt"
txtFile = open(txtName,"w")

txtFile.write(str(sLon)+"\t"+str(sLat)+"\t"+str(sDep)+"\n"+str(westL)+"\t"+str(eastL)+"\t"+str(southL)+"\t"+str(northL)+"\t"+str(topL)+"\t"+str(bottomL)+"\nT3\nT4\n")

for k in xrange(sDep):
	for j in xrange(sLat):
		for i in xrange(sLon):
			temp2 = temp[i,j,k]
			txtFile.write(str(temp2)+"\n")
txtFile.close()

#np.savetxt(,temp2d,header="T1\nT2\nT3\nT4")

