import numpy as np
import os, sys, re

################################################################################
# Configuration
################################################################################

showPlot ='False'

################################################################################
# Setting up classes and methods
################################################################################

cwd = os.getcwd()
libsLocation = os.path.join(cwd,'lib')
sys.path.append(libsLocation)

import classes

################################################################################
# Read data from ./src/params.txt
################################################################################

print("*** Reading initial parameters *** ")
parPath = cwd + '/src/params.txt'
param 	= classes.Dimension(parPath)

param.lat.size = 1

################################################################################
# Reference data
################################################################################

refFilePath 	= cwd + '/src/reference.txt'
dimRef 			= classes.Reference(refFilePath,param.depth.end)

################################################################################
# Plot start
################################################################################

import matplotlib.pyplot as plt
plt.clf()
plt.close('all')
plt.rcParams.update({'font.size': 32})

width, height 	= 45, 10

dticks = 250.
arr_aux = np.arange(0,param.lon.end.km+dticks/2.,dticks)
if (param.lon.end.km-arr_aux[-1]<=dticks/2.):
	arr_aux = np.delete(arr_aux,-1)
xticks = np.append(arr_aux,param.lon.end.km)

dticks_degree = 2.5
arr_aux_degree = np.arange(param.lon.start.degree,param.lon.end.degree+dticks_degree/2.,dticks_degree)
if (param.lon.end.degree-arr_aux_degree[-1]<=dticks_degree/2.):
	arr_aux_degree = np.delete(arr_aux_degree,-1)
xticks_degree = np.append(arr_aux_degree,param.lon.end.degree)

################################################################################
# Load header and data
################################################################################

temperPath 		= './out/Temper_0_3D.txt'

skiprows 		= 4
temp 			= np.loadtxt(temperPath,skiprows=skiprows)
temp 			= np.reshape(temp,(param.lon.size,param.lat.size,param.depth.size),order='F')

extent2d = [param.lon.start.km,param.lon.end.km,param.depth.end,param.depth.start]

fig, ax = plt.subplots(figsize=(width,height))
n = int(np.round(param.lon.end.km,-3)/500.) + 1
l = np.append(np.linspace(param.lon.start.km,np.round(param.lon.end.km,-3),n),param.lon.end.km)
plt.xticks(l)
auxTMax = np.around(np.max(temp),-2)
dT = 200.
ticks = np.arange(0,auxTMax+1,dT)
if (np.max(temp)-ticks[-1])<dT/2:
	ticks = np.delete(ticks,-1)
	ticks = np.append(ticks,np.max(temp))
else:
	ticks = np.append(ticks,np.max(temp))

cs = ax.contourf(temp[:,0,:].transpose(),1001,cmap="jet",extent=extent2d)
ax.set_ylabel("Depth (km)")
ax.set_xlabel("Distance (km)")
ax.set_xticks(xticks)
# ax.set_aspect(aspect=0.5)

ax2 = ax.twiny()
ax2.set_xlim(param.lon.start.degree,param.lon.end.degree)
ax2.set_xticks(xticks_degree)
plt.xlabel("Longitude ($\!^\circ\!$)")

cbar = fig.colorbar(cs,orientation="vertical",label="Temperature ($\!^\circ\!$C)",ticks=ticks, pad=0.015)#,aspect=50,shrink=0.75)

# Basin location
plt.vlines(-58,0,-1000,color=(1,1,1),linestyles="dashed")
plt.vlines(-55,0,-1000,color=(1,1,1),linestyles="dashed")
# plt.hlines(-10,-58,-55,linewidth=6,color=(0,0,0))
plt.ylim(param.depth.end,param.depth.start)

plt.tight_layout(rect=[0,0,1.1,1])
plt.savefig('./figures/temperature-profile.pdf')
if (showPlot=='True'):
	plt.show()




