import os, sys, re
import numpy as np

################################################################################
# Setting up classes and methods
################################################################################

cwd = os.getcwd()
libsLocation = os.path.join(cwd,'lib')
sys.path.append(libsLocation)

import reference, classes, methods, data, projection
import makeApprox as ma

################################################################################
# Control options
# makeArea: 'True' if you want to crop the slab model to the area
# makeApprox: 'True' if you want to fit pols. to each slab in each latitude
# polDegree: set the polynomial degree
# mayaviPermission: 'True' if you want to plot using mayavi2
################################################################################

makePseudo2D		= 'True'
makeArea 			= 'True'
makeApprox 			= 'True'
polDegree			= 4
acceptance			= 0.95
mayaviPermission 	= 'False'

if (mayaviPermission=='True'):
	from mayavi import mlab

################################################################################
# Read dimensional reference values
################################################################################

refValuesPath 	= './src/reference.txt'
refValuesFile 	= open(refValuesPath)
refValues		= []
for line in refValuesFile:
	refValues.append(line)

################################################################################
# Read data from ./src/params.txt
################################################################################

parPath = './src/params.txt'
par 	= open(parPath)
lon 	= classes.Parameters(re.split("[ ]+", par.readline().strip()),"lon")
lat 	= classes.Parameters(re.split("[ ]+", par.readline().strip()),"lat")
depth 	= classes.Parameters(re.split("[ ]+", par.readline().strip()),"depth")
par.close()

dimRef 			= reference.Reference(refValues,depth.end)

################################################################################
# Take model data (slab1.0) for the area 
################################################################################

fullModelPath 	= './src/sam_slab1.0_clip.xyz' # Path to slab1.0
#fullModelPath 	= './src/sam_slab2_dep_02.23.18.xyz' # Path to slab2.0
fullModelRes	= 0.02 # Defined based on the slab1.0 model(do not change!)
#fullModelRes	= 0.05 # Defined based on the slab2.0 model(do not change!)
areaModelPath 	= './out/model/slab.' # Data is written in the /out/ directory

auxSizeLon		= np.round((lon.endDegree-lon.startDegree)/fullModelRes,2)
auxSizeLat		= np.round((lat.endDegree-lat.startDegree)/fullModelRes,2)
sizeLonModel	= int(auxSizeLon) + 1
sizeLatModel	= int(auxSizeLat) + 1

#sizeLonModel	= np.max([2,lon.length])
#sizeLatModel	= np.max([2,lat.length])

if (makeArea == 'True'):
	areaModel		= np.zeros(shape=(sizeLatModel,sizeLonModel))
	areaModel[:]	= np.nan

	fullModel = open(fullModelPath)
	for line in fullModel:
#		auxLon 		= float(re.split("[,]+",line.strip())[0])
		auxLon 		= float(re.split("[\t]+",line.strip())[0])
#		auxLat 		= float(re.split("[,]+",line.strip())[1])
		auxLat 		= float(re.split("[\t]+",line.strip())[1])
		if (((auxLon >= lon.startDegree) and (auxLon <= lon.endDegree)) and
			((auxLat >= lat.startDegree) and (auxLat <= lat.endDegree))):
			iLon 		= ((auxLon - lon.startDegree) / fullModelRes)
			indexLon 	= int(np.round(iLon,0))
#			print(indexLon),
			iLat 		= ((auxLat - lat.startDegree) / fullModelRes)
			indexLat 	= int(np.round(iLat,0))
#			print(indexLat)
#			areaModel[indexLat,indexLon] = float(re.split("[,]+",line.strip())[2])
			areaModel[indexLat,indexLon] = float(re.split("[\t]+",line.strip())[2])
	fullModel.close()

	if (makePseudo2D=='True'):
		areaModel[-1,:] = areaModel[0,:]

	# Save a slice of the model for each latitude in the area
	for j in xrange(sizeLatModel):
		aux1, aux2		= np.round(lat.startDegree*100,0),np.round(j*fullModelRes*100,0)
		auxName 		= str(int(aux1+aux2)).zfill(5)
		modelLatFile 	= open(areaModelPath+auxName,"w")
		count 			= 0
		for i in xrange(sizeLonModel):
			if (~np.isnan(areaModel[j,i])) and (areaModel[j,i]<=0):
				if (count == 0):
					distance 	= methods.km(0,i*fullModelRes)
					dist0		= distance
					lon0		= i * fullModelRes
					lat0		= j * fullModelRes
					# Make linear extrapolation to zero
					if ((np.round(areaModel[j,i]),0)!=0):
						dx 			= methods.km(0,fullModelRes)
						dz 			= areaModel[j,i+1] - areaModel[j,i]
						ang 		= dz / dx
						b 			= areaModel[j,i] - ang * dist0
						cross		= - b / ang
						n			= int(np.ceil((dist0-cross)/dx))
						auxStartKm 	= dist0 - (dx*(n-1))
						auxStartD	= i * fullModelRes - (fullModelRes*(n-1))
						auxXKm 		= np.linspace(auxStartKm,dist0-dx,n)
						auxXD		= np.linspace(auxStartD,(i-1)*fullModelRes,n)
						auxZ		= ang * auxXKm + b
						for pos in xrange(0,n):
							modelLatFile.write(str(np.round(auxXD[pos],2))+"\t"+str(np.round(auxXKm[pos],2))+"\t"+str(np.round(auxZ[pos],5))+"\n")
				else:
					distance = methods.km(lon0,i*fullModelRes) + dist0
				modelLatFile.write(str(lon.startDegree+i*fullModelRes)+"\t"+str(distance)+"\t"+str(areaModel[j,i])+"\n")
				count = count + 1
		modelLatFile.close()

###############################################################################
# Polynomial fitting approximation caller
# Save approximations to a file
# Inward approximation projection
###############################################################################

approxModelPath 	= './out/approx/approx.'
projecModelPath 	= './out/projec/slice.'
limit 				= sizeLatModel

if (makeApprox == 'True'):
	for j in xrange(limit):
#		print(j,"/",limit)
		aux1,aux2		= np.round(lat.startDegree*100,0),np.round(j*fullModelRes*100,0)
		modelLatFile	= areaModelPath + str(int(aux1+aux2)).zfill(5)
		slice 			= np.loadtxt(modelLatFile)
		abscissaData	= slice[:,1]
		ordinateData	= slice[:,2]
		
		N 				= np.size(abscissaData)
		
		weightFactor = 7
		
		abscissaWeight	= np.ones(int(weightFactor*N)) * slice[0,1]
		ordinateWeight	= np.ones(int(weightFactor*N)) * slice[0,2]
		abscissa		= np.concatenate((abscissaWeight,abscissaData),axis=0)
		ordinate		= np.concatenate((ordinateWeight,ordinateData),axis=0)
		
		pd		= polDegree
		count	= 4
		
		# Polynomial approx. caller
		a 		= ma.polApprox(abscissa,ordinate,pd)
		Y 		= ma.f(abscissaData,a)
		reDo 	= ma.coefDetermination(ordinateData,Y,acceptance)
		while ((reDo == 'True') and (count != 15)):
			a 		= ma.polApprox(abscissa,ordinate,count)
			Y 		= ma.f(abscissaData,a)
			reDo 	= ma.coefDetermination(ordinateData,Y,acceptance)
			count += 1
		if (count == 15):
			print('\n\n\nCOULD NOT FIND A POLYNOMIAL TO FIT\n\n\n')
		
		# Save approximations to a file
		aux3, aux4		= np.round(lat.startDegree*100,0),np.round(j*fullModelRes*100,0)
		approxLatFile	= approxModelPath + str(int(aux3+aux4)).zfill(5)
		approxModel 	= open(approxLatFile,"w")
		for i in xrange(N):
			approxModel.write(str(abscissaData[i])+"\t"+str(Y[i])+"\n")
		approxModel.close()

		# Innward approximation projection
		projecDepth = data.slabThickness()
#		absProj, ordProj = projection.makeInnwardProjection(N,abscissaData,ordinateData,projecDepth)
		absProj, ordProj = projection.makeInnwardProjection(N,abscissaData,Y,projecDepth)
		
		# Save innward approximation
		aux5, aux6		= np.round(lat.startDegree*100,0),np.round(j*fullModelRes*100,0)
		modelProjFile 	= projecModelPath + str(int(aux5+aux6)).zfill(5)
		projecFile		= open(modelProjFile,"w")
		for i in xrange(N-1):
			projecFile.write(str(absProj[i])+"\t"+str(ordProj[i])+"\n")
		projecFile.close()
							 
################################################################################
# Test plot
################################################################################

if (mayaviPermission == 'True'):
	mlab.figure(size=(800,800))
	fac = methods.km(0,fullModelRes)

if (mayaviPermission == 'True'):
	extent = [lon.startKm,lon.endKm,lat.startKm,lat.endKm,depth.start,depth.end]
	radius = 1
	for j in xrange(limit):#limit
		aux7, aux8		= np.round(lat.startDegree*100,0),np.round(j*fullModelRes*100,0)
		approxModelFile	= approxModelPath + str(int(aux7+aux8)).zfill(5)
		slice 			= np.loadtxt(approxModelFile)
		abscissa		= slice[:,0]
		ordinate		= slice[:,1]

		aux		= np.ones(np.size(abscissa)) * j * fac
		mlab.plot3d(abscissa,aux,ordinate,tube_radius=radius)
#		mlab.points3d(abscissa,aux,ordinate,scale_factor=1.75)

		aux9, aux10		= np.round(lat.startDegree*100,0),np.round(j*fullModelRes*100,0)
		modelProjFile 	= projecModelPath + str(int(aux9+aux10)).zfill(5)
		projection		= np.loadtxt(modelProjFile)
		absProj			= projection[:,0]
		ordProj			= projection[:,1]

		aux2	= np.ones(np.size(absProj)) * j * fac
		mlab.plot3d(absProj,aux2,ordProj,tube_radius=radius,color=(1,0,0))
#		mlab.points3d(absProj,aux2,ordProj,color=(1,0,0),scale_factor=1.5)
		mlab.view(azimuth=270,elevation=90)


################################################################################
# Region Boundary Plot (Box)
################################################################################

if (mayaviPermission == 'True'):
	mlab.outline(extent=extent)
	mlab.points3d(0,0,0,scale_factor=50)
	mlab.show()

