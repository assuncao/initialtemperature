import numpy as np
import os, sys
from scipy.special import erf

################################################################################
# Setting up classes and methods
################################################################################

cwd = os.getcwd()
libsLocation = os.path.join(cwd,'lib')
sys.path.append(libsLocation)

################################################################################
# Methods/Functions
################################################################################

### Transforms from degrees to radians ###
def rad(angle):
	return np.pi * angle / 180.

### Calculates the distance in km between and arc (approximation 1 degree = 111. km)
def km(lon1,lon2):
	angularDistance = lon2 - lon1
	distance		= 111. * angularDistance
	return distance

def tempLinear(x,x0,x1,y0,y1):
#	if (x1==0):
#		x1 = 1.0E-100
	a = (y1 - y0) / (x1 - x0)
	b = y1 - a * x1
	f = a * x + b
#	print(f)
	return f

def tempAdiabatic(depthCoord,initialDepth,alpha,gravity,specificHeat,POTtemperatureK):
	iniDepth	= initialDepth * 1000 # Converting to meters (m)
	depCoord	= depthCoord * 1000
	aux 		= np.exp(((alpha*gravity)/(specificHeat))*((-1)*depCoord))
	tempField 	= POTtemperatureK * aux - 273.15 # Converting from K to Celsius
	return tempField

def slabLimitCalc(approx,projec):
	dxWest 		= approx.minA - projec.minA
	if (dxWest==0):
		dxWest = 1.0E-100
	dzWest 		= approx.ordinate[-1] - projec.ordinate[-1]
	aWest 		= dzWest / dxWest
	bWest 		= approx.maxO - aWest * approx.minA
#	print(dxWest,approx.minA,projec.minA,aWest)

	dxEast 		= approx.maxA - projec.maxA
	if (dxEast==0):
		dxEast = 1.0E-100
	dzEast 		= approx.ordinate[-1] - projec.ordinate[-1]
	aEast 		= dzEast / dxEast
	bEast 		= approx.ordinate[-1] - aEast * approx.maxA
	
	slabLimit 	= [aWest,bWest,aEast,bEast]
	return slabLimit

def boundary(a,b,x):
	z = a * x + b
	return z

def erfSingle(z0,z1,x,length):
	z = z0 + ((z1 - z0) / 2.) * erf((x/(length/4.)-2)) + ((z1 - z0)/2.)
	return z

def findLongiInterval(projec,lon):
	for i in xrange(projec.size-1):
		if (projec.abscissa[i]<=lon):
			westBoundary = projec.abscissa[i]
			index = i
#		else:
#			continue
	eastBoundary = projec.abscissa[index+1]

	#	a = (projec[index+1,1] - projec[index,1]) / (eastBoundary - westBoundary)
	dz 	= np.max([projec.ordinate[index]-projec.ordinate[index+1],projec.ordinate[index+1]-projec.ordinate[index]])
	a 	= dz / (eastBoundary-westBoundary)
	b 	= projec.ordinate[index] - a * projec.abscissa[index]

	botBoundary = a * lon + b
	return botBoundary

# Maybe this is not used
def surfaceLength(surface):
	x 	= surface[:,0]
	z 	= surface[:,1]
	aux = np.zeros(np.size(x))
	dx2 = 0.
	dz2 = 0.
	len = 0.
	for i in xrange(np.size(x)-1):
		dx2 		= pow(x[i+1]-x[i],2)
		dz2 		= pow(z[i+1]-z[i],2)
		len 		= len + np.sqrt(dx2+dz2)
		aux[i+1] 	= len
	
	return aux

def minDistance2Curve(approx,lon,depth):
	closest = 100000000
	for i in xrange(approx.size):
		cA 	= approx.ordinate[i] - depth
		cB 	= approx.abscissa[i] - lon
		cA2 = pow(cA,2)
		cB2 = pow(cB,2)
		h 	= np.sqrt(cA2+cB2)
#		print(h)
		if (h<=closest):# and (approx.abscissa[i]>=lon):
			closest = h
			p = ([approx.abscissa[i]-approx.minA,approx.ordinate[i]-approx.maxO],closest,i)
		else:
			continue
	return p

def slabTemperature(x,z,niter,slabThickness,R,tm):
	z = slabThickness - z
	h = 0.
	for n in xrange(1,niter):
		c = pow(-1,n) / (n * np.pi)
		b = np.sqrt(pow(R,2) + (pow(n,2) * pow(np.pi,2))) - R
		h = c * np.exp(( - b * x) / (slabThickness)) * np.sin((n * np.pi * z) / (slabThickness)) + h
	s = tm * (1 + 2 * h)
	if (s<0):
		s = 0
	return s

def findContactBoundary(ordinate,abscissa,lithoLon,lithoDepth):
	depth = 0.
	p_aux = 1.0E200
	sz = np.size(abscissa)
	for i in xrange(sz):
		p = np.interp(abscissa[sz-1-i],lithoLon,lithoDepth)
		if (abs(p-ordinate[sz-1-i])<p_aux):
			p_aux = abs(p-ordinate[sz-1-i])
			depth = p
	return depth

def findSlabSurfaceDepth(ordinate,abscissa,z):
	depth = 0.
	if (z<0):
		z = abs(z)
		ordinate = (-1) * ordinate
	for i in xrange(ordinate.size-1):
		if ((z>=ordinate[i]) and (z<=ordinate[i+1])):
			a = (abscissa[i+1]-abscissa[i]) / (ordinate[i+1]-ordinate[i])
			b = abscissa[i] - a * ordinate[i]
			depth = a * z + b
	return depth

def verticalVelocityProfile(westDepth,eastDepth,depth,velo,flag,slabVelocitySI):
	if (flag==1):
		aux1 	= np.average([slabVelocitySI,0.0])
		aux2	= (slabVelocitySI-0.0) / 2
		auxerf	= 4 * (depth-westDepth) / (-westDepth-eastDepth+depth[-1])
		velo = aux1 - aux2 * erf(auxerf-2)
		velo[depth>westDepth] = slabVelocitySI
		velo[depth<(depth[-1]-eastDepth)] = 0.0
	else:
		aux1	= np.average([slabVelocitySI,0.0])
		aux2	= (slabVelocitySI-0.0) / 2
		auxerf	= 4 * (depth-eastDepth) / (-westDepth-eastDepth+depth[-1])
		velo = aux1 + aux2 * erf(auxerf-2)
		velo[depth>eastDepth] = 0.0
		velo[depth<(depth[-1]-westDepth)] = slabVelocitySI
	return velo

def makeInnwardProjection(N,abscissaData,ordinateData,projecDepth):
	angle			= np.ones(N-1)
	absProj			= np.ones(N-1)
	absRefe 		= np.ones(N-1)
	ordProj			= np.ones(N-1)
	ordRefe 		= np.ones(N-1)
	for i in xrange(N-1):
		absRefe[i]	= (abscissaData[i+1] + abscissaData[i]) / 2.
		ordRefe[i]	= (ordinateData[i+1] + ordinateData[i]) / 2.
		dx			= abscissaData[i+1] - abscissaData[i]
		dz 			= ordinateData[i+1] - ordinateData[i]
		tgt 		= dz / dx
		angle[i] 	= np.arctan(tgt)
		if (tgt<0):
			absProj[i]	= absRefe[i] - abs(projecDepth * np.sin(angle[i]))
			ordProj[i]	= ordRefe[i] - abs(projecDepth * np.cos(angle[i]))
		else:
			absProj[i]	= absRefe[i] + abs(projecDepth * np.sin(angle[i]))
			ordProj[i]	= ordRefe[i] - abs(projecDepth * np.cos(angle[i]))

	return absProj, ordProj

def makeOutwardProjection(N,abscissaData,ordinateData,projecDepth):
	angle			= np.ones(N-1)
	absProj			= np.ones(N-1)
	absRefe 		= np.ones(N-1)
	ordProj			= np.ones(N-1)
	ordRefe 		= np.ones(N-1)
	for i in xrange(N-1):
		absRefe[i]	= (abscissaData[i+1] + abscissaData[i]) / 2.
		ordRefe[i]	= (ordinateData[i+1] + ordinateData[i]) / 2.
		dx			= abscissaData[i+1] - abscissaData[i]
		dz 			= ordinateData[i+1] - ordinateData[i]
		tgt 		= np.round(dz,2) / np.round(dx,2)
		angle[i] 	= np.arctan(tgt)
		if (tgt<0):
			absProj[i]	= absRefe[i] + abs(projecDepth * np.sin(angle[i]))
			ordProj[i]	= ordRefe[i] + abs(projecDepth * np.cos(angle[i]))
		else:
			absProj[i]	= absRefe[i] - abs(projecDepth * np.sin(angle[i]))
			ordProj[i]	= ordRefe[i] + abs(projecDepth * np.cos(angle[i]))
	return absProj, ordProj

def calculateIntegral(arr,dx):
	aux 		= np.zeros(arr.size)
	aux[1:-1] 	= arr[1:-1] * dx
	aux[0] 		= arr[0] * dx / 2.
	aux[-1] 	= arr[-1] * dx / 2.0
	integral 	= aux.sum()
	return integral

def velocityIn(depthTop,depthBot,depth,slabVelocitySI,TZdepth):
	velo 		= np.zeros(depth.size)
	depthTopAux = depthTop
	depthBotAux = TZdepth - depthBot
	cond 		= depth[(depth<=depthTopAux)&(depth>=depthBotAux)]
	whre 		= np.where((depth<=depthTopAux)&(depth>=depthBotAux))
	z 			= np.linspace(-2,2,cond.size)
	p 			= (slabVelocitySI/2.) + (slabVelocitySI/2.0) * erf(z)
	velo[whre] 	= p

	velo[depth>=depthTopAux] = slabVelocitySI
	velo[depth<=depthBotAux] = 0.0
	return velo

def velocityOut(depthT,depthB,depth,slabVelocitySI,TZdepth,integralIn):
	
	depthTop = depthT
	depthBot = TZdepth
			
	depthOutChange = -100.
	velo = np.ones(depth.size) * slabVelocitySI

	depth0 = depthBot - depthOutChange - depthB
	cond0 = depth[(depth<=depthTop)&(depth>=depth0)]
	whre0 = np.where((depth<=depthTop)&(depth>=depth0))

	depth1 = depthBot - depthOutChange
	cond1 = depth[(depth<=depth1)&(depth>=depthBot)]
	whre1 = np.where((depth<=depth1)&(depth>=depthBot))

	z0 = np.linspace(-2,2,cond0.size)
	z1 = np.linspace(-2,2,cond1.size)
	p0 = (slabVelocitySI/2.) - (slabVelocitySI/2.0) * erf(z0)
	p1 = (slabVelocitySI/2.) + (slabVelocitySI/2.0) * erf(z1)
	velo[whre0] = p0
	velo[whre1] = p1
	velo[depth>=depthTop] = 0.0
	velo[depth<=depthBot] = 0.0
						
	# Fix mass conservation equation
	integralOut = calculateIntegral(velo,depth[1]-depth[0])
	count = -1
	while (integralIn!=integralOut):
		integralDiff = integralIn - integralOut
#		print "IN",integralDiff
		try:
			idx = whre1[0][count]
		except IndexError:
			print("COULD NOT GUARANTEE MASS CONSERVATION, DIFFERENCE OF"),
			integralOut = calculateIntegral(velo,depth[1]-depth[0])
			print(integralOut-integralIn)
			break
		dz = depth[idx] - depth[idx-1]
		if (integralDiff>0.) and (velo[idx]<slabVelocitySI):
			dArea = velo[idx] * dz
			aVelo = (dArea+integralDiff) / dz
#			print slabVelocitySI,aVelo,velo[idx],
			if (aVelo>slabVelocitySI):
				velo[idx] = slabVelocitySI
			else:
				velo[idx] = aVelo
		else:
			pass
#		print(velo[idx])
		integralOut = calculateIntegral(velo,depth[1]-depth[0])
		count -= 1
	return velo
