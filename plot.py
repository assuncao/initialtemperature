import numpy as np
import os, sys, re

################################################################################
# Read dimensional reference values
################################################################################

#refValuesPath 	= './src/reference.txt'
#refValuesFile 	= open(refValuesPath)
#refValues		= []
#for line in refValuesFile:
#	refValues.append(line)

################################################################################
# Setting up classes and methods
################################################################################

cwd = os.getcwd()
libsLocation = os.path.join(cwd,'lib')
sys.path.append(libsLocation)

import classes

################################################################################
# Read data from ./src/params.txt
################################################################################

print("*** Reading initial parameters *** ")
parPath = cwd + '/src/params.txt'
param 	= classes.Dimension(parPath)

################################################################################
# Reference data
################################################################################

refFilePath 	= cwd + '/src/reference.txt'
dimRef 			= classes.Reference(refFilePath,param.depth.end)

################################################################################
# Plot type
################################################################################

plotType = param.dim

if (plotType=='2d'):
	import matplotlib.pyplot as plt
	plt.clf()
	plt.close('all')
elif (plotType=='3d'):
	from mayavi import mlab
else:
	print("Invalid plot type to choose!")

################################################################################
# Load header and data
################################################################################

temperPath 		= './out/Temper_0_3D.txt'

skiprows 		= 4
temp 			= np.loadtxt(temperPath,skiprows=skiprows)
temp 			= np.reshape(temp,(param.lon.size,param.lat.size,param.depth.size),order='F')

zoom = "off"
if (zoom=="on"):
	maxX = 1500
	minX = 500
	maxZ = 0
	minZ = -250
	minT = np.min(temp)
	maxT = np.max(temp)

extent2d = [param.lon.start.km,param.lon.end.km,param.depth.end,param.depth.start]

if (plotType=='2d'):
	width 		= 15
	height 		= 4
	markersize 	= 0.4
	plt.rcParams.update({'font.size': 12})
	
	if (zoom=="on"):
		figfactor = min(abs((maxX-minX)/(maxZ-minZ)),4)
		factor = 4
		ticks = np.linspace(0,1518,18)
		fig,ax = plt.subplots(figsize=(12,6))
		plt.hlines(-80,0,4000,color=(0,0,0),linestyles="dashed")
		plt.hlines(-150,0,4000,color=(0,0,0),linestyles="dashed")
		ax.set_xlim(minX,maxX)
		plt.xticks(np.linspace(minX,maxX,11))
		plt.yticks([0,-50,-80,-100,-150,-200,-250])
		ax.set_ylim(minZ,maxZ)
	else:
		fig,ax = plt.subplots(figsize=(width,height))
		n = int(np.round(param.lon.end.km,-3)/500.) + 1
		l = np.append(np.linspace(param.lon.start.km,np.round(param.lon.end.km,-3),n),param.lon.end.km)
		plt.xticks(l)
#		ticks = np.linspace(0,dimRef.TZtemperatureC,16)
		ticks = np.linspace(0,dimRef.maxTempC,16)

	cs = plt.contourf(temp[:,1,:].transpose(),ticks,cmap="jet",extent=extent2d)
	plt.ylabel("Depth (km)")
	plt.xlabel("Distance (km)")
#
	ax2 = ax.twiny()
	ax2.set_xlim(param.lon.start.degree,param.lon.end.degree)
	plt.xlabel("Longitude ($\!^\circ\!$)")

	cbar = fig.colorbar(cs,orientation="vertical",label="Temperature ($\!^\circ\!$C)",ticks=ticks, pad=0.015)#,aspect=50,shrink=0.75)

	# Basin location
	plt.hlines(-10,-58,-55,linewidth=6,color=(0,0,0))
	plt.ylim(param.depth.end,param.depth.start)

	plt.tight_layout(rect=[0,0,1.1,1])
	plt.savefig('./figures/temperature-profile.pdf')
	plt.show()

elif (plotType=='3d'):
#	mlab.axes.label_text_property.font_family = 'courier'
#	mlab.axes.label_text_property.font_size = 12
#	ax = mlab.axes()
	contours 	= [300,800,1300,1700]
	extent 		= [param.lon.start.km,param.lon.end.km,param.lat.start.km,param.lat.end.km,param.depth.end,param.depth.start]
	ranges 		= extent
	mlab.figure(size=(1200,800),bgcolor=(1,1,1),fgcolor=(0,0,0))
	mlab.view(azimuth=120, elevation=120, distance=None, focalpoint=None, roll=None, reset_roll=True, figure=None)
	mlab.contour3d(temp,extent=extent,transparent=True,contours=contours,opacity=0.5)
	mlab.outline(color=(0,0,0))
#	mlab.axes(color=(1,1,0),ranges=extent,line_width=.5,xlabel="Distance (km)",zlabel="Depth (km)",z_axis_visibility=False,nb_labels=2)
#	mlab.savefig("./figures/test.pdf")
	mlab.show()
else:
	print("Invalid plot type to plot!")


