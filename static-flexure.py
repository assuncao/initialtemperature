import numpy as np 
import matplotlib.pyplot as plt
import os, sys, re

################################################################################
# Setting up classes and methods
################################################################################

cwd = '/Users/kugelblitz/Desktop/initialTemperature'#os.getcwd()
libsLocation = os.path.join(cwd,'lib')
sys.path.append(libsLocation)

import classes

################################################################################
# Read data from ./src/params.txt and setting up domain
################################################################################

print("*** Reading initial parameters *** ")
parPath = './params.txt'
param 	= classes.Dimension(parPath)

param.lat.size = 1

xmin, xmax, nx= param.lon.start.km, param.lon.end.km, param.lon.size
zmin, zmax, nz= param.depth.start, param.depth.end, param.depth.size
x = np.linspace(xmin,xmax,nx) * 1.0E3 # [m]
z = np.linspace(zmax,zmin,nz) * 1.0E3 # [m]
p = np.zeros(nx)

linewidth	 = 4
f_size = 20
plt.rcParams.update({'font.size': f_size})

TZ = -660.
cwhite= (1,1,1)

x_start, x_end = 1500, 5000.
x_start_degree, x_end_degree = param.lon.start.degree + x_start / 111.,param.lon.start.degree +  x_end / 111.

xmin, xmax, nx= param.lon.start.km, param.lon.end.km, param.lon.size
zmin, zmax, nz= param.depth.start, param.depth.end, param.depth.size
x = np.linspace(xmin,xmax,nx) * 1.0E3 # [m]
z = np.linspace(zmax,zmin,nz) * 1.0E3 # [m]
p = np.zeros(nx)

# Basin location
basinWest, basinEast = -58, -55
startBasin = (basinEast-param.lon.start.degree) * 111.0
endBasin = (basinWest-param.lon.start.degree) * 111.0

dticks = 200.
ddtick = dticks/2
arr_aux = np.arange(x_start,x_end+dticks/2.,dticks)
if (x_end-arr_aux[-1]<=ddtick):
	arr_aux = np.delete(arr_aux,-1)
xticks = np.append(arr_aux,x_end)

dticks_degree = 2.0
ddtick_degree = 1.1
arr_aux_degree = np.arange(np.ceil(x_start_degree),np.floor(x_end_degree)+dticks_degree/2.,dticks_degree)
if (x_end_degree-arr_aux_degree[-1]<=ddtick_degree):
	arr_aux_degree = np.delete(arr_aux_degree,-1)
if (x_start_degree-arr_aux_degree[0]>=-ddtick_degree):
	arr_aux_degree = np.delete(arr_aux_degree,0)
auxaux = np.append(x_start_degree,arr_aux_degree)
xticks_degree = np.append(auxaux,x_end_degree)

################################################################################
# Methods
################################################################################

def topography(data,g=9.8,dz=10.0):
	aux = np.sum(data,axis=1)
	w = aux * g * dz
	return w

def flexura_num(x,p,Te=10.0E3,rhom=3300.,rho0=1000.0,E=1.0E11,nu=0.25,g=9.8):
    dx = x[1] - x[0]
    
    n = np.size(x)

    A = np.zeros((n,n))

    D = E*Te**3 / (12.*(1-nu**2))

    drho = rhom-rho0

    A[range(n),range(n)] =  (6.0*D) + (dx**4*drho*g)
    A[range(n-1),range(1,n)] = -4.0 * D
    A[range(1,n),range(n-1)] = -4.0 * D
    A[range(n-2),range(2,n)] = D
    A[range(2,n),range(n-2)] = D

    # Placa continua
    A[0,1] = -8.0 * D
    A[0,2] =  2.0 * D
    A[1,1] = (7.0*D) + (dx**4*drho*g)

    A[n-1,n-1] = -8.0 * D
    A[n-1,n-3] = 2.0 * D
    A[n-2,n-2] = (7.0*D) + (dx**4*drho*g)

    b = p * dx**4
    
    w = np.linalg.solve(A,b)
    return w

def axx(cs):
	ax1.set_xlabel("Distance (km)")
	ax1.set_xticks(xticks)
	ax1.set_xlim(x_start,x_end)
	ax1.set_ylabel("Subsidence (m)")
#	ax1.set_yticks(yticks)
	y0, y1 = -1500,1500
	ax1.set_ylim(y0,y1)
	ax1.tick_params(length=10,width=2)
#	ax1.set_aspect('equal')

	ax2 = ax1.twiny()
	ax2.set_xlim(param.lon.start.degree+x_start/111.,param.lon.start.degree+x_end/111.)
	ax2.set_xticks(np.round(xticks_degree,2))
	ax2.set_xlabel("Longitude ($\!^\circ\!$)")
	ax2.tick_params(length=10,width=2)

	# Basin location
	ax2.vlines(basinWest,y0,y1,linestyles="dotted",color=(0,0,0),linewidth=linewidth)
	ax2.vlines(basinEast,y0,y1,linestyles="dotted",color=(0,0,0),linewidth=linewidth)
#	ax1.hlines(TZ,x_start,x_end,linestyles="dashed",color=cwhite,linewidth=linewidth)
	
	ax1.autoscale(False)
#	ax2.autoscale(False)

n_plots = 524
color_idx = np.linspace(0, 1, n_plots)

file = open('teste.txt','w')
file2 = open('time-series.txt','w')

extent			= [0,param.lon.end.km,0,param.depth.end]
ref = 10
markersize 		= 0.4
ww = (x_end-x_start) / (-param.depth.end) #+ 0.15

rx = 0.2 # 20% of the height
wx = 0.6 # 60% of the height

width, height 	= ww * wx * ref + 2 * rx * ref, ref
aux = width / ref

tr = 0.025 / aux
r0, r1, r2, r3 = rx/aux - tr, rx , ww * wx / aux, wx
rect = [r0, r1, r2, r3]

fig = plt.figure(figsize=(width,height))
ax1 = plt.axes(rect)

###############################################################################
# Main Loop
###############################################################################

t = 0.0
dt = 5.0E6

for step1 in xrange(1, n_plots, 1):
	################################################################################
	# Load Visc, Rho and Temperature data
	################################################################################
	
	step0 = 0

	skiprows = 3
	shp = (param.lon.size,param.depth.size)

	# Viscosity

	visc_file0 = 'Geoq_' + str(step0) + '.txt'
	visc_file1 = 'Geoq_' + str(step1) + '.txt'

	visc0 = np.loadtxt(visc_file0,unpack=True,skiprows=skiprows,comments='P')
	visc0 = np.log10(visc0)
	visc0 = np.reshape(visc0,shp,order='F')

	visc1 = np.loadtxt(visc_file1,unpack=True,skiprows=skiprows,comments='P')
	visc1 = np.log10(visc1)
	visc1 = np.reshape(visc1,shp,order='F')

	# Density

	rho_file0 = 'Rho_' + str(step0) + '.txt'
	rho_file1 = 'Rho_' + str(step1) + '.txt'

	rho0 = np.loadtxt(rho_file0,unpack=True,skiprows=skiprows,comments='P')
	rho0 = np.reshape(rho0,shp,order='F')

	rho1 = np.loadtxt(rho_file1,skiprows=skiprows,comments='P')
	rho1 = np.reshape(rho1,shp,order='F')

	# Temperature

	temp_file0 = 'Temper_' + str(step0) +'.txt'
	temp_file1 = 'Temper_' + str(step1) +'.txt'

	temp0 = np.loadtxt(temp_file0,skiprows=skiprows,comments='P')
	temp0 = np.reshape(temp0,shp,order='F')

	temp1 = np.loadtxt(temp_file1,skiprows=skiprows,comments='P')
	temp1 = np.reshape(temp1,shp,order='F')

	# New density

	alpha = 3.28E-5
	r0 = rho0 * (1.0-alpha*temp0)
	r1 = rho1 * (1.0-alpha*temp1)

	# Time
	
	time_file = 'Tempo_' + str(step1) + '.txt'
	time = np.loadtxt(time_file,delimiter=":")[0,1]
	print time,

	################################################################################
	# Calculate topography
	################################################################################

	dz = (z[1]-z[0])
	w0 = topography(r0,dz=dz)
	w1 = topography(r1,dz=dz)

	topo = (w0-w1) / (3300.*9.8)

	################################################################################
	# Calculate flexure
	################################################################################

	p = np.copy(topo)
	p_original = np.copy(p)

	rho0 = 2300.
	p = rho0 * 9.8 * p  # densidade da crosta * g * altua do orogeno * largura do orogeno
	w = flexura_num(x,p,Te=70.0E3)/1.0E3
	
	for i in xrange(np.size(w)):
		file.write(str(w[i])+'\n')
	file2.write(str(time)+'\n')
	
	if (time>=t):
		print "plotted",
		plt.plot(x/1.0E3,w*1000.,linewidth=1,color=plt.cm.winter_r(step1))
		t += dt
	print step1
cs = plt.plot(x/1.0E3,w*1000.,linewidth=1,color=plt.cm.winter_r(step1))
axx(cs)
plt.savefig('figures/topography-evolution.png')
#plt.show()

file2.close()
file.close()

################################################################################
# Plots
################################################################################

#linewidth	 = 1
#extent		= [0,param.lon.end.km,0,param.depth.end]
#zticks		= np.linspace(zmax,zmin,6)
#zticks		= np.append(zticks,-660.)
#visc_min, visc_max = 18, 23
#n_visc = visc_max - visc_min + 1
#
#fig, axs = plt.subplots(ncols=1,nrows=5,figsize=(10,10))
#
#cs = axs[0].plot(x/1.0E3,w*1000.,"b",label="Sol. Num.",linewidth=linewidth)
#axs[0].plot(x/1.0E3,p_original,"r",label="Load",linewidth=linewidth)
#axs[0].set_xlim(x[0]/1.0E3,x[-1]/1.0E3)
#axs[0].hlines(0,x[0]/1.0E3,x[-1]/1.0E3,linestyles="dashed")
#axs[0].vlines(startBasin,np.min(w),np.max(w),linestyles="dashed")
#axs[0].vlines(endBasin,np.min(w),np.max(w),linestyles="dashed")
#axs[0].legend(loc=2)

# temp0

#cs0 = axs[1].imshow(temp0.T,extent=extent,cmap="jet",aspect='auto')
#axs[1].set_ylim(param.depth.end,param.depth.start)
#axs[1].set_yticks(zticks)
#axs[1].hlines(-660,x[0]/1.0E3,x[-1]/1.0E3,linestyles="dashed")
#
##aux = np.around(np.max(visc1),-2)
##ticks0 = np.linspace(np.min(temp0),np.max(temp0),10)
##ticks0[1:-1] = np.round(ticks0[1:-1],-2)
##cbar = fig.colorbar(cs0,ax=axs[1],ticks=ticks0,pad=0.015)
##cbar.ax.set_ylabel('Temperature ($^\circ$C)')
#
## temp1
#
#cs = axs[2].imshow(temp1.T,extent=extent,cmap="jet",aspect='auto')
#axs[2].set_ylim(param.depth.end,param.depth.start)
#axs[2].set_yticks(zticks)
#axs[2].hlines(-660,x[0]/1.0E3,x[-1]/1.0E3,linestyles="dashed")
#
#cs = axs[3].imshow(visc0.T,extent=extent,cmap="viridis",aspect='auto')
#axs[3].set_ylim(param.depth.end,param.depth.start)
#axs[3].set_yticks(zticks)
#axs[3].hlines(-660,x[0]/1.0E3,x[-1]/1.0E3,linestyles="dashed")
#
#cs = axs[4].imshow(visc1.T,extent=extent,cmap="viridis",aspect='auto')
#axs[4].set_ylim(param.depth.end,param.depth.start)
#axs[4].set_yticks(zticks)
#axs[4].hlines(-660,x[0]/1.0E3,x[-1]/1.0E3,linestyles="dashed")
#
##aux = np.around(np.max(visc1),-2)
##ticks = np.linspace(visc_min,visc_max,n_visc)
##cbar = fig.colorbar(cs,ticks=ticks,pad=0.015)
##cbar.ax.set_ylabel('$log_{10}\eta$ (Pa s)')
#
#plt.tight_layout()
#plt.savefig('figures/00.png')
#plt.show()





