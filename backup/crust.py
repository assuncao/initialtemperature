import numpy as np
import matplotlib.pyplot as plt
import scipy.interpolate as si
import geopandas as gp
import scipy.ndimage

import os, sys, re

from shapely.geometry import Point
from shapely.geometry.polygon import Polygon

################################################################################
# Start up permissions
################################################################################

clines = 'False'
makeMap = 'False'

################################################################################
# Setting up classes and methods
################################################################################

cwd = os.getcwd()
libsLocation = os.path.join(cwd,'lib')
sys.path.append(libsLocation)

import reference, classes, methods, data

################################################################################
# Read data from ./src/params.txt
################################################################################

parPath = './src/params.txt'
par 	= open(parPath)
lon 	= classes.Parameters(re.split("[ ]+", par.readline().strip()),"lon")
lat 	= classes.Parameters(re.split("[ ]+", par.readline().strip()),"lat")
depth 	= classes.Parameters(re.split("[ ]+", par.readline().strip()),"depth")
par.close()

fieldTemperature 	= np.zeros(shape=(lon.length,lat.length,depth.length))
fieldLon			= np.linspace(lon.startKm,lon.endKm,lon.length)
fieldLat			= np.linspace(lat.startKm,lat.endKm,lat.length)
fieldDepth			= np.linspace(depth.start,depth.end,depth.length)
auxFieldDepth		= np.zeros(np.size(fieldDepth))

################################################################################
# Read LITHO1.0 model -> Crust
################################################################################

modelFolderPath = '/Users/kugelblitz/Documents/USP/models/LITHO1.0/litho1.0/LITHO1.0/litho_model/'

icos = np.loadtxt(modelFolderPath+'Icosahedron_Level7_LatLon_mod.txt')
icos = np.array(icos)

data = np.zeros(shape=np.shape(icos))

for i in xrange(1,np.shape(icos)[0]+1):
	nodeFileName = 'node' + str(i) + '.model'
	file = open(modelFolderPath+nodeFileName,'r')
	
	for line in file:
		line = line.split()
		if (line[-1]=='LID-TOP'):
			# lat
			data[i-1,0] = icos[i-1,0]
			# lon
			data[i-1,1] = icos[i-1,-1]
			data[i-1,2] = float(line[0])
	file.close()

################################################################################
# Dimensional data
################################################################################

if (makeMap=='True'):
	minlon, maxlon = -80., -35.
	minlat, maxlat = -50., 10.
	m = int(abs((minlon-maxlon)/0.5)) + 1
	n = int(abs((minlat-maxlat)/0.5)) + 1

else:
	m,n = lon.length, lat.length

	if (lon.startDegree>180):
		minlon = lon.startDegree - 360.
	else:
		minlon = lon.startDegree

	if (lon.endDegree>180):
		maxlon = lon.endDegree - 360.
	else:
		maxlon = lon.endDegree

	minlat, maxlat = lat.startDegree, lat.endDegree

################################################################################
# LAB Map
################################################################################

fig, ax = plt.subplots(figsize=(8,8))
ax.set_aspect('equal')

X, Y = np.mgrid[minlon:maxlon:m*1j, minlat:maxlat:n*1j]
points = np.column_stack((data[:,1],data[:,0]))
values = data[:,2] / 1.0E3
Z = si.griddata(points, values, (X, Y), method='nearest')

################################################################################
# Load map, LAB plot and map Plot
################################################################################

map = gp.read_file('/Users/kugelblitz/Desktop/initialTemperature/src/lim_pais_a.shp')

#minLitDepth = 100.
#
#for i in xrange(np.shape(Z)[0]):
#	for j in xrange(np.shape(Z)[1]):
#		if (Z[i,j]<=minLitDepth):
#			point = Point(X[i,j],Y[i,j])
#			if (map.contains(point)).any():
#				Z[i,j] = minLitDepth
#			else:
#				continue
#		else:
#			continue

extent = [minlon,maxlon,minlat,maxlat]
contour = plt.imshow(np.transpose(Z),extent=extent,cmap='jet',origin='bottom',interpolation='none')

map.plot(ax=ax,color='none',edgecolor='black')

################################################################################
# Contour Lines
################################################################################

if (clines=='True'):
	xi, yi = np.mgrid[-180:180:361j, -90:90:181j]
	zi = si.griddata(points, values, (xi, yi), method='nearest')

	for i in xrange(np.shape(zi)[0]):
		for j in xrange(np.shape(zi)[1]):
			if (zi[i,j]<=minLitDepth):
				point = Point(xi[i,j],yi[i,j])
				if (map.contains(point)).any():
					zi[i,j] = minLitDepth
				else:
					continue
			else:
				continue

	levels = np.array([25.,50.,75.,100.])

	dg = 3
	depthc = np.copy(np.transpose(zi))
	depthc = scipy.ndimage.zoom(depthc, dg)

	xx, yy = np.mgrid[-180:180:dg*361*1j, -90:90:dg*181j]

	cplot = plt.contour(np.transpose(xx),np.transpose(yy),depthc,extent=extent,levels=levels,colors=('w',),linewidths=0.75)
	ax.clabel(cplot, fmt='%2.1f', colors='w', fontsize=7)

################################################################################
# Color Bar
################################################################################

ticks = np.linspace(np.min(values),np.max(values),11)
ticks[1:-1] = np.round(ticks[1:-1],-1)

cbar = fig.colorbar(contour,orientation='vertical',ticks=ticks,aspect=30)
cbar.ax.set_ylabel("Moho Depth (km)")

if (clines=='True'):
	cbar.add_lines(cplot)

ax.set_title('South America Moho depth')

ax.set_xlabel(r'Longitude ($^\circ$)')
ax.set_ylabel(r'Latitude ($^\circ$)')

ax.set_ylim(minlat,maxlat)
ax.set_xlim(minlon,maxlon)

ax.set_ylim(minlat,maxlat)
ax.set_xlim(minlon,maxlon)

plt.savefig('./figures/crust.pdf')
fig.show()

################################################################################
# Profile Plot
################################################################################

fig, ax = plt.subplots(figsize=(12,4))
ax.set_title(r'Moho Depth Profile at 18$^\circ$S')

cond1 = (Y==-18.)&(X>=-80.)&(X<=-35.)
ax.plot(X[cond1],Z[cond1],label='LITHOS1.0 Regular Grid (nearest)')

if (clines=='True'):
	cond2 = (yi==-18.)&(xi>=-80.)&(xi<=-35.)
	ax.plot(xi[cond2],zi[cond2],label='Interpolated data (spline)')
	ax.set_ylim(np.max([np.max(zi[cond2]),np.max(Z[cond1])]),0)

else:
	ax.set_ylim(np.max(Z[cond1]),0)

ax.set_xlabel('Longitude (in degrees)')
ax.set_ylabel('Moho Depth (km)')

plt.legend()

plt.savefig('./figures/crust_profile.pdf')
plt.show()

np.savetxt('./out/crust.txt',np.transpose(Z))



