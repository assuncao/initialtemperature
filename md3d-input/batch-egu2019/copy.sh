DIR0='/Users/kugelblitz/Desktop/initialTemperature'
DIROUT=$DIR0/out
DIRFIG=$DIR0/figures

cp $DIROUT/interfaces.txt $PWD/.
cp $DIROUT/Temper_0_3D.txt $PWD/.
cp $DIROUT/veloc_0_3D.txt $PWD/.

cp $DIRFIG/interfaces.pdf $PWD/plots/.
cp $DIRFIG/temperature-profile.pdf $PWD/plots/.