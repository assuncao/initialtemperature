import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import os, sys, re
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

################################################################################
# Setting up classes and methods
################################################################################

cwd = '/Users/kugelblitz/Desktop/initialTemperature'#os.getcwd()
libsLocation = os.path.join(cwd,'lib')
sys.path.append(libsLocation)

fig_dir = "figures/"

import classes
import progressBar as pbar

################################################################################
# Read data from ./src/params.txt
################################################################################

print("*** Reading initial parameters *** ")
parPath = './params.txt'
param 	= classes.Dimension(parPath)

param.lat.size = 1

TZ = -660

################################################################################
# Configure NPROC
################################################################################

skiprows, num_processes, aux_print_step = 3, 14, 0
print(sys.argv,np.size(sys.argv))
for i in xrange(1,np.size(sys.argv),2):
	if (sys.argv[i]=='-k'):
		skiprows = int(sys.argv[i+1])
	elif (sys.argv[i]=='-n'):
		num_processes = int(sys.argv[i+1])
	elif (sys.argv[i]=='-step'):
		aux_print_step = int(sys.argv[i+1])

################################################################################
# Print parameters box (check up mistakes)
################################################################################

box = pbar.Box()

box.top()
box.mid("Chosen dimension:\t "+param.dim)
box.mid("Grid dimensions:\t ("+str(param.lon.size)+"x"+str(param.lat.size)+"x"+str(param.depth.size)+")")
box.mid("Longitude interval:\t ("+str(param.lon.start.degree)+","+str(param.lon.end.degree)+","+str(param.lon.delta.degree)+")")
box.mid("Longitude distance:\t ("+str(param.lon.start.km)+","+str(param.lon.end.km)+","+str(param.lon.delta.km)+")")
box.mid("Latitude interval:\t ("+str(param.lat.start.degree)+","+str(param.lat.end.degree)+","+str(param.lat.delta.degree)+")")
box.mid("Latitude distance:\t ("+str(param.lat.start.km)+","+str(param.lat.end.km)+","+str(param.lat.delta.km)+")")
box.mid("Depth interval:\t ("+str(param.depth.start)+","+str(param.depth.end)+","+str(param.depth.delta)+")")
box.top()
box.mid("Number of cores:\t "+str(num_processes))
box.mid("Skip headers:\t\t "+str(skiprows))
box.bot()

################################################################################
# Read Slab
################################################################################

slabGridPath = '/Users/kugelblitz/Desktop/initialTemperature/out/Slab2-clipped/Slab2-top.-1800'
data = np.loadtxt(slabGridPath,unpack='True')

################################################################################
# Read info from the param_1.5.3.txt
################################################################################

with open("param_1.5.3_2D.txt","r") as f:
	# Line 1
	line = f.readline()
	# Line 2
	line = f.readline()
	# Line 3
	line = f.readline()
	# Line 4
	line = f.readline()
	# Line 5
	line = f.readline()
	line = line.split()
	tag, step_max = line[0], int(line[1])
	# Line 6
	line = f.readline()
	# Line 7
	line = f.readline()
	# Line 8
	line = f.readline()
	# Line 9
	line = f.readline()
	# Line 10
	line = f.readline()
	line = line.split()
	tag, print_step = line[0], int(line[1])
	# Line 11
	line = f.readline()
	# Line 12
	line = f.readline()
	line = line.split()
	tag, visc_med = line[0], int(line[1].split("E")[1])
	# Line 13
	line = f.readline()
	line = line.split()
	tag, visc_max = line[0], int(line[1].split("E")[1])
	# Line 14
	line = f.readline()
	line = line.split()
	tag, visc_min = line[0], int(line[1].split("E")[1])

if (aux_print_step!=0):
	print_step = aux_print_step

################################################################################
# Mesh and information for matplotlib
################################################################################

x_start, x_end = 1500, 3000.
x_start_degree, x_end_degree = param.lon.start.degree + x_start / 111.,param.lon.start.degree +  x_end / 111.

extent			= [0,param.lon.end.km,0,param.depth.end]
ref = 10
#width, height 	= ww * ref, ref
markersize 		= 0.4
ww = (x_end-x_start) / (-param.depth.end) #+ 0.15

rx = 0.2 # 20% of the height
wx = 0.6 # 60% of the height

width, height 	= ww * wx * ref + 2 * rx * ref, ref
aux = width / ref

tr = 0.025 / aux
r0, r1, r2, r3 = rx/aux - tr, rx , ww * wx / aux, wx
rect = [r0, r1, r2, r3]


xi 		= np.linspace(0,param.lon.end.km,param.lon.size);
zi 		= np.linspace(param.depth.end,0,param.depth.size);
xx,zz 	= np.meshgrid(xi,zi);

# Basin location
basinWest, basinEast = -58, -55
drawdepth	= -6 # Depth to draw line and identify basin location

dticks = 200.
ddtick = dticks/2
arr_aux = np.arange(x_start,x_end+dticks/2.,dticks)
if (x_end-arr_aux[-1]<=ddtick):
	arr_aux = np.delete(arr_aux,-1)
xticks = np.append(arr_aux,x_end)

dticks_degree = 2.0
ddtick_degree = 1.1
arr_aux_degree = np.arange(np.ceil(x_start_degree),np.floor(x_end_degree)+dticks_degree/2.,dticks_degree)
if (x_end_degree-arr_aux_degree[-1]<=ddtick_degree):
	arr_aux_degree = np.delete(arr_aux_degree,-1)
if (x_start_degree-arr_aux_degree[0]>=-ddtick_degree):
	arr_aux_degree = np.delete(arr_aux_degree,0)
auxaux = np.append(x_start_degree,arr_aux_degree)
xticks_degree = np.append(auxaux,x_end_degree)

yticks = np.linspace(param.depth.start,param.depth.end,6)
yticks = np.append(yticks,TZ)

linewidth = 4
cwhite = (1.,1.,1.)

f_size = 20
plt.rcParams.update({'font.size': f_size})


dT = 200
v_normal = 10.0E-3

step_min = 0

plt.close()

################################################################################
# Plots
################################################################################

temperPlot 		= True
rhoPlot 		= True
strainPlot		= True
hPlot 			= False
geoqPlot 		= True
factorPlot 		= False
velocityPlot 	= True
viscPlot		= False
tempVelo		= True

n_dim_veloc = 3
if (param.lat.size==1):
	n_dim_veloc = 2
	
################################################################################
# Methods
################################################################################

def axx(cs):
	ax1.set_xlabel("Distance (km)")
	ax1.set_xticks(xticks)
	ax1.set_xlim(x_start,x_end)
	ax1.set_ylabel("Depth (km)")
	ax1.set_yticks(yticks)
	ax1.set_ylim(param.depth.end,0)
	ax1.tick_params(length=10,width=2)
#	ax1.set_aspect('equal')

	ax2 = ax1.twiny()
	ax2.set_xlim(param.lon.start.degree+x_start/111.,param.lon.start.degree+x_end/111.)
	ax2.set_xticks(np.round(xticks_degree,2))
	ax2.set_xlabel("Longitude ($\!^\circ\!$)")
	ax2.tick_params(length=10,width=2)

	# Basin location
	ax2.vlines(basinWest,param.depth.start,-150,linestyles="dotted",color=cwhite,linewidth=linewidth)
	ax2.vlines(basinEast,param.depth.start,-150,linestyles="dotted",color=cwhite,linewidth=linewidth)
	ax1.hlines(TZ,x_start,x_end,linestyles="dashed",color=cwhite,linewidth=linewidth)
	
	ax1.autoscale(False)
	ax2.autoscale(False)

def axxin(fac):
	posx = 1. + 0.05 / fac
	axins = inset_axes(	ax1,
						width=0.25,  # width = 5% of parent_bbox width
						height="100%",  # height : 50%
						loc='lower left',
						bbox_to_anchor=(posx, 0., 1, 1),
						bbox_transform=ax1.transAxes,
						borderpad=0,
	)
	return axins
	

dt = 50.0E6
t = 0.
axidx = 1
	
################################################################################
# Main Loop
################################################################################

# Start figure
plt.figure(figsize=(width,height))
#ax1 = plt.axes(rect)
#plt.title(value+" Myrs\n\n",fontsize=f_size)

for cont in range(step_min,step_max+1):

	print("loop step "+str(cont)+" @ file step " + str(cont*print_step))
	yrs = np.loadtxt("Tempo_"+str(cont*print_step)+".txt",comments="P",delimiter=":")
	if (num_processes!=1):
		value = format(np.round(yrs[0,1]/1.0E6,2), '.2f')
	else:
		value = format(np.round(yrs[1]/1.0E6,2), '.2f')
	value_float = float(value)
		
	if (value_float>=t):
		#########################################################################
		# Step "Plot"
		#########################################################################
		
		x, y, z, cc, = [], [], [], []

		for rank in range(num_processes):
			x1,y1,z1,c0,c1,c2,c3,c4 = np.loadtxt("step_"+str(cont*print_step)+"-rank_new"+str(rank)+".txt",unpack=True)
			cor1 = (0,0,0)
			cor2 = (0,0,0)
			cor3 = (0,0,0)
			cc 	= np.append(cc,c1)
			x 	= np.append(x,x1)
			y 	= np.append(y,y1)
			z 	= np.append(z,z1)
		
		difere1 = 2.00
		difere2 = 0.95
		
		cond1 = (cc>=difere2) & (cc<=difere1)
		cond2 = (cc<difere2)
		cond3 = (cc>difere1)
	
		########################################################################
		# Temperature Plot
		########################################################################
	
	
		A = np.loadtxt("Temper_"+str(cont*print_step)+".txt",unpack=True,comments="P",skiprows=skiprows)
		TT 						= A * 1.0
		TT[np.abs(TT)<1.0E-200] = 0
		TT 						= np.reshape(TT,(param.lon.size,param.lat.size,param.depth.size),order='F')
		TTT 					= TT[:,0,:]
		
#		plt.figure(figsize=(width,height))
#		ax1 = plt.axes(rect)
		
#		plt.title(value+" Myrs\n\n",fontsize=f_size)
		
#		cs = plt.imshow(np.transpose(TTT),cmap="jet",extent=extent,aspect='auto')
#		plt.plot(x/1.0E3,y/1.0E3,"c.",color=cor2,markersize=markersize)
#		plt.plot(data[1,:],data[2,:],color=(1,1,1),linewidth=linewidth)
		ax1 = plt.axes([r0, axidx*r1/5., r2, r3/5.])
		axidx += 1
		cs = plt.imshow(np.transpose(TTT),cmap="jet",extent=extent,aspect='auto')
		plt.plot(x/1.0E3,y/1.0E3,"c.",color=cor2,markersize=markersize)
		plt.plot(data[1,:],data[2,:],color=(1,1,1),linewidth=linewidth)
			
		axx(cs)

		# Color bar
		axins = axxin(ww)
		dd = dT
		auxMax = np.around(np.max(TTT),-2)
		ticks = np.arange(0,auxMax+1,dd)
		if (np.max(TTT)-ticks[-1])<dd/2:
			ticks = np.delete(ticks,-1)
			ticks = np.append(ticks,np.max(TTT))
		else:
			ticks = np.append(ticks,np.max(TTT))

		cbar = plt.colorbar(cs,cax=axins,ticks=ticks)
		cbar.ax.set_ylabel('Temperature ($\!^\circ\!$C)')
		t += dt

plt.show()
plt.savefig(fig_dir+"00-TemperPlot_"+str(cont).zfill(5)+".png")
plt.close()
		
