#!/bin/bash

gmt set FONT_TITLE 20p

awk '{print $1, $2, $3}' $PWD/crust.xyz | xyz2grd -R -I0.5 -Gcrust.grd 

gmt makecpt -T0/90/1 -Chaxby > crust.cpt
grdimage crust.grd -R-90/-30/-40/10 -JM15c -Ccrust.cpt -K > map.ps
pscoast -R -JM15c -W0.01p,black -Df -P -Ba10f10g10 -B+t"Crust Thickness" -N1 -L-35/-35/-35/500 -K -O >> map.ps
psxy -R -J $PWD/pantanal-basin.txt -W1p,red  -P -K -O >> map.ps
gmt psscale -Ccrust.cpt -R -J -DjCT+o10c/0.15c+w13c/0.5c+v -Bx10f5+l"Thickness (km)" -N -K -O >> map.ps





#-I2/0.25p,cornflowerblue -I0/0.25p,cornflowerblue -I1/0.25p,cornflowerblue


# psxy -R -J $PWD/Icosahedron_Level7_LatLon_mod.txt -W1p,red  -P -O >> map.ps


