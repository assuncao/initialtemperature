#!/bin/bash

GRD_PATH=/Users/kugelblitz/Documents/USP/gmt_files/ETOPO1_BEd_g_gmt4.grd
CPT_PATH=/Users/kugelblitz/Documents/USP/gmt_files/ETOPO1.cpt

grdimage $GRD_PATH -R-60.5/-52.5/-23/-13.5 -JM15c -P -C$CPT_PATH -K > map.ps
pscoast -R -JM15c -W0.1p,black -Df -P -Ba1f5g5 -B+t"Pantanal Basin" -Ir/0.25p,cornflowerblue -N1 -L-35/-35/-35/500 -K -O >> map.ps

# psxy -R -J $PWD/paraguay-depression.xy -GP+f-+bgrey-P -O -K >> map.ps
psxy -R -J $PWD/paraguay-depression.xy -GP74+f-+bgrey -P -O -K >> map.ps
# psxy -R -J $PWD/paraguay-depression.xy -W2p,blue -GP74+f-+bblue -P -O >> map.ps
psxy -R -J $PWD/pantanal.txt -W2p,red -P -O >> map.ps
