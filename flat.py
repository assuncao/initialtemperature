import os, sys, re

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

################################################################################
# Setting up classes and methods
################################################################################

cwd = os.getcwd()
libsLocation = os.path.join(cwd,'lib')
sys.path.append(libsLocation)

import classes, methods
import progressBar as pbar

################################################################################
# Read data from ./src/params.txt
################################################################################

print("*** Reading initial parameters *** ")
parPath = cwd + '/src/params.txt'
param 	= classes.Dimension(parPath)

################################################################################
# Read dimensional reference values
################################################################################

refFilePath 	= cwd + '/src/reference.txt'
dimRef 			= classes.Reference(refFilePath,param.depth.end)

################################################################################
# Print parameters box (check up mistakes)
################################################################################

box = pbar.Box()

box.top()
box.mid("Chosen dimension:\t "+param.dim)
box.mid("Grid dimensions:\t ("+str(param.lon.size)+"x"+str(param.lat.size)+"x"+str(param.depth.size)+")")
box.mid("Longitude interval:\t ("+str(param.lon.start.degree)+","+str(param.lon.end.degree)+","+str(param.lon.delta.degree)+")")
box.mid("Longitude distance:\t ("+str(param.lon.start.km)+","+str(param.lon.end.km)+","+str(param.lon.delta.km)+")")
box.mid("Latitude interval:\t ("+str(param.lat.start.degree)+","+str(param.lat.end.degree)+","+str(param.lat.delta.degree)+")")
box.mid("Latitude distance:\t ("+str(param.lat.start.km)+","+str(param.lat.end.km)+","+str(param.lat.delta.km)+")")
box.mid("Depth interval:\t ("+str(param.depth.start)+","+str(param.depth.end)+","+str(param.depth.delta)+")")
box.bot()

################################################################################
# Latitude Loop
################################################################################

sPathTop	= './out/Slab2-clipped/Slab2-top.'
sPathBot	= './out/Slab2-clipped/Slab2-bot.'

fPathTop 	= './out/Slab2-resampled/resampled-top.'	# Data written to /out directory
fPathBot 	= './out/Slab2-resampled/resampled-bot.'	# Data written to /out directory

for j in xrange(param.lat.size):
	
	aux1, aux2		= np.round(param.lat.start.degree*100,0), np.round(j*param.lat.delta.degree*100,0)
	auxName 		= str(int(aux1+aux2)).zfill(5)
	
	############################################################################
	# Slab2 - Clipped
	############################################################################
	
	sDataTop		= pd.read_csv(sPathTop+auxName,header=None,sep="\t",dtype=float)
	sDataBot		= pd.read_csv(sPathBot+auxName,header=None,sep="\t",dtype=float)
	sAuxTop			= pd.DataFrame()
	sAuxBot			= pd.DataFrame()
	
	sStartFlatXTop	= sDataTop[1].iloc[-1]
	sEndFlatXTop	= sStartFlatXTop + dimRef.flatLength
	sStartFlatXBot	= sDataBot[1].iloc[-1]
	sEndFlatXBot	= sStartFlatXBot + dimRef.flatLength
	
#	plt.plot(sDataTop[1],sDataTop[2],label="Slab2 (top) - clipped")
#	plt.plot(sDataBot[1],sDataBot[2],label="Slab2 (top) - clipped")

	posXTop 		= sDataTop[1].iloc[-1]
	posZTop			= sDataTop[2].iloc[-1]
	posXBot 		= sDataBot[1].iloc[-1]
	posZBot			= sDataBot[2].iloc[-1]

	longitude		= sDataTop[0].iloc[-1]

	resolution		= 0.05
	degree			= 111.0
	
	dAngle			= 0.0165
	angle			= (sDataTop[2].iloc[-2]-sDataTop[2].iloc[-1]) / (resolution*degree)
	angle2			= angle
	while posXTop < sEndFlatXTop:
		posXTop 	+= resolution * degree
		angle		-= dAngle
		angle		= np.max([angle,0.0])
		posZTop 	-= np.tan(angle) * resolution * degree
		longitude	+= resolution
		aux 		= pd.DataFrame([[longitude,posXTop,posZTop]])
		sAuxTop		= sAuxTop.append(aux,ignore_index=True)

	sDataTop = sDataTop.append(sAuxTop,ignore_index=True)
	plt.plot(sDataTop[1],sDataTop[2],label="Slab2 (top) - clipped & flattened")

	longitude		= sDataBot[0].iloc[-1]
	
	dAngle			= 0.0138
	angle			= angle2
	while posXBot < sEndFlatXTop:
		posXBot 	+= resolution * degree
		angle		-= dAngle
		angle		= np.max([angle,0.0])
		posZBot 	-= np.tan(angle) * resolution * degree
		longitude	+= resolution
		aux 		= pd.DataFrame([[longitude,posXBot,posZBot]])
		sAuxBot		= sAuxBot.append(aux,ignore_index=True)
	
	sDataBot = sDataBot.append(sAuxBot,ignore_index=True)
	plt.plot(sDataBot[1],sDataBot[2],label="Slab2 (bot) - clipped & flattened")
	
	############################################################################
	# Slab2 - Resampled
	############################################################################
	
	fDataTop 		= pd.read_csv(fPathTop+auxName,header=None,sep="\t",dtype=float)
	fDataBot		= pd.read_csv(fPathBot+auxName,header=None,sep="\t",dtype=float)
	fAuxTop			= pd.DataFrame()#pd.DataFrame([[-10000,0]])
	fAuxBot			= pd.DataFrame()
	
	fStartFlatXTop	= fDataTop[0].iloc[-1]
	fEndFlatXTop	= fStartFlatXTop + dimRef.flatLength
	fStartFlatXBot	= fDataBot[0].iloc[-1]
	fEndFlatXBot	= fStartFlatXTop + dimRef.flatLength
	
#	plt.plot(fDataTop[0],fDataTop[1],label="Slab2 (top) - resampled")
#	plt.plot(fDataBot[0],fDataBot[1],label="Slab2 (top) - resampled")

	posXTop 		= fDataTop[0].iloc[-1]
	posZTop			= fDataTop[1].iloc[-1]
	posXBot 		= fDataBot[0].iloc[-1]
	posZBot			= fDataBot[1].iloc[-1]

	dAngle			= 0.0
	angle			= (fDataTop[1].iloc[-2]-fDataTop[1].iloc[-1]) / param.lon.delta.km
	while posXTop < fEndFlatXTop:
		posXTop 	+= param.lon.delta.km
		angle		-= dAngle
		angle		= np.max([angle,0.0])
		posZTop 	-= np.tan(angle) * param.lon.delta.km
		aux 		= pd.DataFrame([[posXTop,posZTop]])
		fAuxTop		= fAuxTop.append(aux,ignore_index=True)
	
	dAngle			= 0.04
	angle			= (fDataTop[1].iloc[-2]-fDataTop[1].iloc[-1]) / param.lon.delta.km
	while posXBot < fEndFlatXBot:
		posXBot 	+= param.lon.delta.km
		angle		-= dAngle
		angle		= np.max([angle,0.0])
		posZBot 	-= np.tan(angle) * param.lon.delta.km
		aux 		= pd.DataFrame([[posXBot,posZBot]])
		fAuxBot		= fAuxBot.append(aux,ignore_index=True)

	fDataTop = fDataTop.append(fAuxTop,ignore_index=True)
	plt.plot(fDataTop[0],fDataTop[1],label="Slab2 (top) - resampled & flattened")

	fDataBot = fDataBot.append(fAuxBot,ignore_index=True)
	plt.plot(fDataBot[0],fDataBot[1],label="Slab2 (top) - resampled & flattened")

	plt.hlines(dimRef.maxFlatDepth,fDataTop[0].iloc[0],fDataTop[0].iloc[-1],linestyles="dashed")
	plt.hlines(dimRef.maxFlatDepth-(dimRef.referenceSlabThickness*dimRef.referenceSlabThkScale),fDataTop[0].iloc[0],fDataTop[0].iloc[-1],linestyles="dashed")
	plt.hlines(-660.0,fDataTop[0].iloc[0],fDataTop[0].iloc[-1],linestyles="dashed")
	plt.legend()
	plt.show()

	ans = raw_input("Save flattened Slab?\n")
	if (ans=="y"):
		sDataTop.to_csv(sPathTop+auxName,index=False,sep="\t",header=None)
		sDataBot.to_csv(sPathBot+auxName,index=False,sep="\t",header=None)
		fDataTop.to_csv(fPathTop+auxName,index=False,sep="\t",header=None)
		fDataBot.to_csv(fPathBot+auxName,index=False,sep="\t",header=None)




