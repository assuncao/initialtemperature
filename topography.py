import numpy as np 
import matplotlib.pyplot as plt 
import os, sys, re

################################################################################
# Setting up classes and methods
################################################################################

cwd = '/Users/kugelblitz/Desktop/initialTemperature'#os.getcwd()
libsLocation = os.path.join(cwd,'lib')
sys.path.append(libsLocation)

import classes

################################################################################
# Read data from ./src/params.txt and setting up domain
################################################################################

print("*** Reading initial parameters *** ")
parPath = './params.txt'
param 	= classes.Dimension(parPath)

param.lat.size = 1

xmin, xmax, nx= param.lon.start.km, param.lon.end.km, param.lon.size
zmin, zmax, nz= param.depth.start, param.depth.end, param.depth.size
x = np.linspace(xmin,xmax,nx) # [km]
z = np.linspace(zmax,zmin,nz) # [km]
p = np.zeros(nx)

# Basin location
basinWest, basinEast = -58, -55
startBasin = (basinEast-param.lon.start.degree) * 111.
endBasin = (basinWest-param.lon.start.degree) * 111.


################################################################################
# Methods
################################################################################

def topography(data,g=9.8,dz=10.0):
	aux = np.sum(data,axis=1)
	w = aux * g * dz
	return w

################################################################################
# Load Rho and Temperature data
################################################################################

step0, step1 = 0, 100

if (np.size(sys.argv)>1):
	step1 = int(sys.argv[1])

skiprows = 3
shp = (param.lon.size,param.depth.size)

# Density

rho_file0 = 'Rho_' + str(step0) + '.txt'
rho_file1 = 'Rho_' + str(step1) + '.txt'

rho0 = np.loadtxt(rho_file0,unpack=True,skiprows=skiprows,comments='P')
rho0 = np.reshape(rho0,shp,order='F')

rho1 = np.loadtxt(rho_file1,skiprows=skiprows,comments='P')
rho1 = np.reshape(rho1,shp,order='F')

# Temperature 

temp_file0 = 'Temper_' + str(step0) +'.txt'
temp_file1 = 'Temper_' + str(step1) +'.txt'

temp0 = np.loadtxt(temp_file0,skiprows=skiprows,comments='P')
temp0 = np.reshape(temp0,shp,order='F')

temp1 = np.loadtxt(temp_file1,skiprows=skiprows,comments='P')
temp1 = np.reshape(temp1,shp,order='F')

# New density

alpha = 3.28E-5
r0 = rho0 * (1.0-alpha*temp0)
r1 = rho1 * (1.0-alpha*temp1)

################################################################################
# Calculate topography
################################################################################

dz = (z[1]-z[0])
w0 = topography(r0,dz=dz)
w1 = topography(r1,dz=dz)

topo = (w0-w1) / (3300.*9.8)

np.savetxt("topo.txt",topo)

################################################################################
# Plots
################################################################################

extent		= [0,param.lon.end.km,0,param.depth.end]
zticks		= np.linspace(zmax,zmin,6)
zticks		= np.append(zticks,-660.)

fig, axs = plt.subplots(nrows=3,ncols=1,figsize=(14,9))

axs[0].hlines(0,x[0],x[-1],color=(0.5,0.5,0.5))
cs = axs[0].plot(x,topo*1000)
axs[0].vlines(startBasin,0,-1000,linestyles="dashed")
axs[0].vlines(endBasin,0,-1000,linestyles="dashed")
axs[0].hlines(-500,x[0],x[-1],linestyles="dashed")

axs[0].set_xlim(x[0],x[-1])
axs[0].set_xlabel("Distance (km)")
axs[0].set_ylabel("Topography (m)")

cs = axs[1].imshow(temp0.T,extent=extent,cmap="jet")
axs[1].set_ylim(param.depth.end,param.depth.start)
axs[1].set_yticks(zticks)
axs[1].hlines(-660,x[0],x[-1],linestyles="dashed")
cs = axs[2].imshow(temp1.T,extent=extent,cmap="jet")
axs[2].set_yticks(zticks)
axs[2].set_ylim(param.depth.end,param.depth.start)
axs[2].hlines(-660,x[0],x[-1],linestyles="dashed")

plt.tight_layout()
plt.savefig('./figures/topography.png')
plt.show()




