import os, sys, re
import numpy as np

from time import time, clock
start_time = time()

################################################################################
# Setting up classes and methods
################################################################################

cwd = os.getcwd()
libsLocation = os.path.join(cwd,'lib')
sys.path.append(libsLocation)

import classes, methods
import progressBar as pbar

################################################################################
# Control options
################################################################################

bar 					= 'True'
mayaviPermission		= 'False'

if (mayaviPermission=='True'):
	from mayavi import mlab

################################################################################
# Read data from ./src/params.txt
################################################################################

print("*** Reading initial parameters *** ")
parPath = cwd + '/src/params.txt'
param 	= classes.Dimension(parPath)

#param.lat.size = 1

fieldTemperature 	= np.zeros(shape=(param.lon.size,param.lat.size,param.depth.size))
fieldLon			= np.linspace(param.lon.start.km,param.lon.end.km,param.lon.size)
fieldLat			= np.linspace(param.lat.start.km,param.lat.end.km,param.lat.size)
fieldDepth			= np.linspace(param.depth.start,param.depth.end,param.depth.size)
auxFieldDepth		= np.zeros(np.size(fieldDepth))

################################################################################
# Read dimensional reference values
################################################################################

refFilePath 	= cwd + '/src/reference.txt'
dimRef 			= classes.Reference(refFilePath,param.depth.end)

################################################################################
# Progress bar
################################################################################

print("*** Calculating temperature field ***")
bar = pbar.Bar(param.lat.size)
bar.start()

################################################################################
# Load surfaces
################################################################################

#topModelPath 	= './out/Slab2-resampled/resampled-top.'
#botModelPath 	= './out/Slab2-resampled/resampled-bot.'
if (param.lon.delta.degree>0.05):
	topModelPath 	= './out/Slab2-clipped/Slab2-top.'
	botModelPath 	= './out/Slab2-clipped/Slab2-bot.'
else:
	topModelPath 	= './out/Slab2-resampled/resampled-top.'
	botModelPath 	= './out/Slab2-resampled/resampled-bot.'

lithoPath = cwd + '/out/litho.txt'
litho = np.loadtxt(lithoPath,unpack=True)
litho = litho * (-1)

#refBotModelPath = './out/Slab2-resampled/resampled-bot.'

################################################################################
# Main latitude Loop
################################################################################

for j in xrange(param.lat.size-1):
	
	aux1,aux2 		= np.round(param.lat.start.degree*100,0),np.round(j*param.lat.delta.degree*100,0)
	if (param.dim=='2d'):
		aux1,aux2 	= np.round(param.lat.start.degree*100,0),0
	
	topModelFile 	= topModelPath + str(int(aux1+aux2)).zfill(5)
	top 			= classes.Surface(np.loadtxt(topModelFile),1,2)
	botModelFile 	= botModelPath + str(int(aux1+aux2)).zfill(5)
	bot 			= classes.Surface(np.loadtxt(botModelFile),1,2)

	############################################################################
	# Background temperature field design
	############################################################################
	
	aW,bW,aE,bE		= methods.slabLimitCalc(top,bot)

	for i in xrange(param.lon.size):
		for k in xrange(param.depth.size):
			westSlabLimitMax = np.max([top.minA,bot.minA])
			eastSlabLimitMin = np.min([top.maxA,bot.maxA])
			
			# Take care of Oceanic Lithosphere
			if (fieldLon[i]<westSlabLimitMax):
				if (fieldDepth[k]>litho[i,j]):
					x,x0,x1,y0,y1 = fieldDepth[k],0.0,litho[i,j],0,dimRef.LABtemperatureC
					fieldTemperature[i,j,np.size(fieldDepth)-k-1] = methods.tempLinear(x,x0,x1,y0,y1)
				else:
					y,y0 = fieldDepth[k],litho[i,j]
					auxT1 = methods.tempAdiabatic(y,y0,dimRef.alpha,dimRef.gravity,dimRef.specificHeat,dimRef.POTtemperatureK)
					x,x0,x1,y0,y1 = fieldDepth[k],0,litho[i,j],0,dimRef.LABtemperatureC
					auxT2 = methods.tempLinear(x,x0,x1,y0,y1)
					fieldTemperature[i,j,np.size(fieldDepth)-k-1] = min(auxT1,auxT2)
		
			# Take care of Continental Lithosphere
			elif (fieldLon[i]>eastSlabLimitMin):
				if (fieldDepth[k]>litho[i,j]):
					x,x0,x1,y0,y1 = fieldDepth[k],0.0,litho[i,j],0,dimRef.LABtemperatureC
					fieldTemperature[i,j,np.size(fieldDepth)-k-1] = methods.tempLinear(x,x0,x1,y0,y1)
				else:
					y,y0 = fieldDepth[k],litho[i,j]
					auxT1 = methods.tempAdiabatic(y,y0,dimRef.alpha,dimRef.gravity,dimRef.specificHeat,dimRef.POTtemperatureK)
					x,x0,x1,y0,y1 = fieldDepth[k],0.0,litho[i,j],0,dimRef.LABtemperatureC
					auxT2 = methods.tempLinear(x,x0,x1,y0,y1)
					fieldTemperature[i,j,np.size(fieldDepth)-k-1] = min(auxT1,auxT2)

			# Take care of Contact (and Continental) Lithosphere
			else:
				length 	= eastSlabLimitMin - westSlabLimitMax
				posX	= fieldLon[i] - westSlabLimitMax#length
				auxDepth1A = methods.findSlabSurfaceDepth(top.abscissa,top.ordinate,fieldLon[i])
				auxDepth1B = litho[i,j]
				auxDepth1 = min(auxDepth1A,auxDepth1B)
				auxDepth2 = litho[i,j]
				auxFieldDepth[k] = max(auxDepth1,auxDepth2)
				if (fieldDepth[k]>auxFieldDepth[k]):
					x,x0,x1,y0,y1 = fieldDepth[k],0.0,auxFieldDepth[k],0,dimRef.LABtemperatureC
					fieldTemperature[i,j,np.size(fieldDepth)-k-1] = methods.tempLinear(x,x0,x1,y0,y1)
				else:
					y,y0 = fieldDepth[k],auxFieldDepth[k]
					auxT1 = methods.tempAdiabatic(y,y0,dimRef.alpha,dimRef.gravity,dimRef.specificHeat,dimRef.POTtemperatureK)
					x,x0,x1,y0,y1 = fieldDepth[k],0.0,auxFieldDepth[k],0,dimRef.LABtemperatureC
					auxT2 = methods.tempLinear(x,x0,x1,y0,y1)
					fieldTemperature[i,j,np.size(fieldDepth)-k-1] = min(auxT1,auxT2)

	############################################################################
	# Slab temperature design
	############################################################################

	surfaceLen		= top.distance# surfaceLength(top)

	for i in xrange(param.lon.size):
		for k in xrange(param.depth.size):
			# Calculate difference between POTtemperatureC and tempAdiabatic(z), set diffAdiabatic to zero if you do not want to use this temperature addition to the subducting slab
			y,y0 = fieldDepth[k],0.0
			temp1 = dimRef.POTtemperatureC
			temp2 = methods.tempAdiabatic(y,y0,dimRef.alpha,dimRef.gravity,dimRef.specificHeat,dimRef.POTtemperatureK)
			diffAdiabatic = (temp2-temp1)
			
			# Check if slab starts going down (normal) or up (inverse)
			westSlabLimitMin = np.min([top.minA,bot.minA])
			westSlabLimitMax = np.max([top.minA,bot.minA])
			if (top.minA>bot.minA):
				flagW = "normal"
			else:
				flagW = "inverse"

			# Check if slab ends up going down (normal) or up (inverse)
			eastSlabLimitMin = np.min([top.maxA,bot.maxA])
			eastSlabLimitMax = np.max([top.maxA,bot.maxA])
			if ([top.maxA>bot.maxA]):
				flagE = "inverse"
			else:
				flagE = "normal"

			# Take care of the beginning of the slab
			if ((fieldLon[i]>=westSlabLimitMin) and (fieldLon[i]<westSlabLimitMax)):
				if (flagW=="normal"):
					topBoundary = methods.boundary(aW,bW,fieldLon[i])
					botBoundary = methods.findLongiInterval(bot,fieldLon[i])
				if (flagW=="inverse"):
					topBoundary = methods.findLongiInterval(top,fieldLon[i])
					botBoundary = methods.boundary(aW,bW,fieldLon[i])
				if ((fieldDepth[k]<=topBoundary) and (fieldDepth[k]>=botBoundary)):
					equivalentP 	= methods.minDistance2Curve(top,fieldLon[i],fieldDepth[k])
					equivalentLon	= surfaceLen[equivalentP[2]]
					fieldTemperature[i,j,np.size(fieldDepth)-k-1] = methods.slabTemperature(equivalentLon,equivalentP[1],dimRef.niter,dimRef.referenceSlabThickness,dimRef.R,dimRef.tm) + diffAdiabatic
				else:
					continue

			# Take care of the end of the slab
			elif ((fieldLon[i]>eastSlabLimitMin) and (fieldLon[i]<=eastSlabLimitMax)):
				if (flagE=="normal"):
					topBoundary = methods.boundary(aE,bE,fieldLon[i])#,flagE)
					botBoundary = methods.findLongiInterval(bot,fieldLon[i])
				if (flagE=="inverse"):
					topBoundary = methods.findLongiInterval(top,fieldLon[i])
					botBoundary = methods.boundary(aE,bE,fieldLon[i])
				if ((fieldDepth[k]<=topBoundary) and (fieldDepth[k]>=botBoundary)):
					equivalentP 	= methods.minDistance2Curve(top,fieldLon[i],fieldDepth[k])
					equivalentLon	= surfaceLen[equivalentP[2]]
					fieldTemperature[i,j,np.size(fieldDepth)-k-1] = methods.slabTemperature(equivalentLon,equivalentP[1],dimRef.niter,dimRef.referenceSlabThickness,dimRef.R,dimRef.tm) + diffAdiabatic
				else:
					continue

			# Take care of the rest of the slab
			elif ((fieldLon[i]>=westSlabLimitMax) and (fieldLon[i]<=eastSlabLimitMin)):
				topBoundary = methods.findLongiInterval(top,fieldLon[i])
				botBoundary = methods.findLongiInterval(bot,fieldLon[i])
				if ((fieldDepth[k]<=topBoundary) and (fieldDepth[k]>=botBoundary)):
					equivalentP 	= methods.minDistance2Curve(top,fieldLon[i],fieldDepth[k])
					equivalentLon	= surfaceLen[equivalentP[2]]
					fieldTemperature[i,j,np.size(fieldDepth)-k-1] = methods.slabTemperature(equivalentLon,equivalentP[1],dimRef.niter,dimRef.referenceSlabThickness,dimRef.R,dimRef.tm) + diffAdiabatic
				else:
					continue

			if (fieldDepth[k]>=0):
#				print(fieldDepth[k],fieldTemperature[i,j,k])
				fieldTemperature[i,j,k] = 0. # set temperature to 0 above the surface

			else:
				continue
	for i in xrange(param.lon.size):
		fieldTemperature[i,j,-1] = 0.0
		fieldTemperature[i,j,0]	= dimRef.maxTempC#dimRef.TZtemperatureC

	bar.update(j)

bar.end()

################################################################################
# Save temperature field
################################################################################

temperatureField 	= './out/Temper_0_3D.txt'
temperatureFile 	= open(temperatureField,"w")

lengthStr			= str(param.lon.size)+"\t"+str(param.lat.size)+"\t"+str(param.depth.size)
extentDistStr		= str(param.lon.start.km)+"\t"+str(param.lon.end.km)+"\t"+str(param.lat.start.km)+"\t"+str(param.lat.end.km)+"\t"+str(param.depth.start)+"\t"+str(param.depth.end)
extentDegrStr		= str(param.lon.start.degree)+"\t"+str(param.lon.end.degree)+"\t"+str(param.lat.start.degree)+"\t"+str(param.lat.end.degree)+"\t"+str(param.depth.start)+"\t"+str(param.depth.end)

temperatureFile.write(lengthStr+"\n")
temperatureFile.write(extentDistStr+"\n")
temperatureFile.write(extentDegrStr+"\n")

temperatureFile.write("T4\n")

for k in xrange(param.depth.size):
	for j in xrange(param.lat.size-1):
		for i in xrange(param.lon.size):
			tempStr = str(fieldTemperature[i,j,k])
			temperatureFile.write(tempStr+"\n")

temperatureFile.close()

end_time = time()
execution_time = np.round(end_time-start_time,4)
print("*** Execution time: "+str(execution_time)+" seconds ***")

################################################################################
# Temperature field plot
################################################################################

if (mayaviPermission == 'True'):
	mlab.figure(size=(800,800))
	extent = [param.lon.start.km,param.lon.end.km,param.lat.start.km,param.lat.end.km,param.depth.start,param.depth.end]
	mlab.contour3d(fieldTemperature,extent=extent,contours=11,transparent=True,colormap="blue-red")
	mlab.view(azimuth=270,elevation=90)

################################################################################
# Region Boundary Plot (Box)
################################################################################

if (mayaviPermission == 'True'):
	mlab.outline(extent=extent)
	mlab.points3d(0,0,0,scale_factor=50)
	for k in xrange(0,np.size(fieldDepth)-1,8):
		mlab.points3d(0,0,fieldDepth[k],scale_factor=25,color=(1,0,0))
		mlab.points3d(fieldLon[param.lon.size-1],0,fieldDepth[k],scale_factor=25,color=(1,0,0))
	for i in xrange(1,np.size(fieldLon)):
		mlab.points3d(fieldLon[i],0,fieldDepth[0],scale_factor=25,color=(0,0,1))
		mlab.points3d(fieldLon[i],0,fieldDepth[param.depth.size-1],scale_factor=25,color=(0,0,1))

if (mayaviPermission == 'True'):
	xmin, xmax, ymin, ymax, zmin, zmax = param.lon.start.km, param.lon.end.km, param.lat.start.km, param.lat.end.km, param.depth.end, param.depth.start
	xx, yy, zz = np.mgrid[xmin:xmax:(param.lon.size)*1j, ymin:ymax:(param.lat.size)*1j, zmin:zmax:(param.depth.size)*1j]
#	mlab.points3d(xx,yy,zz,scale_factor=3)
	mlab.show()



