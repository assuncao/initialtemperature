import sys, re
import numpy as np
from mayavi import mlab
from scipy.special import erf

################################################################################
# Control options
################################################################################

bar 					= 'True'
mayaviPermission		= 'False'

oceanLitThickness 		= -80.
continetLitThickness 	= -200.
slabThickness			= np.abs(oceanLitThickness)
slabVelocity			= 50E-6 # km/ano
LABtemperatureC			= 1300.
LABtemperatureK			= 1573.
TZtemperatureC			= 1900.
alpha					= 1.*10E-5
gravity					= 10
specificHeat			= 1.75E3
Kelvin					= 273.15
niter 					= 100
tm 						= 1300.
kappa					= 10E-6
R  						= slabVelocity * slabThickness / (2 * kappa)

################################################################################
# Functions
################################################################################

def rad(angle):
	return np.pi*angle/180.

def km(lon1,lon2):
	angularDistance = lon2 - lon1
	distance		= 111. * angularDistance
	return distance

def tempLinear(x,x0,x1,y0,y1):
	a = (y1 - y0) / (x1 - x0)
	b = y1 - a * x1
	f = a * x + b
	return f

def tempAdiabatic(depthCoord,initialDepth):
	iniDepth	= initialDepth * 1000 # Converting to meters (m)
	depCoord	= depthCoord * 1000
	aux 		= np.exp(((alpha*gravity)/(specificHeat))*(iniDepth-depCoord))
	tempField 	= LABtemperatureK * aux
	return tempField

def slabLimitCalc(approx,projec):
	dxWest 		= approx[0,0] - projec[0,0]
	dzWest 		= approx[0,1] - projec[0,1]
	aWest 		= dzWest / dxWest
	bWest 		= approx[0,1] - aWest * approx[0,0]
	
	auxApprox 	= np.shape(approx)[0]-1
	auxProjec 	= np.shape(projec)[0]-1
	dxEast 		= approx[auxApprox,0] - projec[auxProjec,0]
	dzEast 		= approx[auxApprox,1] - projec[auxProjec,1]
	aEast 		= dzEast / dxEast
	bEast 		= approx[auxApprox,1] - aEast * approx[auxApprox,0]
	
	slabLimit 	= [aWest,bWest,aEast,bEast]
	return slabLimit

def boundary(a,b,x):
	z = a * x + b
	return z

def erfSingle(z0,z1,x,length):
	z = z0 + ((z1 - z0) / 2.) * erf((x/(length/4.)-2)) + ((z1 - z0)/2.)
	return z

def findLongiInterval(projec,lon):
	sz = np.shape(projec)[0]
	for i in xrange(sz-1):
		if (projec[i,0]<=lon):
			westBoundary = projec[i,0]
			index = i
		else:
			continue
	eastBoundary = projec[index+1,0]

#	a = (projec[index+1,1] - projec[index,1]) / (eastBoundary - westBoundary)
	dz 	= np.max([projec[index,1] - projec[index+1,1],projec[index+1,1] - projec[index,1]])
	a 	= dz / (eastBoundary - westBoundary)
	b 	= projec[index,1] - a * projec[index,0]

	botBoundary = a * lon + b
	return botBoundary

def surfaceLength(surface):
	x 	= surface[:,0]
	z 	= surface[:,1]
	aux = np.zeros(np.size(x))
	dx2 = 0.
	dz2 = 0.
	len = 0.
	for i in xrange(np.size(x)-1):
		dx2 		= pow(x[i+1]-x[i],2)
		dz2 		= pow(z[i+1]-z[i],2)
		len 		= len + np.sqrt(dx2+dz2)
		aux[i+1] 	= len
	
	return aux

def minDistance2Curve(approx,lon,depth):
	sz 		= np.shape(approx)[0]
	closest = 100000000
	for i in xrange(sz):
		cA 	= approx[i,1] - depth
		cB 	= approx[i,0] - lon
		cA2 = pow(cA,2)
		cB2 = pow(cB,2)
		h 	= np.sqrt(cA2+cB2)
		if (h<=closest):
			closest = h
			p = ([approx[i,0]-approx[0,0],approx[i,1]-approx[0,1]],closest,i)
		else:
			continue
	return p

def slabTemperature(x,z,niter):
	z = slabThickness - z
	h = 0.
	for n in xrange(1,niter):
		c = pow(-1,n) / (n * np.pi)
		b = np.sqrt(pow(R,2) + (pow(n,2) * pow(np.pi,2))) - R
		h = c * np.exp(( - b * x) / (slabThickness)) * np.sin((n * np.pi * z) / (slabThickness)) + h
	s = tm * (1 + 2 * h)
	if (s<0):
		s=0
	return s

################################################################################
# Classes
################################################################################

class Parameters:
	def __init__(self,coord,aux):
		if (aux=="depth"):
			self.start 	= float(coord[0])
			self.end 	= float(coord[1])
			self.delta	= float(coord[2])
			self.length = int(np.abs((self.end - self.start) / self.delta) + 1)
		
		else:
			self.startDegree 	= float(coord[0])
			if (self.startDegree < 0) and (aux=="lon"):
				self.startDegree = 360. + self.startDegree
			self.startKm		= 0.
			self.endDegree	= float(coord[1])
			if (self.endDegree < 0) and (aux=="lon"):
				self.endDegree = 360. + self.endDegree
			self.endKm			= km(self.startDegree,self.endDegree)
			self.delta	= float(coord[2])
			if (self.delta == 0):
				self.delta = 1.
			self.length = int((self.endDegree - self.startDegree) / self.delta) + 1

class Model:
	def __init__(self,data):
		self.abscissa	= data[:,0]
		self.distance	= data[:,1]
		self.ordinate	= data[:,2]
		self.size 		= np.shape(data)[0]
		self.length		= 0.
		self.dlengths	= []
		for i in xrange(self.size-1):
			deltaA = km(self.abscissa[i],self.abscissa[i+1])
			deltaO = self.ordinate[i+1] - self.ordinate[i]
			dx = np.sqrt(pow(deltaA,2)+pow(deltaO,2))
			self.dlengths.append(dx)
			self.length = self.length + dx

################################################################################
# Read data from ./src/params.txt
################################################################################

parPath = './src/params.txt'
par 	= open(parPath)
lon 	= Parameters(re.split("[ ]+", par.readline().strip()),"lon")
lat 	= Parameters(re.split("[ ]+", par.readline().strip()),"lat")
depth 	= Parameters(re.split("[ ]+", par.readline().strip()),"depth")
par.close()

fieldTemperature 	= np.zeros(shape=(lon.length,lat.length,depth.length))
fieldLon			= np.linspace(lon.startKm,lon.endKm,lon.length)
fieldLat			= np.linspace(lat.startKm,lat.endKm,lat.length)
fieldDepth			= np.linspace(depth.start,depth.end,depth.length)
auxFieldDepth		= np.zeros(np.size(fieldDepth))

################################################################################
# Progress bar
################################################################################

if (bar == 'True'):
	progressBar_width 	= 80
	increment			= 0
	step				= float(progressBar_width) / float(lat.length)
	print "\nCalculating temperature field"
	sys.stdout.write("[%s]" % (" " * progressBar_width))
	sys.stdout.flush()
	sys.stdout.write("\b" * (progressBar_width+1)) # return to start of line, after '['

################################################################################
# Load surfaces
################################################################################

approxModelPath 	= './out/approx/approx.'
projecModelPath 	= './out/projec/slice.'

for j in xrange(lat.length):
	aux1,aux2 = np.round(lat.startDegree*100,0),np.round(j*lat.delta*100,0)
	approxModelFile = approxModelPath + str(int(aux1+aux2)).zfill(5)
	approx 			= np.loadtxt(approxModelFile)
	projecModelFile = projecModelPath + str(int(aux1+aux2)).zfill(5)
	projec 			= np.loadtxt(projecModelFile)
	
	############################################################################
	# Background temperature field design
	############################################################################
	
	aW,bW,aE,bE		= slabLimitCalc(approx,projec)

	for i in xrange(lon.length):
		for k in xrange(depth.length):
			westLimitRegionMax = np.max([approx[0,0],projec[0,0]])
			eastLimitRegionMin = np.min([approx[np.shape(approx)[0]-1,0],projec[np.shape(projec)[0]-1,0]])
			
			if (fieldLon[i]<westLimitRegionMax):
				if (fieldDepth[k]>oceanLitThickness):
					x,x0,x1,y0,y1 = fieldDepth[k],0,oceanLitThickness,0,LABtemperatureC
					fieldTemperature[i,j,np.size(fieldDepth)-k-1] = tempLinear(x,x0,x1,y0,y1)
				else:
					y,y0 = fieldDepth[k],oceanLitThickness
					fieldTemperature[i,j,np.size(fieldDepth)-k-1] = tempAdiabatic(y,y0) - Kelvin
		
			elif (fieldLon[i]>eastLimitRegionMin):
				if (fieldDepth[k]>continetLitThickness):
					x,x0,x1,y0,y1 = fieldDepth[k],0,continetLitThickness,0,LABtemperatureC
					fieldTemperature[i,j,np.size(fieldDepth)-k-1] = tempLinear(x,x0,x1,y0,y1)
				else:
					y,y0 = fieldDepth[k],continetLitThickness
					fieldTemperature[i,j,np.size(fieldDepth)-k-1] = tempAdiabatic(y,y0) - Kelvin
	
			else:
				length 	= eastLimitRegionMin - westLimitRegionMax
				posX	= fieldLon[i] - westLimitRegionMax#length
				auxFieldDepth[k] = erfSingle(oceanLitThickness,continetLitThickness,posX,length)
				if (fieldDepth[k]>auxFieldDepth[k]):
					x,x0,x1,y0,y1 = fieldDepth[k],0,auxFieldDepth[k],0,LABtemperatureC
					fieldTemperature[i,j,np.size(fieldDepth)-k-1] = tempLinear(x,x0,x1,y0,y1)
				else:
					y,y0 = fieldDepth[k],auxFieldDepth[k]
					fieldTemperature[i,j,np.size(fieldDepth)-k-1] = tempAdiabatic(y,y0) - Kelvin

	############################################################################
	# Slab temperature design
	############################################################################

	surfaceLen		= surfaceLength(approx)

	for i in xrange(lon.length):
		for k in xrange(depth.length):
			westLimitRegionMin = np.min([approx[0,0],projec[0,0]])
			westLimitRegionMax = np.max([approx[0,0],projec[0,0]])
			if (approx[0,0]>projec[0,0]):
				flagW = "normal"
			else:
				flagW = "inverse"

			eastLimitRegionMin = np.min([approx[np.shape(approx)[0]-1,0],projec[np.shape(projec)[0]-1,0]])
			eastLimitRegionMax = np.max([approx[np.shape(approx)[0]-1,0],projec[np.shape(projec)[0]-1,0]])
			if ([approx[np.shape(approx)[0]-1,0]>projec[np.shape(projec)[0]-1,0]]):
				flagE = "inverse"
			else:
				flagE = "normal"

			if ((fieldLon[i]>=westLimitRegionMin) and (fieldLon[i]<westLimitRegionMax)):
				if (flagW=="normal"):
					topBoundary = boundary(aW,bW,fieldLon[i])
					botBoundary = findLongiInterval(projec,fieldLon[i])
				if (flagW=="inverse"):
					topBoundary = findLongiInterval(approx,fieldLon[i])
					botBoundary = boundary(aW,bW,fieldLon[i])
				if ((fieldDepth[k]<=topBoundary) and (fieldDepth[k]>=botBoundary)):
					equivalentP 	= minDistance2Curve(approx,fieldLon[i],fieldDepth[k])
					equivalentLon	= surfaceLen[equivalentP[2]]
					fieldTemperature[i,j,np.size(fieldDepth)-k-1] = slabTemperature(equivalentLon,equivalentP[1],niter)
				else:
					continue

			elif ((fieldLon[i]>eastLimitRegionMin) and (fieldLon[i]<=eastLimitRegionMax)):
				if (flagE=="normal"):
					topBoundary = boundary(aE,bE,fieldLon[i],flagE)
					botBoundary = findLongiInterval(projec,fieldLon[i])
				if (flagE=="inverse"):
					topBoundary = findLongiInterval(approx,fieldLon[i])
					botBoundary = boundary(aE,bE,fieldLon[i])
				if ((fieldDepth[k]<=topBoundary) and (fieldDepth[k]>=botBoundary)):
					equivalentP 	= minDistance2Curve(approx,fieldLon[i],fieldDepth[k])
					equivalentLon	= surfaceLen[equivalentP[2]]
					fieldTemperature[i,j,np.size(fieldDepth)-k-1] = slabTemperature(equivalentLon,equivalentP[1],niter)
				else:
					continue

			elif ((fieldLon[i]>=westLimitRegionMax) and (fieldLon[i]<=eastLimitRegionMin)):
				topBoundary = findLongiInterval(approx,fieldLon[i])
				botBoundary = findLongiInterval(projec,fieldLon[i])
				if ((fieldDepth[k]<=topBoundary) and (fieldDepth[k]>=botBoundary)):
					equivalentP 	= minDistance2Curve(approx,fieldLon[i],fieldDepth[k])
					equivalentLon	= surfaceLen[equivalentP[2]]
					fieldTemperature[i,j,np.size(fieldDepth)-k-1] = slabTemperature(equivalentLon,equivalentP[1],niter)
				else:
					continue

			else:
				continue
					
	# Progress bar update
	if (bar == 'True'):
		pbaux1 = int(increment)
		increment = increment + step
		pbaux2 = int(increment)
		leap = pbaux2 - pbaux1
		sys.stdout.write(">" * leap)
		sys.stdout.flush()

# Progress bar end
if (bar == 'True'):
	sys.stdout.write("\n")

################################################################################
# Save temperature field
################################################################################

temperatureField 	= './out/temperatureField.dat'
temperatureFile 	= open(temperatureField,"w")

lengthStr			= str(lon.length)+"\t"+str(lat.length)+"\t"+str(depth.length)
extentDistStr		= str(lon.startKm)+"\t"+str(lon.endKm)+"\t"+str(lat.startKm)+"\t"+str(lat.endKm)+"\t"+str(depth.start)+"\t"+str(depth.end)
extentDegrStr		= str(lon.startDegree)+"\t"+str(lon.endDegree)+"\t"+str(lat.startDegree)+"\t"+str(lat.endDegree)+"\t"+str(depth.start)+"\t"+str(depth.end)

temperatureFile.write(lengthStr+"\n")
temperatureFile.write(extentDistStr+"\n")
temperatureFile.write(extentDegrStr+"\n")

for j in xrange(lat.length):
	for i in xrange(lon.length):
		for k in xrange(depth.length):
			tempStr = str(fieldTemperature[i,j,k])
			temperatureFile.write(tempStr+"\n")


temperatureFile.close()

################################################################################
# Temperature field plot
################################################################################

if (mayaviPermission == 'True'):
	mlab.figure(size=(800,800))
	extent = [lon.startKm,lon.endKm,lat.startKm,lat.endKm,depth.start,depth.end]
	mlab.contour3d(fieldTemperature,extent=extent,contours=11,transparent=True,colormap="blue-red")
	mlab.view(azimuth=270,elevation=90)

################################################################################
# Region Boundary Plot (Box)
################################################################################

if (mayaviPermission == 'True'):
	radius = 5.
	
	lonMin = lon.startKm
	lonMax = lon.endKm
	latMin = lat.startKm
	latMax = lat.endKm
	
	x0 = np.array([lonMin,lonMax])
	y0 = np.array([latMin,latMin])
	z0 = np.array([depth.start,depth.start])
	mlab.plot3d(x0,y0,z0,tube_radius=radius,color=(0,0,0))
	
	x1 = np.array([lonMin,lonMax])
	y1 = np.array([latMin,latMin])
	z1 = np.array([depth.end,depth.end])
	mlab.plot3d(x1,y1,z1,tube_radius=radius,color=(0,0,0))
	
	x2 = np.array([lonMin,lonMax])
	y2 = np.array([latMax,latMax])
	z2 = np.array([depth.start,depth.start])
	mlab.plot3d(x2,y2,z2,tube_radius=radius,color=(0,0,0))
	
	x3 = np.array([lonMin,lonMax])
	y3 = np.array([latMax,latMax])
	z3 = np.array([depth.end,depth.end])
	mlab.plot3d(x3,y3,z3,tube_radius=radius,color=(0,0,0))
	
	x4 = np.array([lonMin,lonMin])
	y4 = np.array([latMin,latMin])
	z4 = np.array([depth.start,depth.end])
	mlab.plot3d(x4,y4,z4,tube_radius=radius,color=(0,0,0))
	
	x5 = np.array([lonMax,lonMax])
	y5 = np.array([latMax,latMax])
	z5 = np.array([depth.start,depth.end])
	mlab.plot3d(x5,y5,z5,tube_radius=radius,color=(0,0,0))
	
	x6 = np.array([lonMin,lonMin])
	y6 = np.array([latMax,latMax])
	z6 = np.array([depth.start,depth.end])
	mlab.plot3d(x6,y6,z6,tube_radius=radius,color=(0,0,0))
	
	x7 = np.array([lonMax,lonMax])
	y7 = np.array([latMin,latMin])
	z7 = np.array([depth.start,depth.end])
	mlab.plot3d(x7,y7,z7,tube_radius=radius,color=(0,0,0))
	
	x8 = np.array([lonMax,lonMax])
	y8 = np.array([latMin,latMax])
	z8 = np.array([depth.start,depth.start])
	mlab.plot3d(x8,y8,z8,tube_radius=radius,color=(0,0,0))
	
	x9 = np.array([lonMax,lonMax])
	y9 = np.array([latMin,latMax])
	z9 = np.array([depth.end,depth.end])
	mlab.plot3d(x9,y9,z9,tube_radius=radius,color=(0,0,0))
	
	x10 = np.array([lonMin,lonMin])
	y10 = np.array([latMin,latMax])
	z10 = np.array([depth.start,depth.start])
	mlab.plot3d(x10,y10,z10,tube_radius=radius,color=(0,0,0))
	
	x11 = np.array([lonMin,lonMin])
	y11 = np.array([latMin,latMax])
	z11 = np.array([depth.end,depth.end])
	mlab.plot3d(x11,y11,z11,tube_radius=radius,color=(0,0,0))
	
	mlab.points3d(0,0,0,scale_factor=50)
#	mlab.points3d(2200,0,continetLitThickness,scale_factor=50)
#	mlab.points3d(0,0,oceanLitThickness,scale_factor=50)
	for k in xrange(0,np.size(fieldDepth)-1,8):
		mlab.points3d(0,0,fieldDepth[k],scale_factor=25,color=(1,0,0))
		mlab.points3d(fieldLon[lon.length-1],0,fieldDepth[k],scale_factor=25,color=(1,0,0))
	for i in xrange(1,np.size(fieldLon)):
		mlab.points3d(fieldLon[i],0,fieldDepth[0],scale_factor=25,color=(0,0,1))
		mlab.points3d(fieldLon[i],0,fieldDepth[depth.length-1],scale_factor=25,color=(0,0,1))

if (mayaviPermission == 'True'):
	mlab.show()



