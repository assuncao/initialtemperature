import os, sys, re
import numpy as np

################################################################################
# Setting up classes and methods
################################################################################

cwd = os.getcwd()
libsLocation = os.path.join(cwd,'lib')
sys.path.append(libsLocation)

import classes, methods, data
#from progressBar import startBar, updateBar, endBar

################################################################################
# Control options
################################################################################

bar 					= 'True'
mayaviPermission		= 'True'

if (mayaviPermission=='True'):
	from mayavi import mlab

################################################################################
# Read data from ./src/params.txt
################################################################################

parPath = './src/params.txt'
par 	= open(parPath)
lon 	= classes.Parameters(re.split("[ ]+", par.readline().strip()),"lon")
lat 	= classes.Parameters(re.split("[ ]+", par.readline().strip()),"lat")
depth 	= classes.Parameters(re.split("[ ]+", par.readline().strip()),"depth")
auxDim	= re.split("[\t]+", par.readline().strip())
par.close()

#if (auxDim[0]=='2d'):
#	lat.length = 1

fieldTemperature 	= np.zeros(shape=(lon.length,lat.length,depth.length))
fieldLon			= np.linspace(lon.startKm,lon.endKm,lon.length)
fieldLat			= np.linspace(lat.startKm,lat.endKm,lat.length)
fieldDepth			= np.linspace(depth.start,depth.end,depth.length)
auxFieldDepth		= np.zeros(np.size(fieldDepth))

################################################################################
# Make Shell
################################################################################

prm = [lon,lat,depth,auxDim]
shell = classes.Shell(prm)

################################################################################
# Read dimensional reference values
################################################################################

refValuesPath 	= './src/reference.txt'
refValuesFile 	= open(refValuesPath)
refValues		= []
for line in refValuesFile:
	refValues.append(line)

dimRef 			= classes.Reference(refValues,depth.end)

################################################################################
# Progress bar
################################################################################

if (bar=='True'):
	print "\nCalculating temperature field"
#	bar = startBar(lat.length)
	progressBar_width 	= 80
	increment			= 0
	step				= float(progressBar_width) / float(lat.length)
	sys.stdout.write("[%s]" % (" " * progressBar_width))
	sys.stdout.flush()
	sys.stdout.write("\b" * (progressBar_width+1)) # return to start of line, after '['

################################################################################
# Load surfaces
################################################################################

#topModelPath 	= './out/approx/approx.'
#botModelPath 	= './out/projec/slice.'
if (lon.delta>0.05):
	topModelPath 	= './out/model/slab.'
	botModelPath 	= './out/thk/thk.'
else:
	topModelPath 	= './out/approx/approx.'
	botModelPath 	= './out/projec/slice.'

lithoPath = cwd + '/out/litho.txt'
litho = np.loadtxt(lithoPath,unpack=True)
litho = litho * (-1)

#refBotModelPath = './out/projec/slice.'

################################################################################
# Main latitude Loop
################################################################################

for j in xrange(lat.length):
	aux1,aux2 		= np.round(lat.startDegree*100,0),np.round(j*lat.delta*100,0)
	if (auxDim[0]=='2d'):
		aux1,aux2 	= np.round(lat.startDegree*100,0),0
	
	topModelFile 	= topModelPath + str(int(aux1+aux2)).zfill(5)
	top 			= classes.Surface(np.loadtxt(topModelFile),1,2)
	botModelFile 	= botModelPath + str(int(aux1+aux2)).zfill(5)
	bot 			= classes.Surface(np.loadtxt(botModelFile),1,2)

	############################################################################
	# Background temperature field design
	############################################################################
	
	aW,bW,aE,bE		= methods.slabLimitCalc(top,bot)

	for i in xrange(lon.length):
		for k in xrange(depth.length):
			westSlabLimitMax = np.max([top.minA,bot.minA])
			eastSlabLimitMin = np.min([top.maxA,bot.maxA])
			
			# Take care of Oceanic Lithosphere
			if (fieldLon[i]<westSlabLimitMax):
				if (fieldDepth[k]>litho[i,j]):
					x,x0,x1,y0,y1 = fieldDepth[k],0,litho[i,j],0,dimRef.LABtemperatureC
					fieldTemperature[i,j,np.size(fieldDepth)-k-1] = methods.tempLinear(x,x0,x1,y0,y1)
				else:
					y,y0 = fieldDepth[k],litho[i,j]
					auxT1 = methods.tempAdiabatic(y,y0,dimRef.alpha,dimRef.gravity,dimRef.specificHeat,dimRef.POTtemperatureK)
					x,x0,x1,y0,y1 = fieldDepth[k],0,litho[i,j],0,dimRef.LABtemperatureC
					auxT2 = methods.tempLinear(x,x0,x1,y0,y1)
					fieldTemperature[i,j,np.size(fieldDepth)-k-1] = min(auxT1,auxT2)
		
			# Take care of Continental Lithosphere
			elif (fieldLon[i]>eastSlabLimitMin):
				if (fieldDepth[k]>litho[i,j]):
					x,x0,x1,y0,y1 = fieldDepth[k],0,litho[i,j],0,dimRef.LABtemperatureC
					fieldTemperature[i,j,np.size(fieldDepth)-k-1] = methods.tempLinear(x,x0,x1,y0,y1)
				else:
					y,y0 = fieldDepth[k],litho[i,j]
					auxT1 = methods.tempAdiabatic(y,y0,dimRef.alpha,dimRef.gravity,dimRef.specificHeat,dimRef.POTtemperatureK)
					x,x0,x1,y0,y1 = fieldDepth[k],0,litho[i,j],0,dimRef.LABtemperatureC
					auxT2 = methods.tempLinear(x,x0,x1,y0,y1)
					fieldTemperature[i,j,np.size(fieldDepth)-k-1] = min(auxT1,auxT2)

			# Take care of Contact (and Continental) Lithosphere
			else:
				length 	= eastSlabLimitMin - westSlabLimitMax
				posX	= fieldLon[i] - westSlabLimitMax#length
				auxDepth1A = methods.findSlabSurfaceDepth(top.abscissa,top.ordinate,fieldLon[i])
				auxDepth1B = litho[i,j]
				auxDepth1 = min(auxDepth1A,auxDepth1B)
				auxDepth2 = litho[i,j]
				auxFieldDepth[k] = max(auxDepth1,auxDepth2)
				if (fieldDepth[k]>auxFieldDepth[k]):
					x,x0,x1,y0,y1 = fieldDepth[k],0,auxFieldDepth[k],0,dimRef.LABtemperatureC
					fieldTemperature[i,j,np.size(fieldDepth)-k-1] = methods.tempLinear(x,x0,x1,y0,y1)
				else:
					y,y0 = fieldDepth[k],auxFieldDepth[k]
					auxT1 = methods.tempAdiabatic(y,y0,dimRef.alpha,dimRef.gravity,dimRef.specificHeat,dimRef.POTtemperatureK)
					x,x0,x1,y0,y1 = fieldDepth[k],0,auxFieldDepth[k],0,dimRef.LABtemperatureC
					auxT2 = methods.tempLinear(x,x0,x1,y0,y1)
					fieldTemperature[i,j,np.size(fieldDepth)-k-1] = min(auxT1,auxT2)

	############################################################################
	# Slab temperature design
	############################################################################

	surfaceLen		= top.distance# surfaceLength(top)

	for i in xrange(lon.length):
		for k in xrange(depth.length):
			# Calculate difference between POTtemperatureC and tempAdiabatic(z), set diffAdiabatic to zero if you do not want to use this temperature addition to the subducting slab
			y,y0 = fieldDepth[k],0
			temp1 = dimRef.POTtemperatureC
			temp2 = methods.tempAdiabatic(y,y0,dimRef.alpha,dimRef.gravity,dimRef.specificHeat,dimRef.POTtemperatureK)
			diffAdiabatic = (temp2-temp1)
			
			# Check if slab starts going down (normal) or up (inverse)
			westSlabLimitMin = np.min([top.minA,bot.minA])
			westSlabLimitMax = np.max([top.minA,bot.minA])
			if (top.minA>bot.minA):
				flagW = "normal"
			else:
				flagW = "inverse"

			# Check if slab ends up going down (normal) or up (inverse)
			eastSlabLimitMin = np.min([top.maxA,bot.maxA])
			eastSlabLimitMax = np.max([top.maxA,bot.maxA])
			if ([top.maxA>bot.maxA]):
				flagE = "inverse"
			else:
				flagE = "normal"

			# Take care of the beginning of the slab
			if ((fieldLon[i]>=westSlabLimitMin) and (fieldLon[i]<westSlabLimitMax)):
				if (flagW=="normal"):
					topBoundary = methods.boundary(aW,bW,fieldLon[i])
					botBoundary = methods.findLongiInterval(bot,fieldLon[i])
				if (flagW=="inverse"):
					topBoundary = methods.findLongiInterval(top,fieldLon[i])
					botBoundary = methods.boundary(aW,bW,fieldLon[i])
				if ((fieldDepth[k]<=topBoundary) and (fieldDepth[k]>=botBoundary)):
					equivalentP 	= methods.minDistance2Curve(top,fieldLon[i],fieldDepth[k])
					equivalentLon	= surfaceLen[equivalentP[2]]
					fieldTemperature[i,j,np.size(fieldDepth)-k-1] = methods.slabTemperature(equivalentLon,equivalentP[1],dimRef.niter,data.slabThickness(),dimRef.R,dimRef.tm) + diffAdiabatic
				else:
					continue

			# Take care of the end of the slab
			elif ((fieldLon[i]>eastSlabLimitMin) and (fieldLon[i]<=eastSlabLimitMax)):
				if (flagE=="normal"):
					topBoundary = methods.boundary(aE,bE,fieldLon[i])#,flagE)
					botBoundary = methods.findLongiInterval(bot,fieldLon[i])
				if (flagE=="inverse"):
					topBoundary = methods.findLongiInterval(top,fieldLon[i])
					botBoundary = methods.boundary(aE,bE,fieldLon[i])
				if ((fieldDepth[k]<=topBoundary) and (fieldDepth[k]>=botBoundary)):
					equivalentP 	= methods.minDistance2Curve(top,fieldLon[i],fieldDepth[k])
					equivalentLon	= surfaceLen[equivalentP[2]]
					fieldTemperature[i,j,np.size(fieldDepth)-k-1] = methods.slabTemperature(equivalentLon,equivalentP[1],dimRef.niter,data.slabThickness(),dimRef.R,dimRef.tm) + diffAdiabatic
				else:
					continue

			# Take care of the rest of the slab
			elif ((fieldLon[i]>=westSlabLimitMax) and (fieldLon[i]<=eastSlabLimitMin)):
				topBoundary = methods.findLongiInterval(top,fieldLon[i])
				botBoundary = methods.findLongiInterval(bot,fieldLon[i])
				if ((fieldDepth[k]<=topBoundary) and (fieldDepth[k]>=botBoundary)):
					equivalentP 	= methods.minDistance2Curve(top,fieldLon[i],fieldDepth[k])
					equivalentLon	= surfaceLen[equivalentP[2]]
					fieldTemperature[i,j,np.size(fieldDepth)-k-1] = methods.slabTemperature(equivalentLon,equivalentP[1],dimRef.niter,data.slabThickness(),dimRef.R,dimRef.tm) + diffAdiabatic
				else:
					continue

			else:
				continue
	for i in xrange(lon.length):
		fieldTemperature[i,j,-1] = 0.0
		fieldTemperature[i,j,0]	= dimRef.TZtemperatureC

	# Progress bar update
	if (bar == 'True'):
#		bar = updateBar(bar[0],bar[1])
		pbaux1 = int(increment)
		increment = increment + step
		pbaux2 = int(increment)
		leap = pbaux2 - pbaux1
		sys.stdout.write(">" * leap)
		sys.stdout.flush()

# Progress bar end
if (bar == 'True'):
#	endBar()
	sys.stdout.write("\n")

################################################################################
# Save temperature field
################################################################################

temperatureField 	= './out/initial-temperature.txt'
temperatureFile 	= open(temperatureField,"w")


extentDistStr		= "# " + str(lon.startKm)+ "\t" + str(lon.endKm) + "\t" + str(lat.startKm)+"\t"+str(lat.endKm)+"\t"+str(depth.start)+"\t"+str(depth.end)
extentDegrStr		= "# " + str(lon.startDegree)+"\t"+str(lon.endDegree)+"\t"+str(lat.startDegree)+"\t"+str(lat.endDegree)+"\t"+str(depth.start)+"\t"+str(depth.end)
pointsStr			= "# POINTS: " + str(depth.length) + " " + str(lon.length) + " " + str(lat.length)

temperatureFile.write(extentDistStr+"\n")
temperatureFile.write(extentDegrStr+"\n")
temperatureFile.write(pointsStr+"\n")

temperatureFile.write("# Columns: r [m] longitude [rad] colatitude [rad] temperature [K] \n")

for phi in xrange(lat.length):
	for theta in xrange(lon.length):
		for r in xrange(depth.length):
			temperatureFile.write(str(shell.r[depth.length-1-r])+"\t")
			temperatureFile.write(str(shell.azi[theta])+"\t")
			temperatureFile.write(str(shell.inc[lat.length-1-phi])+"\t")
			tempStr = str(fieldTemperature[theta,lat.length-1-phi,r]+273.15)
			temperatureFile.write(tempStr+"\n")

temperatureFile.close()

################################################################################
# Temperature field plot
################################################################################
elon 	= classes.Parameters([280,310,10],"lon")
elat 	= classes.Parameters([-30,-10,10],"lat")
edepth 	= classes.Parameters([0,0,6371000.],"depth")
earth 	= classes.Shell([elon,elat,edepth])


#mlab.figure(size=(800,800))
#mlab.mesh(earth.x[:,:,0],earth.y[:,:,0],earth.z[:,:,0],color=(0.5,0.5,1),opacity=0.1)
#mlab.points3d(shell.x,shell.y,shell.z,scale_factor=10000)


#mlab.points3d(shell.x,shell.y,shell.z,fieldTemperature[:,:,::-1],scale_factor=20)

#mlab.outline()
#mlab.show()

#if (mayaviPermission == 'True'):
#	mlab.figure(size=(800,800))
#	extent = [lon.startKm,lon.endKm,lat.startKm,lat.endKm,depth.start,depth.end]
#	mlab.contour3d(fieldTemperature,extent=extent,contours=11,transparent=True,colormap="blue-red")
#	mlab.view(azimuth=270,elevation=90)
#
#################################################################################
## Region Boundary Plot (Box)
#################################################################################
#
#if (mayaviPermission == 'True'):
#	mlab.outline(extent=extent)
#	mlab.points3d(0,0,0,scale_factor=50)
#	for k in xrange(0,np.size(fieldDepth)-1,8):
#		mlab.points3d(0,0,fieldDepth[k],scale_factor=25,color=(1,0,0))
#		mlab.points3d(fieldLon[lon.length-1],0,fieldDepth[k],scale_factor=25,color=(1,0,0))
#	for i in xrange(1,np.size(fieldLon)):
#		mlab.points3d(fieldLon[i],0,fieldDepth[0],scale_factor=25,color=(0,0,1))
#		mlab.points3d(fieldLon[i],0,fieldDepth[depth.length-1],scale_factor=25,color=(0,0,1))
#
#if (mayaviPermission == 'True'):
#	xmin, xmax, ymin, ymax, zmin, zmax = lon.startKm, lon.endKm, lat.startKm, lat.endKm, depth.end, depth.start
#	xx, yy, zz = np.mgrid[xmin:xmax:(lon.length)*1j, ymin:ymax:(lat.length)*1j, zmin:zmax:(depth.length)*1j]
#	mlab.points3d(xx,yy,zz,scale_factor=3)
#	mlab.show()



