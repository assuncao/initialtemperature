import numpy as np
import matplotlib.pyplot as plt
import os, sys, re

################################################################################
# Loading reference values from reference class
################################################################################

cwd = os.getcwd()
libsLocation = os.path.join(cwd,'lib')
sys.path.append(libsLocation)

import classes, methods

################################################################################
# Read data from ./src/params.txt
################################################################################

print("*** Reading initial parameters *** ")
parPath = cwd + '/src/params.txt'
param 	= classes.Dimension(parPath)

param.lat.size = 1

xi = np.linspace(0,param.lon.end.km,param.lon.size);
zi = np.linspace(param.depth.end,0,param.depth.size);
xx,zz = np.meshgrid(xi,zi);

################################################################################
# Interfaces
################################################################################

scale=2.2
fig, ax = plt.subplots(figsize=(7*scale,2.*scale))
ax.set_xlim(param.lon.start.km,param.lon.end.km)
ax.set_ylim(param.depth.end,0)

skiprows = 7
interfaces = np.loadtxt("out/interfaces_creep.txt",unpack=True,skiprows=skiprows)
interface0 = interfaces[0,:param.lon.size] / 1.0E3
interface1 = interfaces[1,:param.lon.size] / 1.0E3
interface2 = interfaces[2,:param.lon.size] / 1.0E3
interface3 = interfaces[3,:param.lon.size] / 1.0E3
interface4 = interfaces[4,:param.lon.size] / 1.0E3
interface5 = interfaces[5,:param.lon.size] / 1.0E3
interface6 = interfaces[6,:param.lon.size] / 1.0E3
interface7 = interfaces[7,:param.lon.size] / 1.0E3
#interface8 = interfaces[8,:param.lon.size] / 1.0E3
#interface9 = interfaces[9,:param.lon.size] / 1.0E3

plt.plot(xi,interface0,linewidth=0.4,label="Layer 0") # mantle (slab2 bot) - oce.lit
plt.plot(xi,interface1,linewidth=0.4,label="Layer 1") # oce.lit - oce.crust
plt.plot(xi,interface2,linewidth=0.4,label="Layer 2") # oce.crust - mantle (slab2 top)
plt.plot(xi,interface3,linewidth=0.4,label="Layer 3") # mantle (slab2 top) - lower lubrincant layer
plt.plot(xi,interface4,linewidth=0.4,label="Layer 4") # lower lub. layer - upper lub. layer
plt.plot(xi,interface5,linewidth=0.4,label="Layer 5") # upper lub. layer - lower trans. cont. lit
plt.plot(xi,interface6,linewidth=0.4,label="Layer 6") # lower trans. cont. lit - lower cont. lit
plt.plot(xi,interface7,linewidth=0.4,label="Layer 7") # lower cont. lit - upper trans. cont. crust
#plt.plot(xi,interface8,linewidth=0.4,label="Layer 8")
#plt.plot(xi,interface9,linewidth=0.4,label="Layer 9")

################################################################################
# Velocity
################################################################################

skiprows = 4
V = np.loadtxt("out/veloc_0_3D.txt",unpack=True,comments="P",skiprows=skiprows)
VV 						= V * 1.0
VV[np.abs(VV)<1.0E-200] = 0.0
VV 						= np.reshape(VV,(2,param.lon.size,param.lat.size,param.depth.size),order='F')
u 						= np.transpose(VV[0,:,0,:]) #/ VV_max
v 						= np.transpose(VV[1,:,0,:]) #/ VV_max
#w 						= np.transpose(VV[2,:,0,:]) #/ VV_max
colors 					= np.sqrt(u**2+v**2)
colors_max				= np.max(colors)

# Normalization
colors 	= colors / colors_max
u 		= u / colors_max
v 		= v / colors_max

cs = plt.quiver(xi,zi,u,v,units='width',width=0.001,scale=100,pivot='mid')

plt.legend(loc=3)
plt.axis('off')
plt.tight_layout()
plt.show()
