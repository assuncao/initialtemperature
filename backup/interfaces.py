import numpy as np
import re,os,sys

import geopandas as gp
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon

################################################################################
# Plot control
################################################################################

pltPermission = 'True'

if (pltPermission=='True'):
	import matplotlib.pyplot as plt
	plt.clf()

################################################################################
# Loading reference values from reference class
################################################################################

cwd = os.getcwd()
libsLocation = os.path.join(cwd,'lib')
sys.path.append(libsLocation)

import classes, methods
import progressBar as pbar

################################################################################
# Read data from ./src/params.txt
################################################################################

print("*** Reading initial parameters *** ")
parPath = cwd + '/src/params.txt'
param 	= classes.Dimension(parPath)

fieldTemperature 	= np.zeros(shape=(param.lon.size,param.lat.size,param.depth.size))
fieldLon			= np.linspace(param.lon.start.km,param.lon.end.km,param.lon.size)
fieldLat			= np.linspace(param.lat.start.km,param.lat.end.km,param.lat.size)
fieldDepth			= np.linspace(param.depth.start,param.depth.end,param.depth.size)
auxFieldDepth		= np.zeros(np.size(fieldDepth))

x,y 				= np.meshgrid(fieldLon,fieldLat)

longitude			= np.linspace(param.lon.start.degree,param.lon.end.degree,param.lon.size)
latitude 			= np.linspace(param.lat.start.degree,param.lat.end.degree,param.lat.size)

################################################################################
# Read dimensional reference values
################################################################################

refFilePath 	= cwd + '/src/reference.txt'
dimRef 			= classes.Reference(refFilePath,param.depth.end)

################################################################################
# Load slab model (top and bottom surfaces)
################################################################################

resampledTopModelPath 	= './out/Slab2-resampled/resampled-top.'
resampledBotModelPath 	= './out/Slab2-resampled/resampled-bot.'
interfacesPath		= './out/interfaces.txt'

interfaces 	= open(interfacesPath,"w")
# lowerMantle, mantle, oceanicLit, oceanicCrust, mantle, lubricantLayer, transLit, continentalLit, transCrust, continentalCrust
stringFac 		= \
	str(dimRef.lowerMantleFactor) 			+ " " + \
	str(dimRef.mantleFactor) 				+ " " + \
	str(dimRef.oceanicLitFactor) 			+ " " + \
	str(dimRef.oceanicCrustFactor) 			+ " " + \
	str(dimRef.lowerMantleFactor) 			+ " " + \
	str(dimRef.mantleFactor) 				+ " " + \
	str(dimRef.lubricantLayerFactorBot) 	+ " " + \
	str(dimRef.lubricantLayerFactorTop)		+ " " + \
	str(dimRef.transitionalLitFactor) 		+ " " + \
	str(dimRef.continentalLitFactor)		+ " " + \
	str(dimRef.transitionalCrustFactor) 	+ " " + \
	str(dimRef.continentalCrustFactor)
stringDensity = \
	str(dimRef.lowerMantleDensity) 			+ " " + \
	str(dimRef.mantleDensity) 				+ " " + \
	str(dimRef.oceanicLitDensity) 			+ " " + \
	str(dimRef.oceanicCrustDensity) 		+ " " + \
	str(dimRef.lowerMantleDensity) 			+ " " + \
	str(dimRef.mantleDensity) 				+ " " + \
	str(dimRef.lubricantLayerDensityBot)	+ " " + \
	str(dimRef.lubricantLayerDensityTop)	+ " " + \
	str(dimRef.transitionalLitDensity) 		+ " " + \
	str(dimRef.continentalLitDensity) 		+ " " + \
	str(dimRef.transitionalCrustDensity) 	+ " " + \
	str(dimRef.continentalCrustDensity)
stringRadHeat = \
	str(dimRef.lowerMantleRadHeat) 			+ " " + \
	str(dimRef.mantleRadHeat) 				+ " " + \
	str(dimRef.oceanicLitRadHeat) 			+ " " + \
	str(dimRef.oceanicCrustRadHeat) 		+ " " + \
	str(dimRef.lowerMantleRadHeat) 			+ " " + \
	str(dimRef.mantleRadHeat) 				+ " " + \
	str(dimRef.lubricantLayerRadHeatBot)	+ " " + \
	str(dimRef.lubricantLayerRadHeatTop)	+ " " + \
	str(dimRef.transitionalLitRadHeat) 		+ " " + \
	str(dimRef.continentalLitRadHeat) 		+ " " + \
	str(dimRef.transitionalCrustRadHeat) 	+ " " + \
	str(dimRef.continentalCrustRadHeat)

interfaces.write(stringFac+"\n")
interfaces.write(stringDensity+"\n")
interfaces.write(stringRadHeat+"\n")

################################################################################
# Load continent contour
################################################################################

map = gp.read_file('/Users/kugelblitz/Desktop/initialTemperature/src/lim_pais_a.shp')

################################################################################
# Load LAB depth and Moho depth
################################################################################

lithoPath 	= cwd + '/out/litho.txt'
litho 		= np.loadtxt(lithoPath,unpack=True)
litho 		= litho * (-1)

crustPath 	= cwd + '/out/crust.txt'
crust 		= np.loadtxt(crustPath,unpack=True)
crust 		= crust * (-1)

for i in xrange(param.lon.size):
	for j in xrange(param.lat.size):
		if (crust[i,j]>-dimRef.maxOceLitThk):
			crust[i,j] = -dimRef.maxOceLitThk
		else:
			pass

################################################################################
# Main Latitude Loop
################################################################################

print("*** Tracing interfaces ***")
bar = pbar.Bar(param.lat.size)
bar.start()

inter0		= np.zeros(shape=np.shape(x))
inter1 		= np.zeros(shape=np.shape(x))
inter2 		= np.zeros(shape=np.shape(x))
inter3 		= np.zeros(shape=np.shape(x))
interA 		= np.zeros(shape=np.shape(x))
inter4 		= np.zeros(shape=np.shape(x))
inter5		= np.zeros(shape=np.shape(x))
inter6 		= np.zeros(shape=np.shape(x))
inter7 		= np.zeros(shape=np.shape(x))
inter8 		= np.zeros(shape=np.shape(x))
inter9 		= np.zeros(shape=np.shape(x))

for j in xrange(param.lat.size): # Change the latitude limit to see others

	############################################################################
	# Make inicial adjustments and interpolations
	############################################################################
	
	for i in xrange(np.size(longitude)):
		if (longitude[i]>180.):
			longitude[i] = longitude[i] - 360.
	
	aux1,aux2 		= np.round(param.lat.start.degree*100,0),np.round(j*param.lat.delta.degree*100,0)
	slabTopFile 	= resampledTopModelPath + str(int(aux1+aux2)).zfill(5)
	slabTop 		= classes.Surface(np.loadtxt(slabTopFile),0,1)
	slabBottomFile	= resampledBotModelPath + str(int(aux1+aux2)).zfill(5)
	slabBottom 		= classes.Surface(np.loadtxt(slabBottomFile),0,1)
	
	# Slab Top
	condTop			= (fieldLon>=slabTop.abscissa[0])&(fieldLon<=slabTop.abscissa[-1])
	lonTop 			= fieldLon[condTop]
	depthTop		= np.interp(lonTop,slabTop.abscissa,slabTop.ordinate)
	
	# Slab Bottom
	condBottom		= (fieldLon>=slabBottom.abscissa[0])&(fieldLon<=slabBottom.abscissa[-1])
	lonBottom		= fieldLon[condBottom]
	depthBottom		= np.interp(lonBottom,slabBottom.abscissa,slabBottom.ordinate)
	
	# Lubrincant Layer (Top)
	abs,ord = methods.makeOutwardProjection(slabTop.abscissa.size,slabTop.abscissa,slabTop.ordinate,dimRef.lubricantLayerThickness)
	cond			= (fieldLon>=abs[0])&(fieldLon<=abs[-1])
	lonLub			= fieldLon[cond]
	depthLub		= np.interp(lonLub,abs,ord)
	for i in xrange(np.size(lonLub)):
		if (depthLub[i]>0):
			depthLub[i] = 0.0
		if (depthLub[i]<litho[fieldLon==lonLub[i],j]):
			depthLub[i] = litho[fieldLon==lonLub[i],j]

	# Oceanic Crust (Bottom)
	auxOceanicDepth = crust[fieldLon<=slabTop.abscissa[0],j]
	oceanicDepth 	= dimRef.slabOceCrustDepth * (-1)
	abs,ord = methods.makeInnwardProjection(slabTop.abscissa.size,slabTop.abscissa,slabTop.ordinate,oceanicDepth)
	cond 			= (fieldLon>=abs[0])&(fieldLon<=abs[-1])
	lonOCrust 		= fieldLon[cond]
	depthOCrust		= np.interp(lonOCrust,abs,ord)

	# Points inside continent
	isContinent = np.zeros(np.size(fieldLon))
	for i in xrange(np.size(isContinent)):
		point = Point(longitude[i],latitude[j])
		if (map.contains(point)).any():
			isContinent[i] = 1
		else:
			isContinent[i] = 0

	############################################################################
	# Make interfaces
	############################################################################

	# Interface 1: Mantle bottom
	interface1 = np.copy(litho[:,j])
	interface1[(fieldLon>=lonBottom[0])&(fieldLon<=lonBottom[-1])] = depthBottom
	if (lonBottom[-1]<lonTop[-1]):
		cond 	= (fieldLon>=lonBottom[-1])&(fieldLon<=lonTop[-1])
		auxLon 	= fieldLon[cond]
		auxDep 	= np.interp(auxLon,[lonBottom[-1],lonTop[-1]],[depthBottom[-1],depthTop[-1]])
		interface1[(fieldLon>=auxLon[0])&(fieldLon<=auxLon[-1])] = auxDep

	# Interface 0: Lower Mantle - Astenosphere Boudary
	interface0 = np.copy(interface1)
	for i in xrange(np.size(interface0)):
		if (interface0[i]>dimRef.TZdepth):
			interface0[i] = dimRef.TZdepth

	# Interface 2: Oceanic Lithosphere Top
	interface2 = np.copy(interface1)
	interface2[isContinent==0] = crust[isContinent==0,j]
	interface2[(fieldLon>=lonOCrust[0])&(fieldLon<=lonOCrust[-1])] = depthOCrust
	
	if (lonBottom[-1]<=lonTop[-1]):
		cond 	= (fieldLon>=lonOCrust[-1])&(fieldLon<=lonTop[-1])
		auxLon 	= fieldLon[cond]
		auxDep 	= np.interp(auxLon,[lonOCrust[-1],lonTop[-1]],[depthOCrust[-1],depthTop[-1]])
		interface2[(fieldLon>=auxLon[0])&(fieldLon<=auxLon[-1])] = auxDep
	else:
		cond 	= (fieldLon>=lonOCrust[-1])&(fieldLon<=lonTop[-1])
		auxLon 	= fieldLon[cond]
		auxDep 	= np.interp(auxLon,[lonOCrust[-1],lonTop[-1]],[depthOCrust[-1],depthTop[-1]])
		interface2[(fieldLon>=auxLon[0])&(fieldLon<=auxLon[-1])] = auxDep
	
	# Interface 3: Oceanic Crust Top
	interface3 = np.copy(interface2)
	interface3[isContinent==0] = 0.0
	interface3[(fieldLon>=lonTop[0])&(fieldLon<=lonTop[-1])] = depthTop

	if (lonBottom[-1]>=lonTop[-1]):
		cond	= (fieldLon>=lonTop[-1])&(fieldLon<=lonBottom[-1])
		auxLon 	= fieldLon[cond]
		auxDep 	= np.interp(auxLon,[lonOCrust[-1],lonTop[-1]],[depthOCrust[-1],depthTop[-1]])
		interface3[(fieldLon>=auxLon[0])&(fieldLon<=auxLon[-1])] = auxDep
	
	# Interface A: Lower Mantle on the Slab
	interfaceA = np.copy(interface3)
	for i in xrange(np.size(interfaceA)):
		if (interfaceA[i]<dimRef.TZdepth):
			interfaceA[i] = dimRef.TZdepth
	
	# Interface 4: Lubrincat Layer (Bot)
	interface4 = np.copy(interface3)
	for i in xrange(np.size(interface4)):
		if ((fieldLon[i]>=lonTop[0]) and (fieldLon[i]<=lonTop[-1])):
			interface4[i] = np.max([depthTop[lonTop==fieldLon[i]],litho[i,j]])
		elif ((fieldLon[i]>lonTop[-1]) and (isContinent[i]==1)):
			interface4[i] = litho[i,j]
		else:
			interface4[i] = 0.

	# Interface 6: Lubricant Layer
	interface6 = np.copy(interface4)
	interface6[fieldLon<lonLub[0]] = 0.
	interface6[(fieldLon>lonLub[-1])&(isContinent==1)] = litho[(fieldLon>lonLub[-1])&(isContinent==1),j]
	interface6[(fieldLon>lonLub[-1])&(isContinent==0)] = 0.
	interface6[(fieldLon>=lonLub[0])&(fieldLon<=lonLub[-1])] = depthLub

	# Interface 5: Lubrincat Layer (Top)
	lubricantUpperDepth = -52.
	interface5 = np.copy(interface4)
	for i in xrange(np.size(interface5)):
		if ((fieldLon[i]>=lonTop[0]) and (fieldLon[i]<=lonTop[-1])):
			auxMax1 = np.max([depthTop[lonTop==fieldLon[i]],lubricantUpperDepth])
			auxMax2 = np.min([lubricantUpperDepth,interface6[i]])
			interface5[i] = np.min([auxMax1,auxMax2])
		elif ((fieldLon[i]>lonTop[-1]) and (isContinent[i]==1)):
			interface5[i] = litho[i,j]
		else:
			interface5[i] = 0.

	# Limit between contact litosphere and ordinary litosphere
	auxContinentLimit 	= fieldLon[interface6==litho[:,j]]
	continentalLimit 	= auxContinentLimit[0]

	# Interface 6: Contact Continental Bottom
	interface7 = np.copy(interface6)
	for i in xrange(np.size(interface7)):
		if (fieldLon[i]<continentalLimit):
			val1 = interface6[i]
			val2 = crust[i,j]
			interface7[i] = np.max([val1,val2])

	# Interface 7: Continental Crust
	interface8 = np.copy(interface7)
	for i in xrange(np.size(interface8)):
		if (isContinent[i]==1) and (fieldLon[i]>=continentalLimit):
			interface8[i] = crust[i,j]

	# Interface 9: Contac Continental Top
	interface9 = np.copy(interface8)
	for i in xrange(np.size(interface9)):
		if (fieldLon[i]<continentalLimit):
			val1 = interface8[i]
			val2 = 0
			interface9[i] = np.max([val1,val2])

	############################################################################
	# Save horizons
	############################################################################

	for i in xrange(fieldLon.size):
		if (interface0[i]>interface1[i]):
			interface0[i] = interface1[i]
		if (interface0[i]<param.depth.end):
			interface0[i] = param.depth.end

		if (interface1[i]>interface2[i]):
			interface2[i] = interface1[i]
		if (interface1[i]<param.depth.end):
			interface1[i] = param.depth.end

		if (interface2[i]>interface3[i]):
			interface3[i] = interface2[i]
		if (interface2[i]<param.depth.end):
			interface2[i] = param.depth.end
		
		if (interface3[i]>interfaceA[i]):
			interfaceA[i] = interface3[i]
		if (interface3[i]<param.depth.end):
			interface3[i] = param.depth.end

		if (interfaceA[i]>interface4[i]):
			interface4[i] = interfaceA[i]
		if (interfaceA[i]<param.depth.end):
			interfaceA[i] = param.depth.end

		if (interface4[i]>interface5[i]):
			interface5[i] = interface4[i]
		if (interface4[i]<param.depth.end):
			interface4[i] = param.depth.end
		
		if (interface5[i]>interface6[i]):
			interface6[i] = interface5[i]
		if (interface5[i]<param.depth.end):
			interface5[i] = param.depth.end
	
		if (interface6[i]>interface7[i]):
			interface7[i] = interface6[i]
		if (interface6[i]<param.depth.end):
			interface6[i] = param.depth.end
						
		if (interface7[i]>interface8[i]):
			interface8[i] = interface7[i]
		if (interface7[i]<param.depth.end):
			interface7[i] = param.depth.end
			
		if (interface8[i]>interface9[i]):
			interface9[i] = interface8[i]
		if (interface8[i]<param.depth.end):
			interface8[i] = param.depth.end
		
		inter0[j,:] = interface0
		inter1[j,:] = interface1
		inter2[j,:] = interface2
		inter3[j,:] = interface3
		interA[j,:] = interfaceA
		inter4[j,:] = interface4
		inter5[j,:] = interface5
		inter6[j,:] = interface6
		inter7[j,:] = interface7
		inter8[j,:] = interface8
		inter9[j,:] = interface9
		
		string = \
			str(format(interface0[i]*1000,'.10f')) + " " + \
			str(format(interface1[i]*1000,'.10f')) + " " + \
			str(format(interface2[i]*1000,'.10f')) + " " + \
			str(format(interface3[i]*1000,'.10f')) + " " + \
			str(format(interfaceA[i]*1000,'.10f')) + " " + \
			str(format(interface4[i]*1000,'.10f')) + " " + \
			str(format(interface5[i]*1000,'.10f')) + " " + \
			str(format(interface6[i]*1000,'.10f')) + " " + \
			str(format(interface7[i]*1000,'.10f')) + " " + \
			str(format(interface8[i]*1000,'.10f')) + " " + \
			str(format(interface9[i]*1000,'.10f')) + "\n"
		interfaces.write(string)

	bar.update(j)

bar.end()

if(param.dim!='2d'):
	from mayavi import mlab
	mlab.mesh(x,y,inter1,color=(045./255,078./255,095./255))
	mlab.mesh(x,y,inter2,color=(051./255,121./255,180./255))
	mlab.mesh(x,y,inter3,color=(064./255,155./255,248./255))
	mlab.mesh(x,y,interA,color=(045./255,078./255,095./255))
	mlab.mesh(x,y,inter4,color=(045./255,078./255,095./255))
	mlab.mesh(x,y,inter5,color=(088./255,181./255,170./255))
	mlab.mesh(x,y,inter6,color=(167./255,194./255,081./255))
	mlab.mesh(x,y,inter7,color=(252./255,231./255,140./255))
	mlab.mesh(x,y,inter8,color=(245./255,193./255,127./255))
	mlab.mesh(x,y,inter9,color=(239./255,132./255,100./255))
	mlab.outline(extent=[np.min(x),np.max(x),np.min(y),np.max(y),param.depth.end,param.depth.start])
	mlab.show()

else:
	plt.clf()
	plt.close('all')
	scale = 2.2
	
	plt.rcParams.update({'font.size': 12})
	f_size = 10
#	fig, ax = plt.subplots(figsize=(7*scale,2.*scale))
	width, height 	= 14, 4
	fig, ax = plt.subplots(figsize=(width,height))
	#		fig,ax = plt.subplots(figsize=(16,2.55))
	#		fig,ax = plt.subplots(figsize=(12,7))
	#		ax.set_aspect(1.0)
	ax.set_xlim(param.lon.start.km,param.lon.end.km)
	#		ax.set_xlim(500,2000)
#	plt.xticks(np.linspace(500,2000,6))
	dKmTicks = 250
	xticks_km = np.arange(0,np.round(param.lon.end.km,-1),dKmTicks)
	if (param.lon.end.km-xticks_km[-1])<dKmTicks/2.:
		xticks_km = np.delete(xticks_km,-1)
		xticks_km = np.append(xticks_km,param.lon.end.km)
	else:
		xticks_km = np.append(xticks_km,param.lon.end.km)
	plt.xticks(xticks_km)
#		plt.plot(resampled-top.abscissa,resampled-top.ordinate)

#	plt.plot(fieldLon,interface1,linewidth=0.4) # mantle (slab2 bot) - oce.lit
#	plt.plot(fieldLon,interface2,linewidth=0.4) # oce.lit - oce.crust
#	plt.plot(fieldLon,interface3,linewidth=0.4) # oce.crust - mantle (slab2 top)
#	plt.plot(fieldLon,interface4,linewidth=0.4) # mantle (slab2 top) - lower lubrincant layer
#	plt.plot(fieldLon,interface5,linewidth=0.4) # lower lub. layer - upper lub. layer
#	plt.plot(fieldLon,interface6,linewidth=0.4) # upper lub. layer - lower trans. cont. lit
#	plt.plot(fieldLon,interface7,linewidth=0.4) # lower trans. cont. lit - lower cont. lit
#	plt.plot(fieldLon,interface8,linewidth=0.4) # lower cont. lit - upper trans. cont. crust
#	plt.plot(fieldLon,interface9,linewidth=0.4)

	plt.fill_between(fieldLon,interface0,param.depth.end,color=(015./255,058./255,065./255),label="Lower Mantle")
	plt.fill_between(fieldLon,interface1,interface0		,color=(045./255,078./255,095./255),label="Mantle")
	plt.fill_between(fieldLon,interface2,interface1		,color=(051./255,121./255,180./255),label="Oceanic Lithosphere")
	plt.fill_between(fieldLon,interface3,interface2		,color=(064./255,155./255,248./255),label="Oceanic Crust")
	plt.fill_between(fieldLon,interfaceA,interface3		,color=(015./255,058./255,065./255))
	plt.fill_between(fieldLon,interface4,interfaceA		,color=(045./255,078./255,095./255))
	plt.fill_between(fieldLon,interface5,interface4		,color=(088./255,181./255,170./255),label="Lower Lubricant Layer")
	plt.fill_between(fieldLon,interface6,interface5		,color=(167./255,194./255,081./255),label="Upper Lubricant Layer")
	plt.fill_between(fieldLon,interface7,interface6		,color=(252./255,231./255,140./255),label="Contact Litosphere")
	plt.fill_between(fieldLon,interface8,interface7		,color=(245./255,193./255,127./255),label="Continental Lithosphere")
	plt.fill_between(fieldLon,interface9,interface8		,color=(239./255,132./255,100./255),label="Contact Crust")
	plt.fill_between(fieldLon,0			,interface9		,color=(190./255,085./255,081./255),label="Continental Crust")

#	plt.fill_between(fieldLon,interface1,param.depth.end,color=(045./255,078./255,095./255),label="Mantle")
#	plt.fill_between(fieldLon,interface2,interface1		,color=(051./255,121./255,180./255),label="Oceanic Lithosphere")
#	plt.fill_between(fieldLon,interface3,interface2		,color=(064./255,155./255,248./255),label="Oceanic Crust")
#	plt.fill_between(fieldLon,interface4,interface3		,color=(045./255,078./255,095./255))
#	plt.fill_between(fieldLon,interface5,interface4		,color=(088./255,181./255,170./255),label="Lower Lubricant Layer")
#	plt.fill_between(fieldLon,interface6,interface5		,color=(167./255,194./255,081./255),label="Upper Lubricant Layer")
##	plt.fill_between(fieldLon,interface7,interface6		,color=(245./255,193./255,127./255))
#	plt.fill_between(fieldLon,interface8,interface6		,color=(245./255,193./255,127./255),label="Continental Lithosphere")
##	plt.fill_between(fieldLon,interface9,interface8		,color=(190./255,085./255,081./255))
#	plt.fill_between(fieldLon,0			,interface8		,color=(190./255,085./255,081./255),label="Continental Crust")

	plt.legend(loc=3,fontsize=f_size)
	plt.ylim(param.depth.end,param.depth.start)
	plt.xlabel("Distance (km)")
	plt.ylabel("Depth (km)")
	ax2 = ax.twiny()
	ax2.set_xlim(param.lon.start.degree,param.lon.end.degree)

	dDgTicks = 2.5
	xticks_dg = np.arange(param.lon.start.degree,param.lon.end.degree,dDgTicks)
	if (param.lon.end.degree-xticks_dg[-1])<dDgTicks:
		xticks_dg = np.delete(xticks_dg,-1)
		xticks_dg = np.append(xticks_dg,param.lon.end.degree)
	else:
		xticks_dg = np.append(xticks_dg,param.lon.end.degree)
	plt.xticks(xticks_dg)
	
	plt.xlabel("Longitude ($\!^\circ\!$)")
	plt.tight_layout()
	plt.savefig("./figures/interfaces.pdf")
	plt.show()

interfaces.close()
