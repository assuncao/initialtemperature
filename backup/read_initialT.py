import numpy as np
import re
import matplotlib.pyplot as plt
fileName = './out/Temper_0_3D.txt'



data 	= np.loadtxt(fileName,skiprows=4)
T 		= np.reshape(data,(251,2,71),order='F')

plt.contour(T[:,0,:].transpose())
plt.show()

