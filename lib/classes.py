import numpy as np
import methods, os, sys, re

################################################################################
# Setting up classes and methods
################################################################################

cwd = os.getcwd()
libsLocation = os.path.join(cwd,'lib')
sys.path.append(libsLocation)

################################################################################
# SubClass
################################################################################

class LengthUnit:
	def __init__(self,value,pos):
		self.degree = value
		self.km 	= methods.km(pos,self.degree)

class Parameters:
	def __init__(self,coord,flag):
		if (flag=="depth"):
			self.start 	= float(coord[0])
			self.end 	= float(coord[1])
			self.delta	= float(coord[2])
			self.size 	= int(np.round(np.abs((self.end-self.start)/self.delta)+1,0))
		else:
			self.start 	= LengthUnit(float(coord[0]),float(coord[0]))
			self.end 	= LengthUnit(float(coord[1]),self.start.degree)
			self.delta 	= LengthUnit(float(coord[2]),0.0)
			
			if (self.delta.degree>0):
				notZero = 'True'
			else:
				notZero = 'False'
		
			numerator 		= np.abs(np.round((self.end.degree-self.start.degree),4))
			denominator		= self.delta.degree if notZero else numerator
			self.size 	= int(numerator/denominator) + 1


################################################################################
# Parameters Class -> stores the parameters information for the simulation
################################################################################

class Dimension:
	def __init__(self,path):
		file = open(path)
		self.lon 		= Parameters(re.split("[ ]+", file.readline().strip()),"lon")
		self.lat 		= Parameters(re.split("[ ]+", file.readline().strip()),"lat")
		self.depth 		= Parameters(re.split("[ ]+", file.readline().strip()),"depth")
		self.dim		= re.split("[\t]+", file.readline().strip())[0]
		file.close()

################################################################################
# Slab
################################################################################

class Unit:
	def __init__(self,degree_start,degree_end,km_start,km_end,resolution):
		
		self.size	= int(np.round((degree_end-degree_start)/resolution,0))+1
		self.degree = np.linspace(degree_start,degree_end,self.size)
		self.km		= np.linspace(km_start,km_end,self.size)

class Slab:
	def __init__(self,path,resolution):
		param 		= Dimension(path)
		self.lon = Unit(param.lon.start.degree,param.lon.end.degree,param.lon.start.km,param.lon.end.km,param.lon.size)
		self.lat = Unit(param.lat.start.degree,param.lat.end.degree,param.lat.start.km,param.lat.end.km,param.lat.size)
		
#		pass

################################################################################
# Model
################################################################################

class Model:
	def __init__(self,data):
		self.abscissa	= data[:,0]
		self.distance	= data[:,1]
		self.ordinate	= data[:,2]
		self.size 		= np.shape(data)[0]
		self.length		= 0.
		self.dlengths	= []
		for i in xrange(self.size-1):
			deltaA = methods.km(self.abscissa[i],self.abscissa[i+1])
			deltaO = self.ordinate[i+1] - self.ordinate[i]
			dx = np.sqrt(pow(deltaA,2)+pow(deltaO,2))
			self.dlengths.append(dx)
			self.length = self.length + dx

################################################################################
# Surface
################################################################################

class Surface:
	def __init__(self,data,aux0,aux1):
#		aux0 = 0
#		aux0 = 1
#		aux1 = 1
#		aux1 = 2
		self.abscissa	= data[:,aux0]
		self.ordinate	= data[:,aux1]
		self.size		= np.shape(data)[0]
		self.length		= 0
		self.dlengths	= []
		self.minA 		= data[0,aux0]
		self.maxA 		= data[-1,aux0]
		self.minO		= np.min(data[:,aux1])
		self.maxO		= np.max(data[:,aux1])
		self.distance	= np.zeros(self.size)
		for i in xrange(self.size-1):
			deltaA 				= self.abscissa[i+1] - self.abscissa[i]
			deltaO 				= self.ordinate[i+1] - self.ordinate[i]
			dx 					= np.sqrt(pow(deltaA,2)+pow(deltaO,2))
			self.dlengths.append(dx)
			self.length 		= self.length + dx
			self.distance[i+1] 	= self.length

################################################################################
# Spherical coordinate
################################################################################

class Shell:
	def __init__(self,params):
		self.R = 6371000.0
		# Lon
		self.startAzimuth		= params[0].startDegree * np.pi / 180.
		self.endAzimuth			= params[0].endDegree * np.pi / 180.
		self.sizeAzimuth		= params[0].length
		# Colat
		self.startInclination	= (np.pi/2.) - (params[1].startDegree*np.pi/180.)
		self.endInclination		= (np.pi/2.) - (params[1].endDegree*np.pi/180.)
		self.sizeInclination 	= params[1].length
		# R
		self.topR				= self.R + (params[2].start*1000.0)
		self.botR				= self.R + (params[2].end*1000.0)
		self.sizeR 				= params[2].length
		
		self.azi = np.linspace(self.startAzimuth,self.endAzimuth,self.sizeAzimuth)
		self.inc = np.linspace(self.startInclination,self.endInclination,self.sizeInclination)
		self.r = np.linspace(self.topR,self.botR,self.sizeR)
		
		self.inclination, self.azimuth, self.R = np.meshgrid(self.inc,self.azi,self.r)

		self.x = self.R * np.sin(self.inclination) * np.cos(self.azimuth)
		self.y = self.R * np.sin(self.inclination) * np.sin(self.azimuth)
		self.z = self.R * np.cos(self.inclination)

################################################################################
# Reference values for simulation
################################################################################

class Reference:
	def __init__(self,refFilePath,depth):
		# Reading reference.txt file
		refValues 	= []
		file 		= open(refFilePath)
		for line in file:
			refValues.append(line)
		file.close()
		
		self.length = np.shape(refValues)[0]
		for i in xrange(self.length):
			data = re.split("[ ]+", refValues[i].strip())
			if (data[0]=="LABtemperatureC"):
				self.LABtemperatureC = float(data[1])
			elif (data[0]=="TZtemperatureC"):
				self.TZtemperatureC = float(data[1])
			elif (data[0]=="TZdepth"):
				self.TZdepth = float(data[1])
			elif (data[0]=="alpha"):
				self.alpha = float(data[1])
			elif (data[0]=="gravity"):
				self.gravity = float(data[1])
			elif (data[0]=="specificHeat"):
				self.specificHeat = float(data[1])
			elif (data[0]=="niter"):
				self.niter = int(data[1])
			elif (data[0]=="tm"):
				self.tm = float(data[1])
			elif (data[0]=="kappa"):
				self.kappa = float(data[1])
			elif (data[0]=="slabVelocity"):
				self.slabVelocity = float(data[1])
			elif (data[0]=="referenceSlabThickness"):
				self.referenceSlabThickness = float(data[1])
			elif (data[0]=="referenceSlabThkScale"):
				self.referenceSlabThkScale = float(data[1])
			elif (data[0]=="lubricantLayerThickness"):
				self.lubricantLayerThickness = float(data[1])
			elif (data[0]=="minConLitThk"):
				self.minConLitThk = float(data[1])
			elif (data[0]=="maxOceLitThk"):
				self.maxOceLitThk = float(data[1])
			elif (data[0]=="slabOceCrustDepth"):
				self.slabOceCrustDepth = float(data[1])
			elif (data[0]=="maxSlabDepth"):
				self.maxSlabDepth = float(data[1])
			elif (data[0]=="flatLength"):
				self.flatLength = float(data[1])
			elif (data[0]=="maxFlatDepth"):
				self.maxFlatDepth = float(data[1])
			elif (data[0]=="dxRidge"):	# Ridge ramp horizontal length
				self.dxRidge = float(data[1])
			elif (data[0]=="dxBorder"):	# Distance from ridge ramp to the lithosphere
				self.dxBorder = float(data[1])
			
			elif (data[0]=="atmosphereDraw"):
				self.atmosphereDraw = data[1]
			elif (data[0]=="oceanicLitDraw"):
				self.oceanicLitDraw = data[1]
			elif (data[0]=="oceanicCrustDraw"):
				self.oceanicCrustDraw = data[1]
			elif (data[0]=="continentalLitDraw"):
				self.continentalLitDraw = data[1]
			elif (data[0]=="continentalCrustDraw"):
				self.continentalCrustDraw = data[1]
			elif (data[0]=="transitionalLitDraw"):
				self.transitionalLitDraw = data[1]
			elif (data[0]=="transitionalCrustDraw"):
				self.transitionalCrustDraw = data[1]
			elif (data[0]=="lubricantLayerTopDraw"):
				self.lubricantLayerTopDraw = data[1]
			elif (data[0]=="lubricantLayerBotDraw"):
				self.lubricantLayerBotDraw = data[1]
			elif (data[0]=="mantleDraw"):
				self.mantleDraw = data[1]
			elif (data[0]=="lowerMantleDraw"):
				self.lowerMantleDraw = data[1]
			
			elif (data[0]=="atmosphereDensity"):
				self.atmosphereDensity = float(data[1])
			elif (data[0]=="oceanicLitDensity"):
				self.oceanicLitDensity = float(data[1])
			elif (data[0]=="oceanicCrustDensity"):
				self.oceanicCrustDensity = float(data[1])
			elif (data[0]=="continentalLitDensity"):
				self.continentalLitDensity = float(data[1])
			elif (data[0]=="continentalCrustDensity"):
				self.continentalCrustDensity = float(data[1])
			elif (data[0]=="transitionalLitDensity"):
				self.transitionalLitDensity = float(data[1])
			elif (data[0]=="transitionalCrustDensity"):
				self.transitionalCrustDensity = float(data[1])
			elif (data[0]=="lubricantLayerDensityBot"):
				self.lubricantLayerDensityBot = float(data[1])
			elif (data[0]=="lubricantLayerDensityTop"):
				self.lubricantLayerDensityTop = float(data[1])
			elif (data[0]=="mantleDensity"):
				self.mantleDensity = float(data[1])
			elif (data[0]=="lowerMantleDensity"):
				self.lowerMantleDensity = float(data[1])
			
			elif (data[0]=="atmosphereFactor"):
				self.atmosphereFactor = float(data[1])
			elif (data[0]=="oceanicLitFactor"):
				self.oceanicLitFactor = float(data[1])
			elif (data[0]=="oceanicCrustFactor"):
				self.oceanicCrustFactor = float(data[1])
			elif (data[0]=="continentalLitFactor"):
				self.continentalLitFactor = float(data[1])
			elif (data[0]=="continentalCrustFactor"):
				self.continentalCrustFactor = float(data[1])
			elif (data[0]=="transitionalLitFactor"):
				self.transitionalLitFactor = float(data[1])
			elif (data[0]=="transitionalCrustFactor"):
				self.transitionalCrustFactor = float(data[1])
			elif (data[0]=="lubricantLayerFactorBot"):
				self.lubricantLayerFactorBot = float(data[1])
			elif (data[0]=="lubricantLayerFactorTop"):
				self.lubricantLayerFactorTop = float(data[1])
			elif (data[0]=="mantleFactor"):
				self.mantleFactor = float(data[1])
			elif (data[0]=="lowerMantleFactor"):
				self.lowerMantleFactor = float(data[1])
			
			elif (data[0]=="atmosphereRadHeat"):
				self.atmosphereRadHeat = float(data[1])
			elif (data[0]=="oceanicLitRadHeat"):
				self.oceanicLitRadHeat = float(data[1])
			elif (data[0]=="oceanicCrustRadHeat"):
				self.oceanicCrustRadHeat = float(data[1])
			elif (data[0]=="continentalLitRadHeat"):
				self.continentalLitRadHeat = float(data[1])
			elif (data[0]=="continentalCrustRadHeat"):
				self.continentalCrustRadHeat = float(data[1])
			elif (data[0]=="transitionalLitRadHeat"):
				self.transitionalLitRadHeat = float(data[1])
			elif (data[0]=="transitionalCrustRadHeat"):
				self.transitionalCrustRadHeat = float(data[1])
			elif (data[0]=="lubricantLayerRadHeatTop"):
				self.lubricantLayerRadHeatTop = float(data[1])
			elif (data[0]=="lubricantLayerRadHeatBot"):
				self.lubricantLayerRadHeatBot = float(data[1])
			elif (data[0]=="mantleRadHeat"):
				self.mantleRadHeat = float(data[1])
			elif (data[0]=="lowerMantleRadHeat"):
				self.lowerMantleRadHeat = float(data[1])
			else:
				pass
	
		self.referenceSlabThickness = self.referenceSlabThickness * self.referenceSlabThkScale
		self.LABtemperatureK 	= self.LABtemperatureC + 273.15
		self.TZtemperatureK 	= self.TZtemperatureC + 273.15
		self.POTtemperatureK 	= self.TZtemperatureK / (np.exp((-1)*self.TZdepth*1000*self.alpha*self.gravity/self.specificHeat))
		self.POTtemperatureC 	= self.POTtemperatureK - 273.15
		self.maxTempK			= self.POTtemperatureK * (np.exp((-1)*depth*1000*self.alpha*self.gravity/self.specificHeat))
		self.maxTempC			= self.maxTempK - 273.15
		self.slabVelocitySI		= self.slabVelocity * 1.0E3 / (60.0*60.0*24.0*365.25)
		self.R 					= self.slabVelocity * self.referenceSlabThickness / (2 * self.kappa)
