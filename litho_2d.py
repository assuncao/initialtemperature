import numpy as np
import matplotlib.pyplot as plt
from scipy import interpolate, ndimage
import geopandas as gp
import scipy.ndimage

from time import time, clock
start_time = time()

import os, sys, re

from shapely.geometry import Point
from shapely.geometry.polygon import Polygon

rampLength = 200.0E3 # m

################################################################################
# Start up permissions
################################################################################

makeMap 	= 'False'
rampDraw	= 'False'
showPlot	= 'False'

################################################################################
# Setting up classes and methods
################################################################################

cwd = os.getcwd()
libsLocation = os.path.join(cwd,'lib')
sys.path.append(libsLocation)

import classes, methods
import progressBar as pbar

################################################################################
# Read data from ./src/params.txt
################################################################################

print("*** Reading initial parameters *** ")
parPath = cwd + '/src/params.txt'
param 	= classes.Dimension(parPath)

#param.lat.size = 1

fieldTemperature 	= np.zeros(shape=(param.lon.size,param.lat.size,param.depth.size))
fieldLon			= np.linspace(param.lon.start.km,param.lon.end.km,param.lon.size)
fieldLat			= np.linspace(param.lat.start.km,param.lat.end.km,param.lat.size)
fieldDepth			= np.linspace(param.depth.start,param.depth.end,param.depth.size)
auxFieldDepth		= np.zeros(np.size(fieldDepth))

################################################################################
# Read dimensional reference values
################################################################################

refFilePath 	= cwd + '/src/reference.txt'
dimRef 			= classes.Reference(refFilePath,param.depth.end)

################################################################################
# Read LITHO1.0 model -> Astenosphere Top
################################################################################

print("*** Reading LITHO1.0 model ***")

modelFolderPath = '/Users/kugelblitz/Documents/USP/models/LITHO1.0/litho1.0/LITHO1.0/litho_model/'

icos = np.loadtxt(modelFolderPath+'Icosahedron_Level7_LatLon_mod.txt')
icos = np.array(icos)

astheno_top	= np.zeros(shape=np.shape(icos))
mantle_top	= np.zeros(shape=np.shape(icos))

lbar = pbar.Bar(np.shape(icos)[0]+1)
lbar.start()

for i in xrange(1,np.shape(icos)[0]+1):
	lbar.update(i)
	
	nodeFileName = 'node' + str(i) + '.model'
	file = open(modelFolderPath+nodeFileName,'r')
	
	for line in file:
		line = line.split()
		if (line[-1]=='ASTHENO-TOP'):
			# lat
			astheno_top[i-1,0] = icos[i-1,0]
			# lon
			astheno_top[i-1,1] = icos[i-1,-1]
			astheno_top[i-1,2] = float(line[0])
		if (line[-1]=='LID-TOP'):
			# lat
			mantle_top[i-1,0] = icos[i-1,0]
			# lon
			mantle_top[i-1,1] = icos[i-1,-1]
			mantle_top[i-1,2] = float(line[0])
	file.close()

lbar.end()

################################################################################
# Dimensional data
################################################################################

if (makeMap=='True'):
	minlon, maxlon = -95., -25.
	minlat, maxlat = -45., 15.
	m = int(abs((minlon-maxlon)/0.5)) + 1
	n = int(abs((minlat-maxlat)/0.5)) + 1
else:
	m, n = param.lon.size, param.lat.size
	minlon, maxlon = param.lon.start.degree, param.lon.end.degree
	minlat, maxlat = param.lat.start.degree, param.lat.end.degree

################################################################################
# Resample data
################################################################################

print("*** Resampling LITHO1.0 and CRUST ***")
method 			= 'cubic'
X, Y 			= np.mgrid[minlon:maxlon:m*1j, minlat:maxlat:n*1j]

pointsLitho 	= np.column_stack((astheno_top[:,1],astheno_top[:,0]))
valuesLitho 	= astheno_top[:,2] / 1.0E3
ZLitho 			= interpolate.griddata(pointsLitho,valuesLitho,(X, Y),method=method)

pointsCrust		= np.column_stack((mantle_top[:,1],mantle_top[:,0]))
valuesCrust		= mantle_top[:,2] / 1.0E3
ZCrust			= interpolate.griddata(pointsCrust,valuesCrust,(X,Y),method=method)

oceanOrContinent = np.zeros(shape=(param.lon.size,param.lat.size))

if (param.dim=='2d'):
	ZLitho[:,-1] = ZLitho[:,0]
	ZCrust[:,-1] = ZCrust[:,0]

################################################################################
# Progress bar
################################################################################

bar0 = pbar.Bar(np.size(oceanOrContinent))
bar1 = pbar.Bar(np.size(ZLitho))
bar2 = pbar.Bar(np.size(ZCrust))

################################################################################
# Define minimun depth for the LAB interface
################################################################################

map = gp.read_file('/Users/kugelblitz/Desktop/initialTemperature/src/lim_pais_a.shp')

if (makeMap!='True'):
	
	print("*** Defining continetal and oceanic regions ***")
	bar0.start()
	count = 0
	
	for i in xrange(np.shape(oceanOrContinent)[0]):
		for j in xrange(np.shape(oceanOrContinent)[1]):
			count += 1
			bar0.update(count)
			point = Point(X[i,j],Y[i,j])
			if (map.contains(point)).any():
				oceanOrContinent[i,j] = int(1) # If it's continent, receives 1
			else:
				oceanOrContinent[i,j] = int(0) # If it's not continent, receives 0
	bar0.end()

	print("*** Setting LAB minimum depth to " + str(dimRef.minConLitThk) + " km ***")
	bar1.start()
	count = 0

	for i in xrange(np.shape(ZLitho)[0]):
		for j in xrange(np.shape(ZLitho)[1]):
			count += 1
			bar1.update(count)
			if (ZLitho[i,j]<=dimRef.minConLitThk):
#				point = Point(X[i,j],Y[i,j])
#				if (map.contains(point)).any():
				if (oceanOrContinent[i,j]==1):
					ZLitho[i,j] 	= dimRef.minConLitThk
				else:
					pass
			else:
				pass
	bar1.end()

	print("*** Setting Oceanic Crust maximum depth to " + str(dimRef.maxOceLitThk) + " km ***")
	bar2.start()
	count = 0

	for i in xrange(np.shape(ZCrust)[0]):
		for j in xrange(np.shape(ZCrust)[1]):
			count += 1
			bar2.update(count)
			if (ZCrust[i,j]>dimRef.maxOceLitThk):
#				point = Point(X[i,j],Y[i,j])
#				if (map.contains(point)).any():
				if (oceanOrContinent[i,j]==1):
					pass
				else:
					ZCrust[i,j] 	= dimRef.maxOceLitThk
#					if (ZCrust[,j]>)
			else:
				pass
	bar2.end()

################################################################################
# Define minimum depth to coincide with bottom of the Slab2
################################################################################

if (makeMap!='True'):
	refBotModelPath = './out/Slab2-resampled/resampled-bot.'
	refTopModelPath = './out/Slab2-resampled/resampled-top.'

	for j in xrange(np.shape(ZLitho)[1]):
		aux1,aux2 		= np.round(param.lat.start.degree*100,0),np.round(j*param.lat.delta.degree*100,0)

		refBotModelFile	= refBotModelPath + str(int(aux1+aux2)).zfill(5)
		refBot 			= classes.Surface(np.loadtxt(refBotModelFile),0,1)
		refTopModelFile	= refTopModelPath + str(int(aux1+aux2)).zfill(5)
		refTop 			= classes.Surface(np.loadtxt(refTopModelFile),0,1)

		# Where is the slab (top and bottom)
		condBot = (fieldLon>=refBot.abscissa[0])&(fieldLon<=refBot.abscissa[-1])
		auxCondBot = np.where(condBot)
		condTop = (fieldLon>=refTop.abscissa[0])&(fieldLon<=refTop.abscissa[-1])
		auxCondTop = np.where(condTop)

		auxZLitho = ZLitho[condBot,j]
		
		index = -1
		for i in xrange(np.size(auxCondBot)-1,-1,-1):
			zRefBot = np.abs(refBot.ordinate[i])
			if (zRefBot<auxZLitho[i]):
				index = i
				break

		for i in xrange(index,-1,-1):
			auxZLitho[i] = np.abs(refBot.ordinate[i])

		ZLitho[condBot,j] = auxZLitho

		if (index==-1):
			auxZLitho 	= ZLitho[condTop,j]
			auxCrust 	= ZCrust[condTop,j]
			
			for i in xrange(np.size(auxCondTop)-1,-1,-1):
				zRefTop = np.abs(refTop.ordinate[i])
				if (zRefTop<auxZLitho[i]):
					index = i
					break

			for i in xrange(index,-1,-1):
				auxZLitho[i] = np.max([np.abs(refTop.ordinate[i]),auxCrust[i]])

			ZLitho[condTop,j] = auxZLitho
				
		########################################################################
		# Minimum depth for the oceanic crust above the Slab
		########################################################################
		
		auxOceOrCont 	= oceanOrContinent[condTop,j]
		auxZCrust		= ZCrust[condTop,j]
		for i in xrange(np.size(auxCrust)):
			if (auxOceOrCont[i]==0): # if it's not continent
				auxZCrust[i] = np.min([np.abs(refTop.ordinate[i]),52])
			else:
				pass
		
		ZCrust[condTop,j] = auxZCrust

		########################################################################
		# Extrapolate bottom of the Slab2 to be the LAB at the ocean area
		########################################################################

		zDiff 	= np.abs(refBot.ordinate[1]-refBot.ordinate[0])
		refZ	= refBot.ordinate[0]
		
		for i in xrange(auxCondBot[0][0]-1,-1,-1):
			auxZ = np.abs(refZ) - zDiff
			if (ZLitho[i,j]<auxZ):
				ZLitho[i,j] = auxZ
				refZ = auxZ

################################################################################
# Oceanic Lithosphere Ramp (z=2.32*sqrt(kappa*t))
################################################################################

if (rampDraw=='True'):
	rampFactor 		= 2.32
	XX,YY			= np.mgrid[param.lon.start.km:param.lon.end.km:m*1j, param.lat.start.km:param.lat.end.km:n*1j]

	rampMaxCoord = np.zeros(param.lat.size)
	rampMaxZLith = np.zeros(param.lat.size)
	rampMaxZCrus = np.zeros(param.lat.size)
	rampTimeLith = np.zeros(param.lat.size)
	rampTimeCrus = np.zeros(param.lat.size)

	for j in xrange(param.lat.size):
		for i in xrange(param.lon.size):
			rampMaxCoord[j] = int(np.where(XX[:,j]<200.)[0][-1])
			idx = int(rampMaxCoord[j])
			rampMaxZLith[j] = ZLitho[idx,j]
			rampMaxZCrus[j] = ZCrust[idx,j]
			rampTimeLith[j] = pow(rampMaxZLith[j],2) / (pow(rampFactor,2)*dimRef.kappa)
			rampTimeCrus[j] = pow(rampMaxZCrus[j],2) / (pow(rampFactor,2)*dimRef.kappa)

	print(rampMaxCoord)
	print(rampMaxZLith)
	print(rampMaxZCrus)
	for j in xrange(param.lat.size):
		idx 		= int(rampMaxCoord[j])
		rampTime1 	= rampTimeLith[j] / float(idx)
		rampTime2	= rampTimeCrus[j] / float(idx)
		for i in xrange(idx):
			ZLitho[i,j] = rampFactor * np.sqrt(dimRef.kappa*i*rampTime1)
			ZCrust[i,j] = rampFactor * np.sqrt(dimRef.kappa*i*rampTime2)

################################################################################
# Filter Data
################################################################################

#ZLithoInterp = ndimage.filters.gaussian_filter(ZLitho,[1.1,1.1],mode='constant')

end_time = time()
execution_time = np.round(end_time-start_time,4)
print("*** Execution time: " + str(execution_time) + " seconds ***")

plt.clf()
plt.close('all')

#auxXTop = refTop.abscissa / 111. - 90.
#auxXBot = refBot.abscissa / 111. - 90.

################################################################################
# LAB Contour Plot
################################################################################

if (param.dim!='2d') or (makeMap=='True'):
	fig, ax = plt.subplots(figsize=(8,8))
	ax.set_aspect('equal')
	extent = [minlon,maxlon,minlat,maxlat]
	contour = plt.imshow(np.transpose(ZLitho),extent=extent,cmap='jet',origin='bottom',interpolation='none')
	map.plot(ax=ax,color='none',edgecolor='black') # Countries (color fill) and boundaries plot

	############################################################################
	# Color Bar
	############################################################################

	ticks 		= np.linspace(np.min(valuesLitho),np.max(valuesLitho),11)
	ticks[1:-1] = np.round(ticks[1:-1],-1)

	cbar = fig.colorbar(contour,orientation='vertical',ticks=ticks,aspect=30)
	cbar.ax.set_ylabel("LAB Depth (km)")

	ax.set_title('South America LAB depth')
	ax.set_xlabel(r'Longitude ($^\circ$)')
	ax.set_ylabel(r'Latitude ($^\circ$)')
	ax.set_ylim(minlat,maxlat)
	ax.set_xlim(minlon,maxlon)

	plt.savefig('./figures/lithos.pdf')
	if (showPlot=='True'):
		fig.show()

################################################################################
# Profile Plot
################################################################################

if (param.dim=='2d') and (makeMap!='True'):
	j = 0
	fig, ax = plt.subplots(figsize=(16,3))
	auxLat = -18.
	title = r'LAB Depth Profile at ' + str(auxLat) + '$^\circ$S'
	ax.set_title(title)

	ax.plot(X[:,j],ZLitho[:,j],label='LITHO1.0 Regular Grid for LAB Depth (nearest)')
	ax.plot(X[:,j],ZCrust[:,j],label='LITHO1.0 Regular Grid for Crust Depth (nearest)')
	plt.scatter(X[:,j],ZLitho[:,j])
	
#	plt.scatter(X[idxBotC,j],ZLitho[idxBotC,j])
#	ax.plot(X[:,j],ZTest[:,j],label='Test')
#	ax.plot(auxXBot,(-1)*refBot.ordinate,label='Slab Bot')
#	ax.plot(auxXTop,(-1)*refTop.ordinate,label='Slab Top')
#	ax.plot(X[:,j],ZCrust[:,j],label='LITHO1.0 Regular Grid for Crust Depth (nearest)')

	ax.set_ylim(np.max(ZLitho[:,j]),0)

	ax.set_xlabel('Longitude (in degrees)')
	ax.set_ylabel('LAB Depth (km)')

	plt.legend()

	plt.savefig('./figures/lithos_profile.pdf')
	if (showPlot=='True'):
		plt.show()

################################################################################
# Save to file
################################################################################

np.savetxt('./out/litho.txt',np.transpose(ZLitho))
np.savetxt('./out/crust.txt',np.transpose(ZCrust))

if (makeMap!='False'):
	file1 = open('./out/litho.xyz','w')
	file2 = open('./out/crust.xyz','w')
	for i in xrange(m):
		for j in xrange(n):
			string = np.str(X[i,j]) + " " + np.str(Y[i,j]) + " "
			file1.write(string+np.str(ZLitho[i,j])+"\n")
			file2.write(string+np.str(ZCrust[i,j])+"\n")
	file1.close()
	file2.close()




