import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import os, sys, re
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from matplotlib.patches import Rectangle

################################################################################
# Setting up classes and methods
################################################################################

cwd = '/Users/kugelblitz/Desktop/initialTemperature'#os.getcwd()
libsLocation = os.path.join(cwd,'lib')
sys.path.append(libsLocation)

dirname = os.getcwd().split('/')[-1] + '_'
fig_dir = 'figures/' + dirname


import classes
import progressBar as pbar

################################################################################
# Read data from ./src/params.txt
################################################################################

print("*** Reading initial parameters *** ")
parPath = './params.txt'
param 	= classes.Dimension(parPath)

param.lat.size = 1

TZ = -660

################################################################################
# Configure NPROC
################################################################################

skiprows, num_processes, aux_print_step = 3, 14, 0
print(sys.argv,np.size(sys.argv))
for i in xrange(1,np.size(sys.argv),2):
	if (sys.argv[i]=='-k'):
		skiprows = int(sys.argv[i+1])
	elif (sys.argv[i]=='-n'):
		num_processes = int(sys.argv[i+1])
	elif (sys.argv[i]=='-step'):
		aux_print_step = int(sys.argv[i+1])
	elif (sys.argv[i]=='-final'):
		final_step = int(sys.argv[i+1])

################################################################################
# Print parameters box (check up mistakes)
################################################################################

box = pbar.Box()

box.top()
box.mid("Chosen dimension:\t "+param.dim)
box.mid("Grid dimensions:\t ("+str(param.lon.size)+"x"+str(param.lat.size)+"x"+str(param.depth.size)+")")
box.mid("Longitude interval:\t ("+str(param.lon.start.degree)+","+str(param.lon.end.degree)+","+str(param.lon.delta.degree)+")")
box.mid("Longitude distance:\t ("+str(param.lon.start.km)+","+str(param.lon.end.km)+","+str(param.lon.delta.km)+")")
box.mid("Latitude interval:\t ("+str(param.lat.start.degree)+","+str(param.lat.end.degree)+","+str(param.lat.delta.degree)+")")
box.mid("Latitude distance:\t ("+str(param.lat.start.km)+","+str(param.lat.end.km)+","+str(param.lat.delta.km)+")")
box.mid("Depth interval:\t ("+str(param.depth.start)+","+str(param.depth.end)+","+str(param.depth.delta)+")")
box.top()
box.mid("Number of cores:\t "+str(num_processes))
box.mid("Skip headers:\t\t "+str(skiprows))
box.bot()

################################################################################
# Read Slab
################################################################################

slabGridPath = '/Users/kugelblitz/Desktop/initialTemperature/out/Slab2-clipped/Slab2-top.-1800'
data = np.loadtxt(slabGridPath,unpack='True')

################################################################################
# Read info from the param_1.5.3.txt
################################################################################

with open("param_1.5.3_2D.txt","r") as f:
	# Line 1
	line = f.readline()
	# Line 2
	line = f.readline()
	# Line 3
	line = f.readline()
	# Line 4
	line = f.readline()
	# Line 5
	line = f.readline()
	line = line.split()
	tag, step_max = line[0], int(line[1])
	# Line 6
	line = f.readline()
	# Line 7
	line = f.readline()
	# Line 8
	line = f.readline()
	# Line 9
	line = f.readline()
	# Line 10
	line = f.readline()
	line = line.split()
	tag, print_step = line[0], int(line[1])
	# Line 11
	line = f.readline()
	# Line 12
	line = f.readline()
	line = line.split()
	tag, visc_med = line[0], int(line[1].split("E")[1])
	# Line 13
	line = f.readline()
	line = line.split()
	tag, visc_max = line[0], int(line[1].split("E")[1])
	# Line 14
	line = f.readline()
	line = line.split()
	tag, visc_min = line[0], int(line[1].split("E")[1])

if (aux_print_step!=0):
	print_step = aux_print_step

################################################################################
# Mesh and information for matplotlib
################################################################################

x_start, x_end = 1500, 5000.
x_start_degree, x_end_degree = param.lon.start.degree + x_start / 111.,param.lon.start.degree +  x_end / 111.

extent			= [0,param.lon.end.km,0,param.depth.end]
ref = 10
#width, height 	= ww * ref, ref
markersize 		= 0.4
ww = (x_end-x_start) / (-param.depth.end) #+ 0.15

rx = 0.2 # 20% of the height
wx = 0.6 # 60% of the height

width, height 	= ww * wx * ref + 2 * rx * ref, ref
aux = width / ref

tr = 0.025 / aux
r0, r1, r2, r3 = rx/aux - tr, rx , ww * wx / aux, wx
rect = [r0, r1, r2, r3]


xi 		= np.linspace(0,param.lon.end.km,param.lon.size);
zi 		= np.linspace(param.depth.end,0,param.depth.size);
xx,zz 	= np.meshgrid(xi,zi);

# Basin location
basinWest, basinEast = -58, -55
drawdepth	= -6 # Depth to draw line and identify basin location

dticks = 200.
ddtick = dticks/2
arr_aux = np.arange(x_start,x_end+dticks/2.,dticks)
if (x_end-arr_aux[-1]<=ddtick):
	arr_aux = np.delete(arr_aux,-1)
xticks = np.append(arr_aux,x_end)

dticks_degree = 2.0
ddtick_degree = 1.1
arr_aux_degree = np.arange(np.ceil(x_start_degree),np.floor(x_end_degree)+dticks_degree/2.,dticks_degree)
if (x_end_degree-arr_aux_degree[-1]<=ddtick_degree):
	arr_aux_degree = np.delete(arr_aux_degree,-1)
if (x_start_degree-arr_aux_degree[0]>=-ddtick_degree):
	arr_aux_degree = np.delete(arr_aux_degree,0)
auxaux = np.append(x_start_degree,arr_aux_degree)
xticks_degree = np.append(auxaux,x_end_degree)

yticks = np.linspace(param.depth.start,param.depth.end,6)
yticks = np.append(yticks,TZ)

linewidth = 4
cwhite = (1.,1.,1.)

f_size = 20
plt.rcParams.update({'font.size': f_size})


dT = 200
#v_normal = 5.0E-3
v_normal = 10.0E-3

step_min = 0

plt.close()

################################################################################
# Plots
################################################################################

temperPlot 		= True
rhoPlot 		= True
strainPlot		= True
hPlot 			= False
geoqPlot 		= True
factorPlot 		= False
velocityPlot 	= True
viscPlot		= False
tempVelo		= True

n_dim_veloc = 3
if (param.lat.size==1):
	n_dim_veloc = 2
	
################################################################################
# Methods
################################################################################

def axx(cs,cwhite=(1,1,1)):
	ax1.set_xlabel("Distance (km)")
	ax1.set_xticks(xticks)
	ax1.set_xlim(x_start,x_end)
	ax1.set_ylabel("Depth (km)")
	ax1.set_yticks(yticks)
	ax1.set_ylim(param.depth.end,0)
	ax1.tick_params(length=10,width=2)
#	ax1.set_aspect('equal')

	ax2 = ax1.twiny()
	ax2.set_xlim(param.lon.start.degree+x_start/111.,param.lon.start.degree+x_end/111.)
	ax2.set_xticks(np.round(xticks_degree,2))
	ax2.set_xlabel("Longitude ($\!^\circ\!$)")
	ax2.tick_params(length=10,width=2)

	# Basin location
	ax2.vlines(basinWest,param.depth.start,-150,linestyles="dotted",color=cwhite,linewidth=linewidth)
	ax2.vlines(basinEast,param.depth.start,-150,linestyles="dotted",color=cwhite,linewidth=linewidth)
	ax1.hlines(TZ,x_start,x_end,linestyles="dashed",color=cwhite,linewidth=linewidth)
	
	ax1.autoscale(False)
	ax2.autoscale(False)

def axxin(fac):
	posx = 1. + 0.05 / fac
	axins = inset_axes(	ax1,
						width=0.25,  # width = 5% of parent_bbox width
						height="100%",  # height : 50%
						loc='lower left',
						bbox_to_anchor=(posx, 0., 1, 1),
						bbox_transform=ax1.transAxes,
						borderpad=0,
	)
	return axins
		
t = 0
t_max = 80.
dt = t_max/4 # Millions of years

num = 0

for cont in range(step_min,step_max+1):
#	bar.update(cont)
	print("loop step "+str(cont)+" @ file step " + str(cont*print_step))
	lastPlot = False
	try:
		yrs = np.loadtxt("Tempo_"+str(cont*print_step)+".txt",comments="P",delimiter=":")
	except:
		cont -= 1
		lastPlot = True
	
	if (num_processes!=1):
		value = format(np.round(yrs[0,1]/1.0E6,2), '.2f')
	else:
		value = format(np.round(yrs[1]/1.0E6,2), '.2f')
	value_float = float(value)
	
	if (value_float>t_max):
		cont -= 1
		lastPlot = True
	
	if (value_float>=t or lastPlot==True):
		############################################################################
		# Step "Plot"
		############################################################################
		
		x, y, z, cc, = [], [], [], []

		for rank in range(num_processes):
			x1,y1,z1,c0,c1,c2,c3,c4 = np.loadtxt("step_"+str(cont*print_step)+"-rank_new"+str(rank)+".txt",unpack=True)
			cor1 = (0,0,0)
			cor2 = (0,0,0)
			cor3 = (0,0,0)
			cc 	= np.append(cc,c1)
			x 	= np.append(x,x1)
			y 	= np.append(y,y1)
			z 	= np.append(z,z1)
		
		difere1 = 2.00
		difere2 = 0.95
		
		cond1 = (cc>=difere2) & (cc<=difere1)
		cond2 = (cc<difere2)
		cond3 = (cc>difere1)
		
		############################################################################
		# Temperature Plot
		############################################################################
		
		if (temperPlot==True):
			A = np.loadtxt("Temper_"+str(cont*print_step)+".txt",unpack=True,comments="P",skiprows=skiprows)
			TT 						= A * 1.0
			TT[np.abs(TT)<1.0E-200] = 0
			TT 						= np.reshape(TT,(param.lon.size,param.lat.size,param.depth.size),order='F')
			TTT 					= TT[:,0,:]
			
			plt.figure(figsize=(width,height))
			ax1 = plt.axes(rect)
			
			plt.title(value+" Myrs\n\n",fontsize=f_size)
			
			cs = plt.imshow(np.transpose(TTT),cmap="jet",extent=extent,aspect='auto')
#			plt.plot(x/1.0E3,y/1.0E3,"c.",color=cor2,markersize=markersize)
#			plt.plot(data[1,:],data[2,:],color=(1,1,1),linewidth=linewidth)
				
			axx(cs)

			# Color bar
			axins = axxin(ww)
			dd = dT
			auxMax = np.around(np.max(TTT),-2)
			ticks = np.arange(0,auxMax+1,dd)
			if (np.max(TTT)-ticks[-1])<dd/2:
				ticks = np.delete(ticks,-1)
				ticks = np.append(ticks,np.max(TTT))
			else:
				ticks = np.append(ticks,np.max(TTT))

			cbar = plt.colorbar(cs,cax=axins,ticks=ticks)
			cbar.ax.set_ylabel('Temperature ($\!^\circ\!$C)')

			plt.savefig(fig_dir+"TemperPlot_"+str(num).zfill(5)+".png")
			plt.close()

		############################################################################
		# Rho Plot
		############################################################################

		if (rhoPlot==True):
			R = np.loadtxt("Rho_"+str(cont*print_step)+".txt",unpack=True,comments="P",skiprows=skiprows)
			RR 						= R * 1.0
			RR[np.abs(RR)<1.0E-200] = 0
			RR 						= np.reshape(RR,(param.lon.size,param.lat.size,param.depth.size),order='F')
			RRR 					= RR[:,0,:]
			
			fig = plt.figure(figsize=(width,height))
			ax1 = plt.axes(rect)
			plt.title(value+" Myrs\n\n",fontsize=f_size)
			
			cs = ax1.imshow(np.transpose(RRR),extent=extent,aspect='auto')
			
			axx(cs)
			# Color bar
			axins = axxin(ww)
			ticks = np.linspace(np.floor(np.min(RRR)),np.ceil(np.max(RRR)),11)
			cbar = fig.colorbar(cs,cax=axins,ticks=ticks)
			cbar.ax.set_ylabel('Density ($kg/m\!^3\!$)')

			plt.savefig(fig_dir+"RhoPlot_"+str(num).zfill(5)+".png")
			plt.close()

		############################################################################
		# Strain plot
		############################################################################

		if (strainPlot==True):
			A = np.loadtxt("strain_"+str(cont*print_step)+".txt",unpack=True,comments="P",skiprows=skiprows)
			SS 						= A * 1.0
			SS[np.abs(SS)<1.0E-200] = 0
			SS[SS==0.0] 			= 1.0E-10
			SS 						= np.reshape(SS,(param.lon.size,param.lat.size,param.depth.size),order='F')
			SSS 					= SS[:,0,:]
			SSS 					= np.log10(SSS)
			
			fig = plt.figure(figsize=(width,height))
			ax1 = plt.axes(rect)
			plt.title(value+" Myrs\n\n",fontsize=f_size)
			
			cs = ax1.imshow(np.transpose(SSS),extent=extent,aspect='auto')
#			ax1.plot(x/1.0E3,y/1.0E3,"c.",color=cor2,markersize=markersize)
#			ax1.plot(data[1,:],data[2,:],color=(1,1,1),linewidth=10)
			
			# Color bar
			axins = axxin(ww)
			tickss = np.linspace(np.min(SSS),np.max(SSS),10)
			cbar = fig.colorbar(cs,cax=axins,ticks=tickss)
			cbar.ax.set_ylabel('$log_{10}\:\epsilon$')
			
			axx(cs)
			
			plt.savefig(fig_dir+"strainPlot_"+str(num).zfill(5)+".png")
			plt.close()

		############################################################################
		# H plot
		############################################################################

		if (hPlot==True):
			A = np.loadtxt("H_"+str(cont*print_step)+".txt",unpack=True,comments="P",skiprows=skiprows)
			HH 						= A*1.0
			HH[np.abs(HH)<1.0E-200] = 0
			HH 						= np.reshape(HH,(param.lon.size,param.lat.size,param.depth.size),order='F')
			HHH 					= HH[:,0,:]

			fig = plt.figure(figsize=(width,height))
			ax1 = plt.axes(rect)
			plt.title(value+" Myrs\n\n",fontsize=f_size)
			
			cs = ax1.contourf(xx,zz,np.transpose(HHH),extent=extent)
#			ax1.plot(x/1.0E3,y/1.0E3,"c.",color=cor2,markersize=markersize)
#			ax1.plot(data[1,:],data[2,:],color=(1,1,1),linewidth=10)
			
			# Color bar
			axins = axxin(ww)
			cbar = fig.colorbar(cs,cax=axins)
			cbar.ax.set_ylabel('H. Rate')
			
			axx(cs)
			
			plt.savefig(fig_dir+"HPlot_"+str(num).zfill(5)+".png")
			plt.close()

		############################################################################
		# Geoq plot
		############################################################################

		if (geoqPlot==True):
			G = np.loadtxt("Geoq_"+str(cont*print_step)+".txt",unpack=True,comments="P",skiprows=skiprows)
			GG 						= G * 1.0
			GG[np.abs(GG)<1.0E-200] = 0
			GG 						= np.reshape(GG,(param.lon.size,param.lat.size,param.depth.size),order='F')
			GGG 					= np.log10(GG[:,0,:])
	#		GGG 					= GG[:,0,:]

			fig = plt.figure(figsize=(width,height))
			ax1 = plt.axes(rect)
			plt.title(value+" Myrs\n\n",fontsize=f_size)
			
			cs = ax1.imshow(np.transpose(GGG),extent=extent,vmin=visc_min,vmax=visc_max,aspect='auto')
#			ax1.plot(x/1.0E3,y/1.0E3,"c.",color=cor2,markersize=markersize)
			
	#		ax1.plot(data[1,:],data[2,:],color=(1,1,1),linewidth=10)
			
			# Color bar
			axins = axxin(ww)
			ticks = np.linspace(visc_min,visc_max,visc_max-visc_min+1)
			# numticks = int((np.ceil(np.max(GGG))-np.floor(np.min(GGG)))+1)
			# ticks = np.linspace(np.floor(np.min(GGG)),np.ceil(np.max(GGG)),numticks)
			cbar = fig.colorbar(cs,cax=axins,ticks=ticks)
			cbar.ax.set_ylabel('$\log_{10}\:\eta$ (Pa s)')
	#		cbar.ax.set_ylabel('$log_{10}\:C$')

			axx(cs,cwhite=(0,0,0))
			
			
			
			plt.savefig(fig_dir+"GeoqPlot_"+str(num).zfill(5)+".png")
			plt.close()

		############################################################################
		# Factor Plot
		############################################################################

		if (factorPlot==True) and (geoqPlot==True):
			FFF = np.log10(GGG+10.) * RRR

			fig = plt.figure(figsize=(width,height))
			ax1 = plt.axes(rect)
			plt.title(value+" Myrs\n\n",fontsize=f_size)

			cs = ax1.contourf(xx,zz,np.transpose(FFF),extent=extent,cmap="viridis")


	#		ax1.plot(x[cond1]/1.0E3,z[cond1]/1.0E3,"c.",color=cor1,markersize=markersize)
	#		ax1.plot(x[cond2]/1.0E3,z[cond2]/1.0E3,"c.",color=cor3,markersize=markersize)
	#		ax1.plot(x[cond3]/1.0E3,z[cond3]/1.0E3,"c.",color=cor2,markersize=markersize)
			ax1.plot(x/1.0E3,y/1.0E3,"c.",color=cor2,markersize=markersize)
			
			ax1.plot(data[1,:],data[2,:],color=(1,1,1),linewidth=10)
			
			# Color bar
			axins = axxin(ww)
	#		ticks = np.linspace(0,np.max(FFF),10)
			cbar = fig.colorbar(cs,cax=axins)
			cbar.ax.set_ylabel('Factor')

			axx(cs)
			
			plt.savefig(fig_dir+"FactorPlot_"+str(num).zfill(5)+".png")
			plt.close()

		############################################################################
		# Velocity Plot
		############################################################################

		if (velocityPlot==True):
			V = np.loadtxt("Veloc_fut_"+str(cont*print_step)+".txt",unpack=True,comments="P",skiprows=skiprows)
			VV 						= V * 1.0
			VV[np.abs(VV)<1.0E-200] = 0.0
			VV 						= np.reshape(VV,(n_dim_veloc,param.lon.size,param.lat.size,param.depth.size),order='F')
	#		VV_max 					= np.max(np.abs(VV))
			if (n_dim_veloc==3):
				u 						= np.transpose(VV[0,:,0,:]) #/ VV_max
				v 						= np.transpose(VV[1,:,0,:]) #/ VV_max
				w 						= np.transpose(VV[2,:,0,:]) #/ VV_max
			else:
				u 						= np.transpose(VV[0,:,0,:]) #/ VV_max
	#			v 						= np.transpose(VV[1,:,0,:]) #/ VV_max
				w 						= np.transpose(VV[1,:,0,:]) #/ VV_max
			if cont==0:
				colors 					= np.sqrt(u**2+w**2)
				colors_max				= v_normal/(365.25*60*60*24)#np.max(colors)
				v_max = colors_max * 1000. * 365.25 * 60 * 60 * 24
			
			# Normalization
			colors 	= colors / colors_max
			u 		= u / colors_max
			w 		= w / colors_max
			

			
			fig = plt.figure(figsize=(width,height))
			ax1 = plt.axes(rect)
			plt.title(value+" Myrs\n\n",fontsize=f_size)
			
#			cs = ax1.quiver(xi,zi,u,w,colors,width=0.001,scale=param.lon.size,pivot='mid')
			cs = plt.imshow(np.transpose(TTT),cmap="jet",extent=extent,aspect='auto')
			ax1.streamplot(xi,zi,u,w, color=(0,0,0),linewidth=linewidth/2,arrowsize=2,arrowstyle='->')
			
			axx(cs)

			# Color bar
			axins = axxin(ww)
			dd = dT
			auxMax = np.around(np.max(TTT),-2)
			ticks = np.arange(0,auxMax+1,dd)
			if (np.max(TTT)-ticks[-1])<dd/2:
				ticks = np.delete(ticks,-1)
				ticks = np.append(ticks,np.max(TTT))
			else:
				ticks = np.append(ticks,np.max(TTT))

			cbar = plt.colorbar(cs,cax=axins,ticks=ticks)
			cbar.ax.set_ylabel('Temperature ($\!^\circ\!$C)')

			plt.savefig(fig_dir+"VelocityPlot_"+str(num).zfill(5)+".png")
			plt.close()

		############################################################################
		# Viscosity Plot
		############################################################################

		if (viscPlot==True):
			N = np.zeros(shape=(param.lon.size-1,param.lat.size-1,param.depth.size-1))
			for rank in xrange(num_processes):
				viscFile = open("visc_"+str(cont*print_step)+"_"+str(rank)+".txt")
				for line in viscFile:
					line = line.split()
					i 			= int(line[0])
					j 			= int(line[1])
					k 			= int(line[2])
					N[i,j,k] 	= float(line[3])
				viscFile.close()

			N[np.abs(N)<1.0E-200] = 0.0
	#		NNN = N[:,0,:]
	#		print(np.max(NNN),np.min(NNN))
			NNN = np.log10(N[:,0,:])

			fig = plt.figure(figsize=(width,height))
			ax1 = plt.axes(rect)
			plt.title(value+" Myrs\n\n",fontsize=f_size)

			dx = (xx[0,1]-xx[0,0])/2.
			dz = (zz[1,0]-zz[0,0])/2.
			cs = ax1.contourf(xx[:-1,:-1]+dx,zz[:-1,:-1]+dz,np.transpose(NNN),extent=extent)


	#		ax1.plot(x[cond1]/1.0E3,z[cond1]/1.0E3,"c.",color=cor1,markersize=markersize)
	#		ax1.plot(x[cond2]/1.0E3,z[cond2]/1.0E3,"c.",color=cor3,markersize=markersize)
	#		ax1.plot(x[cond3]/1.0E3,z[cond3]/1.0E3,"c.",color=cor2,markersize=markersize)
#			ax1.plot(x/1.0E3,y/1.0E3,"c.",color=cor2,markersize=markersize)
			
#			ax1.plot(data[1,:],data[2,:],color=(1,1,1),linewidth=10)

			
			# Color bar
			axins = axxin(ww)
			ticksN = np.linspace(np.min(NNN),np.max(NNN),10)
			cbar = fig.colorbar(cs,cax=axins)
			cbar.ax.set_ylabel('$\log_{10}\:\eta$ (Pa s)')
		
			axx(cs)

			plt.savefig(fig_dir+"ViscPlot_"+str(num).zfill(5)+".png")
			plt.close()

		if (tempVelo==True):
		
			fig = plt.figure(figsize=(width,height))
			ax1 = plt.axes(rect)
			plt.title(value+" Myrs\n\n",fontsize=f_size)
			
			arrowSkip = 5
			cs = ax1.imshow(np.transpose(TTT),cmap="jet",extent=extent,aspect='auto')
			Q = ax1.quiver(xi[::arrowSkip],zi[::arrowSkip],u[::arrowSkip,::arrowSkip],w[::arrowSkip,::arrowSkip],units='height',scale=param.depth.size/arrowSkip/2,width=0.005)
			
			ppx, ppy = x_end-400, -950
			ddx, ddy = 300, 100
			currentAxis = plt.gca()
			currentAxis.add_patch(Rectangle((ppx, ppy), ddx, ddy, facecolor="white"))
			
#			ax1.quiverkey(Q, 1.025, 1.1, 1, str(np.round(v_max,2))+" mm/yr")
			ax1.quiverkey(Q, ppx+ddx/2., ppy+20, 1, str(np.round(v_max,2))+" mm/yr",coordinates='data')
			
			axx(cs)
			
			# Color bar
			axins = axxin(ww)
			auxTMax = np.around(np.max(TTT),-2)
			ticks = np.arange(0,auxTMax+1,dT)
			if (np.max(TTT)-ticks[-1])<dT/2:
				ticks = np.delete(ticks,-1)
				ticks = np.append(ticks,np.max(TTT))
			else:
				ticks = np.append(ticks,np.max(TTT))
			cbar = fig.colorbar(cs,cax=axins,ticks=ticks)
			cbar.ax.set_ylabel('Temperature ($\!^\circ\!$C)')
			
			

			plt.savefig(fig_dir+"TempVelocityPlot_"+str(num).zfill(5)+".png")
			plt.close()
		t += dt
		num += 1
		if (lastPlot==True):
			break
			

		
		
		
		




