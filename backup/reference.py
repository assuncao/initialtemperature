import numpy as np
import re, os, sys

################################################################################
# Setting up classes and methods
################################################################################

cwd = os.getcwd()
libsLocation = os.path.join(cwd,'lib')
sys.path.append(libsLocation)

import data
slabThickness 	= data.slabThickness()
slabVelocity	= data.slabVelocity()

class Reference:
	def __init__(self,refValues,depth):
		self.length = np.shape(refValues)[0]
		for i in xrange(self.length):
			data = re.split("[ ]+", refValues[i].strip())
#			if (data[0]=="slabVelocity"):
#				self.slabVelocity = float(data[1])
			if (data[0]=="LABtemperatureC"):
				self.LABtemperatureC = float(data[1])
#			elif (data[0]=="POTtemperatureC"):
#				self.POTtemperatureC = float(data[1])
			elif (data[0]=="TZtemperatureC"):
				self.TZtemperatureC = float(data[1])
			elif (data[0]=="alpha"):
				self.alpha = float(data[1])
			elif (data[0]=="gravity"):
				self.gravity = float(data[1])
			elif (data[0]=="specificHeat"):
				self.specificHeat = float(data[1])
			elif (data[0]=="niter"):
				self.niter = int(data[1])
			elif (data[0]=="tm"):
				self.tm = float(data[1])
			elif (data[0]=="kappa"):
				self.kappa = float(data[1])
			
			elif (data[0]=="oceanicLitDraw"):
				self.oceanicLitDraw = data[1]
			elif (data[0]=="oceanicCrustDraw"):
				self.oceanicCrustDraw = data[1]
			elif (data[0]=="continentalLitDraw"):
				self.continentalLitDraw = data[1]
			elif (data[0]=="continentalCrustDraw"):
				self.continentalCrustDraw = data[1]
			elif (data[0]=="transitionalLitDraw"):
				self.transitionalLitDraw = data[1]
			elif (data[0]=="transitionalCrustDraw"):
				self.transitionalCrustDraw = data[1]
			elif (data[0]=="lubricantLayerDraw"):
				self.lubricantLayerDraw = data[1]
			elif (data[0]=="mantleDraw"):
				self.mantleDraw = data[1]
			
			elif (data[0]=="oceanicLitDensity"):
				self.oceanicLitDensity = float(data[1])
			elif (data[0]=="oceanicCrustDensity"):
				self.oceanicCrustDensity = float(data[1])
			elif (data[0]=="continentalLitDensity"):
				self.continentalLitDensity = float(data[1])
			elif (data[0]=="continentalCrustDensity"):
				self.continentalCrustDensity = float(data[1])
			elif (data[0]=="transitionalLitDensity"):
				self.transitionalLitDensity = float(data[1])
			elif (data[0]=="transitionalCrustDensity"):
				self.transitionalCrustDensity = float(data[1])
			elif (data[0]=="lubricantLayerDensity"):
				self.lubricantLayerDensity = float(data[1])
			elif (data[0]=="mantleDensity"):
				self.mantleDensity = float(data[1])

			elif (data[0]=="oceanicLitFactor"):
				self.oceanicLitFactor = float(data[1])
			elif (data[0]=="oceanicCrustFactor"):
				self.oceanicCrustFactor = float(data[1])
			elif (data[0]=="continentalLitFactor"):
				self.continentalLitFactor = float(data[1])
			elif (data[0]=="continentalCrustFactor"):
				self.continentalCrustFactor = float(data[1])
			elif (data[0]=="transitionalLitFactor"):
				self.transitionalLitFactor = float(data[1])
			elif (data[0]=="transitionalCrustFactor"):
				self.transitionalCrustFactor = float(data[1])
			elif (data[0]=="lubricantLayerFactor"):
				self.lubricantLayerFactor = float(data[1])
			elif (data[0]=="mantleFactor"):
				self.mantleFactor = float(data[1])

			elif (data[0]=="oceanicLitRadHeat"):
				self.oceanicLitRadHeat = float(data[1])
			elif (data[0]=="oceanicCrustRadHeat"):
				self.oceanicCrustRadHeat = float(data[1])
			elif (data[0]=="continentalLitRadHeat"):
				self.continentalLitRadHeat = float(data[1])
			elif (data[0]=="continentalCrustRadHeat"):
				self.continentalCrustRadHeat = float(data[1])
			elif (data[0]=="transitionalLitRadHeat"):
				self.transitionalLitRadHeat = float(data[1])
			elif (data[0]=="transitionalCrustRadHeat"):
				self.transitionalCrustRadHeat = float(data[1])
			elif (data[0]=="lubricantLayerRadHeat"):
				self.lubricantLayerRadHeat = float(data[1])
			elif (data[0]=="mantleRadHeat"):
				self.mantleRadHeat = float(data[1])
			else:
				continue
	
		self.LABtemperatureK = self.LABtemperatureC + 273.15
		self.TZtemperatureK = self.TZtemperatureC + 273.15
		self.POTtemperatureK = self.TZtemperatureK / (np.exp((-1)*depth*1000*self.alpha*self.gravity/self.specificHeat))
		self.POTtemperatureC = self.POTtemperatureK - 273.15

#		self.slabThickness = np.abs(self.oceanLitThickness)
		self.R = slabVelocity * slabThickness / (2 * self.kappa)
