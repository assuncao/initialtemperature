#!/bin/bash

SLAB2_PATH=/Users/kugelblitz/Documents/USP/models/Slab2.0/Slab2Distribute_Mar2018/sam_slab2_dep_02.23.18.grd
# grdimage /Users/kugelblitz/Documents/USP/gmt_files/ETOPO1_BEd_g_gmt4.grd -R-90/-30/-40/10 -JM15c -P -C/Users/kugelblitz/Documents/USP/gmt_files/ETOPO1.cpt -K > map.ps

# grdimage /Users/kugelblitz/Documents/USP/models/Slab2.0/Slab2Distribute_Mar2018/sam_slab2_dep_02.23.18.grd -R-90/-30/-40/10 -JM15c -P -Cseis.cpt -Q -K > map.ps
# # grdimage /Users/kugelblitz/Documents/USP/models/Slab2.0/Slab2Distribute_Mar2018/sam_slab2_dep_02.23.18.grd -R-90/-30/-40/10 -JM15c -P -C/Users/kugelblitz/Documents/USP/gmt_files/ETOPO1.cpt -K > map.ps
# #-I2/0.25p,cornflowerblue -I0/0.25p,cornflowerblue -I1/0.25p,cornflowerblue


# pscoast -R -JM15c -W0.01p,black -Df -P -Ba10f10g10 -B+t"Slab2" -Ir/0.25p,cornflowerblue -N1 -L-35/-35/-35/500 -K -O >> map.ps
# psxy -R -J ~/Desktop/initialTemperature/gmt/pantanal.xy -W1p,red  -P -O >> map.ps

# gmt psscale -Cseis.cpt -Dx8c/1c+w12c/0.5c -O >> map.ps

gmt set FONT_TITLE 20p

gmt makecpt -T-700/0/10 -Cseis > slab.cpt
grdimage $SLAB2_PATH -R-90/-30/-40/10 -JM15c -Cslab.cpt -Q -K > map.ps
# psxy -R -J $PWD/pantanal.txt -W0.1p,grey -GP27 -P -O -K >> map.ps
psxy -R -J $PWD/pantanal.txt -W0.1p,grey -Ggrey -P -O -K >> map.ps
pscoast -R -JM15c -W0.01p,black -Df -P -Ba10f10g10 -B+t"Slab surface from the Slab2 model" -N1 -L-35/-35/-35/500 -K -O >> map.ps
gmt psscale -Cslab.cpt -R -J -DjCT+o10c/0.15c+w13c/0.5c+v -Bx100f50+l"Depth (km)" -N -K -O >> map.ps
# gmt psscale -Cslab.cpt -Dx17c/12.7c+w17c/0.5c+jTC+v -Bx60f60+l"Depth (km)" -By+lkm -K -O >> map.ps
# grdimage /Users/kugelblitz/Documents/USP/models/Slab2.0/Slab2Distribute_Mar2018/sam_slab2_dep_02.23.18.grd -R-90/-30/-40/10 -JM15c -P -Cseis.cpt -Q -K > map.ps
gmt pscoast -Rg -JG-65/-18/5 -Bg30 -Swhite -Da -X18 -Y12 -A10000 -G50 -K -O >> map.ps
gmt psxy -W2.0,red  -R -J -O << EOF >> map.ps
-90 -40
-90 10
-30 10
-30 -40
-90 -40
EOF


