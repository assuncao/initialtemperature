import matplotlib.pyplot as plt

import os, sys, re
import numpy as np
from scipy import interpolate

#np.set_printoptions(threshold=np.nan)

################################################################################
# Setting up classes and methods
################################################################################

cwd = os.getcwd()
libsLocation = os.path.join(cwd,'lib')
sys.path.append(libsLocation)

import classes, methods
import progressBar as pbar

################################################################################
# Control options
# makeDepModel: 'True' if you want to crop the slab model to the area
# makeDepInterp: 'True' if you want to fit pols. to each slab in each latitude
# polDegree: set the polynomial degree
# mayaviPermission: 'True' if you want to plot using mayavi2
################################################################################

makeDepModel 		= 'True'
makeDepInterp 		= 'True'
mayaviPermission 	= 'False'

################################################################################
# Read data from ./src/params.txt
################################################################################

print("*** Reading initial parameters *** ")
parPath = cwd + '/src/params.txt'
param 	= classes.Dimension(parPath)

param.lat.size = 1

################################################################################
# Print parameters box (check up mistakes)
################################################################################

box = pbar.Box()

box.top()
box.mid("Chosen dimension:\t "+param.dim)
box.mid("Grid dimensions:\t ("+str(param.lon.size)+"x"+str(param.lat.size)+"x"+str(param.depth.size)+")")
box.mid("Longitude interval:\t ("+str(param.lon.start.degree)+","+str(param.lon.end.degree)+","+str(param.lon.delta.degree)+")")
box.mid("Longitude distance:\t ("+str(param.lon.start.km)+","+str(param.lon.end.km)+","+str(param.lon.delta.km)+")")
box.mid("Latitude interval:\t ("+str(param.lat.start.degree)+","+str(param.lat.end.degree)+","+str(param.lat.delta.degree)+")")
box.mid("Latitude distance:\t ("+str(param.lat.start.km)+","+str(param.lat.end.km)+","+str(param.lat.delta.km)+")")
box.mid("Depth interval:\t ("+str(param.depth.start)+","+str(param.depth.end)+","+str(param.depth.delta)+")")
box.bot()

################################################################################
# Read dimensional reference values
################################################################################

refFilePath 	= cwd + '/src/reference.txt'
dimRef 			= classes.Reference(refFilePath,param.depth.end)

################################################################################
# Take model data (slab2) for the area
################################################################################

fullModelRes		= 0.05 # Defined based on the slab2.0 model(do not change!)

depModelPathIn 		= './src/sam_slab2_dep_02.23.18.xyz' 		# Path to slab2.0 -> dep
thkModelPathIn		= './src/sam_slab2_thk_02.23.18.xyz' 		# Path to slab2.0 -> thk
dipModelPathIn		= './src/sam_slab2_dip_02.23.18.xyz' 		# Path to slab2.0 -> ang
strModelPathIn		= './src/sam_slab2_str_02.23.18.xyz' 		# Path to slab2.0 -> str

depModelPathOut 	= './out/Slab2-clipped/Slab2-top.' 			# Data written to /out directory
rtopModelPathOut 	= './out/Slab2-resampled/resampled-top.'	# Data written to /out directory
thkModelPathOut		= './out/Slab2-clipped/Slab2-bot.' 			# Data written to /out directory
rbotModelPathOut 	= './out/Slab2-resampled/resampled-bot.'	# Data written to /out directory

#Slab2 				= classes.Slab(parPath,fullModelRes)

auxSizeLon		= np.round((param.lon.end.degree-param.lon.start.degree)/fullModelRes,2)
auxSizeLat		= np.round((param.lat.end.degree-param.lat.start.degree)/fullModelRes,2)
sizeLonSlab2	= int(auxSizeLon) + 1
sizeLatSlab2	= int(auxSizeLat) + 1

if (param.dim=='2d'):
	sizeLatSlab2 			= 2
	param.lat.end.degree 	= param.lat.start.degree + fullModelRes
	param.lat.end.km		= param.lat.start.km + methods.km(0,fullModelRes)

slab2LonDomain		= np.arange(param.lon.start.degree,param.lon.end.degree+fullModelRes/2.,fullModelRes)
paramLonDomain		= np.arange(param.lon.start.degree,param.lon.end.degree+param.lon.delta.degree/2.,param.lon.delta.degree)
paramLatDomain		= np.arange(param.lat.start.degree,param.lat.end.degree+param.lat.delta.degree/2.,param.lat.delta.degree)

paramAuxLon			= np.ones(np.size(paramLonDomain)) * param.lon.start.degree
paramLonDomainKm	= methods.km(paramAuxLon,paramLonDomain)
paramAuxLat			= np.ones(np.size(paramLatDomain)) * param.lat.start.degree
paramLatDomainKm	= methods.km(paramAuxLat,paramLatDomain)

################################################################################
# Fixing the longitude to work with the Slab2
################################################################################

if (param.lon.start.degree<0):
	eastLonLimit = 360. + param.lon.start.degree
else:
	eastLonLimit = param.lon.start.degree

if (param.lon.end.degree<0):
	westLonLimit = 360. + param.lon.end.degree
else:
	westLonLimit = param.lon.end.degree

################################################################################
# Main Loop
################################################################################

if (makeDepModel == 'True'):
	depModel 		= np.zeros(shape=(sizeLonSlab2,sizeLatSlab2))
	depModel[:] 	= np.nan
	thkModel		= np.copy(depModel)
	dipModel		= np.copy(depModel)
	strModel 		= np.copy(depModel)
	depModelLon, depModelLat	= np.mgrid[param.lon.start.km:param.lon.end.km:sizeLonSlab2*1j,param.lat.start.km:param.lat.end.km:sizeLatSlab2*1j]
	thkModelLon, thkModelLat	= np.mgrid[param.lon.start.km:param.lon.end.km:sizeLonSlab2*1j,param.lat.start.km:param.lat.end.km:sizeLatSlab2*1j]
	############################################################################
	# Read Slab2 file data -> top surface
	############################################################################
	print("*** Reading Slab2 top surface file (" + depModelPathIn + ") ***")
	depModelFile = open(depModelPathIn)
	for line in depModelFile:
		auxLon 	= float(re.split("[,]+",line.strip())[0])
		auxLat	= float(re.split("[,]+",line.strip())[1])
		auxDep	= float(re.split("[,]+",line.strip())[2])
		if (((auxLon >= eastLonLimit) and (auxLon <= westLonLimit)) and
			((auxLat >= param.lat.start.degree) and (auxLat <= param.lat.end.degree)) and
			(auxDep > dimRef.maxSlabDepth)):
			iLon 		= ((auxLon - eastLonLimit) / fullModelRes)
			indexLon 	= int(np.round(iLon,0))
			iLat 		= ((auxLat - param.lat.start.degree) / fullModelRes)
			indexLat 	= int(np.round(iLat,0))
			depModel[indexLon,indexLat] = float(re.split("[,]+",line.strip())[2])
	depModelFile.close()

	############################################################################
	# Read Slab2 file data -> thickness of the slab
	############################################################################
	print("*** Reading Slab2 thickness file (" + thkModelPathIn + ") ***")
	thkModelFile = open(thkModelPathIn)
	for line in thkModelFile:
		auxLon 	= float(re.split("[,]+",line.strip())[0])
		auxLat	= float(re.split("[,]+",line.strip())[1])
		if (((auxLon >= eastLonLimit) and (auxLon <= westLonLimit)) and
			((auxLat >= param.lat.start.degree) and (auxLat <= param.lat.end.degree))):
			iLon 		= ((auxLon - eastLonLimit) / fullModelRes)
			indexLon 	= int(np.round(iLon,0))
			iLat 		= ((auxLat - param.lat.start.degree) / fullModelRes)
			indexLat 	= int(np.round(iLat,0))
			if (depModel[indexLon,indexLat] > dimRef.maxSlabDepth):
				thkModel[indexLon,indexLat] = float(re.split("[,]+",line.strip())[2])
	thkModelFile.close()
	thkModel = thkModel * dimRef.referenceSlabThkScale
	dimRef.referenceSlabThickness = np.average(thkModel[~np.isnan(thkModel)])
#	print dimRef.referenceSlabThickness

	############################################################################
	# Read Slab2 file data -> dip angle of the slab
	############################################################################
	print("*** Reading Slab2 dip angle file (" + dipModelPathIn + ") ***")
	dipModelFile = open(dipModelPathIn)
	for line in dipModelFile:
		auxLon 	= float(re.split("[,]+",line.strip())[0])
		auxLat	= float(re.split("[,]+",line.strip())[1])
		if (((auxLon >= eastLonLimit) and (auxLon <= westLonLimit)) and
			((auxLat >= param.lat.start.degree) and (auxLat <= param.lat.end.degree))):
			iLon 		= ((auxLon - eastLonLimit) / fullModelRes)
			indexLon 	= int(np.round(iLon,0))
			iLat 		= ((auxLat - param.lat.start.degree) / fullModelRes)
			indexLat 	= int(np.round(iLat,0))
			if (depModel[indexLon,indexLat] > dimRef.maxSlabDepth):
				dipModel[indexLon,indexLat] = float(re.split("[,]+",line.strip())[2])
	dipModelFile.close()
	dipModel = dipModel * np.pi / 180.

	############################################################################
	# Read Slab2 file data -> strike angle of the slab
	############################################################################
	print("*** Reading Slab2 strike angle file (" + strModelPathIn + ") ***")
	strModelFile = open(strModelPathIn)
	for line in strModelFile:
		auxLon 	= float(re.split("[,]+",line.strip())[0])
		auxLat	= float(re.split("[,]+",line.strip())[1])
		if (((auxLon >= eastLonLimit) and (auxLon <= westLonLimit)) and
			((auxLat >= param.lat.start.degree) and (auxLat <= param.lat.end.degree))):
			iLon 		= ((auxLon - eastLonLimit) / fullModelRes)
			indexLon 	= int(np.round(iLon,0))
			iLat 		= ((auxLat - param.lat.start.degree) / fullModelRes)
			indexLat 	= int(np.round(iLat,0))
			if (depModel[indexLon,indexLat] > dimRef.maxSlabDepth):
				strModel[indexLon,indexLat] = float(re.split("[,]+",line.strip())[2])
	strModelFile.close()
	strModel = strModel * np.pi / 180.

	############################################################################
	# Transform thicknessa data to Slab Bottom Depth
	############################################################################
	print("*** Making depth data (Slab2-bot) from thickness data ***")
	auxLonStart = np.ones(shape=(sizeLonSlab2,sizeLatSlab2)) * param.lon.start.degree
	auxLatStart = np.ones(shape=(sizeLonSlab2,sizeLatSlab2)) * param.lat.start.degree
	for j in xrange(sizeLatSlab2):
		for i in xrange(sizeLonSlab2):
			dLonSin = thkModel[i,j] * np.sin(dipModel[i,j])
			dLonCos	= thkModel[i,j] * np.cos(dipModel[i,j])
			if (~np.isnan(thkModel[i,j])):
				thkModelLon[i,j] 	= thkModelLon[i,j] - dLonSin
				thkModel[i,j] 		= depModel[i,j] - dLonCos
#	for j in xrange(sizeLatSlab2):
#		for i in xrange(sizeLonSlab2):
#			dLatSin = thkModel[i,j] * np.sin(strModel[i,j])
#			dLatCos = thkModel[i,j] * np.cos(strModel[i,j])
#			if (~np.isnan(thkModel[i,j])):
#				thkModelLat[i,j] 	= thkModelLat[i,j] - dLatSin
#				thkModel[i,j] 		= depModel[i,j] - thkModel[i,j] * np.cos(strModel[i,j])

	############################################################################
	# Resample Slab Bottom Depth values to coordinate system
	############################################################################
	print("*** Resampling depth data (Slab2-bot) to Slab2's grid ***")
	auxA, auxB, auxC 	= [], [], []
	for j in xrange(sizeLatSlab2):
		auxA	= np.append(auxA,thkModelLon[:,j])
		auxB	= np.append(auxB,thkModelLat[:,j])
		auxC	= np.append(auxC,thkModel[:,j])

	auxNotNan	= ~np.isnan(auxC)
	auxA		= auxA[auxNotNan]
	auxB		= auxB[auxNotNan]
	auxC		= auxC[auxNotNan]

	method		= 'cubic' # method = 'nearest' introduces a lot of bumps
	m, n 		= sizeLonSlab2, sizeLatSlab2
	XThk, YThk	= np.mgrid[param.lon.start.km:param.lon.end.km:m*1j,param.lat.start.km:param.lat.end.km:n*1j]
	points	 	= np.column_stack((auxA,auxB))
	values  	= auxC
	ZThk		= interpolate.griddata(points,values,(XThk,YThk),method=method)

	if (param.dim!='2d' and param.lat.size>2): # Special case if param.dim=='2d'
		eBoundXThk, eBoundYThk, wBoundXThk, wBoundYThk = [], [], [], []
		for j in xrange(sizeLatSlab2):
			notNanThk 	= ~np.isnan(thkModel[:,j])
			notNanXThk 	= thkModelLon[notNanThk,j]
			notNanYThk	= thkModelLat[notNanThk,j]
			eBoundXThk	= np.append(eBoundXThk,notNanXThk[-1])
			eBoundYThk	= np.append(eBoundYThk,notNanYThk[-1])
			wBoundXThk	= np.append(wBoundXThk,notNanXThk[0])
			wBoundYThk	= np.append(wBoundYThk,notNanYThk[0])

		eTckThk = interpolate.splrep(eBoundYThk,eBoundXThk,s=0)
		wTckThk = interpolate.splrep(wBoundYThk,wBoundXThk,s=0)

		eBoundNewYThk = np.copy(depModelLat[0,:])
		wBoundNewYThk = np.copy(depModelLat[0,:])
		eBoundNewXThk = interpolate.splev(eBoundNewYThk,eTckThk,der=0)
		wBoundNewXThk = interpolate.splev(wBoundNewYThk,wTckThk,der=0)

		for j in xrange(np.shape(ZThk)[1]):
			for i in xrange(np.shape(ZThk)[0]):
				if (XThk[i,j]<wBoundNewXThk[j]) or (XThk[i,j]>eBoundNewXThk[j]):
					ZThk[i,j] = np.nan

	thkModel 	= np.copy(ZThk)
	thkModelLon = np.copy(XThk)
	thkModelLat = np.copy(YThk)

	############################################################################
	# Make slab top and bottom intersept z=0
	############################################################################
	print("*** Linearly extrapolating Slab2 to intercept the surface ***")
	for j in xrange(sizeLatSlab2):
		notNanTop 	= np.where(~np.isnan(depModel[:,j]))
		notNanBot	= np.where(~np.isnan(ZThk[:,j]))
		dzTop		= depModel[notNanTop[0][1],j] - depModel[notNanTop[0][0],j]
		dzBot		= thkModel[notNanTop[0][1],j] - thkModel[notNanTop[0][0],j]
		aux 		= -1
		cont 		= 1
		while (aux < 0):
			idxTop 				= notNanTop[0][0] - cont
			idxBot 				= notNanBot[0][0] - cont
			if (idxBot<0 or idxTop<0):
				break
			depModel[idxTop,j] 	= np.min([0,depModel[idxTop+1,j]-dzTop])
			thkModel[idxBot,j] 	= np.min([0,thkModel[idxBot+1,j]-dzBot])
			aux  				= depModel[idxTop,j]
			cont 				+= 1

	if (param.dim=='2d'):
		depModel[:,-1] = depModel[:,0]
		thkModel[:,-1] = thkModel[:,0]

	############################################################################
	# Save a slice of the model for each latitude in the area
	############################################################################
	print("*** Saving a model slice for each latitude ***")
	for j in xrange(sizeLatSlab2):
		aux1, aux2		= np.round(param.lat.start.degree*100,0), np.round(j*fullModelRes*100,0)
		auxName 		= str(int(aux1+aux2)).zfill(5)
		depLatFile 		= open(depModelPathOut+auxName,"w")
		thkLatFile		= open(thkModelPathOut+auxName,"w")

		for i in xrange(sizeLonSlab2):
			if (~np.isnan(depModel[i,j])) and (depModel[i,j]<=0):
				aux1 = str(param.lon.start.degree+i*fullModelRes)
				aux2 = str(depModelLon[i,j])
				aux3 = str(depModel[i,j])
				depLatFile.write(aux1+"\t"+aux2+"\t"+aux3+"\n")
		depLatFile.close()

		for i in xrange(sizeLonSlab2):
			if (~np.isnan(thkModel[i,j])) and (thkModel[i,j]<=0):
				aux1 = str(param.lon.start.degree+i*fullModelRes)
				aux2 = str(thkModelLon[i,j])
				aux3 = str(thkModel[i,j])
				thkLatFile.write(aux1+"\t"+aux2+"\t"+aux3+"\n")
		thkLatFile.close()

################################################################################
# Resampling data based on grid from the values in the param.txt file
################################################################################
print("*** Resampling Slab2 into params.txt grid ***")

if (makeDepInterp=='True'):
	auxA, auxB, auxC, auxD 	= [], [], [], []
	for j in xrange(sizeLatSlab2):
		auxA	= np.append(auxA,depModelLon[:,j])
		auxB	= np.append(auxB,depModelLat[:,j])
		auxC 	= np.append(auxC,depModel[:,j])
		auxD	= np.append(auxD,thkModel[:,j])

	auxNotNanTop	= ~np.isnan(auxC)
	auxATop			= auxA[auxNotNanTop]
	auxBTop			= auxB[auxNotNanTop]
	auxC			= auxC[auxNotNanTop]

	auxNotNanBot	= ~np.isnan(auxD)
	auxABot			= auxA[auxNotNanBot]
	auxBBot			= auxB[auxNotNanBot]
	auxD			= auxD[auxNotNanBot]

	method		= 'cubic'
	m, n 		= np.size(paramLonDomain), np.size(paramLatDomain)
	X, Y 		= np.mgrid[param.lon.start.km:param.lon.end.km:m*1j,param.lat.start.km:param.lat.end.km:n*1j]
	pointsTop 	= np.column_stack((auxATop,auxBTop))
	pointsBot 	= np.column_stack((auxABot,auxBBot))
	valuesTop  	= auxC
	valuesBot	= auxD
	ZTop		= interpolate.griddata(pointsTop,valuesTop,(X,Y),method=method)
	ZBot		= interpolate.griddata(pointsBot,valuesBot,(X,Y),method=method)

	if (param.dim!='2d' and param.lat.size>2): # Special case if param.dim=='2d'
		eBoundXTop, eBoundYTop, wBoundXTop, wBoundYTop = [], [], [], []
		eBoundXBot, eBoundYBot, wBoundXBot, wBoundYBot = [], [], [], []
		for j in xrange(sizeLatSlab2):
			notNanTop 	= ~np.isnan(depModel[:,j])
			notNanXTop 	= depModelLon[notNanTop,j]
			notNanYTop	= depModelLat[notNanTop,j]
			eBoundXTop	= np.append(eBoundXTop,notNanXTop[-1])
			eBoundYTop	= np.append(eBoundYTop,notNanYTop[-1])
			wBoundXTop	= np.append(wBoundXTop,notNanXTop[0])
			wBoundYTop	= np.append(wBoundYTop,notNanYTop[0])

			notNanBot 	= ~np.isnan(thkModel[:,j])
			notNanXBot	= thkModelLon[notNanBot,j]
			notNanYBot	= thkModelLat[notNanBot,j]
			eBoundXBot	= np.append(eBoundXBot,notNanXBot[-1])
			eBoundYBot	= np.append(eBoundYBot,notNanYBot[-1])
			wBoundXBot	= np.append(wBoundXBot,notNanXBot[0])
			wBoundYBot	= np.append(wBoundYBot,notNanYBot[0])

		eTckTop = interpolate.splrep(eBoundYTop,eBoundXTop,s=0)
		wTckTop = interpolate.splrep(wBoundYTop,wBoundXTop,s=0)
		eBoundNewYTop = np.copy(paramLatDomainKm)
		wBoundNewYTop = np.copy(paramLatDomainKm)
		eBoundNewXTop = interpolate.splev(eBoundNewYTop,eTckTop,der=0)
		wBoundNewXTop = interpolate.splev(wBoundNewYTop,wTckTop,der=0)

		eTckBot = interpolate.splrep(eBoundYBot,eBoundXBot,s=0)
		wTckBot = interpolate.splrep(wBoundYBot,wBoundXBot,s=0)
		eBoundNewYBot = np.copy(paramLatDomainKm)
		wBoundNewYBot = np.copy(paramLatDomainKm)
		eBoundNewXBot = interpolate.splev(eBoundNewYBot,eTckBot,der=0)
		wBoundNewXBot = interpolate.splev(wBoundNewYBot,wTckBot,der=0)

		for j in xrange(np.size(paramLatDomain)):
			for i in xrange(np.size(paramLonDomain)):
				if (X[i,j]>eBoundNewXTop[j]) or (X[i,j]<wBoundNewXTop[j]):
					ZTop[i,j] = np.nan
				if (X[i,j]>eBoundNewXBot[j]) or (X[i,j]<wBoundNewXBot[j]):
					ZBot[i,j] = np.nan

################################################################################
# Save resampled data
################################################################################
print("*** Saving resampled data ***")
for j in xrange(np.size(paramLatDomain)):
	aux1, aux2		= np.round(param.lat.start.degree*100,0), np.round(j*param.lat.delta.degree*100,0)
	auxName 		= str(int(aux1+aux2)).zfill(5)
	rtopLatFile 	= open(rtopModelPathOut+auxName,"w")
	rbotLatFile		= open(rbotModelPathOut+auxName,"w")

	for i in xrange(np.size(paramLonDomain)):
		if (~np.isnan(ZTop[i,j])) and (ZTop[i,j]<=0):
			aux1 = str(X[i,j])
			aux2 = str(ZTop[i,j])
			rtopLatFile.write(aux1+"\t"+aux2+"\n")
	rtopLatFile.close()

	for i in xrange(np.size(paramLonDomain)):
		if (~np.isnan(ZBot[i,j])) and (ZBot[i,j]<=0):
			aux1 = str(X[i,j])
			aux2 = str(ZBot[i,j])
			rbotLatFile.write(aux1+"\t"+aux2+"\n")
	rbotLatFile.close()

if (mayaviPermission=='True'):
	from mayavi import mlab
	
	xmin, xmax, ymin, ymax, zmin, zmax = param.lon.start.km, param.lon.end.km, param.lat.start.km, param.lat.end.km, param.depth.end, param.depth.start
	extent = [xmin,xmax,ymin,ymax,zmin,zmax]
	xx, yy, zz = np.mgrid[xmin:xmax:(param.lon.size)*1j, ymin:ymax:(param.lat.size)*1j, zmin:zmax:(param.depth.size)*1j]
	#mlab.points3d(xx,yy,zz,scale_factor=3)
	
	mlab.mesh(X,Y,ZTop,color=(1,0,0))
	mlab.mesh(X,Y,ZBot,color=(0,0,1))
	
	mlab.outline(extent=extent,color=(0,0,0))
	mlab.show()

