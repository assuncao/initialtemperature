import numpy as np
import matplotlib.pyplot as plt

skiprows=4
num_processes=1

with open("param_1.5.3.txt","r") as f:
	line = f.readline()
	line = line.split()
	Nx,Ny,Nz = int(line[0]),int(line[1]),int(line[2])
	line = f.readline()
	line = line.split()
	Lx,Ly,Lz = float(line[0]),float(line[1]),float(line[2])

print(Nx,Ny,Nz,Lx,Ly,Lz)

#xx,zz = np.mgrid[0:Lx:(Nx)*1j,-Lz:0:(Nz)*1j]

xi = np.linspace(0,Lx/1000,Nx);
zi = np.linspace(-Lz/1000,0,Nz);
xx,zz = np.meshgrid(xi,zi);

A = np.loadtxt("./out/Temper_0_3D.txt",unpack=True,comments="P",skiprows=skiprows)
TT = A*1.0
TT[np.abs(TT)<1.0E-200]=0
TT = np.reshape(TT,(Nx,Ny,Nz),order='F')
TTT = TT[:,0,:]
plt.close()
plt.figure(figsize=(10*2,2.5*2))
plt.contourf(xx,zz,np.transpose(TTT),100)
plt.show()
