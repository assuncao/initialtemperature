import numpy as np
import os, sys, re

import matplotlib.pyplot as plt

################################################################################
# Setting up classes and methods
################################################################################

cwd = os.getcwd()
libsLocation = os.path.join(cwd,'lib')
sys.path.append(libsLocation)

import classes

################################################################################
# Read data from ./src/params.txt
################################################################################

parPath = './src/params.txt'
par 	= open(parPath)
lon 	= classes.Parameters(re.split("[ ]+", par.readline().strip()),"lon")
lat 	= classes.Parameters(re.split("[ ]+", par.readline().strip()),"lat")
depth 	= classes.Parameters(re.split("[ ]+", par.readline().strip()),"depth")
par.close()

################################################################################
# Data plot
################################################################################

latitude = -20.00 # in degrees
approxFileName 	= './out/approx/approx.' + str(int(latitude*100)) #str(format(latitude,'.2f'))
projecFileName 	= './out/projec/slice.' + str(int(latitude*100))#str(format(latitude,'.2f'))
slabFileName	= './out/model/slab.' + str(int(latitude*100))

xApprox, zApprox 	= np.loadtxt(approxFileName,unpack=True)
xProjec, zProjec 	= np.loadtxt(projecFileName,unpack=True)
dSlab, xSlab, zSlab	= np.loadtxt(slabFileName,unpack=True)

minX = np.min([xApprox[0],xProjec[0]]) - 20
maxX = np.max([xApprox[-1],xProjec[-1]]) + 20
minZ = np.min([zApprox[-1],zProjec[-1]]) - 20
maxZ = 0#np.max([zApprox[0],zProjec[0]])

figfactor = abs((maxX-minX)/(maxZ-minZ))
factor = 10

#fig, ax = plt.subplots(figsize=(factor*figfactor,factor))
fig, ax = plt.subplots()
plt.plot(xSlab,zSlab,"-",color=(0,0.8,0),label="Slab1.0 model")
plt.plot(xApprox,zApprox,"--",color=(0.2,0.6,1),label="Top of the slab")
plt.plot(xProjec,zProjec,"-",color=(1,0.5,0),label="Bottom of the slab")
plt.ylabel("Depth (km)")
plt.yticks(np.linspace(minZ,maxZ,11))
plt.ylim(minZ,maxZ)
plt.xlabel("Distance (km)")
plt.xticks(np.linspace(minX,maxX,6))
plt.xlim(minX,maxX)

plt.legend(loc=3)
ax.set_aspect(1.0)
#plt.tight_layout()
plt.savefig('./figures/plate.pdf')
plt.show()
