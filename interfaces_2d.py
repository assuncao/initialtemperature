import numpy as np
import pandas as pd
import re,os,sys

import geopandas as gp
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon

import matplotlib.pyplot as plt

################################################################################
# Setup
################################################################################

showPlot = 'False'

################################################################################
# Loading reference values from reference class
################################################################################

cwd = os.getcwd()
libsLocation = os.path.join(cwd,'lib')
sys.path.append(libsLocation)

import classes, methods
import progressBar as pbar

################################################################################
# Read data from ./src/params.txt
################################################################################

print("*** Reading initial parameters *** ")
parPath = cwd + '/src/params.txt'
param 	= classes.Dimension(parPath)

param.lat.size = 1

fieldTemperature 	= np.zeros(shape=(param.lon.size,param.lat.size,param.depth.size))
fieldLon			= np.linspace(param.lon.start.km,param.lon.end.km,param.lon.size)
fieldLat			= np.linspace(param.lat.start.km,param.lat.end.km,param.lat.size)
fieldDepth			= np.linspace(param.depth.start,param.depth.end,param.depth.size)
auxFieldDepth		= np.zeros(np.size(fieldDepth))

x,y 				= np.meshgrid(fieldLon,fieldLat)

longitude			= np.linspace(param.lon.start.degree,param.lon.end.degree,param.lon.size)
latitude 			= np.linspace(param.lat.start.degree,param.lat.end.degree,param.lat.size)

################################################################################
# Read dimensional reference values
################################################################################

refFilePath 	= cwd + '/src/reference.txt'
dimRef 			= classes.Reference(refFilePath,param.depth.end)

################################################################################
# Load slab model (top and bottom surfaces)
################################################################################

resampledTopModelPath 	= './out/Slab2-resampled/resampled-top.'
resampledBotModelPath 	= './out/Slab2-resampled/resampled-bot.'
interfacesPath			= './out/interfaces_creep.txt'

################################################################################
# interfaces.txt header
################################################################################

interfaces 	= open(interfacesPath,"w")

stringFac 		= "C\t" + \
	str(dimRef.lowerMantleFactor) 			+ " " + \
	str(dimRef.mantleFactor) 				+ " " + \
	str(dimRef.oceanicLitFactor) 			+ " " + \
	str(dimRef.oceanicCrustFactor) 			+ " " + \
	str(dimRef.mantleFactor) 				+ " " + \
	str(dimRef.lubricantLayerFactorBot) 	+ " " + \
	str(dimRef.lubricantLayerFactorTop)		+ " " + \
	str(dimRef.continentalLitFactor)	 	+ " " + \
	str(dimRef.continentalCrustFactor)
stringDensity = "rho\t" + \
	str(dimRef.lowerMantleDensity) 			+ " " + \
	str(dimRef.mantleDensity) 				+ " " + \
	str(dimRef.oceanicLitDensity) 			+ " " + \
	str(dimRef.oceanicCrustDensity) 		+ " " + \
	str(dimRef.mantleDensity) 				+ " " + \
	str(dimRef.lubricantLayerDensityBot)	+ " " + \
	str(dimRef.lubricantLayerDensityTop)	+ " " + \
	str(dimRef.continentalLitDensity) 		+ " " + \
	str(dimRef.continentalCrustDensity)
stringRadHeat = "H\t" + \
	str(dimRef.lowerMantleRadHeat) 			+ " " + \
	str(dimRef.mantleRadHeat) 				+ " " + \
	str(dimRef.oceanicLitRadHeat) 			+ " " + \
	str(dimRef.oceanicCrustRadHeat) 		+ " " + \
	str(dimRef.mantleRadHeat) 				+ " " + \
	str(dimRef.lubricantLayerRadHeatBot)	+ " " + \
	str(dimRef.lubricantLayerRadHeatTop)	+ " " + \
	str(dimRef.continentalLitRadHeat) 		+ " " + \
	str(dimRef.continentalCrustRadHeat)
stringA 	= "A\t" + \
	str("0.0")								+ " " + \
	str("0.0")								+ " " + \
	str("0.0")								+ " " + \
	str("0.0")								+ " " + \
	str("0.0")								+ " " + \
	str("0.0")								+ " " + \
	str("0.0")								+ " " + \
	str("0.0")								+ " " + \
	str("0.0")
stringn		= "n\t" + \
	str("0.0")								+ " " + \
	str("0.0")								+ " " + \
	str("0.0")								+ " " + \
	str("0.0")								+ " " + \
	str("0.0")								+ " " + \
	str("0.0")								+ " " + \
	str("0.0")								+ " " + \
	str("0.0")								+ " " + \
	str("0.0")
stringQ		= "Q\t" + \
	str("0.0")								+ " " + \
	str("0.0")								+ " " + \
	str("0.0")								+ " " + \
	str("0.0")								+ " " + \
	str("0.0")								+ " " + \
	str("0.0")								+ " " + \
	str("0.0")								+ " " + \
	str("0.0")								+ " " + \
	str("0.0")
stringV		= "V\t" + \
	str("0.0")								+ " " + \
	str("0.0")								+ " " + \
	str("0.0")								+ " " + \
	str("0.0")								+ " " + \
	str("0.0")								+ " " + \
	str("0.0")								+ " " + \
	str("0.0")								+ " " + \
	str("0.0")								+ " " + \
	str("0.0")

interfaces.write(stringFac+"\n")
interfaces.write(stringDensity+"\n")
interfaces.write(stringRadHeat+"\n")
interfaces.write(stringA+"\n")
interfaces.write(stringn+"\n")
interfaces.write(stringQ+"\n")
interfaces.write(stringV+"\n")

################################################################################
# Load continent contour
################################################################################

map = gp.read_file('/Users/kugelblitz/Desktop/initialTemperature/src/lim_pais_a.shp')

################################################################################
# Load LAB depth and Moho depth
################################################################################

lithoPath 	= cwd + '/out/litho.txt'
litho 		= pd.read_csv(lithoPath,sep=" ",header=None,dtype=float).T
litho 		= litho * (-1)

crustPath 	= cwd + '/out/crust.txt'
crust 		= pd.read_csv(crustPath,sep=" ",header=None,dtype=float).T
crust 		= crust * (-1)

for i in xrange(param.lon.size):
	for j in xrange(param.lat.size):
		if (crust[j][i]>-dimRef.maxOceLitThk):
			crust[j][i] = -dimRef.maxOceLitThk
		else:
			pass

################################################################################
# Main Latitude Loop
################################################################################

print("*** Tracing interfaces ***")

auxShape 	= np.zeros(shape=(param.lon.size,11))
inter 		= pd.DataFrame(auxShape)
interface 	= pd.DataFrame(auxShape)

auxVec 	= np.zeros(shape=(param.lon.size))
#meso	= pd.DataFrame(auxVec)
#mohoOcn = pd.DataFrame(auxVec)
#mohoLub = pd.DataFrame(auxVec)
#mohoCon = pd.DataFrame(auxVec)
#ocnLitTop= pd.DataFrame(auxVec)
#ocnLitBot= pd.DataFrame(auxVec)
#lubLitBot= pd.DataFrame(auxVec)
#lubLitTop= pd.DataFrame(auxVec)
meso	= np.zeros(shape=(param.lon.size))
mohoOcn = np.zeros(shape=(param.lon.size))
mohoLub = np.zeros(shape=(param.lon.size))
mohoCon = np.zeros(shape=(param.lon.size))
ocnLitTop= np.zeros(shape=(param.lon.size))
ocnLitBot= np.zeros(shape=(param.lon.size))
lubLitBot= np.zeros(shape=(param.lon.size))
lubLitTop= np.zeros(shape=(param.lon.size))

for j in xrange(param.lat.size):
	aux1,aux2 		= np.round(param.lat.start.degree*100,0),np.round(j*param.lat.delta.degree*100,0)
	slabTopFile 	= resampledTopModelPath + str(int(aux1+aux2)).zfill(5)
	slabTop 		= classes.Surface(np.loadtxt(slabTopFile),0,1)
	slabBottomFile	= resampledBotModelPath + str(int(aux1+aux2)).zfill(5)
	slabBottom 		= classes.Surface(np.loadtxt(slabBottomFile),0,1)

	# Slab Top
	condTop			= (fieldLon>=slabTop.abscissa[0])&(fieldLon<=slabTop.abscissa[-1])
	lonTop 			= fieldLon[condTop]
	depthTop		= np.interp(lonTop,slabTop.abscissa,slabTop.ordinate)

	# Slab Bottom
	condBottom		= (fieldLon>=slabBottom.abscissa[0])&(fieldLon<=slabBottom.abscissa[-1])
	lonBottom		= fieldLon[condBottom]
	depthBottom		= np.interp(lonBottom,slabBottom.abscissa,slabBottom.ordinate)

	# Lubrincant Layer (Top)
	abs,ord = methods.makeOutwardProjection(slabTop.abscissa.size,slabTop.abscissa,slabTop.ordinate,dimRef.lubricantLayerThickness)
	cond			= (fieldLon>=abs[0])&(fieldLon<=abs[-1])
	lonLub			= fieldLon[cond]
	depthLub		= np.interp(lonLub,abs,ord)
	for i in xrange(np.size(lonLub)):
		if (depthLub[i]>0):
			depthLub[i] = 0.0
		if (depthLub[i]<=litho[j][fieldLon==lonLub[i]].item()):
			depthLub[i] = litho[j][fieldLon==lonLub[i]]

	# Oceanic Crust (Bottom)
	auxOceanicDepth = crust[j][fieldLon<=slabTop.abscissa[0]]
	oceanicDepth 	= dimRef.slabOceCrustDepth * (-1)
	abs,ord = methods.makeInnwardProjection(slabTop.abscissa.size,slabTop.abscissa,slabTop.ordinate,oceanicDepth)
	cond 			= (fieldLon>=abs[0])&(fieldLon<=abs[-1])
	lonOCrust 		= fieldLon[cond]
	depthOCrust		= np.interp(lonOCrust,abs,ord)

	# Points inside continent
	print("*** Defining if longitude points are inside the continental area (latitude "+str((aux1+aux2)/100.)+") ***")
	barIsContinent = pbar.Bar(np.size(fieldLon))
	barIsContinent.start()
	isContinent = np.zeros(np.size(fieldLon))
	for i in xrange(np.size(isContinent)):
		point = Point(longitude[i],latitude[j])
		if (map.contains(point)).any():
			isContinent[i] = 1
		else:
			isContinent[i] = 0
		barIsContinent.update(i)
	barIsContinent.end()

	############################################################################
	# Make interfaces
	############################################################################

	# Lower interface
	meso[:] = dimRef.TZdepth
	aux = 0
	for e in xrange(meso.size):
		if (condBottom[e]):
			meso[e] = np.min([dimRef.TZdepth,depthBottom[aux]])
			aux += 1

	# Oceanic Lithosphere
	ocnLitBot = np.copy(litho[j][:])
	ocnLitBot[(fieldLon>=lonBottom[0])&(fieldLon<=lonBottom[-1])] = depthBottom
	if (lonBottom[-1]<lonTop[-1]):
		cond 	= (fieldLon>=lonBottom[-1])&(fieldLon<=lonTop[-1])
		auxLon 	= fieldLon[cond]
		auxDep 	= np.interp(auxLon,[lonBottom[-1],lonTop[-1]],[depthBottom[-1],depthTop[-1]])
		ocnLitBot[(fieldLon>=auxLon[0])&(fieldLon<=auxLon[-1])] = auxDep

	ocnLitTop = np.copy(ocnLitBot)
	ocnLitTop[isContinent==0] = 0.0
	ocnLitTop[(fieldLon>=lonTop[0])&(fieldLon<=lonTop[-1])] = depthTop

	# Lubricant Lithosphere
	lubLitBot = np.copy(ocnLitTop)
	for i in xrange(np.size(lubLitBot)):
		if ((fieldLon[i]>=lonTop[0]) and (fieldLon[i]<=lonTop[-1])):
			lubLitBot[i] = np.max([depthTop[lonTop==fieldLon[i]],litho[j][i]])
		elif ((fieldLon[i]>lonTop[-1]) and (isContinent[i]==1)):
			lubLitBot[i] = litho[j][i]
		else:
			lubLitBot[i] = 0.

	lubLitTop = np.copy(lubLitBot)
	lubLitTop[fieldLon<lonLub[0]] = 0.
	lubLitTop[(fieldLon>lonLub[-1])&(isContinent==1)] = litho[j][(fieldLon>lonLub[-1])&(isContinent==1)]
	lubLitTop[(fieldLon>lonLub[-1])&(isContinent==0)] = 0.
	lubLitTop[(fieldLon>=lonLub[0])&(fieldLon<=lonLub[-1])] = depthLub

	# Moho Oceanic
	mohoOcn = np.copy(ocnLitTop)
	mohoOcn[isContinent==0] = crust[j][isContinent==0]
	mohoOcn[(fieldLon>=lonOCrust[0])&(fieldLon<=lonOCrust[-1])] = depthOCrust

	if (lonBottom[-1]<=lonTop[-1]):
		cond 	= (fieldLon>=lonOCrust[-1])&(fieldLon<=lonTop[-1])
		auxLon 	= fieldLon[cond]
		auxDep 	= np.interp(auxLon,[lonOCrust[-1],lonTop[-1]],[depthOCrust[-1],depthTop[-1]])
		mohoOcn[(fieldLon>=auxLon[0])&(fieldLon<=auxLon[-1])] = auxDep
	else:
		cond 	= (fieldLon>=lonOCrust[-1])&(fieldLon<=lonTop[-1])
		auxLon 	= fieldLon[cond]
		auxDep 	= np.interp(auxLon,[lonOCrust[-1],lonTop[-1]],[depthOCrust[-1],depthTop[-1]])
		mohoOcn[(fieldLon>=auxLon[0])&(fieldLon<=auxLon[-1])] = auxDep

	# Moho Lubricant Lithosphere
	lubricantUpperDepth = -52.
	mohoLub = np.copy(lubLitBot)
	for i in xrange(np.size(mohoLub)):
		if ((fieldLon[i]>=lonTop[0]) and (fieldLon[i]<=lonTop[-1])):
			auxMax1 = np.max([lubLitBot[i],lubricantUpperDepth])
			auxMax2	= lubLitTop[i]
			mohoLub[i] = np.min([auxMax1,auxMax2])

	# Moho Continent
	auxContinentLimit 	= fieldLon[lubLitTop==litho[j][:]]
	continentalLimit 	= auxContinentLimit[0]

	mohoCon = np.copy(lubLitTop)
	for i in xrange(np.size(mohoCon)):
		val1 = lubLitTop[i]
		val2 = crust[j][i]
		mohoCon[i] = np.max([val1,val2])

	for i in xrange(param.lon.size):
		string = \
			str(format(meso[i]*1000,'.10f')) + " " + \
			str(format(ocnLitBot[i]*1000,'.10f')) + " " + \
			str(format(mohoOcn[i]*1000,'.10f')) + " " + \
			str(format(ocnLitTop[i]*1000,'.10f')) + " " + \
			str(format(lubLitBot[i]*1000,'.10f')) + " " + \
			str(format(mohoLub[i]*1000,'.10f')) + " " + \
			str(format(lubLitTop[i]*1000,'.10f')) + " " + \
			str(format(mohoCon[i]*1000,'.10f')) + "\n"
		interfaces.write(string)

plt.clf()
plt.close('all')
scale = 2.2

plt.rcParams.update({'font.size': 12})
f_size = 10
width, height 	= 14, 5
fig, ax = plt.subplots(figsize=(width,height))
ax.set_xlim(param.lon.start.km,param.lon.end.km)
dKmTicks = 250
xticks_km = np.arange(0,np.round(param.lon.end.km,-1),dKmTicks)
if (param.lon.end.km-xticks_km[-1])<dKmTicks/2.:
	xticks_km = np.delete(xticks_km,-1)
	xticks_km = np.append(xticks_km,param.lon.end.km)
else:
	xticks_km = np.append(xticks_km,param.lon.end.km)
plt.xticks(xticks_km)

#plt.hlines(fieldLon[0],fieldLon[-1],0,linestyles="dotted")
#plt.plot(fieldLon[condTop],depthTop,linestyle="dotted")
#plt.plot(fieldLon[condBottom],depthBottom,linestyle="dotted")
#plt.plot(lonLub,depthLub,linestyle="dotted")
#plt.plot(lonOCrust,depthOCrust,linestyle="dotted")

plt.fill_between(fieldLon,meso,param.depth.end,color=(015./255,058./255,065./255),label="Lower Mantle")
plt.fill_between(fieldLon,ocnLitBot,meso,color=(045./255,078./255,095./255),label="Asthenospheric Mantle")
plt.fill_between(fieldLon,mohoOcn,ocnLitBot,color=(051./255,121./255,180./255),label="Oceanic Lithosphere")
plt.fill_between(fieldLon,ocnLitTop,mohoOcn,color=(064./255,155./255,248./255),label="Oceanic Crust")
plt.fill_between(fieldLon,lubLitBot,ocnLitTop,color=(045./255,078./255,095./255))
plt.fill_between(fieldLon,mohoLub,lubLitBot,color=(088./255,181./255,170./255),label="Lower Lubricant Layer")
plt.fill_between(fieldLon,lubLitTop,mohoLub,color=(239./255,132./255,100./255),label="Upper Lubricant Layer")
plt.fill_between(fieldLon,mohoCon,lubLitTop,color=(245./255,193./255,127./255),label="Continental Lithosphere")
plt.fill_between(fieldLon,0.0,mohoCon,color=(190./255,085./255,081./255),label="Continental Crust")

#plt.plot(fieldLon,meso)
#plt.plot(fieldLon,ocnLitBot)
#plt.plot(fieldLon,mohoOcn)
#plt.plot(fieldLon,ocnLitTop)
#plt.plot(fieldLon,lubLitBot)
#plt.plot(fieldLon,mohoLub)
#plt.plot(fieldLon,lubLitTop)
#plt.plot(fieldLon,mohoCon)

plt.legend(loc=3,fontsize=f_size)
plt.ylim(param.depth.end,param.depth.start)
plt.xlabel("Distance (km)")
plt.ylabel("Depth (km)")
ax2 = ax.twiny()
ax2.set_xlim(param.lon.start.degree,param.lon.end.degree)

dDgTicks = 2.5
xticks_dg = np.arange(param.lon.start.degree,param.lon.end.degree,dDgTicks)
if (param.lon.end.degree-xticks_dg[-1])<dDgTicks/2.:
	xticks_dg = np.delete(xticks_dg,-1)
	xticks_dg = np.append(xticks_dg,param.lon.end.degree)
else:
	xticks_dg = np.append(xticks_dg,param.lon.end.degree)
plt.xticks(xticks_dg)

plt.xlabel("Longitude (degrees)")
plt.tight_layout()
plt.savefig("./figures/interfaces.pdf")

if (showPlot=='True'):
	plt.show()

################################################################################
# Close interfaces.txt file
################################################################################

interfaces.close()
