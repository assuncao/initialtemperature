STEPS=($@)
echo ${STEPS[@]}
DIRNAME=${PWD##*/}

CORE=('Temper_0_3D.txt' 'figures/' 'plots/' 'param_*' 'interfaces*' 'params.txt' 'reference.txt' 'veloc_*' 'log.txt' 'temp_simu.txt' 'roda.sh')
echo "Zipping $DIRNAME directory..."

########################################################
# Para zipar uma sequência, é necessário usar o comando
# $ bash zipper.sh -seq <passo> <valor máximo>
# onde <passo> é o passo da sequência e
# <valor máximo> é o maior valor da sequência. 
########################################################
if [[ "${STEPS[0]}" == "-seq" ]]; then
	IDX=0
	C=0
	M=$(( ${STEPS[2]} - ${STEPS[1]} ))
	while [ ${C} -le ${M} ]; do
		C=$(( $IDX * ${STEPS[1]} ))
		AUX+="Geoq_${C}.txt "
		AUX+="H_${C}.txt "
		AUX+="Pressure_${C}.txt "
		AUX+="Rho_${C}.txt "
		AUX+="Temper_${C}.txt "
		AUX+="Tempo_${C}.txt "
		AUX+="Veloc_fut_${C}.txt "
		AUX+="step_${C}-rank* "
		AUX+="strain_${C}.txt "
		AUX+="visc_${C}_*.txt "
		IDX=$(( $IDX + 1 ))
	done
########################################################
# Para zipar um passo ou uma sequência de passos, usa-se
# $ bash zipper.sh -step <passo 1> <passo 2>
# onde <passo n> é um passo da simulação.
########################################################
elif [[ "${STEPS[0]}" == "-step" ]]; then
	for i in "${STEPS[@]:1}"; do 
		AUX+="Geoq_${i}.txt "
		AUX+="H_${i}.txt "
		AUX+="Pressure_${i}.txt "
		AUX+="Rho_${i}.txt "
		AUX+="Temper_${i}.txt "
		AUX+="Tempo_${i}.txt "
		AUX+="Veloc_fut_${i}.txt "
		AUX+="step_${i}-rank* "
		AUX+="strain_${i}.txt "
		AUX+="visc_${i}_*.txt "
	done
fi
zip -r $DIRNAME.zip ${CORE[@]} $AUX
echo "Zipping completed!"