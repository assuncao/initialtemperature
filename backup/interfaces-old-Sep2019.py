import numpy as np
import pandas as pd
import re,os,sys

import geopandas as gp
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon

import matplotlib.pyplot as plt

################################################################################
# Loading reference values from reference class
################################################################################

cwd = os.getcwd()
libsLocation = os.path.join(cwd,'lib')
sys.path.append(libsLocation)

import classes, methods
import progressBar as pbar

################################################################################
# Read data from ./src/params.txt
################################################################################

print("*** Reading initial parameters *** ")
parPath = cwd + '/src/params.txt'
param 	= classes.Dimension(parPath)

fieldTemperature 	= np.zeros(shape=(param.lon.size,param.lat.size,param.depth.size))
fieldLon			= np.linspace(param.lon.start.km,param.lon.end.km,param.lon.size)
fieldLat			= np.linspace(param.lat.start.km,param.lat.end.km,param.lat.size)
fieldDepth			= np.linspace(param.depth.start,param.depth.end,param.depth.size)
auxFieldDepth		= np.zeros(np.size(fieldDepth))

x,y 				= np.meshgrid(fieldLon,fieldLat)

longitude			= np.linspace(param.lon.start.degree,param.lon.end.degree,param.lon.size)
latitude 			= np.linspace(param.lat.start.degree,param.lat.end.degree,param.lat.size)

################################################################################
# Read dimensional reference values
################################################################################

refFilePath 	= cwd + '/src/reference.txt'
dimRef 			= classes.Reference(refFilePath,param.depth.end)

################################################################################
# Load slab model (top and bottom surfaces)
################################################################################

resampledTopModelPath 	= './out/Slab2-resampled/resampled-top.'
resampledBotModelPath 	= './out/Slab2-resampled/resampled-bot.'
interfacesPath			= './out/interfaces.txt'

################################################################################
# interfaces.txt header
################################################################################

interfaces 	= open(interfacesPath,"w")
# lowerMantle, mantle, oceanicLit, oceanicCrust, mantle, lubricantLayer, transLit, continentalLit, transCrust, continentalCrust
stringFac 		= \
	str(dimRef.lowerMantleFactor) 			+ " " + \
	str(dimRef.mantleFactor) 				+ " " + \
	str(dimRef.oceanicLitFactor) 			+ " " + \
	str(dimRef.oceanicCrustFactor) 			+ " " + \
	str(dimRef.mantleFactor) 				+ " " + \
	str(dimRef.lubricantLayerFactorBot) 	+ " " + \
	str(dimRef.lubricantLayerFactorTop)		+ " " + \
	str(dimRef.transitionalLitFactor) 		+ " " + \
	str(dimRef.continentalLitFactor)		+ " " + \
	str(dimRef.transitionalCrustFactor) 	+ " " + \
	str(dimRef.continentalCrustFactor)
stringDensity = \
	str(dimRef.lowerMantleDensity) 			+ " " + \
	str(dimRef.mantleDensity) 				+ " " + \
	str(dimRef.oceanicLitDensity) 			+ " " + \
	str(dimRef.oceanicCrustDensity) 		+ " " + \
	str(dimRef.mantleDensity) 				+ " " + \
	str(dimRef.lubricantLayerDensityBot)	+ " " + \
	str(dimRef.lubricantLayerDensityTop)	+ " " + \
	str(dimRef.transitionalLitDensity) 		+ " " + \
	str(dimRef.continentalLitDensity) 		+ " " + \
	str(dimRef.transitionalCrustDensity) 	+ " " + \
	str(dimRef.continentalCrustDensity)
stringRadHeat = \
	str(dimRef.lowerMantleRadHeat) 			+ " " + \
	str(dimRef.mantleRadHeat) 				+ " " + \
	str(dimRef.oceanicLitRadHeat) 			+ " " + \
	str(dimRef.oceanicCrustRadHeat) 		+ " " + \
	str(dimRef.mantleRadHeat) 				+ " " + \
	str(dimRef.lubricantLayerRadHeatBot)	+ " " + \
	str(dimRef.lubricantLayerRadHeatTop)	+ " " + \
	str(dimRef.transitionalLitRadHeat) 		+ " " + \
	str(dimRef.continentalLitRadHeat) 		+ " " + \
	str(dimRef.transitionalCrustRadHeat) 	+ " " + \
	str(dimRef.continentalCrustRadHeat)

interfaces.write(stringFac+"\n")
interfaces.write(stringDensity+"\n")
interfaces.write(stringRadHeat+"\n")

################################################################################
# Load continent contour
################################################################################

map = gp.read_file('/Users/kugelblitz/Desktop/initialTemperature/src/lim_pais_a.shp')

################################################################################
# Load LAB depth and Moho depth
################################################################################

lithoPath 	= cwd + '/out/litho.txt'
litho 		= pd.read_csv(lithoPath,sep=" ",header=None,dtype=float).T
litho 		= litho * (-1)

crustPath 	= cwd + '/out/crust.txt'
crust 		= pd.read_csv(crustPath,sep=" ",header=None,dtype=float).T
crust 		= crust * (-1)

for i in xrange(param.lon.size):
	for j in xrange(param.lat.size):
		if (crust[j][i]>-dimRef.maxOceLitThk):
			crust[j][i] = -dimRef.maxOceLitThk
		else:
			pass

################################################################################
# Main Latitude Loop
################################################################################

print("*** Tracing interfaces ***")
#barPlot = pbar.Bar(param.lat.size)
#barPlot.start()

auxShape 	= np.zeros(shape=(param.lon.size,11))
inter 		= pd.DataFrame(auxShape)
interface 	= pd.DataFrame(auxShape)

for j in xrange(param.lat.size):
	aux1,aux2 		= np.round(param.lat.start.degree*100,0),np.round(j*param.lat.delta.degree*100,0)
	slabTopFile 	= resampledTopModelPath + str(int(aux1+aux2)).zfill(5)
	slabTop 		= classes.Surface(np.loadtxt(slabTopFile),0,1)
	slabBottomFile	= resampledBotModelPath + str(int(aux1+aux2)).zfill(5)
	slabBottom 		= classes.Surface(np.loadtxt(slabBottomFile),0,1)

	# Slab Top
	condTop			= (fieldLon>=slabTop.abscissa[0])&(fieldLon<=slabTop.abscissa[-1])
	lonTop 			= fieldLon[condTop]
	depthTop		= np.interp(lonTop,slabTop.abscissa,slabTop.ordinate)

	# Slab Bottom
	condBottom		= (fieldLon>=slabBottom.abscissa[0])&(fieldLon<=slabBottom.abscissa[-1])
	lonBottom		= fieldLon[condBottom]
	depthBottom		= np.interp(lonBottom,slabBottom.abscissa,slabBottom.ordinate)

	# Lubrincant Layer (Top)
	abs,ord = methods.makeOutwardProjection(slabTop.abscissa.size,slabTop.abscissa,slabTop.ordinate,dimRef.lubricantLayerThickness)
	cond			= (fieldLon>=abs[0])&(fieldLon<=abs[-1])
	lonLub			= fieldLon[cond]
	depthLub		= np.interp(lonLub,abs,ord)
	for i in xrange(np.size(lonLub)):
		if (depthLub[i]>0):
			depthLub[i] = 0.0
		if (depthLub[i]<=litho[j][fieldLon==lonLub[i]].item()):
			depthLub[i] = litho[j][fieldLon==lonLub[i]]

	# Oceanic Crust (Bottom)
	auxOceanicDepth = crust[j][fieldLon<=slabTop.abscissa[0]]
	oceanicDepth 	= dimRef.slabOceCrustDepth * (-1)
	abs,ord = methods.makeInnwardProjection(slabTop.abscissa.size,slabTop.abscissa,slabTop.ordinate,oceanicDepth)
	cond 			= (fieldLon>=abs[0])&(fieldLon<=abs[-1])
	lonOCrust 		= fieldLon[cond]
	depthOCrust		= np.interp(lonOCrust,abs,ord)

	# Points inside continent
	print("*** Defining if longitude points are inside the continental area (latitude "+str((aux1+aux2)/100.)+") ***")
	barIsContinent = pbar.Bar(np.size(fieldLon))
	barIsContinent.start()
	isContinent = np.zeros(np.size(fieldLon))
	for i in xrange(np.size(isContinent)):
		point = Point(longitude[i],latitude[j])
		if (map.contains(point)).any():
			isContinent[i] = 1
		else:
			isContinent[i] = 0
		barIsContinent.update(i)
	barIsContinent.end()

	############################################################################
	# Make interfaces
	############################################################################

	# Interface 1: Mantle bottom
	interface[1] = np.copy(litho[j][:])
	interface[1][(fieldLon>=lonBottom[0])&(fieldLon<=lonBottom[-1])] = depthBottom
	if (lonBottom[-1]<lonTop[-1]):
		cond 	= (fieldLon>=lonBottom[-1])&(fieldLon<=lonTop[-1])
		auxLon 	= fieldLon[cond]
		auxDep 	= np.interp(auxLon,[lonBottom[-1],lonTop[-1]],[depthBottom[-1],depthTop[-1]])
		interface[1][(fieldLon>=auxLon[0])&(fieldLon<=auxLon[-1])] = auxDep

	# Interface 0: Lower Mantle - Astenosphere Boudary
	interface[0] = np.copy(interface[1])
	for i in xrange(np.size(interface[0])):
		if (interface[0][i]>dimRef.TZdepth):
			interface[0][i] = dimRef.TZdepth

	# Interface 2: Oceanic Lithosphere Top
	interface[2] = np.copy(interface[1])
	interface[2][isContinent==0] = crust[j][isContinent==0]
	interface[2][(fieldLon>=lonOCrust[0])&(fieldLon<=lonOCrust[-1])] = depthOCrust

	if (lonBottom[-1]<=lonTop[-1]):
		cond 	= (fieldLon>=lonOCrust[-1])&(fieldLon<=lonTop[-1])
		auxLon 	= fieldLon[cond]
		auxDep 	= np.interp(auxLon,[lonOCrust[-1],lonTop[-1]],[depthOCrust[-1],depthTop[-1]])
		interface[2][(fieldLon>=auxLon[0])&(fieldLon<=auxLon[-1])] = auxDep
	else:
		cond 	= (fieldLon>=lonOCrust[-1])&(fieldLon<=lonTop[-1])
		auxLon 	= fieldLon[cond]
		auxDep 	= np.interp(auxLon,[lonOCrust[-1],lonTop[-1]],[depthOCrust[-1],depthTop[-1]])
		interface[2][(fieldLon>=auxLon[0])&(fieldLon<=auxLon[-1])] = auxDep

	# Interface 3: Oceanic Crust Top
	interface[3] = np.copy(interface[2])
	interface[3][isContinent==0] = 0.0
	interface[3][(fieldLon>=lonTop[0])&(fieldLon<=lonTop[-1])] = depthTop

	if (lonBottom[-1]>=lonTop[-1]):
		cond	= (fieldLon>=lonTop[-1])&(fieldLon<=lonBottom[-1])
		auxLon 	= fieldLon[cond]
		auxDep 	= np.interp(auxLon,[lonOCrust[-1],lonTop[-1]],[depthOCrust[-1],depthTop[-1]])
		interface[3][(fieldLon>=auxLon[0])&(fieldLon<=auxLon[-1])] = auxDep

	# Interface 4: Lubrincat Layer (Bot)
	interface[4] = np.copy(interface[3])
	for i in xrange(np.size(interface[4])):
		if ((fieldLon[i]>=lonTop[0]) and (fieldLon[i]<=lonTop[-1])):
			interface[4][i] = np.max([depthTop[lonTop==fieldLon[i]],litho[j][i]])
		elif ((fieldLon[i]>lonTop[-1]) and (isContinent[i]==1)):
			interface[4][i] = litho[j][i]
		else:
			interface[4][i] = 0.

	# Interface 6: Lubricant Layer
	interface[6] = np.copy(interface[4])
	interface[6][fieldLon<lonLub[0]] = 0.
	interface[6][(fieldLon>lonLub[-1])&(isContinent==1)] = litho[j][(fieldLon>lonLub[-1])&(isContinent==1)]
	interface[6][(fieldLon>lonLub[-1])&(isContinent==0)] = 0.
	interface[6][(fieldLon>=lonLub[0])&(fieldLon<=lonLub[-1])] = depthLub

	# Interface 5: Lubrincat Layer (Top)
	lubricantUpperDepth = -52.
	interface[5] = np.copy(interface[4])
	for i in xrange(np.size(interface[5])):
		if ((fieldLon[i]>=lonTop[0]) and (fieldLon[i]<=lonTop[-1])):
			auxMax1 = np.max([interface[4][i],lubricantUpperDepth])
#			auxMax1 = np.max([depthTop[lonTop==fieldLon[i]],lubricantUpperDepth])
#			auxMax2 = np.min([depthTop[lonTop==fieldLon[i]],interface[6][i]])
			auxMax2	= interface[6][i]
			interface[5][i] = np.min([auxMax1,auxMax2])
#		elif ((fieldLon[i]>lonTop[-1]) and (isContinent[i]==1)):
#			interface[5][i] = litho[j][i]
#		else:
#			interface[5][i] = 0.
									
	# Limit between contact litosphere and ordinary litosphere
	auxContinentLimit 	= fieldLon[interface[6]==litho[j][:]]
	continentalLimit 	= auxContinentLimit[0]

	# Interface 6: Contact Continental Bottom
	interface[7] = np.copy(interface[6])
	for i in xrange(np.size(interface[7])):
		if (fieldLon[i]<continentalLimit):
			val1 = interface[6][i]
			val2 = crust[j][i]
			interface[7][i] = np.max([val1,val2])

	# Interface 7: Continental Crust
	interface[8] = np.copy(interface[7])
	for i in xrange(np.size(interface[8])):
		if (isContinent[i]==1) and (fieldLon[i]>=continentalLimit):
			interface[8][i] = crust[j][i]
	
	# Interface 9: Contac Continental Top
	interface[9] = np.copy(interface[8])
	for i in xrange(np.size(interface[9])):
		if (fieldLon[i]<continentalLimit):
			val1 = interface[8][i]
			val2 = 0
			interface[9][i] = np.max([val1,val2])

	############################################################################
	# Save horizons
	############################################################################

	for i in xrange(fieldLon.size):
#		if (interface[0][i]>0.0):
#			interface[0][i] = 0.0
#		if (interface[0][i]>interface[1][i]):
#			interface[0][i] = interface[1][i]
#		if (interface[0][i]<param.depth.end):
#			interface[0][i] = param.depth.end
#
#		if (interface[1][i]>0.0):
#			interface[1][i] = 0.0
#		if (interface[1][i]>interface[2][i]):
#			interface[1][i] = interface[2][i]
#		if (interface[1][i]<param.depth.end):
#			interface[1][i] = param.depth.end
#
#		if (interface[2][i]>0.0):
#			interface[2][i] = 0.0
#		if (interface[2][i]>interface[3][i]):
#			interface[2][i] = interface[3][i]
#		if (interface[2][i]<param.depth.end):
#			interface[2][i] = param.depth.end
#
#		if (interface[3][i]>0.0):
#			interface[3][i] = 0.0
#		if (interface[3][i]>interface[4][i]):
#			interface[3][i] = interface[4][i]
#		if (interface[3][i]<param.depth.end):
#			interface[3][i] = param.depth.end
#
#		if (interface[4][i]>0.0):
#			interface[4][i] = 0.0
#		if (interface[4][i]>interface[5][i]):
#			interface[4][i] = interface[5][i]
#		if (interface[4][i]<param.depth.end):
#			interface[4][i] = param.depth.end
#
#		if (interface[5][i]>0.0):
#			interface[5][i] = 0.0
#		if (interface[5][i]>interface[6][i]):
#			interface[5][i] = interface[6][i]
#		if (interface[5][i]<param.depth.end):
#			interface[5][i] = param.depth.end
#
#		if (interface[6][i]>0.0):
#			interface[6][i] = 0.0
#		if (interface[6][i]>interface[7][i]):
#			interface[6][i] = interface[7][i]
#		if (interface[6][i]<param.depth.end):
#			interface[6][i] = param.depth.end
#
#		if (interface[7][i]>0.0):
#			interface[7][i] = 0.0
#		if (interface[7][i]>interface[8][i]):
#			interface[7][i] = interface[8][i]
#		if (interface[7][i]<param.depth.end):
#			interface[7][i] = param.depth.end
#
#		if (interface[8][i]>0.0):
#			interface[8][i] = 0.0
#		if (interface[8][i]>interface[9][i]):
#			interface[8][i] = interface[9][i]
#		if (interface[8][i]<param.depth.end):
#			interface[8][i] = param.depth.end
#
#		if (interface[9][i]>0.0):
#			interface[9][i] = 0.0
#		if (interface[9][i]<param.depth.end):
#			interface[9][i] = param.depth.end
		string = \
			str(format(interface[0][i]*1000,'.10f')) + " " + \
			str(format(interface[1][i]*1000,'.10f')) + " " + \
			str(format(interface[2][i]*1000,'.10f')) + " " + \
			str(format(interface[3][i]*1000,'.10f')) + " " + \
			str(format(interface[4][i]*1000,'.10f')) + " " + \
			str(format(interface[5][i]*1000,'.10f')) + " " + \
			str(format(interface[6][i]*1000,'.10f')) + " " + \
			str(format(interface[7][i]*1000,'.10f')) + " " + \
			str(format(interface[8][i]*1000,'.10f')) + " " + \
			str(format(interface[9][i]*1000,'.10f')) + "\n"
		interfaces.write(string)

#	barPlot.update(j)
#barPlot.end()

#if(param.dim!='2d'):
#	from mayavi import mlab
#	mlab.mesh(x,y,interface[0],color=(045./255,078./255,095./255))
#	mlab.mesh(x,y,interface[1],color=(051./255,121./255,180./255))
#	mlab.mesh(x,y,interface[2],color=(064./255,155./255,248./255))
#	mlab.mesh(x,y,interface[3],color=(045./255,078./255,095./255))
#	mlab.mesh(x,y,interface[4],color=(088./255,181./255,170./255))
#	mlab.mesh(x,y,interface[5],color=(167./255,194./255,081./255))
#	mlab.mesh(x,y,interface[6],color=(252./255,231./255,140./255))
#	mlab.mesh(x,y,interface[7],color=(245./255,193./255,127./255))
#	mlab.mesh(x,y,interface[8],color=(239./255,132./255,100./255))
#	mlab.outline(extent=[np.min(x),np.max(x),np.min(y),np.max(y),param.depth.end,param.depth.start])
#	mlab.show()

plt.clf()
plt.close('all')
scale = 2.2

plt.rcParams.update({'font.size': 12})
f_size = 10
width, height 	= 14, 4
fig, ax = plt.subplots(figsize=(width,height))
ax.set_xlim(param.lon.start.km,param.lon.end.km)
dKmTicks = 250
xticks_km = np.arange(0,np.round(param.lon.end.km,-1),dKmTicks)
if (param.lon.end.km-xticks_km[-1])<dKmTicks/2.:
	xticks_km = np.delete(xticks_km,-1)
	xticks_km = np.append(xticks_km,param.lon.end.km)
else:
	xticks_km = np.append(xticks_km,param.lon.end.km)
plt.xticks(xticks_km)

plt.fill_between(fieldLon,interface[0],param.depth.end,color=(015./255,058./255,065./255),label="Lower Mantle")
plt.fill_between(fieldLon,interface[1],interface[0]		,color=(045./255,078./255,095./255),label="Mantle")
plt.fill_between(fieldLon,interface[2],interface[1]		,color=(051./255,121./255,180./255),label="Oceanic Lithosphere")
plt.fill_between(fieldLon,interface[3],interface[2]		,color=(064./255,155./255,248./255),label="Oceanic Crust")
plt.fill_between(fieldLon,interface[4],interface[3]		,color=(045./255,078./255,095./255))
plt.fill_between(fieldLon,interface[5],interface[4]		,color=(088./255,181./255,170./255),label="Lower Lubricant Layer")
plt.fill_between(fieldLon,interface[6],interface[5]		,color=(167./255,194./255,081./255),label="Upper Lubricant Layer")
plt.fill_between(fieldLon,interface[7],interface[6]		,color=(252./255,231./255,140./255),label="Contact Litosphere")
plt.fill_between(fieldLon,interface[8],interface[7]		,color=(245./255,193./255,127./255),label="Continental Lithosphere")
plt.fill_between(fieldLon,interface[9],interface[8]		,color=(239./255,132./255,100./255),label="Contact Crust")
plt.fill_between(fieldLon,0			,interface[9]		,color=(190./255,085./255,081./255),label="Continental Crust")

#plt.plot(fieldLon,interface[0])
#plt.plot(fieldLon,interface[1])
#plt.plot(fieldLon,interface[2])
#plt.plot(fieldLon,interface[3])
#plt.plot(fieldLon,interface[4])
#plt.plot(fieldLon,interface[5])
#plt.plot(fieldLon,interface[6])
#plt.plot(fieldLon,interface[7])
#plt.plot(fieldLon,interface[8])
#plt.plot(fieldLon,interface[9])

plt.legend(loc=3,fontsize=f_size)
plt.ylim(param.depth.end,param.depth.start)
plt.xlabel("Distance (km)")
plt.ylabel("Depth (km)")
ax2 = ax.twiny()
ax2.set_xlim(param.lon.start.degree,param.lon.end.degree)

dDgTicks = 2.5
xticks_dg = np.arange(param.lon.start.degree,param.lon.end.degree,dDgTicks)
if (param.lon.end.degree-xticks_dg[-1])<dDgTicks:
	xticks_dg = np.delete(xticks_dg,-1)
	xticks_dg = np.append(xticks_dg,param.lon.end.degree)
else:
	xticks_dg = np.append(xticks_dg,param.lon.end.degree)
plt.xticks(xticks_dg)

plt.xlabel("Longitude ($\!^\circ\!$)")
plt.tight_layout()
plt.savefig("./figures/interfaces.pdf")

plt.show()

################################################################################
# Close interfaces.txt file
################################################################################

interfaces.close()
