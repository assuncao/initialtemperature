import numpy as np
import re
from mayavi import mlab

fileName = './out/temperatureField.dat'

file = open(fileName)
sx,sy,sz 	= re.split("[ \t]+", file.readline().strip())
sLon,sLat,sDep = int(sx),int(sy),int(sz)
w,e,s,n,t,b = re.split("[ \t]+", file.readline().strip())
westL,eastL,southL,northL,topL,bottomL = float(w),float(e),float(s),float(n),float(t),float(b)
w,e,s,n,t,b = re.split("[ \t]+", file.readline().strip())
westD,eastD,southD,northD,topD,bottomD = float(w),float(e),float(s),float(n),float(t),float(b)

temp = np.zeros(shape=(sLon,sLat,sDep))

for j in range(sLat):
	for i in range(sLon):
		for k in range(sDep):
			temp[i,j,k] = file.readline()

mlab.figure(size=(800,800))
extent = [westL,eastL,southL,northL,topL,bottomL]
mlab.contour3d(temp,extent=extent,contours=11,transparent=True,colormap="blue-red")
mlab.view(azimuth=270,elevation=90)

radius = 5.
	
lonMin = westL
lonMax = eastL
latMin = southL
latMax = northL
depthMin = topL
depthMax = bottomL
	
x0 = np.array([lonMin,lonMax])
y0 = np.array([latMin,latMin])
z0 = np.array([depthMin,depthMin])
mlab.plot3d(x0,y0,z0,tube_radius=radius,color=(0,0,0))
	
x1 = np.array([lonMin,lonMax])
y1 = np.array([latMin,latMin])
z1 = np.array([depthMax,depthMax])
mlab.plot3d(x1,y1,z1,tube_radius=radius,color=(0,0,0))
	
x2 = np.array([lonMin,lonMax])
y2 = np.array([latMax,latMax])
z2 = np.array([depthMin,depthMin])
mlab.plot3d(x2,y2,z2,tube_radius=radius,color=(0,0,0))
	
x3 = np.array([lonMin,lonMax])
y3 = np.array([latMax,latMax])
z3 = np.array([depthMax,depthMax])
mlab.plot3d(x3,y3,z3,tube_radius=radius,color=(0,0,0))
	
x4 = np.array([lonMin,lonMin])
y4 = np.array([latMin,latMin])
z4 = np.array([depthMin,depthMax])
mlab.plot3d(x4,y4,z4,tube_radius=radius,color=(0,0,0))
	
x5 = np.array([lonMax,lonMax])
y5 = np.array([latMax,latMax])
z5 = np.array([depthMin,depthMax])
mlab.plot3d(x5,y5,z5,tube_radius=radius,color=(0,0,0))
	
x6 = np.array([lonMin,lonMin])
y6 = np.array([latMax,latMax])
z6 = np.array([depthMin,depthMax])
mlab.plot3d(x6,y6,z6,tube_radius=radius,color=(0,0,0))
	
x7 = np.array([lonMax,lonMax])
y7 = np.array([latMin,latMin])
z7 = np.array([depthMin,depthMax])
mlab.plot3d(x7,y7,z7,tube_radius=radius,color=(0,0,0))
	
x8 = np.array([lonMax,lonMax])
y8 = np.array([latMin,latMax])
z8 = np.array([depthMin,depthMin])
mlab.plot3d(x8,y8,z8,tube_radius=radius,color=(0,0,0))
	
x9 = np.array([lonMax,lonMax])
y9 = np.array([latMin,latMax])
z9 = np.array([depthMax,depthMax])
mlab.plot3d(x9,y9,z9,tube_radius=radius,color=(0,0,0))
	
x10 = np.array([lonMin,lonMin])
y10 = np.array([latMin,latMax])
z10 = np.array([depthMin,depthMin])
mlab.plot3d(x10,y10,z10,tube_radius=radius,color=(0,0,0))
	
x11 = np.array([lonMin,lonMin])
y11 = np.array([latMin,latMax])
z11 = np.array([depthMax,depthMax])
mlab.plot3d(x11,y11,z11,tube_radius=radius,color=(0,0,0))
	
mlab.points3d(0,0,0,scale_factor=50)

mlab.show()

