import numpy as np
import os

cwd = os.getcwd()

################################################################################
# Read data from ./out/interfaces.txt
################################################################################

interPath = cwd + '/out/interfaces.txt'
interfaces = np.loadtxt(interPath,unpack=True,skiprows=3)

for line in xrange(np.shape(interfaces)[1]):
	for column in xrange(np.shape(interfaces)[0]-1):
		if (interfaces[column,line]>interfaces[column+1,line]):
			print("interfaces.txt file has crossing points")
			print(column, line)







