import numpy as np
import matplotlib.pyplot as plt

fac = 1.0E19
dataX, dataY = np.loadtxt("src/visc-profile-forte.csv",unpack=True,delimiter=",")
dataX = dataX * fac

dataV, dataW = np.loadtxt("src/visc-profile-ricard.csv",unpack=True,delimiter=",")
dataV = dataV * fac

dataI, dataJ = np.loadtxt("src/visc-profile-king.csv",unpack=True,delimiter=",")
dataI = dataI * fac

plt.figure(figsize=(5,5))
plt.xlim(1.0E17,1.0E20)
plt.ylim(2800,0)
plt.xlabel("Viscosity (Pa s)")
plt.ylabel("Depth (km)")

plt.semilogx(dataX,dataY,label="Forte et al. (1995)")
plt.semilogx(dataV,dataW,label="Ricard et al. (1989)")
plt.semilogx(dataI,dataJ,label="King and Masters (1992)")
plt.hlines(660,1.0E17,1.0E21,linestyles="dashed",color=(0.5,0.5,0.5))
plt.hlines(1000,1.0E17,1.0E21,linestyles="dashed",color=(0.5,0.5,0.5))
plt.yticks(np.append(np.linspace(0,2500,6),660))

plt.legend(loc=3)
plt.tight_layout()
plt.savefig("figures/visc-profiles.pdf")
plt.show()

