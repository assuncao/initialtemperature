import numpy as np
import matplotlib.pyplot as plt
from pylab import cm

slab_75_6_50 = -47.00727777614565
slab_75_7_50 = -44.856918001162896
slab_75_8_50 = -43.43623256583329
simu_75_6_50 = -51.81022419169523
simu_75_7_50 = -39.748760803315065
simu_75_8_50 = -35.274411963107305

slab_75_6_25 = -45.273359892982974
slab_75_7_25 = -43.940467369343246
slab_75_8_25 = -42.89930062684017
simu_75_6_25 = -50.84911715857997
simu_75_7_25 = -42.17003333264253
simu_75_8_25 = -38.68507651803672

slab_80_6_50 = -47.28865303723519
slab_80_7_50 = -45.66481296970814
slab_80_8_50 = -44.41336434439902
simu_80_6_50 = -52.13358505096557
simu_80_7_50 = -44.01205545771075
simu_80_8_50 = -38.92382182853098

slab_80_6_25 = -45.66481296970814
slab_80_7_25 = -44.856918001162896
slab_80_8_25 = -44.41336434439902
simu_80_6_25 = -51.56833464517102
simu_80_7_25 = -45.56309642712913
simu_80_8_25 = -42.18665419580807

slab_85_6_50 = -51.77824504603317
slab_85_7_50 = -47.00727777614565
slab_85_8_50 = -45.66481296970814
simu_85_6_50 = -66.85368626624762
simu_85_7_50 = -49.0989665989355
simu_85_8_50 = -43.42586372560911

slab_85_6_25 = -50.9398800116804
slab_85_7_25 = -45.273359892982974
slab_85_8_25 = -44.856918001162896
simu_85_6_25 = -66.464220195596
simu_85_7_25 = -48.52967980575713
simu_85_8_25 = -45.00426169872643

slabV50 = np.array([[slab_85_6_50,slab_85_7_50,slab_85_8_50],
					[slab_80_6_50,slab_80_7_50,slab_80_8_50],
				  	[slab_75_6_50,slab_75_7_50,slab_75_8_50]])

simuV50 = np.array([[simu_85_6_50,simu_85_7_50,simu_85_8_50],
					[simu_80_6_50,simu_80_7_50,simu_80_8_50],
					[simu_75_6_50,simu_75_7_50,simu_75_8_50]])

slabV25 = np.array([[slab_85_6_25,slab_85_7_25,slab_85_8_25],
					[slab_80_6_25,slab_80_7_25,slab_80_8_25],
					[slab_75_6_25,slab_75_7_25,slab_75_8_25]])

simuV25 = np.array([[simu_85_6_25,simu_85_7_25,simu_85_8_25],
					[simu_80_6_25,simu_80_7_25,simu_80_8_25],
					[simu_75_6_25,simu_75_7_25,simu_75_8_25]])

x_values 	= [0,1,2]
x_text		= ['6','7','8']
y_values 	= [0,1,2]
y_text		= ['85.5','80.0','75.0']

value50 = simuV50 - slabV50
value25 = simuV25 - slabV25

mintick, maxtick = -10, 10
div = 10
color_under = (000./255,000./255.,070./255.)
color_over = (070./255,000./255.,000./255.)


condNan = ~np.isnan(value50)
cmap = cm.get_cmap('coolwarm', div)
#cticks = np.linspace(np.min(value50[condNan]),np.max(value50[condNan]),div+1)
cticks = np.linspace(mintick,maxtick,div+1)

plt.figure()
plt.title("Dip angle differences (5 Myr)")
cs = plt.imshow(value50,cmap=cmap)
plt.xticks(x_values,x_text)
plt.yticks(y_values,y_text)
plt.xlabel("Oceanic Crust Thk. (km)")
plt.ylabel("Oceanic Lit. Thk. (km)")

cs.set_clim(mintick, maxtick)
cs.cmap.set_over(color=color_over)
cs.cmap.set_under(color=color_under)
plt.colorbar(cs,ticks=cticks,label="Dip Angle diff. (in degrees)",extend='both')

#plt.grid(b=True, which='major', axis='both')
plt.tight_layout()
plt.savefig('./figures/corr_5.png')
#plt.show()

cticks = np.linspace(mintick,maxtick,div+1)

plt.figure()
plt.title("Dip angle differences (2.5 Myr)")
cs = plt.imshow(value25,cmap=cmap)
plt.xticks(x_values,x_text)
plt.yticks(y_values,y_text)
plt.xlabel("Oceanic Crust Thk. (km)")
plt.ylabel("Oceanic Lit. Thk. (km)")

cs.set_clim(mintick, maxtick)
cs.cmap.set_over(color=color_over)
cs.cmap.set_under(color=color_under)
plt.colorbar(cs,ticks=cticks,label="Dip Angle diff. (in degrees)",extend='both')

#plt.grid(b=True, which='major', axis='both')
plt.tight_layout()
plt.savefig('./figures/corr_25.png')
plt.show()
