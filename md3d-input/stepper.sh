MIN=0
MED=$(awk 'FNR==1 {print FILENAME" "$2 ; nextfile}' Tempo_*.txt | awk 'BEGIN {V=0;NAME} {if ($2 <2.5e+06 && $2 > V) {V=$2; NAME=$1}} END {print NAME}')
MAX=$(ls -v Tempo_* | tail -n 1)

NUM_MIN=$(echo $MIN | tr -dc '0-9')
NUM_MED=$(echo $MED | tr -dc '0-9')
NUM_MAX=$(echo $MAX | tr -dc '0-9')

bash ~/runs/zipper.sh -step $NUM_MIN $NUM_MED $NUM_MAX
