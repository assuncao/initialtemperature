import numpy as np
import re
from mayavi import mlab
import matplotlib.pyplot as plt

velocPath 		= './out/veloc_0_3D.txt'
file	 		= open(velocPath,"r")
sx,sy,sz 		= re.split("[ \t]+", file.readline().strip())
sLon,sLat,sDep 	= int(sx),int(sy),int(sz)
w,e,s,n,t,b 	= re.split("[ \t]+", file.readline().strip())
westL,eastL,southL,northL,topL,bottomL = float(w),float(e),float(s),float(n),float(t),float(b)
w,e,s,n,t,b 	= re.split("[ \t]+", file.readline().strip())
westD,eastD,southD,northD,topD,bottomD = float(w),float(e),float(s),float(n),float(t),float(b)
file.close()

filePath		= './out/Veloc_fut_10.txt'
skiprows		= 2
veloc 			= np.loadtxt(filePath,skiprows=skiprows,comments='P')

u = np.zeros(shape=(sLon,sLat,sDep))
v = np.zeros(shape=(sLon,sLat,sDep))
w = np.zeros(shape=(sLon,sLat,sDep))

c = 0
for k in xrange(sDep):
	for j in xrange(sLat):
		for i in xrange(sLon):
			u[i,j,k] = veloc[c]
			v[i,j,k] = veloc[c+1]
			w[i,j,k] = veloc[c+2]
			c += 3

extent3d = [westL,eastL,southL,northL,bottomL,topL]
extent2d = [westL,eastL,bottomL,topL]

x, y, z = np.mgrid[westL:eastL:(sLon*1j), southL:northL:(sLat*1j), bottomL:topL:(sDep*1j)]

mlab.figure(size=(1000,600))

mlab.quiver3d(x,y,z,u,v,w)

#src = mlab.pipeline.vector_field(u,v,w)
#mlab.pipeline.vectors(src,scale_factor=1.5,extent=extent3d)

mlab.outline(extent=extent3d)
mlab.view(azimuth=270,elevation=90)
mlab.show()

