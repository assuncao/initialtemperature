import geopandas as gp
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon

import progressBar as pbar

map = gp.read_file('/Users/kugelblitz/Desktop/initialTemperature/src/lim_pais_a.shp')

for j in xrange(param.lat.size-1):
	aux1,aux2 		= np.round(param.lat.start.degree*100,0),np.round(j*param.lat.delta.degree*100,0)
	print("*** Defining if longitude points are inside the continental area (latitude "+str((aux1+aux2)/100.)+") ***")
	barIsContinent = pbar.Bar(np.size(fieldLon))
	barIsContinent.start()
	isContinent = np.zeros(np.size(fieldLon))
	for i in xrange(np.size(isContinent)):
		point = Point(longitude[i],latitude[j])
		if (map.contains(point)).any():
			isContinent[i] = 1
		else:
			isContinent[i] = 0
		barIsContinent.update(i)
	barIsContinent.end()
