import numpy as np
import matplotlib.pyplot as plt

Nx, Ny, Nz = 501,2,71

filePath = '/Users/kugelblitz/Desktop/interfaces_test_i8/interfaces.txt'

skiprows = 3
interfaces = np.loadtxt(filePath,skiprows=skiprows)

plt.figure(figsize=(15,2))
plt.plot(interfaces[0:501,0])
plt.plot(interfaces[0:501,1])
plt.plot(interfaces[0:501,2])
plt.plot(interfaces[0:501,3])
plt.plot(interfaces[0:501,4])
plt.plot(interfaces[0:501,5])
plt.plot(interfaces[0:501,6])
plt.plot(interfaces[0:501,7])
plt.show()
