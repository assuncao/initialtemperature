import numpy as np
import matplotlib.pyplot as plt

Nx, Ny, Nz = 309, 2, 51

interfaces = np.loadtxt('./out/interfaces.txt',skiprows=3)

plt.figure(figsize=(15,5))

plt.hlines(0,0,309)

plt.plot(interfaces[:,0],label="interface 1")
plt.plot(interfaces[:,1],label="interface 2")
plt.plot(interfaces[:,2],label="interface 3")
plt.plot(interfaces[:,3],label="interface 4")
plt.plot(interfaces[:,4],label="interface 5")
plt.plot(interfaces[:,5],label="interface 6")
plt.plot(interfaces[:,6],label="interface 7")
plt.plot(interfaces[:,7],label="interface 8")

plt.legend()
plt.plot()
plt.show()
