import numpy as np
import re,os,sys

################################################################################
# Plot control
################################################################################

pltPermission = 'True'

if (pltPermission=='True'):
	import matplotlib.pyplot as plt

################################################################################
# Loading reference values from reference class
################################################################################

cwd = os.getcwd()
libsLocation = os.path.join(cwd,'lib')
sys.path.append(libsLocation)

import reference, classes, methods, projection, data

################################################################################
# Read data from ./src/params.txt
################################################################################

parPath = './src/params.txt'
par 	= open(parPath)
lon 	= classes.Parameters(re.split("[ ]+", par.readline().strip()),"lon")
lat 	= classes.Parameters(re.split("[ ]+", par.readline().strip()),"lat")
depth 	= classes.Parameters(re.split("[ ]+", par.readline().strip()),"depth")
par.close()

fieldTemperature 	= np.zeros(shape=(lon.length,lat.length,depth.length))
fieldLon			= np.linspace(lon.startKm,lon.endKm,lon.length)
fieldLat			= np.linspace(lat.startKm,lat.endKm,lat.length)
fieldDepth			= np.linspace(depth.start,depth.end,depth.length)
auxFieldDepth		= np.zeros(np.size(fieldDepth))

refValuesPath 	= './src/reference.txt'
refValuesFile 	= open(refValuesPath)
refValues		= []
for line in refValuesFile:
	refValues.append(line)

dimRef 			= reference.Reference(refValues,depth.end)

################################################################################
# Load slab model (top and bottom surfaces)
################################################################################

approxModelPath 	= './out/approx/approx.'
projecModelPath 	= './out/projec/slice.'
interfacesPath		= './out/interfaces.txt'
interfaces 	= open(interfacesPath,"w")
# mantle, oceanicLit, oceanicCrust, mantle, lubricantLayer, transLit, continentalLit, transCrust, continentalCrust
stringFac 		= str(dimRef.mantleFactor) 				+ " " + \
				str(dimRef.oceanicLitFactor) 			+ " " + \
				str(dimRef.oceanicCrustFactor) 			+ " " + \
				str(dimRef.mantleFactor) 				+ " " + \
				str(dimRef.lubricantLayerFactor) 		+ " " + \
				str(dimRef.transitionalLitFactor) 		+ " " + \
				str(dimRef.continentalLitFactor)		+ " " + \
				str(dimRef.transitionalCrustFactor) 	+ " " + \
				str(dimRef.continentalCrustFactor)
stringDensity = str(dimRef.mantleDensity) 				+ " " + \
				str(dimRef.oceanicLitDensity) 			+ " " + \
				str(dimRef.oceanicCrustDensity) 		+ " " + \
				str(dimRef.mantleDensity) 				+ " " + \
				str(dimRef.lubricantLayerDensity) 		+ " " + \
				str(dimRef.transitionalLitDensity) 		+ " " + \
				str(dimRef.continentalLitDensity) 		+ " " + \
				str(dimRef.transitionalCrustDensity) 	+ " " + \
				str(dimRef.continentalCrustDensity)
stringRadHeat = str(dimRef.mantleRadHeat) 				+ " " + \
				str(dimRef.oceanicLitRadHeat) 			+ " " + \
				str(dimRef.oceanicCrustRadHeat) 		+ " " + \
				str(dimRef.mantleRadHeat) 				+ " " + \
				str(dimRef.lubricantLayerRadHeat) 		+ " " + \
				str(dimRef.transitionalLitRadHeat) 		+ " " + \
				str(dimRef.continentalLitRadHeat) 		+ " " + \
				str(dimRef.transitionalCrustRadHeat) 	+ " " + \
				str(dimRef.continentalCrustRadHeat)

interfaces.write(stringFac+"\n")
interfaces.write(stringDensity+"\n")
interfaces.write(stringRadHeat+"\n")

#class Interface:
#	def __init__(self,N)
#		self.values = np.zeros(N)
#		self.size	= N

################################################################################
# Load LAB depth and Moho depth
################################################################################

lithoPath = cwd + '/out/litho.txt'
litho = np.loadtxt(lithoPath,unpack=True)
litho = litho * (-1)

crustPath = cwd + '/out/crust.txt'
crust = np.loadtxt(crustPath,unpack=True)
crust = crust * (-1)

################################################################################
# Main Latitude Loop
################################################################################

for j in xrange(lat.length): # Change the latitude limit to see others
	
	if (pltPermission=='True'):
#		scale = 2.2
#		plt.figure(figsize=(7*scale,scale))
		fig,ax = plt.subplots(figsize=(16,2.55))
#		fig,ax = plt.subplots(figsize=(12,7))
		ax.set_aspect(1.0)
		plt.xlim(lon.startKm,lon.endKm)
#		ax.set_xlim(500,2000)
#		plt.xticks(np.linspace(500,2000,6))
		plt.xticks(np.linspace(lon.startKm,lon.endKm,11))
	
	aux1,aux2 		= np.round(lat.startDegree*100,0),np.round(j*lat.delta*100,0)
	approxModelFile = approxModelPath + str(int(aux1+aux2)).zfill(5)
	approx 			= classes.Surface(np.loadtxt(approxModelFile))
	projecModelFile = projecModelPath + str(int(aux1+aux2)).zfill(5)
	projec 			= classes.Surface(np.loadtxt(projecModelFile))

	############################################################################
	# Make main surfaces
	############################################################################
	
#	transitionBoundary = methods.findSlabSurfaceDepth(approx.ordinate,approx.abscissa,data.continentLitDepth())

	lithoLon = fieldLon[(fieldLon>=approx.abscissa[0])&(fieldLon<=approx.abscissa[-1])]
	lithoDepth = litho[(fieldLon>=lithoLon[0])&(fieldLon<=lithoLon[-1]),j]
	transitionBoundary = methods.findContactBoundary(approx.ordinate,approx.abscissa,lithoLon,lithoDepth)
	
	# Interface between the mantle and the oceanic lithosphere
	interface1		= np.zeros(lon.length)
	westLimit		= projec.abscissa[0]
	eastLimit 		= projec.abscissa[-1]
	
	if (projec.abscissa[-1]<approx.abscissa[-1]):
		for i in xrange(interface1.size):
			if (fieldLon[i]<westLimit):
				interface1[i] = litho[i,j]
			elif (fieldLon[i]>=westLimit) and (fieldLon[i]<=eastLimit):
				interface1[i] = methods.findSlabSurfaceDepth(projec.abscissa,projec.ordinate,fieldLon[i])
			else:
				aW,bW,aE,bE = methods.slabLimitCalc(approx,projec)
				if (fieldLon[i]<=approx.abscissa[-1]):
					interface1[i] = bE + aE * fieldLon[i]
				else:
					interface1[i] = approx.ordinate[-1]
	else:
		for i in xrange(interface1.size):
			if (fieldLon[i]<westLimit):
				interface1[i] = litho[i,j]
			elif (fieldLon[i]>=westLimit) and (fieldLon[i]<=eastLimit):
				interface1[i] = methods.findSlabSurfaceDepth(projec.abscissa,projec.ordinate,fieldLon[i])
			else:
				interface1[i] = projec.ordinate[-1]

	interface1[interface1>0] = 0.0

	# Oceanic Lithosphere to Oceanic Crust Interface
	abs,ord = projection.makeInnwardProjection(approx.abscissa.size,approx.abscissa,approx.ordinate,(-1)*data.oceanicCrustDepth())
	interface2 	= np.zeros(lon.length)
	westLimit	= abs[0]
	eastLimit 	= abs[-1]

	if (projec.abscissa[-1]<approx.abscissa[-1]):
		for i in xrange(interface2.size):
			if (fieldLon[i]<westLimit):
				interface2[i] = data.oceanicCrustDepth()
			elif (fieldLon[i]>=westLimit) and (fieldLon[i]<=eastLimit):
				interface2[i] = methods.findSlabSurfaceDepth(abs,ord,fieldLon[i])
			else:
				aW,bW,aE,bE = methods.slabLimitCalc(approx,projec)
				if (fieldLon[i]<=abs[-1]):
					interface2[i] = bE + aE * fieldLon[i]
				else:
					interface2[i] = approx.ordinate[-1]
	else:
		for i in xrange(interface2.size):
			if (fieldLon[i]<westLimit):
				interface2[i] = data.oceanicCrustDepth()
			elif (fieldLon[i]>=westLimit) and (fieldLon[i]<=eastLimit):
				interface2[i] = methods.findSlabSurfaceDepth(abs,ord,fieldLon[i])
			else:
				if (fieldLon[i]<=projec.abscissa[-1]):
					interface3[i] = bE + aE * fieldLon[i]
				else:
					interface3[i] = projec.ordinate[-1]

	interface2[interface2>0] = 0.0

	# Oceanic Crust top
	interface3		= np.zeros(lon.length)
	westLimit		= abs[0]#approx.abscissa[0]
	eastLimit		= abs[-1]#approx.abscissa[-1]

	if (projec.abscissa[-1]<approx.abscissa[-1]):
		for i in xrange(interface3.size):
			if (fieldLon[i]<westLimit):
				interface3[i] = 0.
			elif (fieldLon[i]>=westLimit) and (fieldLon[i]<=eastLimit):
				interface3[i] = methods.findSlabSurfaceDepth(approx.abscissa,approx.ordinate,fieldLon[i])
			else:
				interface3[i] = approx.ordinate[-1]
	else:
		for i in xrange(interface3.size):
			if (fieldLon[i]<westLimit):
				interface3[i] = 0.
			elif (fieldLon[i]>=westLimit) and (fieldLon[i]<=eastLimit):
				interface3[i] = methods.findSlabSurfaceDepth(approx.abscissa,approx.ordinate,fieldLon[i])
			else:
				aW,bW,aE,bE = methods.slabLimitCalc(approx,projec)
				if (fieldLon[i]<projec.abscissa[-1]):
					interface3[i] = bE + aE * fieldLon[i]
				else:
					interface3[i] = projec.ordinate[-1]

	interface3[interface3>0] = 0.0

	# Lubricant layer bottom
	interface4	= np.zeros(lon.length)
	westLimit 	= abs[0]#approx.abscissa[0]
	eastLimit 	= transitionBoundary

	for i in xrange(interface4.size):
		if (fieldLon[i]<westLimit):
			interface4[i] = 0.
		elif (fieldLon[i]>=westLimit) and (fieldLon[i]<=eastLimit):
			interface4[i] = methods.findSlabSurfaceDepth(approx.abscissa,approx.ordinate,fieldLon[i])
		else:
			interface4[i] = litho[i,j]#data.continentLitDepth()

	interface4[interface4>0] = 0.0

	# Continental Lithosphere Bottom
	abs,ord = projection.makeOutwardProjection(approx.abscissa.size,approx.abscissa,approx.ordinate,data.lubricantLayerThickness())
#	ord[ord>0] = 0.
	interface5	= np.zeros(lon.length)
#	westLimit	= abs[0]#approx.abscissa[0]
	eastLimit	= np.max(abs[ord>=data.continentLitDepth()])
#	print(eastLimit)

	for i in xrange(interface5.size):
		if (fieldLon[i]<westLimit):
			interface5[i] = 0.
		elif (fieldLon[i]>=westLimit) and (fieldLon[i]<=eastLimit):
			interface5[i] = methods.findSlabSurfaceDepth(abs,ord,fieldLon[i])
			if (interface5[i]<data.continentLitDepth()):
				interface5[i] = litho[i,j]#data.continentLitDepth()
		else:
			interface5[i] = litho[i,j]#data.continentLitDepth()

	interface5[interface5>0] = 0.0
	
	# Transitional Crust Bottom
	interface6		= np.zeros(lon.length)
	westLimit		= approx.abscissa[0]
	eastLimit		= np.max(abs[ord>=data.continentLitDepth()])

	for i in xrange(interface6.size):
		if (fieldLon[i]<westLimit):
			interface6[i] = 0.
		elif ((fieldLon[i]>=westLimit) and (fieldLon[i]<=eastLimit)):
			aux1 = data.continentalCrustDepth()
			aux2 = methods.findSlabSurfaceDepth(abs,ord,fieldLon[i])
			interface6[i] = max(aux1,aux2)
		else:
			if (fieldLon[i]<approx.abscissa[-1]):
				aux1 = litho[i,j]#data.continentLitDepth()
				aux2 = methods.findSlabSurfaceDepth(approx.abscissa,approx.ordinate,fieldLon[i])
				interface6[i] = max(aux1,aux2)
			else:
				interface6[i] = litho[i,j]#data.continentLitDepth()

	interface6[interface6>0] = 0.0

	# Continental Crust Bottom
	interface7 	= np.zeros(lon.length)
	westLimit	= approx.abscissa[0]
	eastLimit	= np.max(abs[ord>=data.continentLitDepth()])

	for i in xrange(interface7.size):
		if (fieldLon[i]<westLimit):
			interface7[i] = 0.
		elif ((fieldLon[i]>=westLimit) and (fieldLon[i]<=eastLimit)):
			aux1 = data.continentalCrustDepth()
			aux2 = methods.findSlabSurfaceDepth(abs,ord,fieldLon[i])
			interface7[i] = max(aux1,aux2)
		else:
			interface7[i] = data.continentalCrustDepth()

	interface7[interface7>0] = 0.0

	# Transitional Crust Top
	interface8	= np.zeros(lon.length)
	westLimit	= approx.abscissa[0]
	eastLimit	= np.max(abs[ord>=data.continentLitDepth()])
	
	for i in xrange(interface8.size):
		if (fieldLon[i]<eastLimit):
			interface8[i] = 0.
		else:
			interface8[i] = data.continentalCrustDepth()

	interface8[interface8>0] = 0.0

	############################################################################
	# Save horizons
	############################################################################

	for i in xrange(fieldLon.size):
		if (interface1[i]>interface2[i]):
			interface2[i] = interface1[i]
		if (interface2[i]>interface3[i]):
			interface3[i] = interface2[i]
		if (interface3[i]>interface4[i]):
			interface4[i] = interface3[i]
		if (interface4[i]>interface5[i]):
			interface5[i] = interface4[i]
		if (interface5[i]>interface6[i]):
			interface6[i] = interface5[i]
		if (interface6[i]>interface7[i]):
			interface7[i] = interface6[i]
		if (interface7[i]>interface8[i]):
			interface8[i] = interface7[i]

		string = str(format(interface1[i]*1000,'.10f')) + " " + \
				 str(format(interface2[i]*1000,'.10f')) + " " + \
				 str(format(interface3[i]*1000,'.10f')) + " " + \
				 str(format(interface4[i]*1000,'.10f')) + " " + \
				 str(format(interface5[i]*1000,'.10f')) + " " + \
				 str(format(interface6[i]*1000,'.10f')) + " " + \
				 str(format(interface7[i]*1000,'.10f')) + " " + \
				 str(format(interface8[i]*1000,'.10f')) + "\n"
		interfaces.write(string)

	if (pltPermission=='True'):
#		plt.plot(approx.abscissa,approx.ordinate)
#		plt.plot(fieldLon,interface1)
#		plt.plot(fieldLon,interface2)
#		plt.plot(fieldLon,interface3)
#		plt.plot(fieldLon,interface4)
#		plt.plot(fieldLon,interface5)
#		plt.plot(fieldLon,interface6)
#		plt.plot(fieldLon,interface7)
#		plt.plot(fieldLon,interface8)

		plt.fill_between(fieldLon,interface1,-700,color=(153.0/255,0.0/255,204.0/255),label="Mantle")
		plt.fill_between(fieldLon,interface2,interface1,color=(102.0/255,0,204.0/255),label="Oceanic Lithosphere")
		plt.fill_between(fieldLon,interface3,interface2,color=(0,102.0/255,204.0/255),label="Oceanic Crust")
		plt.fill_between(fieldLon,interface4,interface3,color=(153.0/255,0.0/255,204.0/255))
		plt.fill_between(fieldLon,interface5,interface4,label="Lubricant Layer",color=(51.0/255,204.0/255,0))
		plt.fill_between(fieldLon,interface6,interface5,label="Contact Litosphere",color=(1,1,0))
		plt.fill_between(fieldLon,interface7,interface6,label="Continental Lithosphere",color=(1,128.0/255,0))
		plt.fill_between(fieldLon,interface8,interface7,label="Contact Crust",color=(1,0,0))
		plt.fill_between(fieldLon,0,interface8,label="Continental Crust",color=(1,0,1))

	if (pltPermission=='True'):
		plt.legend(loc=3)
		plt.ylim(-700,0)
		plt.xlabel("Length (km)")
		plt.ylabel("Depth (km)")
		ax2 = ax.twiny()
		ax2.set_xlim(-80.,-40.)
		#	ax2.set_xlim(-75.5,-62) # originial
		plt.xlabel("Longitude ($\!^\circ\!$)")
		plt.savefig("./figures/interface8.pdf")
		plt.show()

interfaces.close()
