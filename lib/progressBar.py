import numpy as np
import sys

class Bar:
	def __init__(self,length):
		self.width 		= 78
		self.step		= float(self.width) / float(length)
		self.increment 	= self.step
		self.pos		= 0

	def start(self):
		sys.stdout.write("[%s]" % (" " * self.width))
		sys.stdout.flush()
		sys.stdout.write("\b" * (self.width+1)) # return to start of line, after '['

	def update(self,current):
		if (current>=self.pos):
			self.pos += self.increment
			leap = int(np.ceil(self.pos))
			if (leap>self.width):
				leap = self.width
			sys.stdout.write(">" * leap)
			sys.stdout.flush()
			sys.stdout.write("\b" * (leap))
	def end(self):
		sys.stdout.write(">" * self.width)
		sys.stdout.flush()
		sys.stdout.write("\b" * (self.width))
		sys.stdout.write("\n")

class Box:
	def __init__(self):
		self.width = 78
	def top(self):
		sys.stdout.write("*%s*\n" % ("-" * self.width))
	def mid(self,string=""):
		sys.stdout.write("|%s|" % (" " * self.width))
		sys.stdout.write("\b" * (self.width))
		print(string)
	def bot(self):
		sys.stdout.write("*%s*\n" % ("-" * self.width))
