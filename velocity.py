import numpy as np
import re, os, sys

################################################################################
# Plot control
################################################################################

pltPermission	= 'True'
showPlot		= 'False'

if (pltPermission=='True'):
	import matplotlib.pyplot as plt
	plt.clf()
	plt.close('all')

################################################################################
# Loading reference values from reference class
################################################################################

cwd = os.getcwd()
libsLocation = os.path.join(cwd,'lib')
sys.path.append(libsLocation)

import classes, methods

################################################################################
# Load LAB depth
################################################################################

lithoPath = cwd + '/out/litho.txt'
litho = np.loadtxt(lithoPath,unpack=True)
litho = litho * (-1)

################################################################################
# Read data from ./src/params.txt
################################################################################

print("*** Reading initial parameters *** ")
parPath = cwd + '/src/params.txt'
param 	= classes.Dimension(parPath)

velocityX		 	= np.zeros(shape=(param.lon.size,param.lat.size,param.depth.size))
velocityY		 	= np.zeros(shape=(param.lon.size,param.lat.size,param.depth.size))
velocityZ		 	= np.zeros(shape=(param.lon.size,param.lat.size,param.depth.size))
fieldLon			= np.linspace(param.lon.start.km,param.lon.end.km,param.lon.size)
fieldLat			= np.linspace(param.lat.start.km,param.lat.end.km,param.lat.size)
fieldDepth			= np.linspace(param.depth.start,param.depth.end,param.depth.size)
auxFieldDepth		= np.zeros(np.size(fieldDepth))

depth = np.linspace(-1000,0,param.depth.size)
#depth = np.linspace(0,-1000,101)

param.lat.size = 1

################################################################################
# Read dimensional reference values
################################################################################

refFilePath 	= cwd + '/src/reference.txt'
dimRef 			= classes.Reference(refFilePath,param.depth.end)

auxDepth 			= fieldDepth[fieldDepth>=dimRef.TZdepth]

################################################################################
# Read interfaces.txt
################################################################################

skiprows			= 7
interfaceFilePath 	= cwd + '/out/interfaces_creep.txt'
interfaces 			= np.loadtxt(interfaceFilePath,unpack='True',skiprows=skiprows)
interfaces			= np.reshape(interfaces,(8,param.lon.size,param.lat.size),order='F')

################################################################################
# Main loop
################################################################################

offNormalVelocity = 'False'

for j in xrange(param.lat.size):
	westDepth = litho[0,j] #* 2.
	eastDepth = 3.0*litho[-1,j] #* 2.

	veloIn 		= methods.velocityIn(westDepth,eastDepth,depth,dimRef.slabVelocitySI,dimRef.TZdepth)
	integralIn 	= methods.calculateIntegral(veloIn,depth[1]-depth[0])
	
	veloOut 	= methods.velocityOut(eastDepth,westDepth,depth,dimRef.slabVelocitySI,dimRef.TZdepth,integralIn)
	integralOut = methods.calculateIntegral(veloOut,depth[1]-depth[0])
	
	print "Integral diference:",integralOut-integralIn

	if (offNormalVelocity!='True'):
		velocityX[0,j,:] 	= veloIn#[::-1]# * (-1.)
		velocityX[-1,j,:] 	= veloOut#[::-1]# * (-1.)

	interfaceNum 	= 3
	cond 			= np.where(interfaces[interfaceNum,:,j]==0)[0]

	# interface 3 -> Slab2 top -> interfaces[2]
	velocityX[cond,j,-1] = velocityX[0,j,-1]
#	cond 	= np.where(interfaces[5,:,j]==0)[0]
	auxCond = cond[-1]+10 # impose velocity 4 elements after the slab start descending
	for i in xrange(param.lon.size):
		if (i<=auxCond):
			velocityX[i,j,-1] = dimRef.slabVelocitySI#velocityX[0,j,-1]
		else:
			pass

linewidth = 2.5
lettersize = 14
plt.rcParams.update({'font.size': lettersize})
fig,ax1 = plt.subplots(figsize=(6,10))
plt.tight_layout(rect=[0.1,0.05,1,0.95])

#ax1.set_aspect(3.)
ax1.plot(velocityX[0,j,::-1],fieldDepth,label="West Side",linewidth=linewidth)
ax1.plot(velocityX[-1,j,::-1],fieldDepth,label="East Side",linewidth=linewidth)
#plt.plot(veloIn,depth,label="In",linewidth=linewidth)
#plt.plot(veloOut,depth,label="Out",linewidth=linewidth)

#plt.tight_layout()
plt.xlim(np.min(velocityX),np.max(velocityX))
ax1.set_ylim(param.depth.end,param.depth.start)
ax1.set_ylabel("Depth (km)")
ax1.set_yticks([0,westDepth,-100,-200,-300,-400,-500,-600,-660,-700,-800,-900,-1000])
ax1.set_xlabel("Velocity (m/s)")
ax1.set_xticks(np.linspace(np.min(velocityX),np.max(velocityX),7))

ax1.hlines(dimRef.TZdepth,np.min(velocityX),np.max(velocityX),linestyles="dashed")
ax1.hlines(westDepth,np.min(velocityX),np.max(velocityX),linestyles="dashed")
ax1.hlines(eastDepth,np.min(velocityX),np.max(velocityX),linestyles="dashed")
ax1.hlines(-100,np.min(velocityX),np.max(velocityX),linestyles="dashed")

#ax1.hlines(param.depth.end-eastDepth,np.min(velocityX),np.max(velocityX),linestyles="dashed")
#ax1.hlines(param.depth.end-westDepth,np.min(velocityX),np.max(velocityX),linestyles="dashed")

ax1.legend()

ax2 = ax1.twiny()
ax2.set_xlim(np.min(velocityX)*(1000*365.25*60*24*60),np.max(velocityX)*(1000*365.25*60*24*60))
#ax2.set_xlim(np.max(velocityX)*(1000*365.25*60*24*60),np.min(velocityX)*(1000*365.25*60*24*60))
ax2.set_xlabel("Velocity (mm/yr)")

#ax3 = ax1.twinx()
#ax3.set_ylim(param.depth.end,param.depth.start)

#plt.tight_layout()
#ax1.set_aspect(3.)
plt.savefig('./figures/velocity_profile.png')
if (showPlot=='True'):
	plt.show()

velocityPath 	= './out/veloc_0_3D.txt'
velocityFile	= open(velocityPath,"w")

lengthStr			= str(param.lon.size)+"\t"+str(param.lat.size)+"\t"+str(param.depth.size)
extentDistStr		= str(param.lon.start.km)+"\t"+str(param.lon.end.km)+"\t"+str(param.lat.start.km)+"\t"+str(param.lat.end.km)+"\t"+str(param.depth.start)+"\t"+str(param.depth.end)
extentDegrStr		= str(param.lon.start.degree)+"\t"+str(param.lon.end.degree	)+"\t"+str(param.lat.start.degree)+"\t"+str(param.lat.end.degree)+"\t"+str(param.depth.start)+"\t"+str(param.depth.end)

velocityFile.write(lengthStr+"\n")
velocityFile.write(extentDistStr+"\n")
velocityFile.write(extentDegrStr+"\n")

velocityFile.write("# v4\n")

#velocityX = velocityX*0
#velocityY = velocityY*0
#velocityZ = velocityZ*0

for k in xrange(param.depth.size):
	for j in xrange(param.lat.size):
		for i in xrange(param.lon.size):
			velocityFile.write(str(velocityX[i,j,k])+"\n")
			if (param.lat.size>1):
				velocityFile.write(str(velocityY[i,j,k])+"\n")
			velocityFile.write(str(velocityZ[i,j,k])+"\n")

velocityFile.close()
