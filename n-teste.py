import numpy as np
import concurrent.futures

def foo(arg1,arg2):
	print(arg1,arg2)

with concurrent.futures.ProcessPoolExecutor() as executor:
	list = [i for i in xrange(0,10)]
	arg  = [3 for i in xrange(0,10)]
	executor.map(foo,arg,list)
