DIR0='/Users/kugelblitz/Desktop/initialTemperature'
DIROUT=$DIR0/out
DIRFIG=$DIR0/figures
DIRSRC=$DIR0/src

cp $DIROUT/interfaces_creep.txt $PWD/.
cp $DIROUT/Temper_0_3D.txt $PWD/.
cp $DIROUT/veloc_0_3D.txt $PWD/.

cp $DIRFIG/interfaces.pdf $PWD/plots/.
cp $DIRFIG/temperature-profile.pdf $PWD/plots/.

cp $DIRSRC/reference.txt $PWD/.
cp $DIRSRC/params.txt $PWD/.