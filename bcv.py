import numpy as np
import re, os, sys

################################################################################
# Loading reference values from reference class
################################################################################

cwd = os.getcwd()
libsLocation = os.path.join(cwd,'lib')
sys.path.append(libsLocation)

import classes

################################################################################
# Read data from ./src/params.txt
################################################################################

print("*** Reading initial parameters *** ")
parPath = cwd + '/src/params.txt'
param 	= classes.Dimension(parPath)

bcvX		 	= np.zeros(shape=(param.lon.size,param.lat.size,param.depth.size))
bcvY		 	= np.zeros(shape=(param.lon.size,param.lat.size,param.depth.size))
bcvZ		 	= np.zeros(shape=(param.lon.size,param.lat.size,param.depth.size))
fieldLon		= np.linspace(param.lon.start.km,param.lon.end.km,param.lon.size)
fieldLat		= np.linspace(param.lat.start.km,param.lat.end.km,param.lat.size)
fieldDepth		= np.linspace(param.depth.start,param.depth.end,param.depth.size)
auxFieldDepth	= np.zeros(np.size(fieldDepth))

################################################################################
# Read dimensional reference values
################################################################################

refFilePath 	= cwd + '/src/reference.txt'
dimRef 			= classes.Reference(refFilePath,param.depth.end)

################################################################################
# Read interfaces.txt
################################################################################

skiprows			= 3
interfaceFilePath 	= cwd + '/out/interfaces.txt'
interfaces 			= np.loadtxt(interfaceFilePath,unpack='True',skiprows=skiprows)
interfaces			= np.reshape(interfaces,(9,param.lon.size,param.lat.size),order='F')

for k in xrange(param.depth.size):
	for j in xrange(param.lat.size):
		for i in xrange(param.lon.size):
			if (fieldDepth[k]>interfaces[3,i,j]/1.0E3):
				bcvX[i,j,k] = 1
				bcvY[i,j,k] = 1
				bcvZ[i,j,k] = 1
			elif (i==0) or (i==param.lon.size-1):
				bcvX[i,j,k] = 1
				bcvY[i,j,k] = 1
				bcvZ[i,j,k] = 1
			elif (fieldDepth[k]==param.depth.end) or (fieldDepth[k]==param.depth.start):
				bcvX[i,j,k] = 0
				bcvY[i,j,k] = 1
				bcvZ[i,j,k] = 1
			else:
				bcvX[i,j,k] = 0
				bcvY[i,j,k] = 1
				bcvZ[i,j,k] = 0

bcvPath = './out/bcv_0_3D.txt'
bcvFile	= open(bcvPath,"w")

lengthStr			= str(param.lon.size)+"\t"+str(param.lat.size)+"\t"+str(param.depth.size)
extentDistStr		= str(param.lon.start.km)+"\t"+str(param.lon.end.km)+"\t"+str(param.lat.start.km)+"\t"+str(param.lat.end.km)+"\t"+str(param.depth.start)+"\t"+str(param.depth.end)
extentDegrStr		= str(param.lon.start.degree)+"\t"+str(param.lon.end.degree	)+"\t"+str(param.lat.start.degree)+"\t"+str(param.lat.end.degree)+"\t"+str(param.depth.start)+"\t"+str(param.depth.end)

bcvFile.write(lengthStr+"\n")
bcvFile.write(extentDistStr+"\n")
bcvFile.write(extentDegrStr+"\n")

bcvFile.write("# v4\n")

for k in xrange(param.depth.size):
	for j in xrange(param.lat.size):
		for i in xrange(param.lon.size):
			bcvFile.write(str(bcvX[i,j,param.depth.size-1-k])+"\n")
			bcvFile.write(str(bcvY[i,j,param.depth.size-1-k])+"\n")
			bcvFile.write(str(bcvZ[i,j,param.depth.size-1-k])+"\n")

bcvFile.close()
