import numpy as np
import matplotlib.pyplot as plt
from pylab import cm

##################
# 75
##################

slab_75_5_25 = -42.89930062684017
slab_75_6_25 = -42.32934868988758
slab_75_7_25 = -42.32934868988758
slab_75_8_25 = -41.727500703513535
simu_75_5_25 = -41.29178392654861
simu_75_6_25 = -39.28881007249647
simu_75_7_25 = -40.52673405862526
simu_75_8_25 = -38.73675928718059

slab_75_5_50 = -42.89930062684017
slab_75_6_50 = -42.32934868988758
slab_75_7_50 = -42.32934868988758
slab_75_8_50 = -41.09662976402061
simu_75_5_50 = -39.468356714714666
simu_75_6_50 = -37.06626068165929
simu_75_7_50 = -37.44853134801895
simu_75_8_50 = -34.88665953110948

##################
# 80
##################

slab_80_5_25 = -42.89930062684017
slab_80_6_25 = -42.32934868988758
slab_80_7_25 = -42.32934868988758
slab_80_8_25 = -41.727500703513535
simu_80_5_25 = -42.94723307024575
simu_80_6_25 = -41.01737013059537
simu_80_7_25 = -40.702265189901034
simu_80_8_25 = -38.776432327470246

slab_80_5_50 = -43.940467369343246
slab_80_6_50 = -42.89930062684017
slab_80_7_50 = -42.89930062684017
slab_80_8_50 = -41.09662976402061
simu_80_5_50 = -42.47106620721135
simu_80_6_50 = -38.07997170820658
simu_80_7_50 = -37.73686364628171
simu_80_8_50 = -34.86442174931735

##################
# 85
##################

slab_85_5_25 = -42.89930062684017
slab_85_6_25 = -42.89930062684017
slab_85_7_25 = -42.32934868988758
slab_85_8_25 = -42.32934868988758
simu_85_5_25 = -44.04929568019457
simu_85_6_25 = -42.220212451496394
simu_85_7_25 = -40.65329951034645
simu_85_8_25 = -40.42517210986029

slab_85_5_50 = -44.41336434439902
slab_85_6_50 = -43.43623256583329
slab_85_7_50 = -42.89930062684017
slab_85_8_50 = -42.32934868988758
simu_85_5_50 = -45.10914011706761
simu_85_6_50 = -40.271949482756625
simu_85_7_50 = -38.2916897285728
simu_85_8_50 = -37.1931791458257

##################
# 90
##################

slab_90_5_25 = -43.43623256583329
slab_90_6_25 = -42.89930062684017
slab_90_7_25 = -42.89930062684017
slab_90_8_25 = -42.32934868988758
simu_90_5_25 = -44.39141636582614
simu_90_6_25 = -42.74335933180757
simu_90_7_25 = -41.91604138821207
simu_90_8_25 = -40.72427222524731

slab_90_5_50 = -44.856918001162896
slab_90_6_50 = -43.940467369343246
slab_90_7_50 = -43.43623256583329
slab_90_8_50 = -42.32934868988758
simu_90_5_50 = -45.847231034998316
simu_90_6_50 = -41.71412535565396
simu_90_7_50 = -39.76132173756403
simu_90_8_50 = -37.86466978466585

slabV50 = np.array([[slab_90_5_50,slab_90_6_50,slab_90_7_50,slab_90_8_50],
					[slab_85_5_50,slab_85_6_50,slab_85_7_50,slab_85_8_50],
					[slab_80_5_50,slab_80_6_50,slab_80_7_50,slab_80_8_50],
				  	[slab_75_5_50,slab_75_6_50,slab_75_7_50,slab_75_8_50]])

simuV50 = np.array([[simu_90_5_50,simu_90_6_50,simu_90_7_50,simu_90_8_50],
					[simu_85_5_50,simu_85_6_50,simu_85_7_50,simu_85_8_50],
					[simu_80_5_50,simu_80_6_50,simu_80_7_50,simu_80_8_50],
					[simu_75_5_50,simu_75_6_50,simu_75_7_50,simu_75_8_50]])

slabV25 = np.array([[slab_90_5_25,slab_90_6_25,slab_90_7_25,slab_90_8_25],
					[slab_85_5_25,slab_85_6_25,slab_85_7_25,slab_85_8_25],
					[slab_80_5_25,slab_80_6_25,slab_80_7_25,slab_80_8_25],
					[slab_75_5_25,slab_75_6_25,slab_75_7_25,slab_75_8_25]])

simuV25 = np.array([[simu_90_5_25,simu_90_6_25,simu_90_7_25,simu_90_8_25],
					[simu_85_5_25,simu_85_6_25,simu_85_7_25,simu_85_8_25],
					[simu_80_5_25,simu_80_6_25,simu_80_7_25,simu_80_8_25],
					[simu_75_5_25,simu_75_6_25,simu_75_7_25,simu_75_8_25]])

x_values 	= [0,1,2,3]
x_text		= ['5','6','7','8']
y_values 	= [0,1,2,3]
y_text		= ['90','85.5','80.0','75.0']

value50 = simuV50 - slabV50
value25 = simuV25 - slabV25

mintick, maxtick = -7, 7
div = 14
color_under = (000./255,000./255.,070./255.)
color_over = (070./255,000./255.,000./255.)


condNan = ~np.isnan(value50)
cmap = cm.get_cmap('coolwarm', div)
#cticks = np.linspace(np.min(value50[condNan]),np.max(value50[condNan]),div+1)
cticks = np.linspace(mintick,maxtick,div+1)

plt.figure()
plt.title("Dip angle differences (5 Myr)")
cs = plt.imshow(value50,cmap=cmap)
plt.xticks(x_values,x_text)
plt.yticks(y_values,y_text)
plt.xlabel("Oceanic Crust Thk. (km)")
plt.ylabel("Oceanic Lit. Thk. (km)")

cs.set_clim(mintick, maxtick)
cs.cmap.set_over(color=color_over)
cs.cmap.set_under(color=color_under)
plt.colorbar(cs,ticks=cticks,label="Dip Angle diff. (in degrees)",extend='both')

#plt.grid(b=True, which='major', axis='both')
plt.tight_layout()
plt.savefig('./figures/corr_5-2800.png')
#plt.show()

cticks = np.linspace(mintick,maxtick,div+1)

plt.figure()
plt.title("Dip angle differences (2.5 Myr)")
cs = plt.imshow(value25,cmap=cmap)
plt.xticks(x_values,x_text)
plt.yticks(y_values,y_text)
plt.xlabel("Oceanic Crust Thk. (km)")
plt.ylabel("Oceanic Lit. Thk. (km)")

cs.set_clim(mintick, maxtick)
cs.cmap.set_over(color=color_over)
cs.cmap.set_under(color=color_under)
plt.colorbar(cs,ticks=cticks,label="Dip Angle diff. (in degrees)",extend='both')

#plt.grid(b=True, which='major', axis='both')
plt.tight_layout()
plt.savefig('./figures/corr_25-2800.png')
plt.show()
