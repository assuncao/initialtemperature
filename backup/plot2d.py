import numpy as np
import re
import matplotlib.pyplot as plt
fileName = './out/temperatureField.dat'

file = open(fileName)
sx,sy,sz 	= re.split("[ \t]+", file.readline().strip())
sLon,sLat,sDep = int(sx),int(sy),int(sz)
w,e,s,n,t,b = re.split("[ \t]+", file.readline().strip())
westL,eastL,southL,northL,topL,bottomL = float(w),float(e),float(s),float(n),float(t),float(b)
w,e,s,n,t,b = re.split("[ \t]+", file.readline().strip())
westD,eastD,southD,northD,topD,bottomD = float(w),float(e),float(s),float(n),float(t),float(b)

temp = np.zeros(shape=(sLon,sLat,sDep))

for j in xrange(sLat):
	for i in xrange(sLon):
		for k in xrange(sDep):
			temp[i,j,sDep-k-1] = file.readline()

extentDist=([westL,eastL,bottomL,topL])
extentDegr=([westD,eastD,bottomD,topD])

ticks = np.linspace(temp.min(),temp.max(),11)

latDist = 500
latDegr = -10

my_input = input("Type '1' for distance or '2' for degree: ")


if (my_input==1):
	latDist = input("Type the distance you want to show in km: ")
	if ((latDist<southL) or (latDist>northL)):
		print "Invalid chosen latitude!"

	else:
		latStep = (northL) / (sLat - 1)
		indexF 	= np.round(latDist/latStep,0)
		indexI	= int(indexF)
		realLat = indexI * latStep
		
		fig, ax = plt.subplots(figsize=(13,3))
		cs = ax.imshow(np.transpose(temp[:,indexI,:]),extent=extentDist,cmap='jet')
		plt.title("Distance "+str(realLat)+"km")
		fig.colorbar(cs,orientation="horizontal",ticks=ticks)
		plt.show()
else:
	latDegr = input("Type the latitude you want to show in degrees: ")
	if ((latDegr<southD) or (latDegr>northD)):
		print "Invalid chosen latitude!"
	
	else:
		latStep = (northD - southD) / (sLat - 1)
		indexF 	= np.round((latDegr-southD)/latStep,0)
		indexI	= int(indexF)
		realLat = southD + indexI * latStep
		
		fig, ax = plt.subplots(figsize=(13,3))
		cs = ax.imshow(np.transpose(temp[:,indexI,:]),extent=extentDegr,cmap='jet')
		ax.set_aspect(aspect=abs(bottomL/(eastL*10)))
		plt.title("Latitude "+str(realLat)+r"$^\circ$")
		fig.colorbar(cs,orientation="horizontal",ticks=ticks)
		plt.show()

#fig = plt.figure()
#ax = fig.add_subplot(111)
#ax.plot(x, y1)
#ax.plot(x, y2)
#plt.show()

