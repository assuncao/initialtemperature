import numpy as np
import re, os, sys

#plotType = '3d'

################################################################################
# Setting up classes and methods
################################################################################

cwd = os.getcwd()
libsLocation = os.path.join(cwd,'lib')
sys.path.append(libsLocation)

import classes

################################################################################
# Read data from ./src/params.txt
################################################################################

print("*** Reading initial parameters *** ")
parPath = cwd + '/src/params.txt'
param 	= classes.Dimension(parPath)

################################################################################
# Read data (not diffused yet)
################################################################################

temperaturePath 	= './out/Temper_0_3D.txt'
dx = ((param.lon.end.km-param.lon.start.km)/(param.lon.size-1)) * 1.0E3 # [m]
dy = ((param.lat.end.km-param.lat.start.km)/(param.lat.size-1)) * 1.0E3 # [m]
dz = ((param.depth.end-param.depth.start)/(param.depth.size-1)) * 1.0E3 # [m]

skiprows 	= 4
temp 		= np.loadtxt(temperaturePath,skiprows=skiprows)
temp 		= np.reshape(temp,(param.lon.size,param.lat.size,param.depth.size),order='F')

seg 		= 365.25 * 24.0 * 60.0 * 60.0
t 			= 0.0
t_max 		= 10.0E6#4.0E6 # milhoes de anos
t_print		= 0.0
dt_print	= t_max / 10.

kappa 		= 1.0E-6 # [m2/s]
auxdt		= np.min([np.abs(dx),np.abs(dy),np.abs(dz)])

fac = 100.

dt 			= np.min([auxdt**2/(2*kappa),t_max/fac]) # passo de tempo em segundos

CTx 		= kappa * dt * seg / (dx**2)
CTy 		= kappa * dt * seg / (dy**2)
CTz 		= kappa * dt * seg / (dz**2)

extent2d 	= np.array([param.lon.start.km, param.lon.end.km, param.depth.end, param.depth.start])
extent3d	= np.array([param.lon.start.km, param.lon.end.km, param.lat.start.km, param.lat.end.km, param.depth.start, param.depth.end])

import matplotlib.pyplot as plt
if (param.dim!='2d'):
	from mayavi import mlab
else:
	import matplotlib.pyplot as plt

import progressBar as pbar
sz = int(t_max/dt_print)
bar = pbar.Bar(sz)
bar.start()
cont = 0
while (t<=t_max):
	cont += 1
	bar.update(cont)
	if (t>=t_print):
#		print(str(t/dt)+"/"+str(t_max/dt))

		width 		= 15
		height 		= 4.5
		markersize 	= 0.4
		plt.rcParams.update({'font.size': 12})
		
		ticks = np.linspace(np.min(temp),np.max(temp),18)
		fig, ax = plt.subplots(figsize=(width,height))
		plt.title(str(np.round(t/1.0E6,1))+ " Myrs\n\n\n")
		cs = plt.contourf(temp[:,1,:].transpose(),ticks,cmap="jet",extent=extent2d)
		plt.ylim(param.depth.end,param.depth.start)
		plt.ylabel("Depth (km)")
		plt.xlabel("Length (km)")
		ax2 = ax.twiny()
		ax2.set_xlim(param.lon.start.degree,param.lon.end.degree)
		plt.xlabel("Longitude ($\!^\circ\!$)")
		cbar = fig.colorbar(cs,orientation="vertical",label="Temperature ($\!^\circ\!$C)",ticks=ticks, pad=0.015)
		plt.tight_layout(rect=[0,0,1.1,1])
		plt.savefig("./figures/temp_"+str(int(np.round(fac*t/t_max,0))).zfill(3)+".pdf")
		plt.close()

		t_print += dt_print
	
	if (param.lat.size==2):
		auxX 					= temp[2:,:,1:-1] + temp[:-2,:,1:-1] - 2 * temp[1:-1,:,1:-1]
		auxZ 					= temp[1:-1,:,2:] + temp[1:-1,:,:-2] - 2 * temp[1:-1,:,1:-1]
		temp[1:-1,:,1:-1] 		= temp[1:-1,:,1:-1] + CTx * auxX + CTz * auxZ
	else:
		auxX 					= temp[2:,1:-1,1:-1] + temp[:-2,1:-1,1:-1] - 2 * temp[1:-1,1:-1,1:-1]
		auxY 					= temp[1:-1,2:,1:-1] + temp[1:-1,:-2,1:-1] - 2 * temp[1:-1,1:-1,1:-1]
		auxZ 					= temp[1:-1,1:-1,2:] + temp[1:-1,1:-1,:-2] - 2 * temp[1:-1,1:-1,1:-1]
		temp[1:-1,1:-1,1:-1] 	= temp[1:-1,1:-1,1:-1] + CTx * auxX + CTy * auxY + CTz * auxZ

	# Boundary conditions
	temp[0,:,:] = temp[1,:,:]
	temp[-1,:,:] = temp[-2,:,:]

	t += dt

bar.end()
#mlab.figure(size=(1000,400))
#mlab.view(azimuth=270,elevation=120)
#mlab.contour3d(temp)
#mlab.show()

################################################################################
# Save temperature diffused field
################################################################################

savePath 	= './out/Temper_0_3D.txt'
saveFile 	= open(temperaturePath,"w")

lengthStr		= str(param.lon.size)+"\t"+str(param.lat.size)+"\t"+str(param.depth.size)
extentDistStr	= str(param.lon.start.km)+"\t"+str(param.lon.end.km)+"\t"+str(param.lat.start.km)+"\t"+str(param.lat.end.km)+"\t"+str(param.depth.start)+"\t"+str(param.depth.end)
extentDegrStr	= str(param.lon.start.degree)+"\t"+str(param.lon.end.degree)+"\t"+str(param.lat.start.degree)+"\t"+str(param.lat.end.degree)+"\t"+str(param.depth.start)+"\t"+str(param.depth.end)

saveFile.write(lengthStr+"\n")
saveFile.write(extentDistStr+"\n")
saveFile.write(extentDegrStr+"\n")

saveFile.write("T4\n")

for k in xrange(param.depth.size):
	for j in xrange(param.lat.size):
		for i in xrange(param.lon.size):
			tempStr = str(temp[i,j,k])
			saveFile.write(tempStr+"\n")

saveFile.close()
plt.clf()
