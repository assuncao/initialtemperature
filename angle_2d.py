import numpy as np
import matplotlib.pyplot as plt
import os, sys, re

plt.close('all')
font = {'family' : 'normal',
        'weight' : 'normal',
        'size'   : 14}

plt.rc('font', **font)

################################################################################
# Setting up classes and methods
################################################################################

cwd = '/Users/kugelblitz/Desktop/initialTemperature'#os.getcwd()
libsLocation = os.path.join(cwd,'lib')
sys.path.append(libsLocation)

import classes
import progressBar as pbar

################################################################################
# Read data from ./src/params.txt
################################################################################

print("*** Reading initial parameters *** ")
parPath = './params.txt'
param 	= classes.Dimension(parPath)

param.lat.size = 1

################################################################################
# Configure NPROC
################################################################################

skiprows, num_processes, step, minAngleDepth, maxAngleDepth = 3, 14, 210, -150., -480.
for i in xrange(1,np.size(sys.argv),2):
	if (sys.argv[i]=='-k'):
		skiprows = int(sys.argv[i+1])
	elif (sys.argv[i]=='-n'):
		num_processes = int(sys.argv[i+1])
	elif (sys.argv[i]=='-step'):
		step = int(sys.argv[i+1])
	elif (sys.argv[i]=='-begin'):
		minAngleDepth = float(sys.argv[i+1])
	elif (sys.argv[i]=='-end'):
		maxAngleDepth = float(sys.argv[i+1])

################################################################################
# Print parameters box (check up mistakes)
################################################################################

box = pbar.Box()

box.top()
box.mid("Chosen dimension:\t "+param.dim)
box.mid("Grid dimensions:\t ("+str(param.lon.size)+"x"+str(param.lat.size)+"x"+str(param.depth.size)+")")
box.mid("Longitude interval:\t ("+str(param.lon.start.degree)+","+str(param.lon.end.degree)+","+str(param.lon.delta.degree)+")")
box.mid("Longitude distance:\t ("+str(param.lon.start.km)+","+str(param.lon.end.km)+","+str(param.lon.delta.km)+")")
box.mid("Latitude interval:\t ("+str(param.lat.start.degree)+","+str(param.lat.end.degree)+","+str(param.lat.delta.degree)+")")
box.mid("Latitude distance:\t ("+str(param.lat.start.km)+","+str(param.lat.end.km)+","+str(param.lat.delta.km)+")")
box.mid("Depth interval:\t ("+str(param.depth.start)+","+str(param.depth.end)+","+str(param.depth.delta)+")")
box.top()
box.mid("Number of cores:\t "+str(num_processes))
box.mid("Skip headers:\t\t "+str(skiprows))
box.bot()

################################################################################
# Read Slab
################################################################################

slabGridPath = '/Users/kugelblitz/Desktop/initialTemperature/src/resampled-top-full.-1800'
data = np.loadtxt(slabGridPath,unpack='True')
dataX = data[0,:]
dataY = data[1,:]
cond = (dataY<=minAngleDepth) & (dataY>=maxAngleDepth)
dataY = dataY[cond]
dataX = dataX[cond]

################################################################################
# Mesh and information for matplotlib
################################################################################

xi 		= np.linspace(0,param.lon.end.km,param.lon.size);
zi 		= np.linspace(param.depth.end,0,param.depth.size);
xx,zz 	= np.meshgrid(xi,zi);

extent			= [0,param.lon.end.km,0,param.depth.end]
#extent			= [22500.,param.lon.end.km,0,param.depth.end]
width, height 	= 7, 6
markersize 		= 0.4

# Basin location
basinWest, basinEast = -58, -55
drawdepth	= -6 # Depth to draw line and identify basin location

dT = 200
dKmTicks = 250
dDgTicks = 2.5

xticks_km = np.arange(0,np.round(param.lon.end.km,-1),dKmTicks)
if (param.lon.end.km-xticks_km[-1])<dKmTicks/2.:
	xticks_km = np.delete(xticks_km,-1)
	xticks_km = np.append(xticks_km,param.lon.end.km)
else:
	xticks_km = np.append(xticks_km,param.lon.end.km)

xticks_dg = np.arange(param.lon.start.degree,param.lon.end.degree,dDgTicks)
if (param.lon.end.degree-xticks_dg[-1])<dDgTicks/2.:
	xticks_dg = np.delete(xticks_dg,-1)
	xticks_dg = np.append(xticks_dg,param.lon.end.degree)
else:
	xticks_dg = np.append(xticks_dg,param.lon.end.degree)

plt.rcParams.update({'font.size': 12})
f_size = 12

step_min = 0

plt.close()

################################################################################
# Load *_<step>.txt
################################################################################

yrs = np.loadtxt("Tempo_"+str(step)+".txt",comments="P",delimiter=":")
value = format(np.round(yrs[0,1]/1.0E6,2), '.2f')

#------------------------------------------------------------------------------#

fgeoq 					= "Rho_"+str(step)+".txt"
G 						= np.loadtxt(fgeoq,unpack=True,comments="P",skiprows=skiprows)
GG 						= G * 1.0
GG[np.abs(GG)<1.0E-200] = 0
GG 						= np.reshape(GG,(param.lon.size,param.lat.size,param.depth.size),order='F')
geoq 					= np.copy(GG[:,0,:])

#------------------------------------------------------------------------------#

ftemp					= "Temper_"+str(step)+".txt"
T 						= np.loadtxt(ftemp,unpack=True,comments="P",skiprows=skiprows)
TT 						= T * 1.0
TT[np.abs(TT)<1.0E-200] = 0
TT 						= np.reshape(TT,(param.lon.size,param.lat.size,param.depth.size),order='F')
TTT 					= TT[:,0,:]

#------------------------------------------------------------------------------#

fgeoq					= "Geoq_"+str(step)+".txt"
N 						= np.loadtxt(fgeoq,unpack=True,comments="P",skiprows=skiprows)
NN 						= N * 1.0
NN[np.abs(NN)<1.0E-200] = 0
NN 						= np.reshape(NN,(param.lon.size,param.lat.size,param.depth.size),order='F')
NNN 					= np.log10(NN[:,0,:])

#------------------------------------------------------------------------------#

vfile 					= "Veloc_fut_"+str(step)+".txt"
V 						= np.loadtxt(vfile,unpack=True,comments="P",skiprows=skiprows)
VV 						= V * 1.0
VV[np.abs(VV)<1.0E-200] = 0.0
VV 						= np.reshape(VV,(2,param.lon.size,param.lat.size,param.depth.size),order='F')
#		VV_max 					= np.max(np.abs(VV))
u 						= np.transpose(VV[0,:,0,:]) #/ VV_max
v 						= np.transpose(VV[1,:,0,:]) #/ VV_max
#w 						= np.transpose(VV[2,:,0,:]) #/ VV_max
colors 					= np.sqrt(u**2+v**2)
colors_max				= np.max(colors)
colors 					= colors / colors_max
u 						= u / colors_max
v 						= v / colors_max

arrowSkip = 4

################################################################################
# Calculate slab top from the Rho_* files
################################################################################

lon 		= np.linspace(param.lon.start.km,param.lon.end.km,param.lon.size)
depth 		= np.linspace(param.depth.end,param.depth.start,param.depth.size)
new_dlon	= 0.1
new_lon 	= np.arange(param.lon.start.km,param.lon.end.km+new_dlon,new_dlon)
X, Y 		= [], []

for j in xrange(np.size(depth)):
	if (depth[j]<=minAngleDepth) and (depth[j]>=maxAngleDepth):
		auxy = geoq[geoq[:,j]<3300.,j]
		try:
			yyy = auxy[-1]
			xxx = lon[np.where(geoq[:,j]<3300.)[-1][-1]]
			Y = np.append(Y,depth[j])
			X = np.append(X,xxx)
		except:
			pass

newX, newY = [], []
for i in xrange(np.size(X)-1):
	if (X[i]>=X[i+1]):
		newX = np.append(newX,X[i])
		newY = np.append(newY,Y[i])
	if (X[-1]<=X[-2]):
		newX = np.append(newX,X[-1])
		newY = np.append(newY,Y[-1])
		
XX,YY = X, Y
Z = np.polyfit(newX,newY,3)
P = np.poly1d(Z)
Y = np.copy(P(X))

X = X[::-1]
Y = Y[::-1]

################################################################################
# Calculate dip angle for each element
################################################################################

slab2Angle = np.polyfit(dataX,dataY,1)[0] * 180. / np.pi
labelSlab = "Slab2, dip: " + str(np.round(slab2Angle,2))
print(str(slab2Angle)+","),

simuAngle = np.polyfit(X,Y,1)[0] * 180. / np.pi
labelSimu = "N. Model, dip: " + str(np.round(simuAngle,2))
print(simuAngle)

#print(simuAngle-slab2Angle)

################################################################################
# Plot Profile -> Temperature
################################################################################

levels = np.linspace(np.min(TTT),np.max(TTT),501)
yticks = np.linspace(0,-1000,6)
yticks = np.append(yticks,-660.)

fig, ax1 = plt.subplots(figsize=(width,height))
plt.title(value+" Myr\n\n",fontsize=f_size)

xx0, xx1 = 2000, 3000
ax1.set_xlim(xx0, xx1)

# cs = ax1.contourf(xx,zz,np.transpose(TTT),500,cmap='jet',extent=extent)
cs = ax1.imshow(np.transpose(TTT),cmap='jet',extent=extent,interpolation='none')
ax1.quiver(xi[::arrowSkip],zi[::arrowSkip],u[::arrowSkip,::arrowSkip],v[::arrowSkip,::arrowSkip],units='width',width=0.0025,scale=0.08*param.lon.size/(arrowSkip),pivot='mid')
#ax1.plot(data[0],data[1],color=(0.5,0.5,0.5),linewidth=2,label=labelSlab)
ax1.plot(dataX[(dataX<=newX[0])&(dataX[-1]>=newX[-1])],dataY[(dataX<=newX[0])&(dataX[-1]>=newX[-1])],color=(1.0,0.,0.),linewidth=2,label=labelSlab)#,linestyle='dashdot')
ax1.plot(X,Y,color=(0,0,0),linewidth=2,label=labelSimu)
ax1.set_yticks(yticks)
ax1.legend(loc=3,fontsize=10)


ax1.set_xlabel("Length (km)")
ax1.set_ylabel("Depth (km)")
#ax1.set_xticks(xticks_km)

ax2 = plt.twiny()
ax2.set_xlim(param.lon.start.degree+(xx0/111.),param.lon.start.degree+(xx1/111.))
ax2.set_xlabel("Longitude ($\!^\circ\!$)")
#ax2.hlines(drawdepth,basinWest,basinEast,linewidth=6,color=(0,0,0))
ax2.vlines(basinWest,0,-1000,linestyles="dashed",color=(1,1,1))
ax2.vlines(basinEast,0,-1000,linestyles="dashed",color=(1,1,1))
ax2.hlines(-660.0,-180,180,linestyles="dashed",color=(1,1,1))
#ax2.set_xticks(xticks_dg)
ax2.set_ylim(-1000,0)

# Color bar
auxTMax = np.around(np.max(TTT),-2)
ticks = np.arange(0,auxTMax+1,dT)
if (np.max(TTT)-ticks[-1])<dT/2:
	ticks = np.delete(ticks,-1)
	ticks = np.append(ticks,np.max(TTT))
else:
	ticks = np.append(ticks,np.max(TTT))
cbar = fig.colorbar(cs,ticks=ticks,pad=0.015)
cbar.ax.set_ylabel('Temperature ($\!^\circ\!$C)')

plt.tight_layout(rect=[0,0,1.0,1])
plt.savefig("figures/angle-step_"+str(step)+".png")
#plt.show()
plt.close()

################################################################################
# Plot Profile -> Viscosity
################################################################################

levels = np.linspace(np.min(NNN),np.max(NNN),501)
yticks = np.linspace(0,-1000,6)
yticks = np.append(yticks,-660.)

fig, ax3 = plt.subplots(figsize=(width,height))
plt.title(value+" Myr\n\n",fontsize=f_size)

xx0, xx1 = 2000, 3000
ax3.set_xlim(xx0, xx1)

cs = ax3.imshow(np.transpose(NNN),cmap='viridis',extent=extent,vmin=18,interpolation='none')
ax3.quiver(xi[::arrowSkip],zi[::arrowSkip],u[::arrowSkip,::arrowSkip],v[::arrowSkip,::arrowSkip],units='width',width=0.0025,scale=0.08*param.lon.size/(arrowSkip),pivot='mid')
#ax1.plot(data[0],data[1],color=(0.5,0.5,0.5),linewidth=2,label=labelSlab)
ax3.plot(dataX[(dataX<=newX[0])&(dataX[-1]>=newX[-1])],dataY[(dataX<=newX[0])&(dataX[-1]>=newX[-1])],color=(1.0,0.,0.),linewidth=2,label=labelSlab)#,linestyle='dashdot')
ax3.plot(X,Y,color=(0,0,0),linewidth=2,label=labelSimu)
ax3.set_yticks(yticks)
ax3.legend(loc=3,fontsize=10)


ax3.set_xlabel("Length (km)")
ax3.set_ylabel("Depth (km)")

ax4 = plt.twiny()
ax4.set_xlim(param.lon.start.degree+(xx0/111.),param.lon.start.degree+(xx1/111.))
ax4.set_xlabel("Longitude ($\!^\circ\!$)")
#ax2.hlines(drawdepth,basinWest,basinEast,linewidth=6,color=(0,0,0))
ax4.vlines(basinWest,0,-1000,linestyles="dashed",color=(1,1,1))
ax4.vlines(basinEast,0,-1000,linestyles="dashed",color=(1,1,1))
ax4.hlines(-660.0,-180,180,linestyles="dashed",color=(1,1,1))
#ax2.set_xticks(xticks_dg)
ax4.set_ylim(-1000,0)

# Color bar
maxN = np.around(np.max(NNN))
minN = 18
ticks = np.linspace(minN,maxN,maxN-minN+1)
# if (np.max(NNN)-ticks[-1])<dT/2:
# 	ticks = np.delete(ticks,-1)
# 	ticks = np.append(ticks,np.max(NNN))
# else:
# 	ticks = np.append(ticks,np.max(NNN))
cbar = fig.colorbar(cs,ticks=ticks,pad=0.015)
cbar.ax.set_ylabel('$log_{10}\eta$ (Pa s)')

plt.tight_layout(rect=[0,0,1.0,1])
plt.savefig("figures/visc-step_"+str(step)+".png")
#plt.show()
plt.close()



