import numpy as np
import matplotlib.pyplot as plt

basinWest, basinEast = -58, -55

def topography(data,g=10.,dz=10.0):
	aux = np.sum(data,axis=1)
	w = aux * g * dz
	return w
	
step = 1600
path_0 = './Rho_0.txt'
path_1 = './Rho_'+str(step)+'.txt'
path_temper0 = 'Temper_0.txt'
path_temper1 = 'Temper_'+str(step)+'.txt'

skiprows = 3
shp = (551,101)

h_himalaya = 8.800
alpha = 3.28E-5

data_0 = np.loadtxt(path_0,unpack=True,skiprows=skiprows,comments='P')
data_0 = np.reshape(data_0,shp,order='F')
data_temper0 = np.loadtxt(path_temper0,unpack=True,skiprows=skiprows,comments='P')
data_temper0 = np.reshape(data_temper0,shp,order='F')

data_1 = np.loadtxt(path_1,unpack=True,skiprows=skiprows,comments='P')
data_1 = np.reshape(data_1,shp,order='F')
data_temper1 = np.loadtxt(path_temper1,unpack=True,skiprows=skiprows,comments='P')
data_temper1 = np.reshape(data_temper1,shp,order='F')


data_novo0 = data_0*(1-alpha*data_temper0)
data_novo1 = data_0*(1-alpha*data_temper1)

w0 = topography(data_novo0)
w1 = topography(data_novo1)

x = np.linspace(-89,-39.5,551)

plt.figure(figsize=(15,4))
plt.xlabel("Longitude (degrees)")
plt.ylabel("Subsidence (km)")
plt.xlim(x.min(),x.max())

#plt.ylim()
#plt.vlines(basinEast,0,-3000)
#plt.plot(x,w)
#plt.plot(x,w0,label="Begin")
#plt.plot(x,w1,label="End")
plt.plot(x,(w0-w1)/(3300.*10.),label="Difference")
#plt.plot(x,(w0-w1),label="Difference")
plt.vlines(basinWest,-h_himalaya,h_himalaya,linestyles="dashed")
plt.vlines(basinEast,-h_himalaya,h_himalaya,linestyles="dashed")
plt.hlines(0,x[0],x[-1],linestyles="dashed")
plt.legend()
plt.tight_layout()
plt.show()
