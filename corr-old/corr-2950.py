import numpy as np
import matplotlib.pyplot as plt
from pylab import cm

##################
# 75
##################

slab_75_5_25 = -42.89930062684017
slab_75_6_25 = -42.89930062684017
slab_75_7_25 = -42.89930062684017
slab_75_8_25 = -42.32934868988758
simu_75_5_25 = -44.43042833540404
simu_75_6_25 = -42.519982696564014
simu_75_7_25 = -42.58802947863509
simu_75_8_25 = -42.112732453660946

slab_75_5_50 = -44.41336434439902
slab_75_6_50 = -43.43623256583329
slab_75_7_50 = -43.43623256583329
slab_75_8_50 = -43.43623256583329
simu_75_5_50 = -45.778979649306834
simu_75_6_50 = -41.20008388461648
simu_75_7_50 = -41.2589260054106
simu_75_8_50 = -40.652308064332324

##################
# 80
##################

slab_80_5_25 = -43.43623256583329
slab_80_6_25 = -43.43623256583329
slab_80_7_25 = -43.43623256583329
slab_80_8_25 = -42.32934868988758
simu_80_5_25 = -45.276497610108684
simu_80_6_25 = -44.2988615118549
simu_80_7_25 = -44.09575350946464
simu_80_8_25 = -42.41911512041933

slab_80_5_50 = -44.41336434439902
slab_80_6_50 = -43.940467369343246
slab_80_7_50 = -43.940467369343246
slab_80_8_50 = -43.43623256583329
simu_80_5_50 = -45.778867837147615
simu_80_6_50 = -42.85610863923592
simu_80_7_50 = -43.09431530636191
simu_80_8_50 = -41.6400224115403

##################
# 85
##################

slab_85_5_25 = -43.43623256583329
slab_85_6_25 = -43.43623256583329
slab_85_7_25 = -43.43623256583329
slab_85_8_25 = -42.89930062684017
simu_85_5_25 = -45.67568811476003
simu_85_6_25 = -43.89099950501335
simu_85_7_25 = -44.76674032154076
simu_85_8_25 = -43.615294758489654

slab_85_5_50 = -44.856918001162896
slab_85_6_50 = -44.41336434439902
slab_85_7_50 = -44.41336434439902
slab_85_8_50 = -43.940467369343246
simu_85_5_50 = -47.71791145760275
simu_85_6_50 = -44.632065956374376
simu_85_7_50 = -45.272089355566045
simu_85_8_50 = -42.080786702807224

##################
# 90
##################

slab_90_5_25 = -43.940467369343246
slab_90_6_25 = -43.940467369343246
slab_90_7_25 = -43.43623256583329
slab_90_8_25 = -42.89930062684017
simu_90_5_25 = -46.646116874338645
simu_90_6_25 = -45.74388827332987
simu_90_7_25 = -44.98207815354403
simu_90_8_25 = -42.70654657068632

slab_90_5_50 = -44.856918001162896
slab_90_6_50 = -44.856918001162896
slab_90_7_50 = -44.41336434439902
slab_90_8_50 = -44.41336434439902
simu_90_5_50 = -47.489855056972644
simu_90_6_50 = -46.4397588998191
simu_90_7_50 = -45.39704749347766
simu_90_8_50 = -44.08276138301874

slabV50 = np.array([[slab_90_5_50,slab_90_6_50,slab_90_7_50,slab_90_8_50],
					[slab_85_5_50,slab_85_6_50,slab_85_7_50,slab_85_8_50],
					[slab_80_5_50,slab_80_6_50,slab_80_7_50,slab_80_8_50],
				  	[slab_75_5_50,slab_75_6_50,slab_75_7_50,slab_75_8_50]])

simuV50 = np.array([[simu_90_5_50,simu_90_6_50,simu_90_7_50,simu_90_8_50],
					[simu_85_5_50,simu_85_6_50,simu_85_7_50,simu_85_8_50],
					[simu_80_5_50,simu_80_6_50,simu_80_7_50,simu_80_8_50],
					[simu_75_5_50,simu_75_6_50,simu_75_7_50,simu_75_8_50]])

slabV25 = np.array([[slab_90_5_25,slab_90_6_25,slab_90_7_25,slab_90_8_25],
					[slab_85_5_25,slab_85_6_25,slab_85_7_25,slab_85_8_25],
					[slab_80_5_25,slab_80_6_25,slab_80_7_25,slab_80_8_25],
					[slab_75_5_25,slab_75_6_25,slab_75_7_25,slab_75_8_25]])

simuV25 = np.array([[simu_90_5_25,simu_90_6_25,simu_90_7_25,simu_90_8_25],
					[simu_85_5_25,simu_85_6_25,simu_85_7_25,simu_85_8_25],
					[simu_80_5_25,simu_80_6_25,simu_80_7_25,simu_80_8_25],
					[simu_75_5_25,simu_75_6_25,simu_75_7_25,simu_75_8_25]])

x_values 	= [0,1,2,3]
x_text		= ['5','6','7','8']
y_values 	= [0,1,2,3]
y_text		= ['90','85.5','80.0','75.0']

value50 = simuV50 - slabV50
value25 = simuV25 - slabV25

mintick, maxtick = -7, 7
div = 14
color_under = (000./255,000./255.,070./255.)
color_over = (070./255,000./255.,000./255.)


condNan = ~np.isnan(value50)
cmap = cm.get_cmap('coolwarm', div)
#cticks = np.linspace(np.min(value50[condNan]),np.max(value50[condNan]),div+1)
cticks = np.linspace(mintick,maxtick,div+1)

plt.figure()
plt.title("Dip angle differences (5 Myr)")
cs = plt.imshow(value50,cmap=cmap)
plt.xticks(x_values,x_text)
plt.yticks(y_values,y_text)
plt.xlabel("Oceanic Crust Thk. (km)")
plt.ylabel("Oceanic Lit. Thk. (km)")

cs.set_clim(mintick, maxtick)
cs.cmap.set_over(color=color_over)
cs.cmap.set_under(color=color_under)
plt.colorbar(cs,ticks=cticks,label="Dip Angle diff. (in degrees)",extend='both')

#plt.grid(b=True, which='major', axis='both')
plt.tight_layout()
plt.savefig('./figures/corr_5-2950.png')
#plt.show()

cticks = np.linspace(mintick,maxtick,div+1)

plt.figure()
plt.title("Dip angle differences (2.5 Myr)")
cs = plt.imshow(value25,cmap=cmap)
plt.xticks(x_values,x_text)
plt.yticks(y_values,y_text)
plt.xlabel("Oceanic Crust Thk. (km)")
plt.ylabel("Oceanic Lit. Thk. (km)")

cs.set_clim(mintick, maxtick)
cs.cmap.set_over(color=color_over)
cs.cmap.set_under(color=color_under)
plt.colorbar(cs,ticks=cticks,label="Dip Angle diff. (in degrees)",extend='both')

#plt.grid(b=True, which='major', axis='both')
plt.tight_layout()
plt.savefig('./figures/corr_25-2950.png')
plt.show()
