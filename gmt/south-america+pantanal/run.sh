#!/bin/bash

grdimage /Users/kugelblitz/Documents/USP/gmt_files/ETOPO1_BEd_g_gmt4.grd -R-80/-30/-40/10 -JM15c -P -C/Users/kugelblitz/Documents/USP/gmt_files/ETOPO1.cpt -K > map.ps
#-I2/0.25p,cornflowerblue -I0/0.25p,cornflowerblue -I1/0.25p,cornflowerblue

pscoast -R -JM15c -W0.01p,black -Df -P -Ba10f10g10 -B+t"Pantanal Basin" -N1 -L-35/-35/-35/500 -K -O >> map.ps
# pscoast -R -JM15c -W0.01p,black -Df -P -Ba10f10g10 -B+t"Pantanal Basin" -Ir/0.25p,cornflowerblue -N1 -L-35/-35/-35/500 -K -O >> map.ps
psxy -R -J $PWD/pantanal.txt -W1p,red  -P -O >> map.ps
# psxy -R -J $PWD/chaco-parana.xy -W1p,red  -P -O >> map.ps
