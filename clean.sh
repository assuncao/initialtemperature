#!/bin/bash

BOLD=$(tput bold)
NORMAL=$(tput sgr0)

BLACK='\033[0;30m'
RED='\033[0;31m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
CYAN='\033[0;36m'
YELLOW='\033[1;33m'
WHITE='\033[1;37m'
NC='\033[0m' # No Color

echo -e "\n\tChoose an option to continue \n\n\t - ${CYAN}${BOLD}c${NORMAL}${NC}\t cleans the content of the ${ORANGE}./out/Slab2-resampled/${NC} directory;\n\t - ${RED}${BOLD}f${NORMAL}${NC}\t completely cleans the directories within ${ORANGE}./out/${NC};\n\t - ${GREE}${BOLD}e${NORMAL}${NC}\t to exit."

read -n1 OPT

case "$OPT" in
	"c")
		echo -e "\n\tAre you sure you want to delete ${ORANGE}./out/Slab2-resampled/${NC} (y/n)?"
		read ANS
		if [ "$ANS" != "n" ]
		then
			PW=`pwd`
			find "$PW/out/Slab2-resampled/" -maxdepth 1 -name "*" -type f -exec rm '{}' \;
		fi
		;;
	"f")
echo -e "\n\tAre you sure you want to completely delete the content of \n\t\t${ORANGE}./out/Slab2-resampled/${NC}, \n\t\t${ORANGE}./out/Slab2-clipped/${NC}, and the files \n\t\t${ORANGE}Temper_0_3D.txt${NC}, \n\t\t${ORANGE}interfaces.txt${NC}, \n\t\t${ORANGE}veloc_0_3D.txt${NC}, \n\t\t${ORANGE}crust.txt${NC}, and \n\t\t${ORANGE}litho.txt${NC} (y/n)?"
		read ANS
		if [ "$ANS" != "n" ]
		then
			PW=`pwd`
			find "$PW/out/Slab2-resampled/" -maxdepth 1 -name "*" -type f -exec rm '{}' \;
			find "$PW/out/Slab2-clipped/" -maxdepth 1 -name "*" -type f -exec rm '{}' \;
			rm $PW/Temper_0_3D.txt \;
			rm $PW/veloc_0_3D.txt \;
			rm $PW/interfaces.txt \;
			rm $PW/crust.txt \;
			rm $PW/litho.txt \;
		fi
		;;
	*)
		echo -e "\n\tNothing to be done, invalid input!\n"
		;;
esac
