NPROC=16
DIRNAME=${PWD##*/}
touch log.txt
touch temp_simu.txt
date >> temp_simu.txt
/home/kugelblitz/opt/petsc/petsc_debug_0/bin/mpirun -n $NPROC /home/kugelblitz/opt/md3d/MD3D_4.9_swarm -te 1 -ve 1 -print_visc 1 | tee log.txt
date >> temp_simu.txt
cd ..
zip -r $DIRNAME.zip $DIRNAME
sudo poweroff
