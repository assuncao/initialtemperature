import numpy as np
import os, sys, re

################################################################################
# Setting up classes and methods
################################################################################

cwd = os.getcwd()
libsLocation = os.path.join(cwd,'lib')
sys.path.append(libsLocation)

import classes

################################################################################
# Read data from ./src/params.txt
################################################################################

parPath = './src/params.txt'
par 	= open(parPath)
lon 	= classes.Parameters(re.split("[ ]+", par.readline().strip()),"lon")
lat 	= classes.Parameters(re.split("[ ]+", par.readline().strip()),"lat")
depth 	= classes.Parameters(re.split("[ ]+", par.readline().strip()),"depth")
auxDim	= re.split("[\t]+", par.readline().strip())
par.close()

################################################################################
# Earth
################################################################################

elon 	= classes.Parameters([0,360,10],"lon")
elat 	= classes.Parameters([-90,90,10],"lat")
edepth 	= classes.Parameters([0,0,6371000.],"depth")
earth 	= classes.Shell([elon,elat,edepth])

mlon 	= classes.Parameters([0,360,1],"lon")
mlat 	= classes.Parameters([0,0,1],"lat")
mdepth 	= classes.Parameters([0,0,6371000.],"depth")
equator = classes.Shell([mlon,mlat,mdepth])

################################################################################
# Data
################################################################################

prm = [lon,lat,depth,auxDim]
shell = classes.Shell(prm)

################################################################################
# Plot
################################################################################

from mayavi import mlab

mlab.mesh(earth.x[:,:,0],earth.y[:,:,0],earth.z[:,:,0],color=(0.5,0.5,1),opacity=0.1)
mlab.points3d(equator.x,equator.y,equator.z,scale_factor=100000)

xx, yy, rr = 0, 0, 6371000.
px = rr * np.sin(yy) * np.cos(xx)
py = rr * np.sin(yy) * np.sin(xx)
pz = rr * np.cos(yy)
mlab.points3d(px,py,pz,scale_factor=500000)

xx, yy, rr = 0, np.pi/2, 6371000.
px = rr * np.sin(yy) * np.cos(xx)
py = rr * np.sin(yy) * np.sin(xx)
pz = rr * np.cos(yy)
mlab.points3d(px,py,pz,scale_factor=500000,color=(1,0,0))

mlab.points3d(shell.x,shell.y,shell.z,scale_factor=10000)

mlab.show()

