import os, sys, re
import numpy as np
import subprocess as sp
import matplotlib.pyplot as plt
import geopandas as gp
import scipy.ndimage

plotMap = 'True'
test = 'True'
realData = 'False'

################################################################################
# Setting up classes and methods
################################################################################

cwd = os.getcwd()
libsLocation = os.path.join(cwd,'lib')
sys.path.append(libsLocation)

import reference, classes, methods, data

################################################################################
# Read data from ./src/params.txt
################################################################################

parPath = './src/params.txt'
par 	= open(parPath)
lon 	= classes.Parameters(re.split("[ ]+", par.readline().strip()),"lon")
lat 	= classes.Parameters(re.split("[ ]+", par.readline().strip()),"lat")
depth 	= classes.Parameters(re.split("[ ]+", par.readline().strip()),"depth")
par.close()

fieldTemperature 	= np.zeros(shape=(lon.length,lat.length,depth.length))
fieldLon			= np.linspace(lon.startKm,lon.endKm,lon.length)
fieldLat			= np.linspace(lat.startKm,lat.endKm,lat.length)
fieldDepth			= np.linspace(depth.start,depth.end,depth.length)
auxFieldDepth		= np.zeros(np.size(fieldDepth))

################################################################################
# Read dimensional reference values
################################################################################

refValuesPath 	= './src/reference.txt'
refValuesFile 	= open(refValuesPath)
refValues		= []
for line in refValuesFile:
	refValues.append(line)

dimRef 			= reference.Reference(refValues,depth.end)

################################################################################
# Make astensophere map
################################################################################

modelPath = '/Users/kugelblitz/Documents/USP/models/LITHO1.0/litho1.0/LITHO1.0/bin/access_litho'

fig,ax = plt.subplots(figsize=(8,8))
ax.set_aspect('equal')

if (realData=='True'):
	longitude = np.linspace(lon.startDegree,lon.endDegree,lon.length)
	for i in xrange(lon.length):
		if (longitude[i]>180.0):
			longitude[i] = longitude[i] - 360.0
		else:
			continue
	latitude = np.linspace(lat.startDegree,lat.endDegree,lat.length)
	depth = np.zeros(shape=(lon.length,lat.length))

	for j in xrange(lat.length):
		for i in xrange(lon.length):
			print(str(latitude[j])+" "),
			print(str(longitude[i])+" "),
			aux = sp.Popen([modelPath,'-p',str(latitude[j]),str(longitude[i])],stdout=sp.PIPE).communicate()[0]
			aux = aux.split()
			if (aux[9]=='ASTHENO-TOP'):
				depth[i,j] = -1. * np.abs(float(aux[0]))
				print(str(depth[i,j]))
			else:
				print("Couldn't find a value for the asthenosphere depth.")
				break

	depth = depth / 1.0E3
	levels = np.linspace(np.min(depth),np.max(depth),101)
	extent = [longitude[0],longitude[-1],latitude[0],latitude[-1]]
	plt.contourf(np.transpose(depth),extent=extent,cmap='jet',levels=levels)
	plt.colorbar(orientation='vertical')

#plt.plot(longitude,depth[:,0])
#plt.ylim(np.min(depth),0)
##plt.yticks([np.linspace(-300000,0,31),np.min(depth),np.max(depth)])
#plt.show()

if (test=='True'):
	minlat, maxlat, minlon, maxlon = -50., 10., -80., -30.
	
	n = 41#21
	m = 49#25
	longitude = np.linspace(minlon,maxlon,n)
	latitude = np.linspace(minlat,maxlat,m)
	xx, yy = np.meshgrid(longitude,latitude)
	depth = yy*0#np.zeros(shape=(n,n))

	for j in xrange(m):
		for i in xrange(n):
			print(str(latitude[j])+" "),
			print(str(longitude[i])+" "),
			aux = sp.Popen([modelPath,'-p',str(latitude[j]),str(longitude[i])],stdout=sp.PIPE).communicate()[0]
			aux = aux.split()
			
			if not aux:
				print("Could not find a depth for the LAB. Calculating based on the neighbors.")
				auxN = sp.Popen([modelPath,'-p',str(latitude[j+1]),str(longitude[i])],stdout=sp.PIPE).communicate()[0]
				auxN = auxN.split()
				if (auxN[9]=='ASTHENO-TOP'):
					auxNn = np.abs(float(auxN[0]))
				else:
					print("Couldn't find a value for the LAB depth.")
					break
				auxS = sp.Popen([modelPath,'-p',str(latitude[j-1]),str(longitude[i])],stdout=sp.PIPE).communicate()[0]
				auxS = auxS.split()
				if (auxS[9]=='ASTHENO-TOP'):
					auxSn = np.abs(float(auxS[0]))
				else:
					print("Couldn't find a value for the LAB depth.")
					break
				auxW = sp.Popen([modelPath,'-p',str(latitude[j]),str(longitude[i-1])],stdout=sp.PIPE).communicate()[0]
				auxW = auxW.split()
				if (auxW[9]=='ASTHENO-TOP'):
					auxWn = np.abs(float(auxW[0]))
				else:
					print("Couldn't find a value for the LAB depth.")
					break
				auxE = sp.Popen([modelPath,'-p',str(latitude[j]),str(longitude[i+1])],stdout=sp.PIPE).communicate()[0]
				auxE = auxE.split()
				if (auxE[9]=='ASTHENO-TOP'):
					auxEn = np.abs(float(auxE[0]))
				else:
					print("Couldn't find a value for the LAB depth.")
					break
				depth[m-j-1,i] = -1. * (auxEn+auxNn+auxSn+auxWn) / 4.0
			else:
				if (aux[9]=='ASTHENO-TOP'):
					depth[m-j-1,i] = -1. * np.abs(float(aux[0]))
					print(str(depth[m-j-1,i]))
				else:
					print("Couldn't find a value for the asthenosphere depth.")
					break
			


	depth = depth / 1.0E3
	levels = np.linspace(np.min(depth),np.max(depth),101)
	extent = [minlon,maxlon,minlat,maxlat]

	ishow = plt.imshow(depth,cmap='jet',interpolation='spline16',extent=extent)

	# Contour Plot over data
	dlevels = 50
	levels = np.arange(np.round(np.min(depth),-1),np.round(np.max(depth),-1)+dlevels,dlevels)
	depthc = np.zeros(shape=np.shape(depth))
	for i in xrange(m):
		depthc[i,:] = depth[m-i-1,:]
	# Resample data with spline
	depthc = scipy.ndimage.zoom(depthc, 3)
	cplot = plt.contour(depthc,extent=extent,levels=levels,colors=('w',))
	ax.clabel(cplot, fmt='%2.1f', colors='w', fontsize=10)

	# Color bar
	ticks = np.array(np.min(depth))
	dbar = 25
	ticks = np.append(ticks,np.arange(np.round(np.min(depth),-1),np.round(np.max(depth),-1)+dbar,dbar))
	ticks = np.append(ticks,np.max(depth))
	
	cbar = fig.colorbar(ishow,orientation='vertical',ticks=ticks,aspect=25)
	cbar.ax.set_ylabel("LAB Depth (km)")
	cbar.add_lines(cplot)

if (plotMap=='True'):
	map = gp.read_file('/Users/kugelblitz/Desktop/initialTemperature/src/lim_pais_a.shp')
	map.plot(ax=ax,color='none',edgecolor='black')

plt.savefig('./figures/astenosphere.pdf')
plt.show()
#plt.clf()
