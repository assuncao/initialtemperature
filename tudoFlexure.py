import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import os, sys, re
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from matplotlib.patches import Rectangle

################################################################################
# Setting up classes and methods
################################################################################

cwd = '/Users/kugelblitz/Desktop/initialTemperature'#os.getcwd()
libsLocation = os.path.join(cwd,'lib')
sys.path.append(libsLocation)

dirname = os.getcwd().split('/')[-1] + '_'
fig_dir = 'plots/' + dirname


import classes
import progressBar as pbar

################################################################################
# Read data from ./src/params.txt
################################################################################

print("*** Reading initial parameters *** ")
parPath = './params.txt'
param 	= classes.Dimension(parPath)

param.lat.size = 1

TZ = -660

################################################################################
# Configure NPROC
################################################################################

skiprows, num_processes, aux_print_step = 3, 14, 0
print(sys.argv,np.size(sys.argv))
for i in xrange(1,np.size(sys.argv),2):
	if (sys.argv[i]=='-k'):
		skiprows = int(sys.argv[i+1])
	elif (sys.argv[i]=='-n'):
		num_processes = int(sys.argv[i+1])
	elif (sys.argv[i]=='-step'):
		aux_print_step = int(sys.argv[i+1])
	elif (sys.argv[i]=='-final'):
		final_step = int(sys.argv[i+1])

################################################################################
# Print parameters box (check up mistakes)
################################################################################

box = pbar.Box()

box.top()
box.mid("Chosen dimension:\t "+param.dim)
box.mid("Grid dimensions:\t ("+str(param.lon.size)+"x"+str(param.lat.size)+"x"+str(param.depth.size)+")")
box.mid("Longitude interval:\t ("+str(param.lon.start.degree)+","+str(param.lon.end.degree)+","+str(param.lon.delta.degree)+")")
box.mid("Longitude distance:\t ("+str(param.lon.start.km)+","+str(param.lon.end.km)+","+str(param.lon.delta.km)+")")
box.mid("Latitude interval:\t ("+str(param.lat.start.degree)+","+str(param.lat.end.degree)+","+str(param.lat.delta.degree)+")")
box.mid("Latitude distance:\t ("+str(param.lat.start.km)+","+str(param.lat.end.km)+","+str(param.lat.delta.km)+")")
box.mid("Depth interval:\t ("+str(param.depth.start)+","+str(param.depth.end)+","+str(param.depth.delta)+")")
box.top()
box.mid("Number of cores:\t "+str(num_processes))
box.mid("Skip headers:\t\t "+str(skiprows))
box.bot()

################################################################################
# Read Slab
################################################################################

slabGridPath = '/Users/kugelblitz/Desktop/initialTemperature/out/Slab2-clipped/Slab2-top.-1800'
data = np.loadtxt(slabGridPath,unpack='True')

################################################################################
# Read info from the param_1.5.3.txt
################################################################################

with open("param_1.5.3_2D.txt","r") as f:
	# Line 1
	line = f.readline()
	# Line 2
	line = f.readline()
	# Line 3
	line = f.readline()
	# Line 4
	line = f.readline()
	# Line 5
	line = f.readline()
	line = line.split()
	tag, step_max = line[0], int(line[1])
	# Line 6
	line = f.readline()
	# Line 7
	line = f.readline()
	# Line 8
	line = f.readline()
	# Line 9
	line = f.readline()
	# Line 10
	line = f.readline()
	line = line.split()
	tag, print_step = line[0], int(line[1])
	# Line 11
	line = f.readline()
	# Line 12
	line = f.readline()
	line = line.split()
	tag, visc_med = line[0], int(line[1].split("E")[1])
	# Line 13
	line = f.readline()
	line = line.split()
	tag, visc_max = line[0], int(line[1].split("E")[1])
	# Line 14
	line = f.readline()
	line = line.split()
	tag, visc_min = line[0], int(line[1].split("E")[1])

if (aux_print_step!=0):
	print_step = aux_print_step

################################################################################
# Mesh and information for matplotlib
################################################################################

x_start, x_end = 1500, 5000.
x_start_degree, x_end_degree = param.lon.start.degree + x_start / 111.,param.lon.start.degree +  x_end / 111.

extent			= [0,param.lon.end.km,0,param.depth.end]
ref = 10
#width, height 	= ww * ref, ref
markersize 		= 0.4
ww = (x_end-x_start) / (-param.depth.end) #+ 0.15

rx = 0.2 # 20% of the height
wx = 0.6 # 60% of the height

width, height 	= ww * wx * ref + 2 * rx * ref, ref
aux = width / ref

tr = 0.025 / aux
r0, r1, r2, r3 = rx/aux - tr, rx , ww * wx / aux, wx
rect = [r0, r1, r2, r3]


xi 		= np.linspace(0,param.lon.end.km,param.lon.size);
zi 		= np.linspace(param.depth.end,0,param.depth.size);
xx,zz 	= np.meshgrid(xi,zi);

# Basin location
basinWest, basinEast = -58, -55
drawdepth	= -6 # Depth to draw line and identify basin location

xmin, xmax, nx= param.lon.start.km, param.lon.end.km, param.lon.size
zmin, zmax, nz= param.depth.start, param.depth.end, param.depth.size
x = np.linspace(xmin,xmax,nx) * 1.0E3 # [m]
z = np.linspace(zmax,zmin,nz) * 1.0E3 # [m]
p = np.zeros(nx)

dticks = 200.
ddtick = dticks/2
arr_aux = np.arange(x_start,x_end+dticks/2.,dticks)
if (x_end-arr_aux[-1]<=ddtick):
	arr_aux = np.delete(arr_aux,-1)
xticks = np.append(arr_aux,x_end)

dticks_degree = 2.0
ddtick_degree = 1.1
arr_aux_degree = np.arange(np.ceil(x_start_degree),np.floor(x_end_degree)+dticks_degree/2.,dticks_degree)
if (x_end_degree-arr_aux_degree[-1]<=ddtick_degree):
	arr_aux_degree = np.delete(arr_aux_degree,-1)
if (x_start_degree-arr_aux_degree[0]>=-ddtick_degree):
	arr_aux_degree = np.delete(arr_aux_degree,0)
auxaux = np.append(x_start_degree,arr_aux_degree)
xticks_degree = np.append(auxaux,x_end_degree)

y_start, y_end = -1500, 1500
yticks = np.arange(y_start,y_end+1,250)
#yticks = np.append(yticks,TZ)

linewidth = 4
cwhite = (1.,1.,1.)

f_size = 20
plt.rcParams.update({'font.size': f_size})


dT = 200
v_normal = 10.0E-3

step_min = 0

plt.close()

################################################################################
# Plots
################################################################################

temperPlot 		= True
rhoPlot 		= True
strainPlot		= True
hPlot 			= False
geoqPlot 		= True
factorPlot 		= False
velocityPlot 	= True
viscPlot		= False
tempVelo		= True

n_dim_veloc = 3
if (param.lat.size==1):
	n_dim_veloc = 2
	
################################################################################
# Methods
################################################################################

def topography(data,g=9.8,dz=10.0):
	aux = np.sum(data,axis=1)
	w = aux * g * dz
	return w

def flexura_num(x,p,Te=10.0E3,rhom=3300.,rho0=1000.0,E=1.0E11,nu=0.25,g=9.8):
    dx = x[1] - x[0]
    
    n = np.size(x)

    A = np.zeros((n,n))

    D = E*Te**3 / (12.*(1-nu**2))

    drho = rhom-rho0

    A[range(n),range(n)] =  (6.0*D) + (dx**4*drho*g)
    A[range(n-1),range(1,n)] = -4.0 * D
    A[range(1,n),range(n-1)] = -4.0 * D
    A[range(n-2),range(2,n)] = D
    A[range(2,n),range(n-2)] = D

    # Placa continua
    A[0,1] = -8.0 * D
    A[0,2] =  2.0 * D
    A[1,1] = (7.0*D) + (dx**4*drho*g)

    A[n-1,n-1] = -8.0 * D
    A[n-1,n-3] = 2.0 * D
    A[n-2,n-2] = (7.0*D) + (dx**4*drho*g)

    b = p * dx**4
    
    w = np.linalg.solve(A,b)
    return w

def axx(cs,cwhite=(1,1,1)):
	ax1.set_xlabel("Distance (km)")
	ax1.set_xticks(xticks)
	ax1.set_xlim(x_start,x_end)
	ax1.set_ylabel("Depth (km)")
	ax1.set_yticks(yticks)
#	ax1.set_ylim(param.depth.end,0)
	ax1.set_ylim(y_start, y_end)
	ax1.tick_params(length=10,width=2)
#	ax1.set_aspect('equal')

	ax2 = ax1.twiny()
	ax2.set_xlim(param.lon.start.degree+x_start/111.,param.lon.start.degree+x_end/111.)
	ax2.set_xticks(np.round(xticks_degree,2))
	ax2.set_xlabel("Longitude ($\!^\circ\!$)")
	ax2.tick_params(length=10,width=2)

	# Basin location
	ax2.vlines(basinWest,y_start,y_end,linestyles="dotted",color=cwhite,linewidth=linewidth)
	ax2.vlines(basinEast,y_start,y_end,linestyles="dotted",color=cwhite,linewidth=linewidth)
#	ax1.hlines(TZ,x_start,x_end,linestyles="dashed",color=cwhite,linewidth=linewidth)
	
	ax1.autoscale(False)
	ax2.autoscale(False)

def axxin(fac):
	posx = 1. + 0.05 / fac
	axins = inset_axes(	ax1,
						width=0.25,  # width = 5% of parent_bbox width
						height="100%",  # height : 50%
						loc='lower left',
						bbox_to_anchor=(posx, 0., 1, 1),
						bbox_transform=ax1.transAxes,
						borderpad=0,
	)
	return axins
		
t = 0
t_max = 99#80.
dt = t_max/4 # Millions of years

num = 0

n_plots = final_step
color_idx = np.linspace(0, 1, n_plots)

################################################################################
# Plot start
################################################################################

extent			= [0,param.lon.end.km,0,param.depth.end]
ref = 10
markersize 		= 0.4
ww = (x_end-x_start) / (-param.depth.end) #+ 0.15

rx = 0.2 # 20% of the height
wx = 0.6 # 60% of the height

width, height 	= ww * wx * ref + 2 * rx * ref, ref
aux = width / ref

tr = 0.025 / aux
r0, r1, r2, r3 = rx/aux - tr, rx , ww * wx / aux, wx
rect = [r0, r1, r2, r3]

plt.figure(figsize=(width,height))
ax1 = plt.axes(rect)

for cont in range(step_min,step_max+1):
#	bar.update(cont)
	print("loop step "+str(cont)+" @ file step " + str(cont*print_step))
	lastPlot = False
	try:
		yrs = np.loadtxt("Tempo_"+str(cont*print_step)+".txt",comments="P",delimiter=":")
	except:
		cont -= 1
		lastPlot = True
	
	if (num_processes!=1):
		value = format(np.round(yrs[0,1]/1.0E6,2), '.2f')
	else:
		value = format(np.round(yrs[1]/1.0E6,2), '.2f')
	value_float = float(value)
	
	if (value_float>t_max):
		cont -= 1
		lastPlot = True
	
	shp = (param.lon.size,param.depth.size)
	step0 = 0
	step1 = cont*print_step
	
	if (value_float>=t or lastPlot==True):
		# Viscosity

		visc_file0 = 'Geoq_' + str(step0) + '.txt'
		visc_file1 = 'Geoq_' + str(step1) + '.txt'

		visc0 = np.loadtxt(visc_file0,unpack=True,skiprows=skiprows,comments='P')
		visc0 = np.log10(visc0)
		visc0 = np.reshape(visc0,shp,order='F')

		visc1 = np.loadtxt(visc_file1,unpack=True,skiprows=skiprows,comments='P')
		visc1 = np.log10(visc1)
		visc1 = np.reshape(visc1,shp,order='F')

		# Density

		rho_file0 = 'Rho_' + str(step0) + '.txt'
		rho_file1 = 'Rho_' + str(step1) + '.txt'

		rho0 = np.loadtxt(rho_file0,unpack=True,skiprows=skiprows,comments='P')
		rho0 = np.reshape(rho0,shp,order='F')

		rho1 = np.loadtxt(rho_file1,skiprows=skiprows,comments='P')
		rho1 = np.reshape(rho1,shp,order='F')

		# Temperature

		temp_file0 = 'Temper_' + str(step0) +'.txt'
		temp_file1 = 'Temper_' + str(step1) +'.txt'

		temp0 = np.loadtxt(temp_file0,skiprows=skiprows,comments='P')
		temp0 = np.reshape(temp0,shp,order='F')

		temp1 = np.loadtxt(temp_file1,skiprows=skiprows,comments='P')
		temp1 = np.reshape(temp1,shp,order='F')

		# New density

		alpha = 3.28E-5
		r0 = rho0 * (1.0-alpha*temp0)
		r1 = rho1 * (1.0-alpha*temp1)

		# Time

		time_file = 'Tempo_' + str(step1) + '.txt'
		time = np.loadtxt(time_file,delimiter=":")[0,1]

		time_series = np.loadtxt('../time-series.txt')
		if (step1!=0):
			idx = np.where(time_series<=time)[0][-1]
		
		# Static-Flexure

		static_flexure 	= np.loadtxt('../static-flexure.txt')
		static_flexure 	= np.reshape(static_flexure,(param.lon.size,524-1),order='F')
		if (step1!=0):
			flexure0		= static_flexure[:,idx] * 1.0E3
		else:
			flexure0		= static_flexure[:,0] * 1.0E3 * 0
		
		################################################################################
		# Calculate topography
		################################################################################

		dz = (z[1]-z[0])
		w0 = topography(r0,dz=dz)
		w1 = topography(r1,dz=dz)

		topo = (w0-w1) / (3300.*9.8)

		################################################################################
		# Calculate flexure
		################################################################################

		p = np.copy(topo)
		p_original = np.copy(p)

		rho0 = 2300.
		p = rho0 * 9.8 * p  # densidade da crosta * g * altua do orogeno * largura do orogeno
		w = flexura_num(x,p,Te=20.0E3)#/1.0E3

		################################################################################
		# Plots
		################################################################################

		label = str(np.round(value_float,2)) + ' My'
		ax1.hlines(0,x_start,x_end,linestyles="dashed")
		cs, = ax1.plot(x/1.0E3,w-flexure0,linewidth=linewidth,color=plt.cm.winter_r(color_idx[step1]))
		cs.set_label(label)
		ax1.legend(loc=1)
		print label
#		ax1.plot(x/1.0E3,w,'r',label="Flexural and Thermal Subsidence",linewidth=1)
#		ax1.plot(x/1.0E3,w-flexure0,label="Dynamic Topography",linewidth=1,'g')
		

		
		
		t += dt
		num += 1
		if (lastPlot==True):
			break
axx(cs,(0,0,0))
#plt.legend(loc=1)
plt.savefig(fig_dir+'topo-evolution.png')
#plt.show()
			

		
		
		
		




