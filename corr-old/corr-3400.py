import numpy as np
import matplotlib.pyplot as plt
from pylab import cm

##################
# 75
##################

slab_75_5_25 = -45.273359892982974
slab_75_6_25 = -45.273359892982974
slab_75_7_25 = -45.273359892982974
slab_75_8_25 = -44.856918001162896
simu_75_5_25 = -52.065186997894735
simu_75_6_25 = -52.14384820915145
simu_75_7_25 = -52.62844726918562
simu_75_8_25 = -52.721596650940526

slab_75_5_50 = -46.03302957925985
slab_75_6_50 = -47.28865303723519
slab_75_7_50 = -47.28865303723519
slab_75_8_50 = -47.28865303723519
simu_75_5_50 = -56.683120178266066
simu_75_6_50 = -59.339371150082435
simu_75_7_50 = -59.28096903718025
simu_75_8_50 = -59.93955979795783

##################
# 80
##################

slab_80_5_25 = -44.856918001162896
slab_80_6_25 = -45.273359892982974
slab_80_7_25 = -45.66481296970814
slab_80_8_25 = -45.273359892982974
simu_80_5_25 = -52.69898171515437
simu_80_6_25 = -52.894709279967564
simu_80_7_25 = -53.52391755810456
simu_80_8_25 = -52.93429510845622

slab_80_5_50 = -47.28865303723519
slab_80_6_50 = -47.28865303723519
slab_80_7_50 = -47.28865303723519
slab_80_8_50 = -47.28865303723519
simu_80_5_50 = -60.04368934347865
simu_80_6_50 = -60.43692967343048
simu_80_7_50 = -59.9447322796612
simu_80_8_50 = -59.9447322796612

##################
# 85
##################

slab_85_5_25 = -45.273359892982974
slab_85_6_25 = -45.66481296970814
slab_85_7_25 = -45.273359892982974
slab_85_8_25 = -45.273359892982974
simu_85_5_25 = -52.92753864823848
simu_85_6_25 = -54.06052366714288
simu_85_7_25 = -53.09923920367397
simu_85_8_25 = -53.51291864582872

slab_85_5_50 = -47.28865303723519
slab_85_6_50 = -47.547238435338336
slab_85_7_50 = -47.547238435338336
slab_85_8_50 = -47.547238435338336
simu_85_5_50 = -60.43640400444504
simu_85_6_50 = -61.23911128659134
simu_85_7_50 = -61.50007542781627
simu_85_8_50 = -61.81583364469526

##################
# 90
##################

slab_90_5_25 = -45.273359892982974
slab_90_6_25 = -45.66481296970814
slab_90_7_25 = -45.66481296970814
slab_90_8_25 = -45.273359892982974
simu_90_5_25 = -53.26153345333241
simu_90_6_25 = -53.93485232629098
simu_90_7_25 = -54.68268948255426
simu_90_8_25 = -53.97140973148883

slab_90_5_50 = -47.547238435338336
slab_90_6_50 = -47.78203106288517
slab_90_7_50 = -47.547238435338336
slab_90_8_50 = -47.78203106288517
simu_90_5_50 = -61.49218317616502
simu_90_6_50 = -62.17367750762261
simu_90_7_50 = -62.37654773623261
simu_90_8_50 = -63.109097554671685

slabV50 = np.array([[slab_90_5_50,slab_90_6_50,slab_90_7_50,slab_90_8_50],
					[slab_85_5_50,slab_85_6_50,slab_85_7_50,slab_85_8_50],
					[slab_80_5_50,slab_80_6_50,slab_80_7_50,slab_80_8_50],
				  	[slab_75_5_50,slab_75_6_50,slab_75_7_50,slab_75_8_50]])

simuV50 = np.array([[simu_90_5_50,simu_90_6_50,simu_90_7_50,simu_90_8_50],
					[simu_85_5_50,simu_85_6_50,simu_85_7_50,simu_85_8_50],
					[simu_80_5_50,simu_80_6_50,simu_80_7_50,simu_80_8_50],
					[simu_75_5_50,simu_75_6_50,simu_75_7_50,simu_75_8_50]])

slabV25 = np.array([[slab_90_5_25,slab_90_6_25,slab_90_7_25,slab_90_8_25],
					[slab_85_5_25,slab_85_6_25,slab_85_7_25,slab_85_8_25],
					[slab_80_5_25,slab_80_6_25,slab_80_7_25,slab_80_8_25],
					[slab_75_5_25,slab_75_6_25,slab_75_7_25,slab_75_8_25]])

simuV25 = np.array([[simu_90_5_25,simu_90_6_25,simu_90_7_25,simu_90_8_25],
					[simu_85_5_25,simu_85_6_25,simu_85_7_25,simu_85_8_25],
					[simu_80_5_25,simu_80_6_25,simu_80_7_25,simu_80_8_25],
					[simu_75_5_25,simu_75_6_25,simu_75_7_25,simu_75_8_25]])

x_values 	= [0,1,2,3]
x_text		= ['5','6','7','8']
y_values 	= [0,1,2,3]
y_text		= ['90','85.5','80.0','75.0']

value50 = simuV50 - slabV50
value25 = simuV25 - slabV25

mintick, maxtick = -13, 7
div = 20
color_under = (000./255,000./255.,070./255.)
color_over = (070./255,000./255.,000./255.)


condNan = ~np.isnan(value50)
cmap = cm.get_cmap('coolwarm', div)
#cticks = np.linspace(np.min(value50[condNan]),np.max(value50[condNan]),div+1)
cticks = np.linspace(mintick,maxtick,div+1)

plt.figure()
plt.title("Dip angle differences (5 Myr)")
cs = plt.imshow(value50,cmap=cmap)
plt.xticks(x_values,x_text)
plt.yticks(y_values,y_text)
plt.xlabel("Oceanic Crust Thk. (km)")
plt.ylabel("Oceanic Lit. Thk. (km)")

cs.set_clim(mintick, maxtick)
cs.cmap.set_over(color=color_over)
cs.cmap.set_under(color=color_under)
plt.colorbar(cs,ticks=cticks,label="Dip Angle diff. (in degrees)",extend='both')

#plt.grid(b=True, which='major', axis='both')
plt.tight_layout()
plt.savefig('./figures/corr_5-3400.png')
#plt.show()

cticks = np.linspace(mintick,maxtick,div+1)

plt.figure()
plt.title("Dip angle differences (2.5 Myr)")
cs = plt.imshow(value25,cmap=cmap)
plt.xticks(x_values,x_text)
plt.yticks(y_values,y_text)
plt.xlabel("Oceanic Crust Thk. (km)")
plt.ylabel("Oceanic Lit. Thk. (km)")

cs.set_clim(mintick, maxtick)
cs.cmap.set_over(color=color_over)
cs.cmap.set_under(color=color_under)
plt.colorbar(cs,ticks=cticks,label="Dip Angle diff. (in degrees)",extend='both')

#plt.grid(b=True, which='major', axis='both')
plt.tight_layout()
plt.savefig('./figures/corr_25-3400.png')
plt.show()
