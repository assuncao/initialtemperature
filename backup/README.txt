
🌎 Instructions to run the python code

Before using the python code, run the scrip clean.sh to delete previous works (if you are changing the input parameters in the /src/params.txt file and/or if you are not running the code only to visualise previous works).

Change the model parameters in the params.txt file in the /src/ directory
Change the physical parameters in the reference.txt file in the /src/ directory

🌎 Running the makeArea.py code

Change in the makeArea.py code the option makeArea to 'True' if you need to pick a new area in the slab1.0 model, or 'False' if you are using the previous area (from the previous run)

Change in the makeArea.py file the option makeApprox to 'True' if you want to calculate a polynomial with degree polDegree=4. If the polinomial doesn't fit with acceptance per cent of the data, the degree will increase until a maximum of 15.

Change in the makeArea.py file mayaviPermission to 'True' if you want to se the area, the slab surface (polynomial fit) and the inward projection of the slab surface of a slab inwardThickness thick.

🌎 Running the temperature.py code

You can also disable mayavi plot changing the correspondent portion in the code (from 'True' to 'False').

🌎 Running the diffuse.py code

You can run this code to diffuse t_max million years (change the varibale withing the code). This script generates several figures stored in the ./figures/ directory which correspond to dt_print times steps.

🌎 Running the velocity.py code

🌎 Running the interfaces.py code
