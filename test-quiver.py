import numpy as np
import matplotlib.pyplot as plt

X, Y = np.meshgrid(np.arange(0, 2 * np.pi, .2), np.arange(0, 2 * np.pi, .2))
U = np.cos(X)
V = np.sin(Y)

fig1, (ax1,ax2) = plt.subplots(nrows=2,ncols=1,figsize=(10,10))
ax1.set_title('Arrows scale with plot width, not view')
Q = ax1.quiver(X, Y, U, V, units='xy',width=1.0E-2)
# qk = ax1.quiverkey(Q, 0.5, 0.9, 1, r'$2 \frac{m}{s}$', labelpos='E', coordinates='figure')

QQ = ax2.quiver(X, Y, U, V, units='x',scale=7)
qqk = ax2.quiverkey(QQ, 0.9, 0.9, 1.41, r'$1.41 \frac{m}{s}$', labelpos='E', coordinates='figure')

plt.show()              