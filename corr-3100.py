import numpy as np
import matplotlib.pyplot as plt
from pylab import cm

##################
# 75
##################

slab_75_5_25 = -43.940467369343246
slab_75_6_25 = -43.940467369343246
slab_75_7_25 = -43.940467369343246
slab_75_8_25 = -43.43623256583329
simu_75_5_25 = -46.93358899317318
simu_75_6_25 = -45.91614764301351
simu_75_7_25 = -46.062535303216784
simu_75_8_25 = -45.00116210574149

slab_75_5_50 = -45.273359892982974
slab_75_6_50 = -45.273359892982974
slab_75_7_50 = -45.273359892982974
slab_75_8_50 = -44.41336434439902
simu_75_5_50 = -49.34870694655332
simu_75_6_50 = -48.223130165648925
simu_75_7_50 = -47.897088583920834
simu_75_8_50 = -45.2103462216744

##################
# 80
##################

slab_80_5_25 = -43.940467369343246
slab_80_6_25 = -43.940467369343246
slab_80_7_25 = -44.41336434439902
slab_80_8_25 = -43.940467369343246
simu_80_5_25 = -47.50175267540701
simu_80_6_25 = -46.3405436853536
simu_80_7_25 = -47.34414114795675
simu_80_8_25 = -46.32252874067633

slab_80_5_50 = -45.66481296970814
slab_80_6_50 = -45.66481296970814
slab_80_7_50 = -45.66481296970814
slab_80_8_50 = -45.273359892982974
simu_80_5_50 = -50.45212924468576
simu_80_6_50 = -49.487520568287124
simu_80_7_50 = -49.485016897702614
simu_80_8_50 = -48.15020345875868

##################
# 85
##################

slab_85_5_25 = -44.41336434439902
slab_85_6_25 = -42.89930062684017
slab_85_7_25 = -44.41336434439902
slab_85_8_25 = -43.940467369343246
simu_85_5_25 = -48.67708942526466
simu_85_6_25 = -46.03302957925985
simu_85_7_25 = -47.15584050668042
simu_85_8_25 = -46.502036654307624

slab_85_5_50 = -45.66481296970814
slab_85_6_50 = -46.03302957925985
slab_85_7_50 = -45.66481296970814
slab_85_8_50 = -45.273359892982974
simu_85_5_50 = -51.85003827062397
simu_85_6_50 = -50.73823505438995
simu_85_7_50 = -49.509593998737174
simu_85_8_50 = -48.63446047434398

##################
# 90
##################

slab_90_5_25 = -44.41336434439902
slab_90_6_25 = -43.940467369343246
slab_90_7_25 = -44.41336434439902
slab_90_8_25 = -43.940467369343246
simu_90_5_25 = -49.04034035049176
simu_90_6_25 = -48.18078267454559
simu_90_7_25 = -48.31766060833768
simu_90_8_25 = -47.03115804923849

slab_90_5_50 = -46.03302957925985
slab_90_6_50 = -46.03302957925985
slab_90_7_50 = -45.66481296970814
slab_90_8_50 = -45.273359892982974
simu_90_5_50 = -52.4200343327041
simu_90_6_50 = -52.09505021912738
simu_90_7_50 = -51.08843592220971
simu_90_8_50 = -50.845038434272084

slabV50 = np.array([[slab_90_5_50,slab_90_6_50,slab_90_7_50,slab_90_8_50],
					[slab_85_5_50,slab_85_6_50,slab_85_7_50,slab_85_8_50],
					[slab_80_5_50,slab_80_6_50,slab_80_7_50,slab_80_8_50],
				  	[slab_75_5_50,slab_75_6_50,slab_75_7_50,slab_75_8_50]])

simuV50 = np.array([[simu_90_5_50,simu_90_6_50,simu_90_7_50,simu_90_8_50],
					[simu_85_5_50,simu_85_6_50,simu_85_7_50,simu_85_8_50],
					[simu_80_5_50,simu_80_6_50,simu_80_7_50,simu_80_8_50],
					[simu_75_5_50,simu_75_6_50,simu_75_7_50,simu_75_8_50]])

slabV25 = np.array([[slab_90_5_25,slab_90_6_25,slab_90_7_25,slab_90_8_25],
					[slab_85_5_25,slab_85_6_25,slab_85_7_25,slab_85_8_25],
					[slab_80_5_25,slab_80_6_25,slab_80_7_25,slab_80_8_25],
					[slab_75_5_25,slab_75_6_25,slab_75_7_25,slab_75_8_25]])

simuV25 = np.array([[simu_90_5_25,simu_90_6_25,simu_90_7_25,simu_90_8_25],
					[simu_85_5_25,simu_85_6_25,simu_85_7_25,simu_85_8_25],
					[simu_80_5_25,simu_80_6_25,simu_80_7_25,simu_80_8_25],
					[simu_75_5_25,simu_75_6_25,simu_75_7_25,simu_75_8_25]])

x_values 	= [0,1,2,3]
x_text		= ['5','6','7','8']
y_values 	= [0,1,2,3]
y_text		= ['90','85.5','80.0','75.0']

value50 = simuV50 - slabV50
value25 = simuV25 - slabV25

mintick, maxtick = -7, 7
div = 14
color_under = (000./255,000./255.,070./255.)
color_over = (070./255,000./255.,000./255.)


condNan = ~np.isnan(value50)
cmap = cm.get_cmap('coolwarm', div)
#cticks = np.linspace(np.min(value50[condNan]),np.max(value50[condNan]),div+1)
cticks = np.linspace(mintick,maxtick,div+1)

plt.figure()
plt.title("Dip angle differences (5 Myr)")
cs = plt.imshow(value50,cmap=cmap)
plt.xticks(x_values,x_text)
plt.yticks(y_values,y_text)
plt.xlabel("Oceanic Crust Thk. (km)")
plt.ylabel("Oceanic Lit. Thk. (km)")

cs.set_clim(mintick, maxtick)
cs.cmap.set_over(color=color_over)
cs.cmap.set_under(color=color_under)
plt.colorbar(cs,ticks=cticks,label="Dip Angle diff. (in degrees)",extend='both')

#plt.grid(b=True, which='major', axis='both')
plt.tight_layout()
plt.savefig('./figures/corr_5-3100.png')
#plt.show()

cticks = np.linspace(mintick,maxtick,div+1)

plt.figure()
plt.title("Dip angle differences (2.5 Myr)")
cs = plt.imshow(value25,cmap=cmap)
plt.xticks(x_values,x_text)
plt.yticks(y_values,y_text)
plt.xlabel("Oceanic Crust Thk. (km)")
plt.ylabel("Oceanic Lit. Thk. (km)")

cs.set_clim(mintick, maxtick)
cs.cmap.set_over(color=color_over)
cs.cmap.set_under(color=color_under)
plt.colorbar(cs,ticks=cticks,label="Dip Angle diff. (in degrees)",extend='both')

#plt.grid(b=True, which='major', axis='both')
plt.tight_layout()
plt.savefig('./figures/corr_25-3100.png')
plt.show()
