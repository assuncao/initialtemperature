import numpy as np
import matplotlib.pyplot as plt


R = 8.31
E = 240000.0
visc_ref = 1.0E19

b = 1.0E7




K = 273.15
Tb = - E / (R*np.log(1/b)) - K




T = np.linspace(0+K,Tb+K,1001)

aux = - (T*E) / (R*Tb*Tb)
n = visc_ref * b * np.exp(aux)

plt.plot(n,T)
plt.ylim(np.max(T),np.min(T))
plt.show()

