import numpy as np

def makeInnwardProjection(N,abscissaData,ordinateData,projecDepth):
	angle			= np.ones(N-1)
	absProj			= np.ones(N-1)
	absRefe 		= np.ones(N-1)
	ordProj			= np.ones(N-1)
	ordRefe 		= np.ones(N-1)
	for i in xrange(N-1):
		absRefe[i]	= (abscissaData[i+1] + abscissaData[i]) / 2.
		ordRefe[i]	= (ordinateData[i+1] + ordinateData[i]) / 2.
		dx			= abscissaData[i+1] - abscissaData[i]
#		dx 			= np.round(dxAux,2)
		dz 			= ordinateData[i+1] - ordinateData[i]
		tgt 		= dz / dx
		angle[i] 	= np.arctan(tgt)
		if (tgt<0):
			absProj[i]	= absRefe[i] - abs(projecDepth * np.sin(angle[i]))
			ordProj[i]	= ordRefe[i] - abs(projecDepth * np.cos(angle[i]))
		else:
			absProj[i]	= absRefe[i] + abs(projecDepth * np.sin(angle[i]))
			ordProj[i]	= ordRefe[i] - abs(projecDepth * np.cos(angle[i]))
#		if (angle[i]<0):
#			angle[i] 	= (np.pi / 2.) + angle[i]
#			absProj[i]	= absRefe[i] - projecDepth * np.cos(angle[i])
#			ordProj[i]	= ordRefe[i] - projecDepth * np.sin(angle[i])
#		else:
#			angle[i] 	= (np.pi / 2.) - angle[i]
#			absProj[i]	= absRefe[i] + projecDepth * np.cos(angle[i])
#			ordProj[i]	= ordRefe[i] - projecDepth * np.sin(angle[i])
	return absProj, ordProj

def makeOutwardProjection(N,abscissaData,ordinateData,projecDepth):
	angle			= np.ones(N-1)
	absProj			= np.ones(N-1)
	absRefe 		= np.ones(N-1)
	ordProj			= np.ones(N-1)
	ordRefe 		= np.ones(N-1)
	for i in xrange(N-1):
		absRefe[i]	= (abscissaData[i+1] + abscissaData[i]) / 2.
		ordRefe[i]	= (ordinateData[i+1] + ordinateData[i]) / 2.
		dx			= abscissaData[i+1] - abscissaData[i]
		dz 			= ordinateData[i+1] - ordinateData[i]
		tgt 		= np.round(dz,2) / np.round(dx,2)
		angle[i] 	= np.arctan(tgt)
		if (tgt<0):
			absProj[i]	= absRefe[i] + abs(projecDepth * np.sin(angle[i]))
			ordProj[i]	= ordRefe[i] + abs(projecDepth * np.cos(angle[i]))
		else:
			absProj[i]	= absRefe[i] - abs(projecDepth * np.sin(angle[i]))
			ordProj[i]	= ordRefe[i] + abs(projecDepth * np.cos(angle[i]))
	return absProj, ordProj
