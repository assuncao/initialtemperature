import numpy as np
import matplotlib.pyplot as plt
import os, sys, re

################################################################################
# Setting up classes and methods
################################################################################

cwd = '/Users/kugelblitz/Desktop/initialTemperature'#os.getcwd()
libsLocation = os.path.join(cwd,'lib')
sys.path.append(libsLocation)

import classes

################################################################################
# Read data from ./src/params.txt
################################################################################

print("*** Reading initial parameters *** ")
parPath = './src/params.txt'
param 	= classes.Dimension(parPath)

param.lat.size = 1

################################################################################
# Read Slab
################################################################################

slabGridPath = '/Users/kugelblitz/Desktop/initialTemperature/out/Slab2-resampled/resampled-top.-1800'
data = np.loadtxt(slabGridPath,unpack='True')
dataX = data[0,:]
dataY = data[1,:]
#cond = (dataY<=0) & (dataY>=maxAngleDepth)
dataY = dataY*1.0E3
dataX = dataX*1.0E3

y = np.linspace(dataY[0],dataY[-1],1001)

X = np.polyfit(dataY,dataX,10)
P = np.poly1d(X)
X = np.copy(P(y))



plt.plot(dataX,dataY,"b-",label="Slab2")
plt.plot(X,y,"r-",label="approx")
#plt.scatter(dataX,dataY,1)
#print(Y[0])
plt.legend()
plt.show()

for i in xrange(P.order):
	print("({0:.8e}".format(P[i])+"*pow(z,"+str(i)+")) +"),
	
print("({0:.8e}".format(P[P.order])+"*pow(z,"+str(P.order)+"));")
#plt.plot()
