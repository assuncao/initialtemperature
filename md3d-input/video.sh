rm *.mp4
ffmpeg -r 20 -f image2 -s 1920x1080 -i TemperPlot_%05d.png -vcodec libx264 -crf 25  -pix_fmt yuv420p temp.mp4
ffmpeg -r 20 -f image2 -s 1920x1080 -i GeoqPlot_%05d.png -vcodec libx264 -crf 25  -pix_fmt yuv420p geoq.mp4
ffmpeg -r 20 -f image2 -s 1920x1080 -i RhoPlot_%05d.png -vcodec libx264 -crf 25  -pix_fmt yuv420p rho.mp4
ffmpeg -r 20 -f image2 -s 1920x1080 -i FactorPlot_%05d.png -vcodec libx264 -crf 25  -pix_fmt yuv420p factor.mp4
ffmpeg -r 20 -f image2 -s 1920x1080 -i strainPlot_%05d.png -vcodec libx264 -crf 25  -pix_fmt yuv420p strain.mp4
ffmpeg -r 20 -f image2 -s 1920x1080 -i VelocityPlot_%05d.png -vcodec libx264 -crf 25  -pix_fmt yuv420p veloc.mp4
ffmpeg -r 20 -f image2 -s 1920x1080 -i ViscPlot_%05d.png -vcodec libx264 -crf 25  -pix_fmt yuv420p visc.mp4
ffmpeg -r 20 -f image2 -s 1920x1080 -i TempVelocityPlot_%05d.png -vcodec libx264 -crf 25  -pix_fmt yuv420p tempvelocity.mp4
ffmpeg -i tempvelocity.mp4 -i geoq.mp4 -i strain.mp4 -filter_complex vstack=inputs=3 comb.mp4