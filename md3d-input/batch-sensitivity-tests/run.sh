cd ~/runs/batch/simu00_v30/
touch log.txt
touch temp_simu.txt
date >> temp_simu.txt
/home/kugelblitz/opt/petsc/petsc_debug_0/bin/mpirun -n 16 /home/kugelblitz/opt/md3d/MD3D_4.9_swarm -te 1 -ve 1 > log.txt
date >> temp_simu.txt

cd ~/runs/batch/simu01_v80/
touch log.txt
touch temp_simu.txt
date >> temp_simu.txt
/home/kugelblitz/opt/petsc/petsc_debug_0/bin/mpirun -n 16 /home/kugelblitz/opt/md3d/MD3D_4.9_swarm -te 1 -ve 1 > log.txt
date >> temp_simu.txt

cd ~/runs/batch/simu02_k08/
touch log.txt
touch temp_simu.txt
date >> temp_simu.txt
/home/kugelblitz/opt/petsc/petsc_debug_0/bin/mpirun -n 16 /home/kugelblitz/opt/md3d/MD3D_4.9_swarm -te 1 -ve 1 > log.txt
date >> temp_simu.txt

cd ~/runs/batch/simu03_k12/
touch log.txt
touch temp_simu.txt
date >> temp_simu.txt
/home/kugelblitz/opt/petsc/petsc_debug_0/bin/mpirun -n 16 /home/kugelblitz/opt/md3d/MD3D_4.9_swarm -te 1 -ve 1 > log.txt
date >> temp_simu.txt

cd ~/runs/batch/simu04_n1k/
touch log.txt
touch temp_simu.txt
date >> temp_simu.txt
/home/kugelblitz/opt/petsc/petsc_debug_0/bin/mpirun -n 16 /home/kugelblitz/opt/md3d/MD3D_4.9_swarm -te 1 -ve 1 > log.txt
date >> temp_simu.txt

cd ~/runs/batch/simu05_n100k/
touch log.txt
touch temp_simu.txt
date >> temp_simu.txt
/home/kugelblitz/opt/petsc/petsc_debug_0/bin/mpirun -n 16 /home/kugelblitz/opt/md3d/MD3D_4.9_swarm -te 1 -ve 1 > log.txt
date >> temp_simu.txt

cd ~/runs/batch/simu06_Lo82/
touch log.txt
touch temp_simu.txt
date >> temp_simu.txt
/home/kugelblitz/opt/petsc/petsc_debug_0/bin/mpirun -n 16 /home/kugelblitz/opt/md3d/MD3D_4.9_swarm -te 1 -ve 1 > log.txt
date >> temp_simu.txt

cd ~/runs/batch/simu07_Lo85/
touch log.txt
touch temp_simu.txt
date >> temp_simu.txt
/home/kugelblitz/opt/petsc/petsc_debug_0/bin/mpirun -n 16 /home/kugelblitz/opt/md3d/MD3D_4.9_swarm -te 1 -ve 1 > log.txt
date >> temp_simu.txt

cd ~/runs/batch/simu08_Lc152/
touch log.txt
touch temp_simu.txt
date >> temp_simu.txt
/home/kugelblitz/opt/petsc/petsc_debug_0/bin/mpirun -n 16 /home/kugelblitz/opt/md3d/MD3D_4.9_swarm -te 1 -ve 1 > log.txt
date >> temp_simu.txt

cd ~/runs/batch/simu09_Lc160/
touch log.txt
touch temp_simu.txt
date >> temp_simu.txt
/home/kugelblitz/opt/petsc/petsc_debug_0/bin/mpirun -n 16 /home/kugelblitz/opt/md3d/MD3D_4.9_swarm -te 1 -ve 1 > log.txt
date >> temp_simu.txt

cd ~/runs/batch/simu10_Cp1125/
touch log.txt
touch temp_simu.txt
date >> temp_simu.txt
/home/kugelblitz/opt/petsc/petsc_debug_0/bin/mpirun -n 16 /home/kugelblitz/opt/md3d/MD3D_4.9_swarm -te 1 -ve 1 > log.txt
date >> temp_simu.txt

cd ~/runs/batch/simu11_Cp1250/
touch log.txt
touch temp_simu.txt
date >> temp_simu.txt
/home/kugelblitz/opt/petsc/petsc_debug_0/bin/mpirun -n 16 /home/kugelblitz/opt/md3d/MD3D_4.9_swarm -te 1 -ve 1 > log.txt
date >> temp_simu.txt

