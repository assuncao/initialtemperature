import numpy as np
import matplotlib.pyplot as plt

T0 = 1300.
Cp = 1250.
alpha = 1.0e-5
g = 10.
Tp = 1400.

def tempAdiabatic(z,L):
	for j in xrange(L.size):
		for i in xrange(z.size):
			T1 = T0 * z[i] / L[j]
			T2 = Tp * np.exp(g*alpha*z[i]/Cp)
			T[i] = np.min([T1,T2])
	return T

L = np.linspace(80,150,71)
z = np.linspace(0,700,71)
T = np.zeros(shape=(z.size,L.size))
T = tempAdiabatic(z,L)

plt.plot(T)
plt.show()
