import os, sys, re
import numpy as np

################################################################################
# Setting up classes and methods
################################################################################

cwd = os.getcwd()
libsLocation = os.path.join(cwd,'lib')
sys.path.append(libsLocation)

import reference, classes, methods, data
#from progressBar import startBar, updateBar, endBar

################################################################################
# Control options
################################################################################

bar 					= 'True'
mayaviPermission		= 'False'

if (mayaviPermission=='True'):
	from mayavi import mlab

################################################################################
# Read data from ./src/params.txt
################################################################################

parPath = './src/params.txt'
par 	= open(parPath)
lon 	= classes.Parameters(re.split("[ ]+", par.readline().strip()),"lon")
lat 	= classes.Parameters(re.split("[ ]+", par.readline().strip()),"lat")
depth 	= classes.Parameters(re.split("[ ]+", par.readline().strip()),"depth")
par.close()

fieldTemperature 	= np.zeros(shape=(lon.length,lat.length,depth.length))
fieldLon			= np.linspace(lon.startKm,lon.endKm,lon.length)
fieldLat			= np.linspace(lat.startKm,lat.endKm,lat.length)
fieldDepth			= np.linspace(depth.start,depth.end,depth.length)
auxFieldDepth		= np.zeros(np.size(fieldDepth))

################################################################################
# Read dimensional reference values
################################################################################

refValuesPath 	= './src/reference.txt'
refValuesFile 	= open(refValuesPath)
refValues		= []
for line in refValuesFile:
	refValues.append(line)

dimRef 			= reference.Reference(refValues,depth.end)

################################################################################
# Progress bar
################################################################################

if (bar=='True'):
	print "\nCalculating temperature field"
#	bar = startBar(lat.length)
	progressBar_width 	= 80
	increment			= 0
	step				= float(progressBar_width) / float(lat.length)
	sys.stdout.write("[%s]" % (" " * progressBar_width))
	sys.stdout.flush()
	sys.stdout.write("\b" * (progressBar_width+1)) # return to start of line, after '['

################################################################################
# Load surfaces
################################################################################

approxModelPath 	= './out/approx/approx.'
projecModelPath 	= './out/projec/slice.'

for j in xrange(lat.length):
	aux1,aux2 		= np.round(lat.startDegree*100,0),np.round(j*lat.delta*100,0)
	approxModelFile = approxModelPath + str(int(aux1+aux2)).zfill(5)
	approx 			= classes.Surface(np.loadtxt(approxModelFile))
	projecModelFile = projecModelPath + str(int(aux1+aux2)).zfill(5)
	projec 			= classes.Surface(np.loadtxt(projecModelFile))
	
	############################################################################
	# Background temperature field design
	############################################################################
	
	aW,bW,aE,bE		= methods.slabLimitCalc(approx,projec)

	for i in xrange(lon.length):
		for k in xrange(depth.length):
			westLimitRegionMax = np.max([approx.minA,projec.minA])
			eastLimitRegionMin = np.min([approx.maxA,projec.maxA])
			
			if (fieldLon[i]<westLimitRegionMax):
				if (fieldDepth[k]>data.oceanLitDepth()):
					x,x0,x1,y0,y1 = fieldDepth[k],0,data.oceanLitDepth(),0,dimRef.LABtemperatureC
					fieldTemperature[i,j,np.size(fieldDepth)-k-1] = methods.tempLinear(x,x0,x1,y0,y1)
				else:
					y,y0 = fieldDepth[k],data.oceanLitDepth()
					auxT1 = methods.tempAdiabatic(y,y0,dimRef.alpha,dimRef.gravity,dimRef.specificHeat,dimRef.POTtemperatureK)
					x,x0,x1,y0,y1 = fieldDepth[k],0,data.oceanLitDepth(),0,dimRef.LABtemperatureC
					auxT2 = methods.tempLinear(x,x0,x1,y0,y1)
					fieldTemperature[i,j,np.size(fieldDepth)-k-1] = min(auxT1,auxT2)
#					fieldTemperature[i,j,np.size(fieldDepth)-k-1] = methods.tempAdiabatic(y,y0,dimRef.alpha,dimRef.gravity,dimRef.specificHeat,dimRef.POTtemperatureK)

			elif (fieldLon[i]>eastLimitRegionMin):
				if (fieldDepth[k]>data.continentLitDepth()):
					x,x0,x1,y0,y1 = fieldDepth[k],0,data.continentLitDepth(),0,dimRef.LABtemperatureC
					fieldTemperature[i,j,np.size(fieldDepth)-k-1] = methods.tempLinear(x,x0,x1,y0,y1)
				else:
					y,y0 = fieldDepth[k],data.continentLitDepth()
					auxT1 = methods.tempAdiabatic(y,y0,dimRef.alpha,dimRef.gravity,dimRef.specificHeat,dimRef.POTtemperatureK)
					x,x0,x1,y0,y1 = fieldDepth[k],0,data.continentLitDepth(),0,dimRef.LABtemperatureC
					auxT2 = methods.tempLinear(x,x0,x1,y0,y1)
					fieldTemperature[i,j,np.size(fieldDepth)-k-1] = min(auxT1,auxT2)
#					fieldTemperature[i,j,np.size(fieldDepth)-k-1] = methods.tempAdiabatic(y,y0,dimRef.alpha,dimRef.gravity,dimRef.specificHeat,dimRef.POTtemperatureK)

			else:
				length 	= eastLimitRegionMin - westLimitRegionMax
				posX	= fieldLon[i] - westLimitRegionMax#length
				auxDepth1A = methods.findSlabSurfaceDepth(approx.abscissa,approx.ordinate,fieldLon[i])
				auxDepth1B = data.oceanLitDepth()
				auxDepth1 = min(auxDepth1A,auxDepth1B)
				auxDepth2 = data.continentLitDepth()
				auxFieldDepth[k] = max(auxDepth1,auxDepth2)
#				auxFieldDepth[k] = methods.erfSingle(data.oceanLitDepth(),data.continentLitDepth(),posX,length)
				if (fieldDepth[k]>auxFieldDepth[k]):
					x,x0,x1,y0,y1 = fieldDepth[k],0,auxFieldDepth[k],0,dimRef.LABtemperatureC
					fieldTemperature[i,j,np.size(fieldDepth)-k-1] = methods.tempLinear(x,x0,x1,y0,y1)
				else:
					y,y0 = fieldDepth[k],auxFieldDepth[k]
					auxT1 = methods.tempAdiabatic(y,y0,dimRef.alpha,dimRef.gravity,dimRef.specificHeat,dimRef.POTtemperatureK)
					x,x0,x1,y0,y1 = fieldDepth[k],0,auxFieldDepth[k],0,dimRef.LABtemperatureC
					auxT2 = methods.tempLinear(x,x0,x1,y0,y1)
					fieldTemperature[i,j,np.size(fieldDepth)-k-1] = min(auxT1,auxT2)
#					fieldTemperature[i,j,np.size(fieldDepth)-k-1] = methods.tempAdiabatic(y,y0,dimRef.alpha,dimRef.gravity,dimRef.specificHeat,dimRef.POTtemperatureK)

	############################################################################
	# Slab temperature design
	############################################################################

	surfaceLen		= approx.distance# surfaceLength(approx)

	for i in xrange(lon.length):
		for k in xrange(depth.length):
			# Calculate difference between POTtemperatureC and tempAdiabatic(z), set diffAdiabatic to zero if you do not want to use this temperature addition to the subducting slab
			y,y0 = fieldDepth[k],0
			temp1 = dimRef.POTtemperatureC
			temp2 = methods.tempAdiabatic(y,y0,dimRef.alpha,dimRef.gravity,dimRef.specificHeat,dimRef.POTtemperatureK)
			diffAdiabatic = (temp2-temp1)
			
			westLimitRegionMin = np.min([approx.minA,projec.minA])
			westLimitRegionMax = np.max([approx.minA,projec.minA])
			if (approx.minA>projec.minA):
				flagW = "normal"
			else:
				flagW = "inverse"

			eastLimitRegionMin = np.min([approx.maxA,projec.maxA])
			eastLimitRegionMax = np.max([approx.maxA,projec.maxA])
			if ([approx.maxA>projec.maxA]):
				flagE = "inverse"
			else:
				flagE = "normal"

			if ((fieldLon[i]>=westLimitRegionMin) and (fieldLon[i]<westLimitRegionMax)):
				if (flagW=="normal"):
					topBoundary = methods.boundary(aW,bW,fieldLon[i])
					botBoundary = methods.findLongiInterval(projec,fieldLon[i])
				if (flagW=="inverse"):
					topBoundary = methods.findLongiInterval(approx,fieldLon[i])
					botBoundary = methods.boundary(aW,bW,fieldLon[i])
				if ((fieldDepth[k]<=topBoundary) and (fieldDepth[k]>=botBoundary)):
					equivalentP 	= methods.minDistance2Curve(approx,fieldLon[i],fieldDepth[k])
					equivalentLon	= surfaceLen[equivalentP[2]]
					fieldTemperature[i,j,np.size(fieldDepth)-k-1] = methods.slabTemperature(equivalentLon,equivalentP[1],dimRef.niter,data.slabThickness(),dimRef.R,dimRef.tm) + diffAdiabatic
				else:
					continue

			elif ((fieldLon[i]>eastLimitRegionMin) and (fieldLon[i]<=eastLimitRegionMax)):
				if (flagE=="normal"):
					topBoundary = methods.boundary(aE,bE,fieldLon[i],flagE)
					botBoundary = methods.findLongiInterval(projec,fieldLon[i])
				if (flagE=="inverse"):
					topBoundary = methods.findLongiInterval(approx,fieldLon[i])
					botBoundary = methods.boundary(aE,bE,fieldLon[i])
				if ((fieldDepth[k]<=topBoundary) and (fieldDepth[k]>=botBoundary)):
					equivalentP 	= methods.minDistance2Curve(approx,fieldLon[i],fieldDepth[k])
					equivalentLon	= surfaceLen[equivalentP[2]]
					fieldTemperature[i,j,np.size(fieldDepth)-k-1] = methods.slabTemperature(equivalentLon,equivalentP[1],dimRef.niter,data.slabThickness(),dimRef.R,dimRef.tm) + diffAdiabatic
				else:
					continue

			elif ((fieldLon[i]>=westLimitRegionMax) and (fieldLon[i]<=eastLimitRegionMin)):
				topBoundary = methods.findLongiInterval(approx,fieldLon[i])
				botBoundary = methods.findLongiInterval(projec,fieldLon[i])
				if ((fieldDepth[k]<=topBoundary) and (fieldDepth[k]>=botBoundary)):
					equivalentP 	= methods.minDistance2Curve(approx,fieldLon[i],fieldDepth[k])
					equivalentLon	= surfaceLen[equivalentP[2]]
					fieldTemperature[i,j,np.size(fieldDepth)-k-1] = methods.slabTemperature(equivalentLon,equivalentP[1],dimRef.niter,data.slabThickness(),dimRef.R,dimRef.tm) + diffAdiabatic
				else:
					continue

			else:
				continue
	for i in xrange(lon.length):
		fieldTemperature[i,j,np.size(fieldDepth)-1] = 0.0
		fieldTemperature[i,j,0]						= dimRef.TZtemperatureC
					
	# Progress bar update
	if (bar == 'True'):
#		bar = updateBar(bar[0],bar[1])
		pbaux1 = int(increment)
		increment = increment + step
		pbaux2 = int(increment)
		leap = pbaux2 - pbaux1
		sys.stdout.write(">" * leap)
		sys.stdout.flush()

# Progress bar end
if (bar == 'True'):
#	endBar()
	sys.stdout.write("\n")

################################################################################
# Save temperature field
################################################################################

temperatureField 	= './out/Temper_0_3D.txt'
temperatureFile 	= open(temperatureField,"w")

lengthStr			= str(lon.length)+"\t"+str(lat.length)+"\t"+str(depth.length)
extentDistStr		= str(lon.startKm)+"\t"+str(lon.endKm)+"\t"+str(lat.startKm)+"\t"+str(lat.endKm)+"\t"+str(depth.start)+"\t"+str(depth.end)
extentDegrStr		= str(lon.startDegree)+"\t"+str(lon.endDegree)+"\t"+str(lat.startDegree)+"\t"+str(lat.endDegree)+"\t"+str(depth.start)+"\t"+str(depth.end)

temperatureFile.write(lengthStr+"\n")
temperatureFile.write(extentDistStr+"\n")
temperatureFile.write(extentDegrStr+"\n")

temperatureFile.write("T4\n")

for k in xrange(depth.length):
	for j in xrange(lat.length):
		for i in xrange(lon.length):
			tempStr = str(fieldTemperature[i,j,k])
			temperatureFile.write(tempStr+"\n")

#for j in xrange(lat.length):
#	for k in xrange(depth.length):
#		for i in xrange(lon.length):
#			tempStr = str(fieldTemperature[i,j,k])
#			temperatureFile.write(tempStr+"\n")

temperatureFile.close()

################################################################################
# Temperature field plot
################################################################################

if (mayaviPermission == 'True'):
	mlab.figure(size=(800,800))
	extent = [lon.startKm,lon.endKm,lat.startKm,lat.endKm,depth.start,depth.end]
	mlab.contour3d(fieldTemperature,extent=extent,contours=11,transparent=True,colormap="blue-red")
	mlab.view(azimuth=270,elevation=90)

################################################################################
# Region Boundary Plot (Box)
################################################################################

if (mayaviPermission == 'True'):
	mlab.outline(extent=extent)
	mlab.points3d(0,0,0,scale_factor=50)
	for k in xrange(0,np.size(fieldDepth)-1,8):
		mlab.points3d(0,0,fieldDepth[k],scale_factor=25,color=(1,0,0))
		mlab.points3d(fieldLon[lon.length-1],0,fieldDepth[k],scale_factor=25,color=(1,0,0))
	for i in xrange(1,np.size(fieldLon)):
		mlab.points3d(fieldLon[i],0,fieldDepth[0],scale_factor=25,color=(0,0,1))
		mlab.points3d(fieldLon[i],0,fieldDepth[depth.length-1],scale_factor=25,color=(0,0,1))

if (mayaviPermission == 'True'):
	mlab.show()



