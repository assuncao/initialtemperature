import os, sys, re
import numpy as np
from scipy import interpolate
import pandas as pd

################################################################################
# Setting up classes and methods
################################################################################

cwd = os.getcwd()
libsLocation = os.path.join(cwd,'lib')
sys.path.append(libsLocation)

import classes, methods
import progressBar as pbar

################################################################################
# Read data from ./src/params.txt
################################################################################

print("*** Reading initial parameters *** ")
parPath = cwd + '/src/params.txt'
param 	= classes.Dimension(parPath)

################################################################################
# Print parameters box (check up mistakes)
################################################################################

box = pbar.Box()

box.top()
box.mid("Chosen dimension:\t "+param.dim)
box.mid("Grid dimensions:\t ("+str(param.lon.size)+"x"+str(param.lat.size)+"x"+str(param.depth.size)+")")
box.mid("Longitude interval:\t ("+str(param.lon.start.degree)+","+str(param.lon.end.degree)+","+str(param.lon.delta.degree)+")")
box.mid("Longitude distance:\t ("+str(param.lon.start.km)+","+str(param.lon.end.km)+","+str(param.lon.delta.km)+")")
box.mid("Latitude interval:\t ("+str(param.lat.start.degree)+","+str(param.lat.end.degree)+","+str(param.lat.delta.degree)+")")
box.mid("Latitude distance:\t ("+str(param.lat.start.km)+","+str(param.lat.end.km)+","+str(param.lat.delta.km)+")")
box.mid("Depth interval:\t ("+str(param.depth.start)+","+str(param.depth.end)+","+str(param.depth.delta)+")")
box.bot()

################################################################################
# Read dimensional reference values
################################################################################

refFilePath 	= cwd + '/src/reference.txt'
dimRef 			= classes.Reference(refFilePath,param.depth.end)

################################################################################
# Take model data (slab2) for the area
################################################################################

fullModelRes		= 0.05 # Defined based on the slab2.0 model(do not change!)

depModelPathIn 		= './src/sam_slab2_dep_02.23.18.xyz' 		# Path to slab2.0 -> dep
thkModelPathIn		= './src/sam_slab2_thk_02.23.18.xyz' 		# Path to slab2.0 -> thk
dipModelPathIn		= './src/sam_slab2_dip_02.23.18.xyz' 		# Path to slab2.0 -> ang
strModelPathIn		= './src/sam_slab2_str_02.23.18.xyz' 		# Path to slab2.0 -> str

depModelPathOut 	= './out/Slab2-clipped/Slab2-top.' 			# Data written to /out directory
rtopModelPathOut 	= './out/Slab2-resampled/resampled-top.'	# Data written to /out directory
thkModelPathOut		= './out/Slab2-clipped/Slab2-bot.' 			# Data written to /out directory
rbotModelPathOut 	= './out/Slab2-resampled/resampled-bot.'	# Data written to /out directory

#Slab2 				= classes.Slab(parPath,fullModelRes)

auxSizeLon		= np.round((param.lon.end.degree-param.lon.start.degree)/fullModelRes,2)
auxSizeLat		= np.round((param.lat.end.degree-param.lat.start.degree)/fullModelRes,2)
sizeLonSlab2	= int(auxSizeLon) + 1
sizeLatSlab2	= int(auxSizeLat) + 1

if (param.dim=='2d'):
	sizeLatSlab2 			= 2
	param.lat.end.degree 	= param.lat.start.degree + fullModelRes
	param.lat.end.km		= param.lat.start.km + methods.km(0,fullModelRes)

slab2LonDomain		= np.arange(param.lon.start.degree,param.lon.end.degree+fullModelRes/2.,fullModelRes)
paramLonDomain		= np.arange(param.lon.start.degree,param.lon.end.degree+param.lon.delta.degree/2.,param.lon.delta.degree)
paramLatDomain		= np.arange(param.lat.start.degree,param.lat.end.degree+param.lat.delta.degree/2.,param.lat.delta.degree)

paramAuxLon			= np.ones(np.size(paramLonDomain)) * param.lon.start.degree
paramLonDomainKm	= methods.km(paramAuxLon,paramLonDomain)
paramAuxLat			= np.ones(np.size(paramLatDomain)) * param.lat.start.degree
paramLatDomainKm	= methods.km(paramAuxLat,paramLatDomain)

################################################################################
# Fixing the longitude to work with the Slab2
################################################################################

if (param.lon.start.degree<0):
	eastLonLimit = 360. + param.lon.start.degree
else:
	eastLonLimit = param.lon.start.degree

if (param.lon.end.degree<0):
	westLonLimit = 360. + param.lon.end.degree
else:
	westLonLimit = param.lon.end.degree

################################################################################
# Main Loop
################################################################################

depModel 		= np.zeros(shape=(sizeLonSlab2,sizeLatSlab2))
depModel[:] 	= np.nan
thkModel		= np.copy(depModel)
dipModel		= np.copy(depModel)
strModel 		= np.copy(depModel)
depModelLon, depModelLat	= np.mgrid[param.lon.start.km:param.lon.end.km:sizeLonSlab2*1j,param.lat.start.km:param.lat.end.km:sizeLatSlab2*1j]
thkModelLon, thkModelLat	= np.mgrid[param.lon.start.km:param.lon.end.km:sizeLonSlab2*1j,param.lat.start.km:param.lat.end.km:sizeLatSlab2*1j]

for j in xrange(sizeLatSlab2):
	aux1, aux2		= np.round(param.lat.start.degree*100,0), np.round(j*param.lat.delta.degree*100,0)
	auxName 		= str(int(aux1+aux2)).zfill(5)
	fPathTop		= rtopModelPathOut + auxName
	fPathBot		= rbotModelPathOut + auxName
	dataTop 		= pd.read_csv(fPathTop,sep='\t',header=None)
	dataBot			= pd.read_csv(fPathBot,sep='\t',header=None)

	auxA, auxB, auxC, auxD 	= [], [], [], []

	auxA	= np.append(auxA,depModelLon[:,j])
	auxB	= np.append(auxB,depModelLat[:,j])
	auxC 	= np.append(auxC,dataTop[0])
	auxD	= np.append(auxD,dataBot[0])

	auxNotNanTop	= ~np.isnan(auxC)
	auxATop			= auxA[auxNotNanTop]
	auxBTop			= auxB[auxNotNanTop]
	auxC			= auxC[auxNotNanTop]

auxNotNanBot	= ~np.isnan(auxD)
auxABot			= auxA[auxNotNanBot]
auxBBot			= auxB[auxNotNanBot]
auxD			= auxD[auxNotNanBot]

	method		= 'cubic'
	m, n 		= np.size(paramLonDomain), np.size(paramLatDomain)
	X, Y 		= np.mgrid[param.lon.start.km:param.lon.end.km:m*1j,param.lat.start.km:param.lat.end.km:n*1j]
	pointsTop 	= np.column_stack((auxATop,auxBTop))
	pointsBot 	= np.column_stack((auxABot,auxBBot))
	valuesTop  	= auxC
	valuesBot	= auxD
	ZTop		= interpolate.griddata(pointsTop,valuesTop,(X,Y),method=method)
	ZBot		= interpolate.griddata(pointsBot,valuesBot,(X,Y),method=method)

if (param.dim!='2d' and param.lat.size>2): # Special case if param.dim=='2d'
	eBoundXTop, eBoundYTop, wBoundXTop, wBoundYTop = [], [], [], []
	eBoundXBot, eBoundYBot, wBoundXBot, wBoundYBot = [], [], [], []
	for j in xrange(sizeLatSlab2):
		notNanTop 	= ~np.isnan(depModel[:,j])
		notNanXTop 	= depModelLon[notNanTop,j]
		notNanYTop	= depModelLat[notNanTop,j]
		eBoundXTop	= np.append(eBoundXTop,notNanXTop[-1])
		eBoundYTop	= np.append(eBoundYTop,notNanYTop[-1])
		wBoundXTop	= np.append(wBoundXTop,notNanXTop[0])
		wBoundYTop	= np.append(wBoundYTop,notNanYTop[0])
		
		notNanBot 	= ~np.isnan(thkModel[:,j])
		notNanXBot	= thkModelLon[notNanBot,j]
		notNanYBot	= thkModelLat[notNanBot,j]
		eBoundXBot	= np.append(eBoundXBot,notNanXBot[-1])
		eBoundYBot	= np.append(eBoundYBot,notNanYBot[-1])
		wBoundXBot	= np.append(wBoundXBot,notNanXBot[0])
		wBoundYBot	= np.append(wBoundYBot,notNanYBot[0])

	eTckTop = interpolate.splrep(eBoundYTop,eBoundXTop,s=0)
	wTckTop = interpolate.splrep(wBoundYTop,wBoundXTop,s=0)
	eBoundNewYTop = np.copy(paramLatDomainKm)
	wBoundNewYTop = np.copy(paramLatDomainKm)
	eBoundNewXTop = interpolate.splev(eBoundNewYTop,eTckTop,der=0)
	wBoundNewXTop = interpolate.splev(wBoundNewYTop,wTckTop,der=0)

eTckBot = interpolate.splrep(eBoundYBot,eBoundXBot,s=0)
wTckBot = interpolate.splrep(wBoundYBot,wBoundXBot,s=0)
eBoundNewYBot = np.copy(paramLatDomainKm)
wBoundNewYBot = np.copy(paramLatDomainKm)
eBoundNewXBot = interpolate.splev(eBoundNewYBot,eTckBot,der=0)
wBoundNewXBot = interpolate.splev(wBoundNewYBot,wTckBot,der=0)

for j in xrange(np.size(paramLatDomain)):
	for i in xrange(np.size(paramLonDomain)):
		if (X[i,j]>eBoundNewXTop[j]) or (X[i,j]<wBoundNewXTop[j]):
			ZTop[i,j] = np.nan
			if (X[i,j]>eBoundNewXBot[j]) or (X[i,j]<wBoundNewXBot[j]):
				ZBot[i,j] = np.nan

