import numpy as np
import re, os, sys

################################################################################
# Plot control
################################################################################

pltPermission = 'True'

if (pltPermission=='True'):
	import matplotlib.pyplot as plt
	plt.clf()
	plt.close('all')

################################################################################
# Loading reference values from reference class
################################################################################

cwd = os.getcwd()
libsLocation = os.path.join(cwd,'lib')
sys.path.append(libsLocation)

import classes, methods

################################################################################
# Load LAB depth
################################################################################

lithoPath = cwd + '/out/litho.txt'
litho = np.loadtxt(lithoPath,unpack=True)
litho = litho * (-1)

################################################################################
# Read data from ./src/params.txt
################################################################################

print("*** Reading initial parameters *** ")
parPath = cwd + '/src/params.txt'
param 	= classes.Dimension(parPath)

velocityX		 	= np.zeros(shape=(param.lon.size,param.lat.size,param.depth.size))
velocityY		 	= np.zeros(shape=(param.lon.size,param.lat.size,param.depth.size))
velocityZ		 	= np.zeros(shape=(param.lon.size,param.lat.size,param.depth.size))
fieldLon			= np.linspace(param.lon.start.km,param.lon.end.km,param.lon.size)
fieldLat			= np.linspace(param.lat.start.km,param.lat.end.km,param.lat.size)
fieldDepth			= np.linspace(param.depth.start,param.depth.end,param.depth.size)
auxFieldDepth		= np.zeros(np.size(fieldDepth))

################################################################################
# Read dimensional reference values
################################################################################

refFilePath 	= cwd + '/src/reference.txt'
dimRef 			= classes.Reference(refFilePath,param.depth.end)

################################################################################
# Read interfaces.txt
################################################################################

skiprows			= 3
interfaceFilePath 	= cwd + '/out/interfaces.txt'
interfaces 			= np.loadtxt(interfaceFilePath,unpack='True',skiprows=skiprows)
interfaces			= np.reshape(interfaces,(10,param.lon.size,param.lat.size),order='F')

################################################################################
# Main loop
################################################################################

offNormalVelocity = 'True'

for j in xrange(param.lat.size):
	westDepth = litho[0,j] * 2.
	eastDepth = litho[-1,j] * 2.

	auxVelo1 = methods.verticalVelocityProfile(westDepth,eastDepth,fieldDepth,velocityX[0,0,:],1,dimRef.slabVelocitySI)
	auxVelo2 = methods.verticalVelocityProfile(westDepth,eastDepth,fieldDepth,velocityX[0,0,:],-1,dimRef.slabVelocitySI)
	aux = 365.25*60*24*60

	if (offNormalVelocity!='True'):
		velocityX[0,j,:] 	= auxVelo2# * (-1.)
		velocityX[-1,j,:] 	= auxVelo1# * (-1.)

	cond 	= np.where(interfaces[5,:,j]==0)[0]
	
	# interface 3 -> Slab2 top -> interfaces[2]
#	velocityX[cond,j,-1] = velocityX[0,j,-1]
	cond 	= np.where(interfaces[3,:,j]==0)[0]
	auxCond = cond[-1]+13 # impose velocity 4 elements after the slab start descending
	for i in xrange(param.lon.size):
		if (i<=auxCond):
			velocityX[i,j,-1] = dimRef.slabVelocitySI#velocityX[0,j,-1]
		else:
			pass

linewidth = 2.5
fig,ax1 = plt.subplots(figsize=(7,8))
ax1.plot(velocityX[0,j,:],fieldDepth,label="East Side",linewidth=linewidth)
ax1.plot(velocityX[-1,j,:],fieldDepth,label="West Side",linewidth=linewidth)

#plt.tight_layout()
plt.xlim(np.min(velocityX),np.max(velocityX))
ax1.set_ylim(param.depth.end,param.depth.start)
ax1.set_ylabel("Depth (km)")
#ax1.set_yticks([0,eastDepth,-100,westDepth,-300,-400,-700-westDepth,-600,-700-eastDepth,-700])
ax1.set_xlabel("Velocity (m/s)")
ax1.set_xticks(np.linspace(np.min(velocityX),np.max(velocityX),7))
ax1.hlines(westDepth,np.min(velocityX),np.max(velocityX),linestyles="dashed")
ax1.hlines(eastDepth,np.min(velocityX),np.max(velocityX),linestyles="dashed")
ax1.hlines(param.depth.end-eastDepth,np.min(velocityX),np.max(velocityX),linestyles="dashed")
ax1.hlines(param.depth.end-westDepth,np.min(velocityX),np.max(velocityX),linestyles="dashed")
ax1.legend()

ax2 = ax1.twiny()
ax2.set_xlim(np.min(velocityX)*(1000*365.25*60*24*60),np.max(velocityX)*(1000*365.25*60*24*60))
ax2.set_xlabel("Velocity (mm/yr)")

ax3 = ax1.twinx()
ax3.set_ylim(param.depth.end,param.depth.start)

plt.tight_layout()
plt.savefig('./figures/velocity_profile.pdf')
plt.show()


velocityPath 	= './out/veloc_0_3D.txt'
velocityFile	= open(velocityPath,"w")

lengthStr			= str(param.lon.size)+"\t"+str(param.lat.size)+"\t"+str(param.depth.size)
extentDistStr		= str(param.lon.start.km)+"\t"+str(param.lon.end.km)+"\t"+str(param.lat.start.km)+"\t"+str(param.lat.end.km)+"\t"+str(param.depth.start)+"\t"+str(param.depth.end)
extentDegrStr		= str(param.lon.start.degree)+"\t"+str(param.lon.end.degree	)+"\t"+str(param.lat.start.degree)+"\t"+str(param.lat.end.degree)+"\t"+str(param.depth.start)+"\t"+str(param.depth.end)

velocityFile.write(lengthStr+"\n")
velocityFile.write(extentDistStr+"\n")
velocityFile.write(extentDegrStr+"\n")

velocityFile.write("# v4\n")

for k in xrange(param.depth.size):
	for j in xrange(param.lat.size):
		for i in xrange(param.lon.size):
			velocityFile.write(str(velocityX[i,j,k])+"\n")
			velocityFile.write(str(velocityY[i,j,k])+"\n")
			velocityFile.write(str(velocityZ[i,j,k])+"\n")

velocityFile.close()
